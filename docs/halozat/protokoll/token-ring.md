## 1. Bevezetés
A Token-Ring (vezérjeles gyűrű) elnevezésű, a helyi hálózatoknál (LAN) használt technológiát az IBM fejlesztette ki, és ajánlotta az 1980-as évek elején, majd IEEE 802.5 néven az Institute of Electrical and Electronics Engineers szabványosította. A technológia kezdetben nagyon sikeres volt, de a 10Base-T kábelezésű Ethernet és az EIA/TIA 568 kábelezési szabvány 1990-es évek elején történt bejelentése óta hanyatlóban van. Az IBM marketing megállapításai szerint a Token-Ring teljesítménye és megbízhatósága, a használt determinisztikus hozzáférési módszernek köszönhetően jobb, mint az Ethernet. Ennek ellenére a Token-Ring piaci helyzete megegyezik az IBM Micro Channel architektúrájával történtekkel.

## 2. Áttekintés
Egy Token-Ring LAN-on az állomások logikailag gyűrűtopológiát alkotnak, az adatokat sorosan küldi egy állomás a körbe a gyűrűn a következő állomásnak, egy vezérjellel együtt (ezt a vezérjelet nevezik angolul tokennek). Ez a vezérjel-továbbítási (token passing) mechanizmus ismert az ARCNET, a tokenbusz, és a FDDI megvalósításoknál, és elméletileg előnyösebb az Ethernetnél használt sztochasztikus (véletlenszerű) Csma/cd algoritmusnál.
Token-Ring hálózat

Fizikailag a Token-Ring hálózat valójában egy csillag hálózat, 'hub'-bal, amihez az állomások egy oda-vissza vezető hurokkal csatlakoznak. A kábelezési rendszer általában az IBM „Type-1” árnyékolt csavart érpár, egy teljesen egyedi, „hermafrodita” (anya- és apacsatlakozót egyszerre tartalmazó) csatlakozóval.

Kezdetben, (1985-ben) a Token-Ring 4 Mbit/s-el üzemelt, de 1989-ben az IBM bejelentette a 16 Mbit/s-os Token-Ring termékét és a 802.5 szabványt kiterjesztették ennek a támogatására is. 1981-ben, az Apollo Computers bejelentette a saját 12 Mbit/s-os Apollo Token Ringet (ATR) és a Proteon bejelentette az ő 10 Mbit/s-os ProNet-10 Token Ring hálózatát. Az IBM Token-Ringgel azonban nem volt kompatibilis sem az ATR sem pedig a ProNet-10.

Technikai szemszögből nézve a Token-Ring az OSI modell szerinti adatkapcsolati rétegben (DLL) elhelyezkedő LAN protokoll. Egy speciális három bájtos keretet használ vezérlésre, ez a vezérjel (token), amely a hálózatban körbe „utazik”. A Token Ring keretei végighaladnak a teljes gyűrűn.

Minden állomás vagy továbbítja, vagy ismétli a vezérjel-keretet a hozzá legközelebb lévő állomáshoz. Ez a speciális vezérjel-keret továbbítás szolgál a közösen használt media hozzáférés ellenőrzésére. Egy állomás az adatot tartalmazó keretet csak akkor küldheti, ha előtte elküldi a speciális vezérjel-keretet. A Token-Ring eljárás normál esetben differenciál Manchester kódolást használ a bitek küldésénél.

A Token-Ring eljárást Olof Sönderblom dolgozta ki, az 1960-as évek végén. Az IBM később ezt szabadalmaztatta, és elkezdte népszerűsíteni és használni a Token-Ring LAN-okat az 1980-as évek közepe táján, amikor kidolgozta az IBM Token-Ring architektúráját az aktív több állomásos (multi-station) hozzáférési egységekkel (multi-station access unit – MSAU vagy MAU) és az IBM strukturált kábelezési rendszerével. Az Institute of Electrical and Electronics Engineers (IEEE) később a IEEE 802.5 számú szabványával szabványosította a Token-Ring LAN-t.[1]

A Token-Ring hálózatok sebességeit 4 Mbit/s, 16 Mbit/s, 100 Mbit/s és 1 Gbit/s értékekkel szabványosította az IEEE 802.5 munkacsoportja.

A Token-Ring hálózatok határozottan jobb teljesítménnyel és megbízhatósággal rendelkeztek, mint a korai megosztott média használatú hálózat Ethernet (IEEE 802.3) megvalósításai, és szélesebb körben elterjedtek, mint az Ethernet nagyobb teljesítményű alternatívája.

Ennek ellenére a kapcsolt Ethernet fejlesztései megindultak, és a Token-Ring architektúrák megbízhatóságát és teljesítményét hamarosan el is érték. A nagyszámú Ethernet-eladás hatására az árak csökkenni kezdtek, és az egyéb előnyei az Ethernet felé billentették a mérleget.

A Token-Ring hálózatok egyre ritkábbak lettek, és a szabványosítási törekvések is abba az irányba mozdították el a piacot, hogy az Ethernet lassan dominánssá vált a LAN/2-es réteg hálózatoknál.

## 3. Vezérjeles keretek
Amikor nincsen adatot tartalmazó, küldeni való keret a hálózatban, akkor egy speciális vezérjel-keret cirkulál a gyűrűben. Ennek a speciális vezérjel-keretnek meg kell érkeznie az állomáshoz, hogy az adatot tartalmazó keretet tudjon küldeni. Ha az állomás adatot akar küldeni – és nála a vezérjel-keret – akkor átalakítja a vezérjel-keretet adatkeretté. Ez a speciális vezérjel-keret három bájtot tartalmaz a következők szerint:

* Kezdetelválasztó – egy speciális bitsorozatot tartalmaz, amely a keret elejét jelzi. Ez a bitsorozat sorrendben a J,K,0,J,K,0,0,0. Ahol J és K kódsértő. Mivel a manchester kódolás saját órajeles (önszinkronizáló), és minden 1 vagy 0 bit kódolása jelátmenetet okoz, azonban a J és a K bitek kódolása megsérti ezt a szabályt, ezért a hardver ezt képes detektálni.
* Hozzáférés-ellenőrzés – ez a bájt a következő biteket tartalmazza (ebben a sorrendben): P,P,P,T,M,R,R,R. Ahol a P bitek a prioritás bitek, T a vezérjel (token) bit, amely jelzi, hogy ez a keret vezérjel-keret, M a monitor bit, amelyet az Active Monitor (AM) állomás állít be, ha látja ezt a keretet, és az R bitek fenntartottak.
* Végelválasztó – A kezdőelválasztó ellenpárja, ez a mező jelöli a keret végét, és következő bitekből áll (ebben a sorrendben): J,K,1,J,K,1,I,E. Ahol I a közbenső keretbit, és E a hibabit.

## 4. Token-Ring keretek formátumai
Egy adat vezérjel-keret nem más, mint a vezérjel-keret kibővítése a medium access control (MAC) menedzsment információkkal, vagy felsőbb rétegektől érkezett protokolladatokkal vagy alkalmazásoktól származó adatokkal való kibővítése.

Egy Token-Ring keret a következő formátumú:

* Kezdetelválasztó – az előzőekben leírtak szerint.
* Hozzáférés-vezérlés – az előzőekben leírtak szerint.
* Keretvezérlés – egy 1 bájtos mező, amelynek bitjei meghatározzák a keretben lévő adatrész tartalmát.
* Célcím – egy 6 bájtos mező, amely a cél(ok) cím(ei)t határozza meg.
* Forráscím – egy 6 bájtos mező, amely a küldő adapter vagy egy helyileg hozzárendelt címét (local assigned address – LAA), vagy egy általánosan hozzárendelt címét (universally assigned address – UAA) határozza meg.
* Adat – egy változó hosszúságú mező, 0 vagy több bájt hosszú. A maximálisan megengedett hosszúság a gyűrű sebességétől függ. Az adat vagy MAC-menedzsment adat, vagy felsőbb rétegből származó információ.
* Keretellenőrző sorozat – egy 4 bájtos ellenőrző CRC összeg, amit a keret sértetlenségének ellenőrzésére használ a vevő.
* Végelválasztó – előzőekben leírtak szerint.
* Keretállapot – egy bájtos mező, egy primitív nyugtázás, hogy a keretet felismerte és átmásolta a vevője.

## 5. Aktív- és készenléti figyelési állapotok
A Token-Ring hálózaton belül minden állomás vagy aktív figyelési állapotban (active monitor – AM) vagy készenléti figyelési állapotban (standby monitor – SM) lehet. Ugyanakkor, a hálózaton belül egy időben csak egy állomás lehet aktív figyelési állapotban. Az aktív figyelési állapotot vagy egy elektromos jellel, vagy egy figyelési vetélkedés eljárással lehet létrehozni.

A figyelési vetélkedés eljárás akkor indul el, ha

* A gyűrűn belül jelvesztést érzékel bármelyik állomás,
* Egy állomás nem érzékeli, hogy a gyűrűben van aktív figyelő állapotú állomás, vagy
* Ha egy számláló lejár, ami azt jelzi, hogy az állomás az elmúlt 7 másodpercben nem érzékelt vezérjel-keretet.

Az állomást elektromos jellel a legmagasabb MAC címre be lehet állítani. Minden másik állomás ekkor készenléti figyelő állapotba kerül az előzőekben elmondottak szerint. Viszont minden állomásnak rendelkeznie kell azzal a képességgel, hogy aktív figyelő állapotba tudjon kerülni, ha arra szükség lenne.

Az aktív figyelő állomás végrehajt egy gyűrűben aktív állomás számlálási funkciót is. Az első beavatkozás az, hogy a gyűrűben az alapórajel (master clock) beállítással biztosítja a szinkronizálást a kábeleken lévő állomások számára. Az aktív figyelő állomás másik feladata, hogy beszúr egy 24 bites késleltetést a gyűrű forgalmába, hogy biztosítsa a puffereket a gyűrűn körbehaladó vezérjeles keret számára. Az aktív figyelő harmadik feladata, hogy gondoskodjon a vezérjeles keret körbeküldéséről, ha nincs a gyűrűben adatkeret, és detektálja a gyűrű szakadását. Végül, az aktív figyelő állomás felelős azért, hogy a eltávolítsa azokat a kereteket, amelyek körbehaladtak a gyűrűben.

## 6. Token-Ring állomás hálózatba illesztési eljárása
Egy Token-Ring állomás hálózatba illesztése meglehetősen bonyolult, 5 fázisból álló eljárást igényel. Ha bármelyik fázisban hiba jelentkezne, az állomást nem lehet "beilleszteni" a hálózatba, a csatoló hibát jelezne. A hiba pontos okát az állomás Token-Ring csatolójából ki lehet olvasni.

0. fázis (Hurok ellenőrzés – Lobe Check) – Az állomás először ellenőrzi a csatlakozó hurkot. Az állomás még "be van csomagolva" a MAU szempontjából, de képes arra, hogy 2000 teszt üzenetet küldjön a hurkon, és ezeket vegye is – a MAU "zárja" össze a hurkot. Az állomás ellenőrzi, hogy a teszt üzeneteket hiba nélkül vette.

1. fázis (Fizikai beillesztés – physical insertion) – Az állomás egy 5 voltos jelet küld a MAU felé, hogy nyissa a relét (a hurok üzemszerű állapotba kerül).

2. fázis (Cím ellenőrzés – address control) – Az állomás ezután elküld egy MAC keretet a saját MAC címével a célcím mezőben. Ha a keret visszaérkezik, és a cím átmásolódott, akkor az állomásnak részt kell vennie a periodikus (minden 7. másodpercben) lekérdezésben. Ez az a fázis, amikor az állomás azonosítja saját magát a gyűrűben lévő állomások számára, valamint a hálózat MAC funkciói számára.

3. fázis (Részvétel a gyűrű lekérdezésében – participation in a ring poll) – Az állomás megismeri a hozzá legközelebbi, a gyűrű forgalmi irányával ellentétes irányban lévő másik állomás (Nearest Active Upstream Neighbor – NAUN) címét és a saját címét pedig beállítja az állomás számára, mint következő címet. Az állomás addig vár, amig egy AMP vagy SMP keretet nem vesz, ahol az ARI és az FCI bitek 0-ában állnak. Ha ez megtörtént, akkor az állomás mindkét bitet (ARI és FCI) 1 állapotba billenti, ha elegendő erőforrása áll rendelkezésre, és egy SMP keretet tesz az adási sorba. Ha ilyen keretek közül egy sem érkezik 18 másodpercen belül, akkor az állomás jelenti a hibát, és elindít egy beillesztés-visszaállítási eljárást. Ha az állomás sikeresen részt vett a gyűrű lekérdezésben, akkor áttér az utolsó fázisra, az inicializálási kérésre.

4. fázis (Inicializálási kérés – initialisation request) – Az állomás végül egy speciális üzenetben (keretben) konfigurációs paramétereket küld a gyűrű konfiguráláshoz. Ezt a keretet egy speciális funkcionális címmel küldik el, tipikusan egy Token-Ring bridzsnek, amely az időzítési információkat és a gyűrűben lévő állomások számával kapcsolatos információkat tárolja, és ezeket megküldi az új állomásnak.
