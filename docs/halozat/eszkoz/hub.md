## 1. Bevezetés
A hub (ejtsd: háb – az angol szó jelentései: kerékagy, középpont, csomópont) a számítógépes hálózatok egy hardvereleme, amely fizikailag összefogja a hálózati kapcsolatokat. Másképpen szólva a hub a hálózati szegmensek egy csoportját egy hálózati szegmensbe vonja össze, egyetlen ütközési tartományként láttatja a hálózat számára. Leegyszerűsítve: az egyik csatlakozóján érkező adatokat továbbítja az összes többi csatlakozója felé. Ez passzívan megy végbe, anélkül, hogy ténylegesen változtatna a rajta áthaladó adatforgalmon.

## 2. Típusok
A hubok között 2 alaptípust különböztetünk meg:

* **Aktív hub**: az állomások összefogásán kívül a jeleket is újragenerálja, erősíti, tehát ebben a formában valójában egy többportos repeater

* **Passzív hub**: csupán fizikai összekötő pontként szolgál, nem módosítja vagy figyeli a rajta keresztülhaladó forgalmat.

A legelterjedtebbek a 8, 16, 24 portos eszközök, de találkozhatunk kisebb, 4 portossal is. A passzív hubok elektromos tápellátást nem igényelnek. Az intelligens hubok aktív hubként üzemelnek, mikroprocesszorral és hibakereső képességekkel rendelkeznek.
