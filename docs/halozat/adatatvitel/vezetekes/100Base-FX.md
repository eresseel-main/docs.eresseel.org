## 1. Bevezetés
A 100Base-FX a gyors Ethernet fényvezető szálat használó kábelezési rendszere.

A gyors Ethernet (fast Ethernet) szabványt, a 802.3u-t 1995 júniusában fogadta el a IEEE.

A kábelezés tervezésekor három lehetséges kábelt vett a bizottság figyelembe:

* 3-as kategóriájú sodrott érpár (lásd: 100Base-T4)
* 5-ös kategóriájú sodrott érpár (lásd: 100Base-TX)
* Fényvezető szál vagy kábel.

A két többmódusú fényvezető szálat a 100Base-FX kábelezés használja, mindkét irányban egyet-egyet, így ez is 100Mb/s-os duplex átvitelt biztosít mindkét irányban. Az elosztó és az állomások közötti távolság akár 2 km is lehet.