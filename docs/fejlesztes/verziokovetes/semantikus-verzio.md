## 1. Bevezetés
A szoftverkezelés világában létezik egy rettegett hely, amelyet "függőségi pokolnak" hívnak. Minél nagyobb a rendszerünk, és minél több csomagot integrálunk a szoftverbe, annál valószínűbb, hogy egy napon a kétségbeesés gödörében találjuk magunkat.

Sok függőségű rendszerben az új csomagverziók kiadása gyorsan rémálommá válhat. Ha a függőségi előírások túl szigorúak, akkor a verziózár veszélye fenyeget (egy csomag frissítésének képtelensége anélkül, hogy minden függő csomag új verzióját ki kellene adni). Ha a függőségek túl lazán vannak megadva, akkor elkerülhetetlen lesz a "verziódússág" (feltételezve, hogy több jövőbeni verzióval kompatibilis). A függőségi pokol az a hely, ahol vagy, amikor a verziózár és/vagy a verziódússág megakadályozza, hogy könnyedén és biztonságosan haladjon előre a projektben.

A probléma megoldására javaslatot teszünk egy egyszerű szabályrendszerre és követelményekre, amelyek meghatározzák a verziószámok hozzárendelését és növelését. Ezek a szabályok a zárt és a nyílt forráskódú szoftverekben egyaránt használt, már meglévő, széles körben elterjedt általános gyakorlatokon alapulnak, de nem feltétlenül azokra korlátozódnak. A rendszer működéséhez először deklarálnunk kell egy nyilvános API-t. Ez állhat dokumentációból, vagy érvényesítheti maga a kód. Ettől függetlenül fontos, hogy ez az API világos és pontos legyen. Miután azonosítottuk nyilvános API-nkat, a változtatásokat a verziószámának meghatározott növekményeivel közli. Vegyük fontolóra az XYZ (Major.Minor.Patch) verzióformátumát. A hibajavítások, amelyek nem befolyásolják az API-t, növelik a javítás verzióját, visszafelé kompatibilis API-kiegészítések/változtatások növelik a kisebb verziót, és visszafelé inkompatibilis API-változások növelik a fő verziót.

Ezt a rendszert "szemantikus verziózásnak" hívjuk. Ebben a sémában a verziószámok és a változtatás módja jelentést ad az alapul szolgáló kódról és arról, hogy mi változott egyik verzióról a másikra.

Ha megadjuk a MAJOR.MINOR.PATCH verziószámot, az alábbi szerint növelhetjük:

1. MAJOR verzió, ha inkompatibilis API-módosításokat hajtunk végre,
1. KISEBB verzió, ha visszafelé kompatibilis módon adjuk hozzá funkciókat,
1. PATCH verzió, ha visszafelé kompatibilis hibajavításokat hajtunk végre.

A kiadás előtti és a build-metaadatok további címkéi a MAJOR.MINOR.PATCH formátum kiterjesztéseként állnak rendelkezésre.

## 2. Miért érdemes használni a szemantikus verziót
Ez nem egy új forradalmi ötlet. Valószínűleg már ehhez hasonlót is csináltunk. Valamiféle hivatalos specifikáció betartása nélkül a verziószámok lényegében feleslegesek a függőségkezeléshez. Azáltal, hogy nevet és világos meghatározást ad a fenti ötleteknek, könnyebbé válik szándékainak közlése a szoftver felhasználóival. Amint ezek a szándékok világosak, végül rugalmas (de nem túl rugalmas) függőségi specifikációk készíthetünk.

Egy egyszerű példa bemutatja, hogy a szemantikus verziókészítés hogyan teheti múltba a függőséget. Vegyünk egy "Firetruck" nevű könyvtárat. Szüksége van egy "Létra" nevű szemantikusan verziós csomagra. A Firetruck létrehozásakor a Ladder a 3.1.0 verziót használja. Mivel a Firetruck olyan funkciókat használ, amelyeket először a 3.1.0-ban vezettek be, nyugodtan megadhatja a létra függőséget, amely nagyobb vagy egyenlő, mint 3.1.0, de kevesebb, mint 4.0.0. Most, amikor elérhetővé válnak a Ladder 3.1.1 és 3.2.0 verziói, kiadhatja őket a csomagkezelő rendszerébe, és tudja, hogy kompatibilisek lesznek a meglévő függő szoftverekkel.

Felelõs fejlesztõként természetesen meg akarjuk gyõzõdni arról, hogy minden csomagfrissítés a hirdetett módon mûködik-e. A való világ rendetlen hely, ez ellen nem tehetünk mást, csak éberek vagyunk. Amit tehetünk, hagy a "Semantic Versioning" módot kínáljuk a csomagok kiadására és frissítésére anélkül, hogy a függő csomagok új verzióit kellene bevezetnünk, ezzel időt és gondot takarítva meg.

Ha mindez kívánatosnak tűnik, a Semantic Versioning használatának megkezdéséhez mindössze annyit kell tennünk, hogy kijelentjük, majd betartjuk a szabályokat.
