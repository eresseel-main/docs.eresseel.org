## 1. Bevezetés
A páros programozás egy agilis szoftverfejlesztési technika, amelyben két programozó dolgozik egy munkaállomásnál. Az egyikük a kódot írja, míg a másik pedig ellenőrzi az elkészült munkát. A páros programozás során a kódot író személyt sofőrnek, az ellenőrzést végzős megfigyelőnek vagy navigátornak nevezik. A két programozó gyakran felcseréli a szerepeket.

A megfigyelő a kódellenőrzés során figyelembe veszi a munka stratégiai irányát, és javaslatokat dolgoz ki a tökéletesítésre és a jövőbeli problémák megoldására. Ennek az a célja, hogy a sofőrnek ne kelljen fókuszálnia az éppen végrehajtott feladat taktikai szempontjaira. A sofőr számára a megfigyelő biztonsági hálót és vezetőt jelent.

## 2. Gazdaságosság
A páros programozás több munkaórát igényel, hogy leszállítson egy adott feladatot, mint a függetlenül dolgozó programozók esetén. A létrejött kód azonban sokkal kevesebb hibát tartalmaz. A páros programozás elméletileg ellensúlyozhatja a fejlesztési időt, minőségellenőrzést és egyéb kiadási faktorokat, hiszen csökkenti a programhibák számát.

A hibák csökkentése mellett egyéb előnyök is lehetségesek. Például a sofőrnek nem kell telefonhívásokkal és egyéb zavaró tényezőkkel foglalkoznia, mert ezeket elintézi helyette a megfigyelő. Amíg ketten együtt dolgoznak, kevesebb rendkívüli szünetet kell tartani, pontosabban lehet tartani a munkaidő-beosztást. A megfigyelő kiszúrhatja a sofőr által elkövetett programhibákat, emellett segíthet neki fókuszálni a munkára. A programozó pár egyik tagjának lehet magasabb tudása bizonyos témákban vagy technikákban. Emiatt könnyebb lehet megtalálni a megoldást bizonyos problémákra, és a kisebb tudású programozók készségei fejlődhetnek. Ezeket az előnyöket nehéz pontos számadatokkal mérni, de egyértelműen hozzájárulnak a hatékonyabb munkaidő kihasználáshoz.

## 3. A tervezés minősége
A páros programozás az alábbi okok miatt nagy potenciállal rendelkezik a problémák változatosabb megoldására:

* a feladatban részt vevő programozók különböző korábbi tapasztalatokkal rendelkeznek,
* különböző módokon értékelik a feladathoz releváns információt,
* a funkcionális szerepeik miatt különböző kapcsolatban vannak a megoldandó problémával.

A programozó párnak közösen kell megbeszélnie a teendőket, hogy eredményesen tudjanak dolgozni. Közös célokkal és tervekkel kell rendelkezzenek, és egyetértésben kell kidolgozniuk a megoldásukat. Sokféle lehetséges megoldást kell fontolóra venniük. Többet, mint egy egyedül dolgozó programozónak. Ez a metódus nagymértékben növeli a kód dizájn minőségét, mivel csökkenti az alacsony minőségű megoldási lehetőségek kiválasztásának esélyét.

## 4. Elégedettség
Egy páros programozókról 2000-ben készített online kutatás szerint a programozók 96 százaléka azt állította, hogy jobban élvezte a munkát párban, mint egyedül. 95 százalékuk azt nyilatkozta, hogy magabiztosabb volt munka közben, amikor párban programozott.

## 5. Tanulás
A párban dolgozó programozók folyamatosan megosztják egymással a tudásukat. Főként programozási nyelvi fortélyokat és átfogó tervezési elveket tanulnak egymástól. A vegyes párosítású módszer szerint a programozók nem egy meghatározott párral dolgoznak együtt, hanem a folyamatosan cserélgetik a párosításokat a munkahelyen dolgozó programozók között. Ez a változatos beosztás azt eredményezi, hogy a programozók átlagos tudása folyamatosan emelkedik a csapatban. A páros programozás során a programozók megvizsgálják a partnerük kódját, és elmondják a véleményüket. Ez ahhoz szükséges, hogy fejlesszék saját képességeiket, valamint hogy tanuljanak és tanítsanak is egy időben.

## 6. Csapatépítés és kommunikáció
A páros programozás megengedi a csapattagoknak, hogy gyorsan megosszák egymással tudásukat és véleményüket. Ez segít a programozóknak, hogy könnyebben kommunikáljanak. A páros programozás során növekszik a csapaton belüli információáramlás.

## 7. Tanulmányok
Empirikus tanulmányok és metaelemzések is készültek a páros programozás témakörében. Az empirikus tanulmányok inkább a termékenység szintjét és a kód minőségét vizsgálják, míg a meta-elemzések a tesztelés és publikálás által létrejött elfogultságokra fókuszálnak.

A meta-elemzések szerint a programozó párok több tervezési lehetőséget vesznek fontolóra, mint az egyedülálló programozók. A páros programozók által készített kód karbantartása egyszerűbb és a tervezési hibákat hamarabb észreveszik a kód írása közben. A meta-elemzések eredményeinek értékelésekor elfogultságot állapítottak meg. Ennek folyományaként nem lehet megállapítani, hogy a páros programozás egységesen előnyös vagy hatékony lenne.

Annak ellenére, hogy páros programozás alkalmazásával a feladat hamarabb elkészülhet, mintha egyedülálló programozó dolgozna rajta, a munkaórák száma párok esetén magasabb. Az egyes feladatok között egyensúlyozni kell a gyorsabb kivitelezést és a csökkentett tesztelési időt a kódolás magasabb óradíjával. Az előbbi faktorok súlyozása projektenként és feladatonként vizsgálva eltérhet.

A páros programozás azon feladatoknál a legelőnyösebb, amelyeket a programozók nem tudnak teljes mértékben megérteni a munka elkezdésekor. Ezen feladatok nagy kihívást jelentenek, és magasszintű kreativitást és kifinomultságot követelnek. Az egyes feladatok között egyensúlyozni kell a gyorsabb kivitelezést és a csökkentett tesztelési időt a kódolás magasabb óradíjával. Az előbbi faktorok súlyozása projektenként és feladatonként vizsgálva eltérhet.

A programozók által teljes mértékben megértett, egyszerűbb feladatok esetén a páros programozás termelékenysége jóval alacsonyabb az egyke programozóhoz képest. Lehetséges, hogy csökkenti a kódfejlesztés idejét, de fennáll a kód minőség csökkenésének kockázata. A termelékenység ugyancsak csökken, ha két kezdő programozót állítanak rá egy feladatra megfelelő mentor segítsége nélkül.

## 8. A teljesítménycsökkenés jelei
Az alábbi jelek mutatják, hogyha egy programozó pár nem teljesít megfelelően:

* Elszakadásról beszélünk, amikor a pár egyike fizikailag otthagyja a fejlesztőkörnyezetet, megnézi az e-mail fiókját, vagy elalszik.
* A „Figyeld a mestert” jelenség következhet be akkor, amikor a pár egyik tagja sokkal tapasztaltabb, mint a másik. Ebben a helyzetben a tapasztalatlanabb tag megfigyelő szerepbe kényszerülhet, a kódolás többségi része pedig a tapasztaltabb tagra marad. Ez a jelenség könnyen elszakadáshoz vezethet.

## 9. Párok létrehozásának lehetőségei
### 9.1. Szakértő – Szakértő
Egy ilyen páros nyilvánvaló választás lehet a legmagasabb termelékenység eléréséhez. Egy ilyen páros nagyszerű eredmények elérésére képes, de sokszor nem vesződnek új megoldások keresésével, mert nem kérdőjelezik meg a régi jól bevált gyakorlatokat.

### 9.2. Szakértő – Kezdő
Ez a párosítás sok lehetőséget jelent a kezdőnek, hogy tanuljon a szakértőtől. Ez a párosítás új ötleteket létrehozására is alkalmas, mivel egy kezdő nagyobb eséllyel kérdőjelezi meg a bevált gyakorlatokat. A szakértőnek meg kell magyaráznia ezeket a módszereket, és eközben átgondolja ezeket, és lehetnek ötletei hogyan lehetséges ezeket tökéletesíteni. Ebben a párosításban a kezdő passzívvá válhat, és hezitálhat abban, hogy kifejtse a konstruktív ötleteit. Néhány szakértőnek valószínűleg nincs ahhoz türelme, hogy támogassa a kezdőt a konstruktív ötletelésben.

### 9.3. Kezdő – Kezdő
Ez a párosítás jelentősen jobb eredményt biztosít, mintha ezek a kezdők külön-külön dolgoznának. Szakértői mentorálás nélkül nehezebb a kezdőknek fejlődni, ezért ezt a párosítást nem javasolják.

## 10. Távoli páros programozás
A távoli páros programozás, másnéven virtuális vagy elosztott páros programozás során a két programozó földrajzi értelemben két különböző helyen tartózkodik, és közös munkát lehetővé tevő valós-idejű szerkesztő, megosztott asztal vagy IDE kiegészítő alkalmazás használatával dolgoznak. A személyes kapcsolat és a verbális kommunikáció hiánya miatt konfliktusok és zűrzavarok alakulhatnak ki.

Támogató eszközök

* Képernyőmegosztó szoftverek
* Terminál multiplexerek
* Speciális elosztott szerkesztő eszközök
* Audio chat programok vagy VoIP szoftverek nagy segítséget nyújthatnak, ha a képernyőmegosztó program nem támogatja a hang alapú kommunikációt. Headsetek használatával a programozók keze szabad marad.
* Felhőalapú fejlesztő környezetek
* Együttműködést támogató páros programozás szolgáltatások