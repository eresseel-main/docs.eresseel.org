## 1. Bevezetés
A Google Cloud Platform (GCP) számítástechnikai szolgáltatások csomagja által kínált felhőalapú tárhely amely, ugyanazon az infrastruktúrán fut, amelyet a Google belsőleg használ végfelhasználói termékeihez, például a Google Kereséshez , a Gmailhez , a Google Drive -hoz és a YouTube. A felügyeleti eszközök mellett egy sor moduláris felhőszolgáltatást is kínál, beleértve az adattárolást, az adatelemzést és a gépi tanulást.A regisztrációhoz hitelkártya vagy bankszámla adatok szükségesek.

A Google Cloud Platform infrastruktúrát biztosít a szolgáltatásokhoz a szerver nélküli számítási környezeteket.

2008 áprilisában a Google bejelentette az App Engine-t, a webalkalmazások fejlesztésére és tárolására szolgáló platformot a Google által kezelt adatközpontokban, amely a vállalat első számítási felhő szolgáltatása volt. A szolgáltatás 2011 novemberében vált általánosan elérhetővé. Az App Engine bejelentése óta a Google több felhőszolgáltatást is hozzáadták a platformhoz.

A Google magában foglalja a Google Cloud Platform nyilvános felhőinfrastruktúráját, valamint a Google Workspace-t (G Suite), az Androidot és Chrome OS-t, valamint alkalmazásprogramozási felületeket (API-kat), a gépekhez tanulási és vállalati térképészeti szolgáltatásokat.

A Google több mint 100 terméket sorol fel a Google Cloud márkanév alatt. Az alábbiakban felsorolok néhány kulcsfontosságú szolgáltatást.

## 2. Compute
* App Engine: Platform mint szolgáltatás telepítéséhez Java, PHP, Node.js, Python, C#, .Net, Ruby és Go.
* Compute Engine: Infrastruktúra mint szolgáltatás futtatásához Microsoft Windows és Linux virtuális gépek.
* Google Kubernetes Engine (GKE) vagy GKE on-prem az Anthos platform részeként - tárolók és szolgáltatások Kubernetesen.
* Cloud Functions: Szolgáltatásként funkcionál Node.js, Java, Python vagy Go nyelven írt eseményvezérelt kód futtatására.
* Cloud Run: Knative alapú számítási végrehajtási környezet. Cloud Run (teljesen felügyelt) vagy Cloud Run az Anthos számára. Jelenleg támogatja a GCP, AWS és VMware kezelését.

## 3. Storage & Databases
* Cloud Storage: Integrált szélső gyorsítótárral rendelkezo strukturálatlan adatok tárolására alkalmas objektum tároló.
* Cloud SQL: Database mint szolgáltatás alapú MySQL, PostgreSQL és Microsoft SQL Server.
* Cloud Bigtable: Felügyelt NoSQL adatbázis-szolgáltatás.
* Cloud Spanner: Vízszintesen méretezhető, erősen konzisztens, relációs adatbázis-szolgáltatás.
* Cloud Datastore: NoSQL adatbázis webes és mobilalkalmazásokhoz.
* Persistent Disk: Blokk tárhely a Compute Engine virtuális gépei számára.
* Cloud Memorystore: Felügyelt, memórián belüli adattár a Redis és Memcached számára.
* Local SSD: Nagy teljesítményű átmeneti helyi blokktároló.
* Filestore: Nagy teljesítményű fájltárhely a Google Cloud felhasználóinak.
* AlloyDB: Teljesen felügyelt PostgreSQL adatbázis-szolgáltatás.

## 4. Networking
* VPC: Virtuális privát felhő kezelésére meghatározott hálózat. Szoftveresen lehet kezelni benne a felhő-erőforrásokat.
* Cloud Load Balancing: Szoftver által definiált és felügyelt szolgáltatás, alkalmazások terheléselosztásához.
* Cloud Armor: Webalkalmazások tűzfala a munkaterhelések védelmére a DDoS támadásokkal szemben.
* Cloud CDN: Tartalomszolgáltató hálózat, amely a Google globálisan elosztott jelenléti pontjain alapul.
* Cloud Interconnect: Adatközpont és a Google Cloud Platform összekapcsolására szolgáló szolgáltatás.
* Cloud DNS: Felügyelt, hiteles DNS -szolgáltatás, amely ugyanazon az infrastruktúrán fut, mint a Google.
* Network Service Tiers: Lehetőség a Prémium vagy a Standard hálózati szint kiválasztására a nagyobb teljesítményű hálózatokhoz. 

## 5. Big Data
* BigQuery: Skálázható, felügyelt vállalati adattárház elemzésekhez használják.
* Cloud Dataflow: Cloud Dataflow-n alapuló felügyelt szolgáltatás Apache adatfolyam- és kötegelt adatfeldolgozáshoz.
* Dataproc: Nagy mennyiségű adatok futtatásához használják. Apache Hadoop és Apache Spark feladatok elvegzésére.
* Cloud Composer: Apache Airflow-ra épülő felügyelt munkafolyamat szolgáltatás.
* Cloud Datalap: Eszköz adatfeltáráshoz, elemzéshez, vizualizációhoz és gépi tanuláshoz használják. Ez egy teljesen felügyelt Jupyter Notebook szolgáltatás.
* Cloud Pub/Sub: Méretezhető eseményfeldolgozási szolgáltatás Üzenetsorokhoz.
* Cloud Data Studio: Üzleti intelligencia eszköz az adatok megjelenítéséhez. Irányítópultok és jelentések segítségére használják.

## 6. Cloud AI
* Cloud AutoML: Szolgáltatás egyéni gépi tanulási modellek betanításához és üzembe helyezéséhez. 2018 szeptemberétől a szolgáltatás béta állapotban van.
* Cloud TPU: A Google által a gépi tanulási modellek betanításához használt gyorsítók.
* Cloud Machine Learning Engine: Felügyelt szolgáltatás a főbb keretrendszereken alapuló képzéshez és gépi tanulási modellek építéséhez.
* Cloud Talent Solution (korábban Cloud Job Discovery): A Google keresési és gépi tanulási képességein alapuló szolgáltatás a toborzási ökoszisztéma számára.
* Dialogflow Enterprise: A Google gépi tanulásán alapuló fejlesztői környezet, társalgási felületekhez.
* Cloud Natural Language: Szövegelemző szolgáltatás, amely a Google Deep Learning modelljein alapul.
* Cloud Speech-to-Text: Beszéd-szöveg gépi tanuláson alapuló szolgáltatás.
* Cloud Translation API: Több ezer elérhető nyelvpár közötti dinamikus fordítást biztosító szolgáltatás.
* Cloud Vision API: Képelemző gépi tanuláson alapuló szolgáltatás.
* Cloud Video Intelligence: Videóelemző Gépi tanuláson alapuló szolgálgtatás. 
