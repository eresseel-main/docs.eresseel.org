## 1. Bevezetés
A MySQL egy többfelhasználós, többszálú, SQL-alapú relációs adatbázis-kezelő szerver.

A szoftver eredeti fejlesztője a svéd MySQL AB cég, amely kettős licenceléssel tette elérhetővé a MySQL-t; választható módon vagy a GPL szabad szoftver licenc, vagy egy zárt (tulajdonosi) licenc érvényes a felhasználásra. 2008 januárjában a Sun felvásárolta 800 millió dollárért a céget. 2010. január 27-én a Sunt felvásárolta az Oracle Corporation, így a MySQL is az Oracle tulajdonába került.

A MySQL az egyik legelterjedtebb adatbázis-kezelő, aminek egyik oka lehet, hogy a teljesen nyílt forráskódú LAMP (Linux–Apache–MySQL–PHP) összeállítás részeként költséghatékony és egyszerűen beállítható megoldást ad dinamikus webhelyek szolgáltatására. 

## 2. Elérhetősége programnyelvekből
Egyedi illesztőfelületekkel az adatbázis-kezelő elérhető C, C++ , C#, Delphi, Eiffel, Smalltalk, Java, Lisp, Perl, PHP, Python, Ruby és Tcl programozási nyelvvel. Egy MyODBC nevű ODBC interfész további, ODBC-t kezelő nyelvek számára is hozzáférhetővé teszi az adatbázis-kezelőt. A MySQL számára az ANSI C a natív nyelv. 

## 3. Adminisztrációja
A MySQL adatbázisok adminisztrációjára a mellékelt parancssori eszközöket (mysql és mysqladmin) használhatjuk. A MySQL honlapjáról grafikus felületű adminisztráló eszközök is letölthetők: MySQL Administrator és MySQL Query Browser.

Széles körben elterjedt és népszerű adminisztrációs eszköz a PHP nyelven írt, nyitott forráskódú phpMyAdmin. A phpMyBackupPro (amelyet szintén PHP-ban írtak) adatbázisok (akár időzített, ismétlődő) mentésére szolgál eszközül.

## 4. Platformok 
A MySQL különböző platformokon futtatható: AIX, BSDi, FreeBSD, HP-UX, Linux, Mac OS X, NetBSD, Novell NetWare, OpenBSD, OS/2 Warp, QNX, SGI IRIX, Solaris, SunOS, SCO OpenServer, SCO UnixWare, OpenVMS, Tru64, Windows 95, Windows 98, Windows NT, Windows 2000, Windows XP és a Windows frissebb verziói. Már Mac OS X 10.4-re is elérhető. 

## 5. Képességek
### 5.1. A MySQL 5.x képességei
* ANSI SQL 99, számos kiegészítéssel
* Keresztplatformos elérhetőség
* Tárolt eljárások
* Adatbázis triggerek
* Kurzor adatbázisok
* "View" adatbázisok
* Valódi VARCHAR támogatás
* INFORMATION_SCHEMA támogatás
* "Strict" (szigorú) mód
* X/Open XA elosztott tranzakció-feldolgozás (DTP) támogatása; az Innobase InnoDB motorjának használata
* Különálló tároló motorok,(MyISAM olvasási sebességért, InnoDB a tranzakciókhoz és a referenciális integrációhoz, MySQL Archive az elavult adatok kevés helyen történő tárolására
* Tranzakciók az InnoDB, BDB és Cluster tároló motorokkal
* SSL támogatás
* Lekérdezés gyorstár (cache)
* Egymásba ágyazott SELECT -ek
* Szöveges indexelés és keresés a MyISAM motorral
* Beágyazott adatbázis-könyvtár
* Részleges Unicode támogatás
* ACID megfelelés az InnoDB-vel, BDB-vel és Cluster-rel
* Továbbfejlesztett MySQL Cluster
* „Példányosítás”

### 5.2. Megkülönböztető képességek
A következő képességekkel a MySQL rendelkezik, számos más relációs adatbázisrendszerrel ellentétben.

* Többféle tároló motor, amelyek között bármely táblához szabadon választhatunk
* Natív tároló motorok (MyISAM, Falcon, Merge, Memory (heap), MySQL Federated, MySQL Archive, CSV, Blackhole, MySQL Cluster, Berkeley DB, EXAMPLE, és Maria)
* Partnerek által fejlesztett tároló motorok (InnoDB, solidDB, NitroEDB, BrightHouse)
* Közösségi fejlesztésű tároló motorok (memcached, httpd, PBXT, Revision Engine)
* Akár egyéni tároló motor
* Commitek csoportosítása, több tranzakció fogadása többféle kapcsolatról, melyek meggyorsítják a tranzakciók lefolyását.