## 1. Bevezetés
A tesztvezérelt fejlesztés (Test-driven development, TDD) egy szoftverfejlesztési folyamat, ami egy nagyon rövid fejlesztési ciklus ismételgetésén alapul, tehát a követelményeket speciális teszt esetekként fogalmazzuk meg, a kódot pedig ehhez mérten írjuk meg, így az át fog menni a teszten. Ez tökéletes ellentéte a hagyományos szoftverfejlesztésnek, ami megengedi olyan kódrészletek meglétét is, amelyek nem felelnek meg teljesen a követelményeknek.

Kent Beck amerikai szoftvermérnököt tartjuk számon a technika kifejlesztéséért, úgymond az "újra-felfedezéséért", 2003-ban úgy fogalmazott, hogy a tesztvezérelt fejlesztés ösztönzi az egyszerű tervezési elemek használatát, valamint hatással van az önbizalom növekedésére.

A TDD összefüggésben áll az extrém programozás koncepciójával, miszerint először teszteljünk és ha minél többet tesztelünk, annál több hibát tudunk kiküszöbölni a kódban.Az utóbbi 1999-ben indult útjára, a TDD-nek pedig az idő előrehaladtával kialakultak sajátos megoldásai.

Rengeteg programozó alkalmazza ezt a technikát arra is, hogy egyszerűbbé tegyék az olyan legacy kódok fejlesztését, hibakövetését, amik régebbi eszközök felhasználásával készültek.

## 2. Tesztvezérelt fejlesztési ciklus
A tesztvezérelt fejlesztési életciklus grafikus ábrázolása

A következő lépések a Test-Driven Development by Example könyv elméletén alapulnak:[2]

### 2.1. Teszt hozzáadása
A tesztvezérelt fejlesztés során minden új funkció teszt írásával kezdődik. Olyan tesztet kell írni, amely tömören és pontosan meghatározza az új funkciót vagy a már meglévő, javításra szánt funkciót. Teszt írásához a fejlesztőnek egyértelműen meg kell értenie a szolgáltatás specifikációját és követelményeit. A fejlesztő felhasználási esetek és a felhasználóval történt beszélgetések alapján pontosan lefedheti a követelményeket és az elvárásokat, így képes lesz a teszteket megírni, függetlenül a tesztelési és fejlesztői környezettől. Mindez alkalmazható már meglévő tesztek módosítására is. Ez a tulajdonság különbözteti meg igazán a tesztvezérelt fejlesztést az olyan unit tesztek írásától, amelyek már a kód megírása után készültek. Apró, de fontos eltérés ez.

### 2.2. Minden korábbi teszt futtatása, hogy kiderüljön az új teszt megbukik e
Ez megmutatja, hogy a tesztkihasználtság jól működik, az új teszt új kód igénylése nélkül nem teljesít sikeresen, mert még nem létezik a kívánt viselkedés, és ez megfelelően kiküszöböli annak a lehetőségét, hogy az új teszt selejtes és minden esetben átmegy a teszten. Az új tesztnek kudarcot kell vallania a várt eredmények alapján. Ez a lépés növeli a fejlesztő magabiztosságát az új tesztek megírásával kapcsolatban.

### 2.3. Kód írás
A következő lépés egy olyan kód írása, amely alapján a teszt sikeres lesz. Az ebben a szakaszban írt új kód nem lesz tökéletes, és valószínűleg egy nem túl elegáns módon fogja teljesíteni a tesztet. Ez azért elfogadható, mert az 5. lépésben úgyis csiszolni, szépíteni fogjuk a kódot. Ezen a ponton az írott kód egyetlen célja a teszt teljesítése. A programozó nem írhat olyan kódot, amely meghaladja a teszt által ellenőrzött funkciókat.

### 2.4. Minden teszt újbóli futtatása
Ha az összes tesztelési eset működik, a programozó biztos lehet benne, hogy az új kód megfelel a tesztelési követelményeknek, és nem ront a meglévő szolgáltatások minőségén. Ha mégis hibát okoz, akkor az új kódot addig kell módosítani, amíg meg nem felel az elvárásoknak.

### 2.5. Kódszépítés
A növekvő kódot rendszeresen meg kell tisztítani a tesztvezérelt fejlesztés során. Az új kód áthelyezhető onnan, ahol alkalmas volt a tesztelésre, oda, ahova logikusan tartozik. A másolatot el kell távolítani. Az objektum-, osztály-, modul-, változó- és metódusneveknek egyértelműen tükrözniük kell jelenlegi céljaikat és felhasználás mikéntjét, hogy az extra funkciók gond nélkül hozzáadhatóak legyenek. A szolgáltatások hozzáadásával a metódusok testei hosszabbá válhatnak, és más objektumok nagyobbak lehetnek. Előnyünkre válhat az ilyen részek feldarabolása az olvashatóság és a karbantarthatóság javításának érdekében, ami értékes lépésnek fog bizonyulni a szoftver későbbi életciklusában . Az öröklési hierarchiákat át lehet rendezni, hogy logikusabbak és hasznosabbak legyenek, és hogy előnyükre váljanak a különböző tervezési mintáknak. Ismeretesek speciális és általános iránymutatások a refaktoráláshoz és a tiszta kód létrehozásához. A teszt esetek folyamatos újraindításával az egyes refaktorozási szakaszok során a fejlesztő biztos lehet benne, hogy a folyamat nem változtat meg meglévő funkciókat.

A sokszorosítás megszüntetése minden szoftvertervezési minta fontos aspektusa. Ebben az esetben azonban ez vonatkozik a duplikációs kód eltávolítására is a teszt kód és a végleges kód között – mint ahogyan azt említettük is a 3. lépésben.

### 2.6. Lépések ismétlése
Új teszt írásával a ciklus újra fog indulni. A lépések méretének mindig kicsinek kell lennie, mindegyik teszt futtatása között csak körülbelül 1-10 szerkesztéssel. Ha az új kód nem elégíti ki gyorsan az új tesztet, vagy ha más tesztek váratlanul sikertelennek bizonyulnak, akkor a programozónak vissza kell vonnia vagy vissza kell állítania a kódot szemben a túlzott hibakereséssel. A folyamatos integráció visszafordítható ellenőrzőpontok állításával segít ezen a problémán. Külső könyvtárak használatakor fontos, hogy ne dolgozzunk olyan kicsi lépésekben, amikkel effektíven teszteljük csupán csak magát a könyvtárat, kivéve, ha okkal feltételezhető, hogy a könyvtár hibás vagy nem elégíti ki a szolgáltatásokat teljes körűen.

## 3. Fejlesztési stílus
A tesztvezérelt fejlesztés alkalmazásának különféle aspektusai vannak, ilyen például a „tartsd egyszerűen, ostoba” ( KISS ) és a „Nem lesz rá szükséged” (YAGNI) alapelvek. Azáltal, hogy csak a tesztek lebonyolításához szükséges kódra fókuszálunk, a formaterv gyakran tisztább lehet, mint más módszerekkel. Kent Beck az általa írt könyvében, ami a Test-Driven Development by Example, javasolja még a " hamisítsd, amíg el nem készíted " elvet is.

Ahhoz hogy olyan fejlett tervezési koncepciókat érjenek el, mint például a tervezési minták, a megírt tesztek maguk generálják ezeket a tervezési koncepciókat. Lehet, hogy a kód egyszerűbb marad, mint a célmintázat, de teljesíti az összes szükséges tesztet. Ez elsőre zavaró lehet, de lehetővé teszi a fejlesztőnek, hogy csak arra koncentráljon, ami ténylegesen számít.

Első lépés, a teszt írás: A teszteket a tesztelni kívánt funkció előtt kell megírni. Ennek a későbbiekben számos előnye lesz. Az alkalmazás így a tesztelhetőség érdekében lesz megírva, a fejlesztő így figyelembe veheti, hogy hogyan fogja tesztelni a projektet mielőtt azt ténylegesen megírná. Biztosítja azt is, hogy minden szolgáltatáshoz tartozzon megfelelő teszteset. Ezenkívül a tesztek írása közben a fejlesztő a termékigényeket mélyebben és korában megértheti, ezáltal biztosítja a tesztkód hatékonyságát, és folyamatosan a szoftver minőségére összpontosít. Ha viszont funkciók minél gyorsabb megírására törekszenek, előfordulhat, hogy a többi fejlesztő és a szervezetek hajlamosak a fejlesztőt sürgetni a következő funkció megírásával, ami azt eredményezi hogy a tesztelést akár teljes egészében figyelmen kívül fogja hagyni. Az első TDD teszt először talán még le sem fog fordulni, mert az előírt osztályok és módszerek még a kezdeti szakaszban nem léteznek. Ennek ellenére az első teszt a végrehajtható specifikáció kezdeteként működik.

### 3.1. Az egységek legyenek minél kisebbek
A TDD esetében az egységet leggyakrabban osztályként vagy kapcsolódó funkciók csoportjaként definiálják, amelyet gyakran modulnak hívnak. Az egységek viszonylag kis méretben tartása nagyszerű előnyökkel járnak, többek között:

* Csökkenti a hibakeresésre szánt időt – Amikor a teszthiba észlelhetővé válik, a kisebb egységek elősegítik a hiba vagy hibák felderítését.
* Öndokumentáló tesztek – A kis tesztesetek gyorsabban olvashatóak és könnyebben megérthetőek.

A tesztvezérelt fejlesztés fejlett gyakorlatai hasonlóak lehetnek az elfogadási tesztvezérelt fejlesztés (ATDD) és a Specifikáció példákon keresztül szemléletéhez. Utóbbi például azt jelenti, hogy az ügyfél által megadott kritériumokat automatizálják, mint elfogadási tesztek, amelyek ezután vezetik a hagyományos egység tesztvezérelt fejlesztési (UTDD) folyamatokat. Ez a folyamat biztosítja, hogy az ügyfél automatizált mechanizmussal rendelkezik annak eldöntésére, hogy a szoftver megfelel-e a követelményeinek. Az ATDD-vel a fejlesztői csapatnak konkrét célt kell teljesítenie – az elfogadási teszteket -, amelyek folyamatosan arra összpontosítanak, hogy az ügyfél pontosan mit is akar látni az egyes felhasználói sztorikból.

### 3.2. Színek jelentése
Minden teszt kezdetben kudarcot vall: Ez biztosítja, hogy a teszt valóban működik és képes megtalálni a hibát. Miután ez megvalósult, a mögöttes funkciók ezután megírhatóak. Ez vezetett a "tesztvezérelt fejlesztési mantrához", azaz "vörös / zöld / kék / vörös". teszteléshez, ahol a piros a kudarcot, azaz a teszt sikertelenségét jelenti, a zöld pedig azt, hogy sikeres volt a teszt. A refaktorálás nélküli tesztelést Piros-Zöld-Piros tesztelésnek nevezzük. A tesztvezérelt fejlesztés folyamatosan megismétli a az újabb tesztesemények hozzáadását, amik sikertelenül végződnek, később sikeresek lesznek, és ezeket folyamatosan refaktorálja, azaz szépíti. A kódrefaktorálás színe egységesen a kék. Elvileg minden zöld lépés után kék jön, ezért a TDD-t nevezhetjük Piros-Zöld-Kék-Piros tesztelésnek is. A várt teszteredmények minden szakaszban történő lefutása megerősíti a fejlesztő kódjának mentális modelljét, növeli az önbizalmát és növeli a termelékenységét.

## 4. Legjobb gyakorlatok

### 4.1 A teszt felépítése
A teszt eset hatékony elrendezése biztosítja az összes szükséges művelet elvégzését, javítja a teszt eset olvashatóságát és megkönnyíti a végrehajtás folyamatát. A konzisztens felépítés segít az öndokumentáló tesztesetek létrehozásában. A teszt esetekben általánosan alkalmazott struktúrának van (1) beállítása, (2) végrehajtása, (3) érvényesítése és (4) megtisztítása.

* `Beállítás`: Helyezze az egységet tesztelés alá (UUT) vagy a teljes tesztrendszert a teszt futtatásához szükséges állapotba.
* `Végrehajtás`: Indítsa el/vezesse az UUT-t a célviselkedés végrehajtásához és rögzítse az összes kimenetet, mint például a visszatérési értékeket és kimeneti paramétereket. Ez a lépés általában nagyon egyszerű.
* `Érvényesítés`: Ellenőrizze, hogy a teszt eredményei helyesek-e. Ezek az eredmények tartalmazhatnak a végrehajtás során rögzített explicit kimeneteket vagy az UUT állapotváltozásait.
* `Tisztítás`: Állítsa vissza az UUT-t vagy a teljes tesztrendszert a teszt előtti állapotba. Ez a helyreállítás lehetővé teszi egy újabb teszt végrehajtását közvetlenül a teszt után.

### 4.2. Egyéni legjobb gyakorlatok
Néhány bevált gyakorlat, amelyet az egyén követhet. A közös beállítási és lebontási logika szétválasztása teszt támogató szolgáltatásokba amelyeket a megfelelő tesztesetek használnak fel, hogy minden egyes teszt orákulum csak a teszt validálásához szükséges eredményekre összpontosítson, és tervezzen időfüggő teszteket a végrehajtás toleranciájának lehetővé tétele érdekében a nem valósidejű operációs rendszerekben. Az általános gyakorlat az, hogy 5-10 százalékos tartalékot hagynak a késői végrehajtáshoz, csökkentve ezáltal a hamis negatívok számát a teszt végrehajtások során. Azt is gyakran javasolják, hogy a tesztkód ugyanazt a bánásmódot kapja, mint a termelési kód. A tesztkódnak helyesen kell működnie mind a pozitív, mind a negatív esetekben, legyen időtálló, olvasható és karbantartható. A csapatok összegyűlhetnek és felülvizsgálhatják a teszteket és a tesztelési gyakorlatokat, hogy megosszák egymással a hatékony technikákat és kiküszöböljék a rossz szokásokat.

### 4.3. Kerülendő gyakorlatok vagy „antiminták”

* Olyan tesztesetek megléte, melyek a korábban végrehajtott tesztesetekkel manipulált rendszer állapotától függenek (azaz az egység tesztet mindig egy ismert és előre konfigurált állapotból kell elindítani).
* A teszt esetek közötti függőségek. Az a tesztkészlet, amelyben a teszt esetek egymástól függenek, törékeny és túlságosan összetett. A végrehajtási sorrend ne legyen kikövetkeztethető. A kezdeti teszt esetek vagy az UUT struktúrájának alapvető refaktorálása egyre inkább átfogó hatások spirálját okozza a kapcsolódó tesztekben.
* Interdependens tesztek, azaz egymástól függő tesztek. Ezek sorban egymáshoz kapcsolódó hamis eredményeket generálnak. A korai teszt hibája egy későbbi teszt esetét akkor is megszakítja, ha nincs tényleges hiba az UUT-ban, ami növeli a hibaelemzésre szánt időt és a hibakeresési erőfeszítések számát.
* A végrehajtási viselkedés pontos időzítésének vagy teljesítményének tesztelése.
* "Mindent tudó orákulumok" építése. Egy olyan orákulum, amely a szükségesnél többet vizsgál, drágábbá és törékenyebbé válik az idő múlásával. Ez a nagyon gyakori hiba azért veszélyes, mert kényes, de átfogó időelnyelőt okozhat egy komplex projekt során.
* A kivitelezés részleteinek tesztelése.
* Lassú lefutású tesztek.

## 5. Előnyök
Egy 2005-ös tanulmány megállapította, hogy TDD használatával jóval több teszt íródik, így azok a programozók, akik több tesztet írtak, sokkal termelékenyebbnek bizonyultak. Azonban a kódminőséggel kapcsolatos hipotézisek, valamint a TDD és a termelékenység közvetett összefüggése nem volt meggyőző.

Azok a programozók, akik tiszta TDD-t használtak az új („ zöldmezős ”) projekteknél, csak ritkán érezték szükségét a hibakeresõ használatának. Verziókezelő rendszerrel együtt használva, amikor a tesztek váratlanul kudarcot vallanak, a kód visszaállítása a legutóbbi verzióra, amely minden tesztet sikeresen végrehajtott, gyakran eredményesebb lehet, mint maga a hibakeresés.

A tesztvezérelt fejlesztés nem csupán a helyesség érvényességét jelzi, hanem a program megtervezését is segíti. Ha először a teszt esetekre összpontosítunk, el kell képzelnünk, hogy az ügyfelek hogyan használják majd a funkciókat (az első esetben a teszt eseteket). A programozó tehát a felülettel foglalkozik a megvalósítás előtt. Ez az előny kiegészíti a szerződéses tervezést, mivel a kódot tesztesetekkel, nem pedig matematikai állításokkal közelíti meg.

A tesztvezérelt fejlesztés lehetőséget biztosít arra, hogy szükség esetén csak kis lépésekben haladjon a fejlesztő. Ez lehetővé teszi a programozók számára, hogy a jelenlegi feladatra összpontosítsanak, mivel az első és legfontosabb cél a teszt sikeressége. A kivételes eseteket és a hibakezelést kezdetben nem veszik figyelembe, és ezen idegen körülmények megteremtésére szolgáló teszteket külön-külön hajtják végre. A teszt-vezérelt fejlesztés így biztosítja, hogy az összes írott kódot legalább egy teszt lefedje. Ez nagyobb fokú magabiztosságot ad a kóddal kapcsolatban a programozó csapatnak és az azt követő felhasználóknak.

Noha igaz, hogy a TDD-vel több kódra van szükség, mint TDD nélkül, az egység tesztek miatt, a teljes kód implementálási ideje rövidebbé válhat a Müller és Padberg modell alkalmazásával. A vizsgálatok minél nagyobb száma segíti korlátozni a kód hibáinak számát. A tesztelés korai és gyakori jellege elősegíti a hibák észlelését a fejlesztési ciklus korai szakaszában, megakadályozva őket, hogy endemikus és drága problémává váljanak. A hibák kiküszöbölése a folyamat elején általában segít elkerülni a hosszabb és fárasztóbb hibakeresést a projekt későbbi szakaszában.

A TDD használata modulárisabb, rugalmasabb és bővíthetőbb kódhoz vezethet. Ez a hatás gyakran azért jön létre, mert a módszertan megköveteli, hogy a fejlesztők a szoftvert olyan kis egységekben lássák, amelyeket önállóan meg lehet írni és tesztelni, majd ezeket később össze lehet fűzni. Mindez kisebb, koncentráltabb osztályokhoz, lazább kapcsolódásokhoz és tisztább interfészekhez vezet. A Mock objektum tervezési minta használata szintén hozzájárul a kód általános modulálásához, mivel ez a minta megköveteli a kód meglétét, így a modulok között könnyen át lehet váltani, azokról amelyek az egység tesztelését szolgálták azokra amelyek valódi verziói a projektnek.

Mivel nem írnak annál több kódot, mint amennyire szükség van egy sikertelen teszteset teljesítéséhez, az automatizált tesztek lefedik az összes lehetséges kód kimenetelt. Például ahhoz, hogy egy TDD fejlesztő hozzáadjon egy else ágat egy létező if utasításhoz, a fejlesztőnek először írnia kell egy hibás teszt esetet. Ennek eredményeként a TDD-ből származó automatizált tesztek általában nagyon alaposak: észlelnek minden váratlan változást a kód viselkedésében. Ez olyan problémákat is észlel, amelyek akkor merülhetnek fel, ha a fejlesztési ciklus későbbi változástatásai hatással lesznek a többi funkcióra.

Madeyski empirikus bizonyítékokat szolgáltatott (több mint 200 fejlesztővel végzett laboratóriumi kísérletek sorozatán keresztül) a TDD gyakorlatának fölényével kapcsolatban, ami lenyomta a hagyományos tesztelj-utóbb megközelítést és a helyesség-tesztelési módszert. Az átlagos hatásméret egy közepes (de közel a nagyhoz) hatást képvisel a végrehajtott kísérletek meta-analízise alapján, amely egy igazán lényeges megállapítás. A moduláció javítását (azaz egy modulárisabb kialakítást), a kifejlesztett szoftvertermékek könnyebb újrafelhasználását és tesztelését javasolja, ami a TDD programozási gyakorlatnak köszönhető. Madeyski a TDD gyakorlatnak az egységtesztekre gyakorolt hatását az ág lefedettség (BC) és a mutációs pontszám indikátor (MSI) felhasználásával is megmérte, amelyek az egységtesztek alaposságának és hibadetektálási hatékonyságának mutatói. A TDD hatása az ágak lefedettségére közepes méretű volt, ezért ez már lényeges hatásnak tekinthető.

## 6. Korlátozások
A tesztvezérelt fejlesztés nem hajt végre elegendő tesztelést olyan helyzetekben, amikor a teljes működési tesztekre szükség van a siker vagy kudarc meghatározásához, ennek oka az egységtesztek széles körű használata. Ezekre jó példa a felhasználói felületek, az adatbázisokkal működő programok, és néhány olyan rendszer, amely az adott hálózati konfigurációtól függ. A TDD arra ösztönzi a fejlesztőket, hogy minimális mennyiségű kódot írjanak az ilyen modulokba, és maximalizálják a logikát, amely a tesztelhető könyvtári kódban van, így a külvilágot hamis és mock objektumok felhasználásával reprezentálják.

A menedzsment támogatása alapvető fontossággal bír. Anélkül, hogy az egész szervezet úgy gondolná, hogy a teszt-alapú fejlesztés javítani fog a terméken, a vezetés úgy érezheti, hogy a tesztek írására fordított idő egyszerű pazarlás.

A teszt-vezérelt fejlesztői környezetben létrehozott egységteszteket általában ugyanaz a fejlesztő hozza létre, aki a tesztelt kódot is írja. Ezért a tesztek vak területeket oszthatnak meg a kóddal: ha például egy fejlesztő nem veszi észre, hogy bizonyos bemeneti paramétereket ellenőrizni kell, akkor valószínű, hogy sem a teszt, sem a kód nem ellenőrzi ezeket a paramétereket. Egy másik példa: ha a fejlesztő félreérti a fejlesztett modulra vonatkozó követelményeket, akkor mind a kód, mind az általa írt egység tesztek ugyanúgy tévedni fognak. Ezért a tesztek ugyan sikeresek lesznek, de téves értelmet adnak a helyességről.

A nagy számú sikeres egységteszt téves biztonságérzetet okozhat, ami kevesebb kiegészítő szoftver tesztelési tevékenységhez vezethet, ilyenek például az integrációs tesztek és megfelelőségi-tesztek.

A tesztek a projekt karbantartási költségeinek részévé válnak. A rosszul megírt tesztek, például azok, amelyek kódolt hibaszövegeket tartalmaznak, maguk is hajlamosak a kudarcra, és költséges a karbantartásuk. Ez különösen igaz a törékeny tesztekre. Fennáll annak a veszélye, hogy a rendszeresen hamis hibákat generáló teszteket figyelmen kívül hagyják, így amikor valódi hiba fordul elő, akkor azt nem veszik észre. Lehetőség van tesztek írására az alacsony és könnyű karbantartás érdekében, például a hibaszövegek újbóli felhasználásával, ez a fentiekben ismertetett kódrefaktorálási szakasz egyik fő célja.

A túl sok teszt írása és fenntartása sok időbe telik. Ezenkívül a rugalmasabb (korlátozott tesztekkel rendelkező) modulok új követelményeket is elfogadhatnak anélkül, hogy a teszteken bármit is változtatni kellene. Ezen okok miatt a csak szélsőséges körülmények között végzett tesztelés, vagy a kisebb adatmintákon végzett tesztelés jobban megkönnyítheti a munkát, mint a túl részletesen felépített tesztesetek.

Az ismételt TDD ciklusok során elért lefedettségi szintet és a tesztelési részletességet a későbbiekben egyre nehezebbé válik újraalkotni, megváltoztatni. Ezért ezek az eredeti, vagy a korai tesztek egyre értékesebbé válnak az idő múlásával. A taktika az, hogy korán fixálják az ezekkel kapcsolatban felmerülő problémákat. Továbbá, ha rossz az architektúra, a tervezés vagy a tesztelési stratégia, az később változásokhoz vezethet, amely több tucat meglévő teszt kudarcba fulladását eredményezheti, így tehát fontos, hogy azokat külön-külön rögzítsék. Ha pusztán csak törlik, letiltják vagy a változtatásokat meggondolatlanul hajtják végre, azok valószínűleg nem észlelhető lyukakhoz vezethetnek a teszt lefedettségében.

## 7. Tesztvezérelt munka
A teszt-vezérelt fejlesztést mára már a termék-, és a szolgáltatócsoportok is átvették, mint teszt-vezérelt munka. A TDD-hez hasonlóan a nem-szoftver csapatok a munka minden egyes vonatkozásában a munka megkezdése előtt minőség-ellenőrzési (QC) ellenőrzéseket hajtanak végre(általában manuális teszteket, nem automatizált teszteket dolgoznak ki). Ezeket a QC ellenőrzéseket ezután felhasználják a terv tájékoztatására és a kapcsolódó eredmények validálására. A TDD sorozat hat lépését kisebb szemantikai változtatásokkal alkalmazzák:

* A "Ellenőrzés hozzáadása" a "Teszt hozzáadása" helyett
* Az "Összes ellenőrzés futtatása" a "Az összes teszt futtatása" helyett
* A "Csináld a munkát" a "Kód írása" helyett
* Az "Összes ellenőrzés újbóli futtatása" a "A tesztek újbóli futtatása" helyett
* A "Tisztítsd meg a munkát" a "Kódszépítés" helyett
* "Ismétlés"

## 8. TDD és ATDD
A tesztvezérelt fejlesztés kapcsolódik, de különbözik az elfogadási tesztvezérelt fejlesztéstől (ATDD). A TDD elsősorban egy fejlesztői eszköz, amely elősegíti az egységkódok helyes megírását (funkció, osztály vagy modul), amely helyesen fogja végrehajtani műveletek adott halmazát. Az ATDD egy kommunikációs eszköz az ügyfél, a fejlesztő és a tesztelő között, ami azt a célt szolgálja, hogy a követelményeket pontosabban lehessen meghatározni. A TDD tesztek automatizálást igényelnek. Az ATDD nem, bár az automatizálás segíti a regressziós tesztelést. A TDD-ben használt tesztek gyakran ATDD tesztekből származtathatók, mivel a kód egységek végrehajtják a követelmények egy részét. Az ATDD teszteknek olvashatónak kell lenniük az ügyfél számára. A TDD tesztnek ezzel szemben nem kell annak lennie.

## 9. TDD és BDD
A BDD (viselkedésvezérelt fejlesztés) egyesíti a TDD és az ATDD gyakorlatait. Ez magában foglalja a tesztek írásának gyakorlatát, de a viselkedést leíró tesztekre összpontosít, nem pedig a végrehajtási egységet tesztelő tesztekre. Az olyan eszközök, mint a JBehave, a Cucumber, az Mspec és a SpecFlow olyan szintaxisokat biztosítanak, amelyek lehetővé teszik a terméktulajdonosok, a fejlesztők és a tesztmérnökök számára, hogy együttesen határozzák meg a viselkedést, amelyet azután automatizált tesztekké alakíthatnak.

## 10. Kódláthatóság
A tesztsorozatnak hozzá kell férnie a tesztelésre szánt kódhoz. Azonban, a normál tervezési követelmények szerint, mint az információrejtés, enkapszuláció és az ügyek szétválasztása, az adatok nem veszélyeztethetőek. így tehát, a TDD egység tesztjei általában ugyanazon projektben vagy modulban találhatóak, mint a tesztelni kívánt kód.

Objektum-orientált tervezésnél ez továbbra sem biztosít hozzáférést a bizalmas adatokhoz és módszerekhez. Ezért további munkára lehet szükség az egységtesztek megírásánál. Java és más nyelveken a fejlesztő reflexiót használhat a privát mezők és metódusok eléréséhez. Alternatív megoldásként egy belső osztály is létrehozható az egység tesztek lebonyolításához, hogy láthatóak legyenek a mellékelt osztály tagjai és attribútumai. A . A NET Framework-ben és néhány más programozási nyelvben, részleges osztályok használhatóak fel, így hozzáférhetővé válnak a szükséges metódusok és adatok a teszt elvégzéséhez.

Fontos, hogy az ilyen tesztelési trükkök ne maradjanak benne a végleges kódban. C-ben és más nyelvekben olyan fordító irányelvek, mint az #if DEBUG ... #endif helyezhetőek el kiegészítő osztályok és valójában az összes többi, a teszthez kapcsolódó kód körül, hogy megakadályozzák őket, hogy a kiadott kódban is leforduljanak. Ez azt jelenti, hogy a kiadott kód nem pontosan egyezik meg azzal, amit korábban teszteltek. A kevesebb, de átfogóbb, végponttól kezdődő integrációs tesztek rendszeres futtatása a végleges kiadás összeállításában biztosíthatja (többek között), hogy nem létezik olyan gyártási kód, amely kicsit is támaszkodik a tesztköteg szempontjaira.

A TDD-t aktívan használók között vita folyik, amelyet a blogjaikban és más írásaiban dokumentálnak, arról, hogy mindenképpen bölcs dolog-e a privát adatok és metódusok tesztelése. Egyesek szerint a privát tagok pusztán végrehajtási részletek, amelyek megváltozhatnak, és ezeknek engedni kell, anélkül, hogy a tesztszámokon csökkentenének. Ily módon elegendő bármilyen osztály tesztelése a nyilvános interfészein vagy alosztályi interfészein keresztül, amelyet néhány nyelv "védett" felületnek hív. Mások szerint a funkcionalitás kritikus szempontjai megvalósíthatók privát metódusokkal, és ezeket közvetlenül lehet tesztelni a kisebb és specifikusabb egységtesztekkel.

## 11. Szoftverek tesztvezérelt fejlesztéshez
Számos olyan tesztelési keretrendszer és eszköz áll rendelkezésre, ami hasznos a TDD megvalósításához.

### 11.1. xUnit keretrendszerek
A fejlesztők használhatnak számítógéppel támogatott tesztelési keretrendszereket, ezek közös megnevezése a xUnit (mely az 1998-ban létrehozott SUnit elnevezésből származik). Ezek azt a célt szolgálják, hogy önmaguk megalkossák és automatikusan futtassák a teszteseteket. Az xUnit keretrendszerek állításalapú érvényességi vizsgálatokat és eredményeket biztosítanak. Ezek a képességek kritikusak az automatizálás szempontjából, mivel a végrehajtás érvényesítésének terheit egy független utófeldolgozási tevékenységből átvitték a teszt végrehajtásába. Az ezen tesztelési keretrendszerek által biztosított végrehajtási keret lehetővé teszi az összes teszteset vagy különféle részhalmaz automatikus végrehajtását más funkciókkal együtt.[33]

### 11.2. TAP-eredmények
A tesztelési keretrendszerek elfogadhatják az egységteszt kimenetét az 1987-ben létrehozott nyelv-agnosztikai Test Anything Protokoll alapján.

## 12. Hamis, mock objektumok és integrációs tesztek
Az egységteszteket azért így nevezték el, mert egy egységnyi kódot tesztelnek. Egy komplex modulnak ezer egységtesztje lehet, egy egyszerű modulnak csak tíz lehet. A TDD-hez használt egységtesztnek soha nem szabad átlépnie a program határait, nem is beszélve a hálózati kapcsolatokról. Ha ezek mégis megtörténnek, a bemutatás csúszhat, amely miatt a tesztek lassan futnak le, és elriasztják a fejlesztőket a teljes tesztcsomag futtatásától. A külső moduloktól vagy adatoktól való függőség az egységteszteket integrációs tesztekké alakítja. Ha az egyik modul hibásan működik egymással összekapcsolt modulok láncában, akkor annyira nem lesz egyértelmű, hogy hol kell keresni a hiba okát.

Ha a fejlesztés alatt álló kód adatbázisra, webszolgáltatásra vagy bármilyen más külső folyamatra vagy szolgáltatásra támaszkodik, az egység-tesztelhetőség szétválasztása szintén lehetőséget biztosít a modulárisabb, tesztelhetőbb és újrafelhasználhatóbb kódok tervezéséhez. Ennek megvalósításához két lépés szükséges:

* Amikor a végleges tervhez külső hozzáférésre van szükség, meg kell határozni egy interfészt, amely leírja a rendelkezésre álló hozzáférést. Lásd a függőségi inverzió elvét, ami ennek előnyeit vitatja, függetlenül a TDD-től.
* Az interfészt kétféle módon lehet megvalósítani: az egyik valóban hozzáfér a külső folyamathoz, a másik pedig egy hamis vagy mock objektum. A hamis objektumoknak többet kell tennie annál, mint hogy egy „Személyes objektum mentve” üzenetet mentenek a nyomkövetési naplóba, amelyet könnyen helyettesíthet egy állításalapú teszt, mely futtatható a helyes viselkedés ellenőrzésének érdekében. A mock-objektumok abban különböznek ettől, hogy maguk tartalmaznak teszt-állításokat amelyek miatt a teszt sikertelen lehet, például ha a személy neve vagy egyéb adatai nem azok ami az elvárt lenne.

A hamis és álobjektumok módszere ami adatokkal tér vissza, látszólag egy adattárból vagy felhasználótól jönnek, elősegíthetik a teszt folyamatát azáltal, hogy mindig ugyanazokat a reális adatokat szolgáltatják vissza, amikre a tesztek támaszkodhatnak. Beállíthatók előre meghatározott hibamódok is, hogy a hibakezelési rutinok fejleszthetők és megbízhatóan tesztelhetők legyenek. Hiba módban egy metódus visszatérhet érvénytelen, hiányos vagy nulla válasszal, vagy kivételt dobhat. Az adattárolóktól eltérő hamis szolgáltatások szintén hasznosak lehetnek a TDD-ben: A hamis titkosítási szolgáltatás valójában nem titkosítja az átadott adatokat; egy hamis véletlen szám szolgáltatás mindig visszatérhet 1-el. Hamis vagy ál-megvalósítás még a függőségi injekció is.

A Teszt Páros egy tesztspecifikus képesség, amely helyettesíti a rendszer képességét, általában egy osztályt vagy funkciót, amelytől az UUT függ. Két olyan alkalom van, amikor be lehet vezetni a tesztpárost egy rendszerbe: kapcsolódás és végrehajtás. A kapcsolódási idő helyettesítése akkor történik, amikor a tesztpárost összeállítják a betöltési modulba, amelyet a tesztelés validálásának érdekében hajtanak végre. Ezt a megközelítést általában akkor alkalmazzák, ha olyan környezetben futnak, amely nem a célkörnyezet, ahol a párosításhoz a hardver szintű kód megkétszerezése szükséges a lefutáshoz. A kapcsolóhelyettesítés alternatívája a futásidejű helyettesítés, amelyben a valós funkciók kicserélésre kerülnek egy teszt végrehajtása során. Ezt a helyettesítést általában az ismert funkciómutatók újbóli hozzárendelésével vagy az objektum cseréjével hajtják végre.

A tesztpárosok különféle típusúak és különböző bonyolultságúak lehetnek:

* Dummy – A dummy, azaz próbabábu a kettős teszt legegyszerűbb formája. Megkönnyíti a kapcsolóidő-helyettesítést azáltal, hogy szükség esetén megad egy alapértelmezett visszatérési értéket.
* Stub – A stub, azaz a csonk egyszerűsített logikát ad a próbabábu számára, különféle kimeneteket biztosítva.
* Kém – A kém rögzíti és elérhetővé teszi a paraméter- és állapotinformációkat, közzéteszi azokat akik hozzáférnek a teszt kódhoz, így privát információk tesztkódjával teszi lehetővé a fejlettebb állapotérvényesítést.
* Mock – A modellt egy egyedi teszt eset határoz meg, ami a teszt-specifikus viselkedés validálását, a paraméterértékek ellenőrzését és a hívások sorrendjének eldöntését szolgálja.
* Szimulátor – A szimulátor egy átfogó elem, amely a cél képességének nagyobb pontosságú megközelítését biztosítja (a dolog megduplázódik). A szimulátor általában jelentős további fejlesztési erőfeszítéseket igényel.

Az ilyen függőség-befecskendezések következménye az, hogy a tényleges adatbázist vagy más külső hozzáférésű kódot soha nem teszteli maga a TDD-folyamat. Az ebből adódó hibák elkerülése érdekében más tesztekre is szükség van, amelyek a teszt-vezérelt kódot a fentiekben tárgyalt interfészek "valódi" megvalósításával kivitelezik. Ezek integrációs tesztek, és teljesen elkülönülnek a TDD egység tesztjeitől. Ezekből lényegesen kevesebb van és ritkábban kell futtatni őket, mint az egységteszteket. Mindazonáltal ugyanazon tesztelési keretrendszerrel is megvalósíthatóak.

A folyamatos tárolót vagy adatbázist megváltoztató integrációs teszteket mindig körültekintően kell megtervezni, figyelembe véve a fájlok vagy az adatbázis kezdeti és végső állapotát, még akkor is, ha bármelyik teszt sikertelen is lesz. Ezt gyakran a következő technikák valamilyen kombinációjával érik el:

* A TearDown módszer, melynek szerves részét képezi a rengeteg tesztelési keretrendszer.
* try...catch...finally kivételkezelési szerkezet, ott ahol ezek elérhetőek.
* Adatbázis-tranzakciók, ahol egy tranzakció atomilag tartalmazhat írási, olvasási és megfelelő törlési műveletet.
* A „pillanatfelvétel” készítése az adatbázisból bármilyen teszt futtatása előtt, és visszatérés a pillanatképhez minden tesztfutás után. Ez automatizálható egy keret, például Ant vagy NAnt által, vagy egy folyamatos integrációs rendszer, mint például CruiseControl felhasználásával is.
* Az adatbázis tiszta állapotba állítása a tesztek előtt, ahelyett, hogy utána kellene megtisztítani. Ez akkor lehet releváns, ha a takarítás megnehezítheti a teszteredmények diagnosztizálását azáltal, hogy törli az adatbázis végleges állapotát, mielőtt a részletes diagnózist el lehetne végezni.

## 13. TDD komplex rendszerekhez
A TDD nagy, kihívást jelentő rendszereken történő gyakorlása moduláris architektúrát, jól definiált komponenseket, publikus interfészeket és fegyelmezetten rétegzett rendszert igényel, a platform függetlenségének maximalizálásával. Ezek a bevált gyakorlatok növelik a tesztelhetőséget és megkönnyítik az építkezés és a tesztelés automatizálását.

### 13.1. Tesztelhetőség szerinti tervezés
A komplex rendszerekhez egy sor követelményt kielégítő architektúrára van szükség. E követelmények kulcsfontosságú részhalmaza magában foglalja a rendszer teljes és hatékony tesztelésének támogatását. A hatékony és moduláris kialakítás olyan komponenseket eredményez, amelyek osztoznak a tényleges, TDD szempontjából nélkülözhetetlen tulajdonságokon.

* A magas kohézió biztosítja, hogy minden egység biztosítsa a kapcsolódó képességeket, valamint megkönnyíti ezen képességek tesztelését.
* Az alacsony tengelykapcsoló lehetővé teszi az egyes egységek elszigetelten történő hatékony tesztelését.
* A publikált interfészek korlátozzák a komponensek hozzáférését, így a tesztek kapcsolattartó pontjaiként szolgálnak, megkönnyítve a teszt létrehozását, biztosítva a legnagyobb pontosságot a teszt és a gyártóegység konfigurálása között.

A hatékony moduláris architektúra felépítésének kulcsfontosságú módszere a forgatókönyv-modellezés, ahol sorozatdiagramok készíthetők, amelyek mindegyike egyetlen rendszerszintű végrehajtási forgatókönyvre koncentrál. A forgatókönyv modell kiváló eszköz a komponensek közötti interakció stratégiájának megalkotásához, ami egy specifikus stimulusra adott válasz. Ezen forgatókönyv-modellek mindegyike gazdag szolgáltatásokkal és funkciókkal kapcsolatos követelményeknek felelnek meg, ezek leírják, hogy egy összetevőnek miket kell biztosítania, és ez egyúttal diktálja az összetevők és a szolgáltatások egymás közötti kölcsönhatásának sorrendjét is. A forgatókönyv modellezése nagyban megkönnyítheti a TDD tesztek összeállítását egy komplex rendszer számára.

### 13.2. Tesztek vezetése nagy csapatok számára
Egy nagyobb rendszerben a rossz komponensminőség hatását az interakciók bonyolultsága csak növeli. A tesztek teljes populációjának összetettsége azonban önmagában is problémává válhat, csökkentve a lehetséges nyereségeket. Ez egyszerűen hangzik, de a kezdeti kulcsfontosságú lépés annak felismerése, hogy a tesztkód egyben fontos szoftver is, ezért azonosan szigorúan kell eljárni az elkészítés és a fenntartás folyamatában, csakúgy mint a gyártási kód esetében.

A tesztszoftver architektúrájának összetett rendszeren belüli létrehozása és kezelése ugyanolyan fontos, mint az alaptermék architektúrája. A tesztvezetők kölcsönhatásba lépnek az UUT-val, a teszt párosaival és az egység teszt keretrendszerével.