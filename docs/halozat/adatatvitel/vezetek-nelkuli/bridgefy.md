## 1. Bevezetés
A Bridgefy üzenetküldő alkalmazás. Különlegessége, hogy csupán Bluetooth kapcsolódás segítségével képes hálózatot létrehozni okostelefonok között. Készítői a mexikói Jorge Ríos, Diego García Roberto Betancourt, akikben 2014-ben fogalmazódott meg az ötlet, és 2016 környékén hoztak működőképes állapotba. A Bluetoothon kialakított hálózati technológiát mostanra más alkalmazások is használják.

Az alkalmazás segítségével a 2019-es hongkongi tüntetések résztvevői az erős kínai internetcenzúrát kikerülve tudtak egymással kommunikálni.

## 2. Jellemzői
Android és iOS operációs rendszereken érhető el. Minden üzenet csomagokra bomlik fel, melyek „intelligensen” keresik meg célpontjukat. Valamennyi adatcsomag titkosított. Az adatcsomagoknak maximális élettartama van, ugrásokra értve.
