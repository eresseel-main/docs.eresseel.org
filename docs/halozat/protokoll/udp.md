## 1. Bevezetés
A User Datagram Protocol (UDP) az internet egyik alapprotokollja. Feladata ún. kapcsolat nélküli (angolul connectionless) datagram alapú szolgáltatás biztosítása, azaz rövid, gyors üzenetek küldése. Jellemzően akkor használják, amikor a gyorsaság fontosabb a megbízhatóságnál, mert az UDP nem garantálja sem az átvitel hibamentességét sem a csomag megérkezését. Ilyen szolgáltatások például a DNS, a valós idejű multimédia átvitelek, vagy a hálózati játékok.

## 2. Az UDP csomag szerkezete

```
0      7 8     15 16    23 24    31
+--------+--------+--------+--------+
|     Forrás      |       Cél       |
|      Port       |      Port       |
+--------+--------+--------+--------+
|                 |                 |
|     Hossz       | Ellenőrző összeg|
+--------+--------+--------+--------+
|
|              Adat …
+---------------- …
```

### 2.1. A mezők leírása
**Forrás port**: A küldő (forrás) alkalmazás portjának száma 16 biten ábrázolva
**Cél port**: A vevő portjának száma.
**Hossz**: A csomag hosszát adja meg (fejléc + adatmező). (Az adatmező változó hosszúságú lehet.) A csomag minimális mérete 8 bájt, ekkor csak fejlécet tartalmaz.
**Ellenőrző összeg**: A csomag tartalmának sértetlenségét ellenőrzi. Kiszámolása nem kötelező, ekkor ezt a mezőt 0-ra kell állítani.
