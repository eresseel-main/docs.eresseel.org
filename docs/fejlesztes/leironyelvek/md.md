## 1. Bevezetés
A Markdown-t szövegek formázására használjuk a weben. Többnyire a Markdown csak normál szöveg lehet. Irányítani lehet a dokumentum megjelenítését az alábbiak szerint.

* Szavak lehetnek félkövérek, dőltek és áthúzott,
* Képek,
* Listákat,
* Feladatlisták,
* Struktúra (h1-h6),
* Kvóta,
* Linkek,
* Inline kód,
* Kód részlet hozzáadása, szintakszis kiemeléssel,
* Legördülő menu,
* Választó menu,
* Táblázat,
* Forráskód letöltés,
* Lábjegyzet.

## 2. Komponensek
### 2.1. Szavak formázása

* **Félkövér:** **Félkövér**`**félkövér**`
* *Dölt:* `*Dölt*`
* _**Félkövér és dölt:**_ `_**Félkövér és dölt**_`
* *Dőlt szövegben **félkövér** szó:* `*Dőlt szövegben **félkövér** szó:*`
* **Félkövér szövegben _dölt_ szó:** `**Félkövér szövegben _dölt_ szó**`
* ~~Áthúzott szoveg~~: `~~Áthúzott szoveg~~`

### 2.2. Képek beszúrása
* Kép beszúrása: `![Alt Text](url)`

### 2.3. Listák
Rendezett:

1. Item 1
1. Item 2
    1. Item 3a
    1. Item 3b

```
1. Item 1
1. Item 2
    1. Item 3a
    1. Item 3b
```
Rendezettlen:

* Item 1
* Item 2
    * Item 2a
    * Item 2b

```
* Item 1
* Item 2
    * Item 2a
    * Item 2b
```

### 2.4. Feladatlisták
- [x] @mentions, #refs, [links](), **formatting**, and <del>tags</del> supported
- [x] list syntax required (any unordered or ordered list supported)
- [x] this is a complete item
- [ ] this is an incomplete item

```
- [x] @mentions, #refs, [links](), **formatting**, and <del>tags</del> supported
- [x] list syntax required (any unordered or ordered list supported)
- [x] this is a complete item
- [ ] this is an incomplete item
```

### 2.5. Struktúra
```
# This is an <h1> tag
## This is an <h2> tag
###### This is an <h6> tag
```

### 2.6. Kvóta
As Kanye West said:

> We're living the future so
> the present is our past.

```
As Kanye West said:

> We're living the future so
> the present is our past.
```

### 2.7. Linkek
* [GitHub](http://github.com): `[GitHub](http://github.com)`

### 2.8. Inline kód
* Ez egy `inline kód`: \`Inline kód\`

### 2.8. Kód részlet hozzáadása, szintakszis kiemeléssel,
```bash
echo "hello"
```

```
    ```bash
    echo "hello"
    ```
```

### 2.9. Legördülő menu
??? question "Fejléc"

    ide jon a tartalom

```
??? question "Fejléc"

    ide jon a tartalom
```

### 2.10. Választó menu
=== "Material for MkDocs"

    ```
    szoveg1
    ```

=== "Insiders"

    ```
    szoveg2
    ```

```
=== "Material for MkDocs"

    ```
    szoveg1
    ```

=== "Insiders"

    ```
    szoveg2
    ```
```

### 2.11. Táblázat
First Header | Second Header
------------ | -------------
Content from cell 1 | Content from cell 2
Content in the first column | Content in the second column

```
First Header | Second Header
------------ | -------------
Content from cell 1 | Content from cell 2
Content in the first column | Content in the second column
```
### 2.12. Forráskód letöltés
[:octicons-file-code-24: Forrás][3]
[3]: https://www.eresseel.org/

```
[:octicons-file-code-24: Forrás][3]
[3]: https://www.eresseel.org/
```

### 2.13. Lábjegyzet
Itt egy lábjegyzet[^2].

[^2]:
    Itt a lábjegyzet magyarázata

```
Itt egy lábjegyzet[^2].

[^2]:
    Itt a lábjegyzet magyarázata
```
