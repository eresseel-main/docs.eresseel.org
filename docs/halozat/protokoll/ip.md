## 1. Bevezetés
Az internetprotokoll (angolul Internet Protocol, rövidítve: IP) az internet (és internetalapú) hálózat egyik alapvető szabványa (avagy protokollja). Ezen protokoll segítségével kommunikálnak egymással az internetre kötött csomópontok (számítógépek, hálózati eszközök, webkamerák stb.). A protokoll meghatározza az egymásnak küldhető üzenetek felépítését, sorrendjét stb.

## 2. Az IP-ről általában
Kialakításában fontos szerepet játszott az egyszerűség, és a robusztusság. Ezek egy olcsó technológiát eredményeztek, aminek segítségével gyorsan terjedt az IP, kiszorítva például a jóval komplexebb de igen drága konkurenst, az ATM-et. Fejlesztését még az ARPA későbbi nevén DARPA (Egyesült Államok Védelmi Minisztériumának kutatásokért felelős részlege) kezdte el, jelenleg az IETF felügyeli. 4-es verziójának (IPv4) leírását az RFC 791 tartalmazza.

## 3. Az IP jellemzői
Az IP a klasszikus OSI besorolás alapján a 3., a hálózati rétegben helyezkedik el. Csomagkapcsolt hálózatot valósít meg, azaz nem építi fel a kapcsolatot a forrás és a cél között, hanem minden egyes csomagot külön irányít (route-ol). Hibadetektálást és hibajavítást nem végez (ezeket nevezzük „megbízhatatlan” protokollnak), ezeket a funkciókat főleg a szállítási rétegben elhelyezkedő protokollokra bízza (például TCP). Ennek a kialakításnak az oka az, hogy az egyszerűségre törekedtek. Így a hibajavítás terhe főképp a forrás és a cél számítógépeknél jelentkezik, és nem terheli feleslegesen az egyébként is leterhelt hálózati útirányválasztó csomópontokat (router).

## 4. IP-cím
Az IP-ben a forrás- és célállomásokat (az úgynevezett hostokat) címekkel (IP-címek) azonosítja, amelyek 32 biten ábrázolt egész számok; azonban ezt hagyományosan négy darab 8 bites (azaz 1 byte-os, vagyis 0 és 255 közé eső), ponttal elválasztott számmal írjuk le a könnyebb olvashatóság miatt (pl: 192.168.42.1). A címek felépítése hierarchikus: a szám bal oldala (vagy szakmai nevén a legnagyobb helyiértékű bitek felől indulva) a legfelső szintet jelenti, és jobbra haladva az ez alatti szinteket kapjuk meg, például egy szolgáltatót, a szolgáltató alatti ügyfeleket, és az ügyfelek alatti egyes számítógépeket.

A teljes IP-cím két részre osztható: egy hálózati és egy hoszt azonosítókból áll. A hálózati azonosító hossza változó méretű lehet, azt a teljes cím első bitjei határozzák meg, az IP-címeket ez alapján címosztályokba soroljuk.

### 4.1. Hálózat osztályok
### 4.1.1. „A” osztály
Az 1.0.0.0 és 127.0.0.0 közötti hálózatokat foglalja magában. Itt az első szám a hálózat száma. Az „A” osztályban nem osztják ki a következő IP címeket Internetes hálózat céljára:

10.0.0.0 – belső hálózatokban lehet használni (Intranet);

127.0.0.0 – belső hálózati tesztelési címek (loopback).

Az „A” osztályban így 125 darab hálózatot lehet létrehozni, melyekben egyenként 232-2, azaz 16,777,214 darab IP címet lehet kiosztani. Nem osztható ki gépeknek a x.0.0.0 és a x.255.255.255 IP cím. Az első a hálózat címe, a második az ú.n. „broadcast” cím. Ha erre a címre van egy üzenet címezve, akkor azt a hálózatban lévő összes gép megkapja.

Az „A” osztályba tartozó hálózatok olyan nagyok lehetnek, hogy csak néhány ilyen hálózat létezik a világon (pl. IBM hálózata).

### 4.1.2. „B” osztály
A 128.0.0.0 és a 191.255.0.0 közötti hálózatokat foglalja magában. Itt az első két szám a hálózat száma. A „B” osztályban nem osztják ki a következő IP címeket Internet-es hálózat céljára:

172.16.0.0 – 172.31.0.0 – belső hálózatokban lehet használni (Intranet).

Az „B” osztályban így 16384-16, azaz 16368 darab hálózatot lehet létrehozni, melyekben egyenként 216-2, azaz 65,534 darab IP címet lehet kiosztani. Nem osztható ki gépeknek a x.y.0.0 és a x.y.255.255 IP cím az „A” osztályhoz hasonlóan.

### 4.1.3. „C” osztály
A 192.0.0.0 és a 223.255.255.0 közötti hálózatokat foglalja magában. Itt az első három szám a hálózat száma. A „C” osztályban nem osztják ki a következő IP címeket Internet-es hálózat céljára:

192.168.1.0 – 192.168.255.0 – belső hálózatokban lehet használni (Intranet).

Az „C” osztályban így 2,097,152-255, azaz 2,096,897 darab hálózatot lehet létrehozni, melyekben egyenként 28-2, azaz 254 darab IP címet lehet kiosztani. Nem osztható ki gépeknek a x.y.z.0 és a x.y.z.255 IP cím az „A” osztályhoz hasonlóan.

### 4.1.4. A „D” és „E” osztályok
A „D” és „E” osztályokban nem oszthatók ki IP címek:

* „D” osztály - a 224.0.0.0 - 239.0.0.0 közötti címek tartoznak hozzájuk, multicasting eljárás céljaira vannak fenntartva.

* „E” osztály - a 240.0.0.0 - 255.0.0.0 közötti címek tartoznak hozzájuk, melyek az Internet saját céljaira fenntartott címek.

### 4.1.5. Fenttartott osztalyok
#### 4.1.5.1. APIPA
Az APIPA segítségével a DHCP kliensek automatikusan beállítanak egy IP címet és alhálózati maszkot, ha nem érhető el DHCP szerver. A készülék a 169.254.1.0 és 169.254.254.255 közötti tartományból választja ki IP címét. Az alhálózati maszk automatikusan 255.255.0.0, az átjárócím 0.0.0.0 értéket kap.
Alapértelmezésben az APIPA protokoll engedélyezett. Az APIPA protokollt letilthatja a BRAdmin segédprogramokkal vagy egy Web Based Management (webböngésző) használatával.

Ha az APIPA protokoll nem engedélyezett, a Brother nyomtatószerver IP címe 192.0.0.192. Ezt az IP cím számot azonban könnyen megváltoztathatja, a hálózat IP címeinek megfelelő értékre.

#### 4.1.5.2. localhost
A localhost a számítógép-hálózatoknál az egyes állomások saját magukra mutató neve. (IPv4 címe: 127.0.0.1; IPv6: ::1).

Ha a web böngészőbe leírjuk a http://localhost címet, automatikusan a saját gépünkön lévő webszerver kezdőlapját próbálja lekérni. Ha nem futtatunk http szervert (Apache,Nginx), akkor "A kapcsolódás sikertelen" (Firefox), "A webhely nem érhető el" (Chrome), "Ez a lap nem jeleníthető meg" (Explorer) üzenetet kapunk.

A localhost pingelésére minden esetben lehetőség van. Amennyiben a munkaállomásban van loopback interfész, a ping minden esetben választ fog kapni. A loopback interfészt mint egyfajta ellenőrzést használhatjuk mikor ping kérést küldünk felé. Amennyiben válaszol az azt jelenti, hogy az adott operációs rendszer IP szintig megfelelően működik. Ez természetesen nem jelenti, hogy a felette álló protokollokban nem lehet hiba, de mindenképpen egy jó kiindulási alap amennyiben hálózati problémát próbálunk felderíteni.

##### 4.1.5.2.1 A localhost felhasználása
* Szoftver-fejlesztési célra.A programozó csak egy számítógépet használ, mégis tud kliens- és szerveroldali programokat is írni.
* Néhány webview-alapú asztali alkalmazás használja (Brackets kódszerkesztő node.js-sel, GitHub desktop). Ezek a programok nem igényelnek webkiszolgálót.
* Linux-os programok egymás közti kommunikációra, emiatt fontos, hogy ezt átengedjük a tűzfalon.
* A legtöbb adatbázis-kezelőhöz lehet localhost-on csatlakozni. A MySQL egyes kiadásai ehhez egy Unix domain socket-et használnak.

| Címosztály  | Cím bitjei  | Hálózati + Osztály azonosító hossza  | Hálózatok száma  | Hoszt azonosító hossza  | Kiosztható hosztok száma  |
|:-----------:|:-----------:|:------------------------------------:|:----------------:|:-----------------------:|:-------------------------:|
| A osztály   | 0...x       | 8 bit                                | 126              | 24 bit                  | 224-2 = 16777214          |
| B osztály   | 10...x      | 16 bit                               | 16382            | 16 bit                  | 216-2 = 65534             |
| C osztály   | 110...x     | 24 bit                               | 2097150          | 8 bit                   | 28-2 = 254                |

A címosztályok alkalmazása lehetővé teszi a címek optimálisabb kiosztását, azáltal, hogy egy intézmény, szervezet stb. számára egy alacsonyabb osztályú cím is kiosztható adott esetben (kevés hosztja van) így nem foglal le felesleges - fel nem használt, ki nem osztott - címeket, ha nincs rájuk szüksége.

### 4.1. Alhálózati maszk
Annak az érdekében, hogy a szervezetek a nekik kiosztott címosztályokat további alhálózatokra bonthassák, vezették be az alhálózatot jelölő maszkot. Ezzel lehetővé válik pl. egy B osztályú cím két vagy több tartományra bontása, így elkerülhető további internetcímek igénylése.

Az alhálózati maszk szintén 32 bitből áll: az IP-cím hálózati részének hosszáig csupa egyeseket tartalmaz, utána nullákkal egészül ki - így egy logikai ÉS művelettel a hoszt mindig megállapíthatja egy címről, hogy az ő hálózatában van-e.

Az IP-címekhez hasonlóan az alhálózati maszkot is byte-onként (pontozott decimális formában) szokás megadni - például 255.255.255.0. De gyakran találkozhatunk az egyszerűsített formával - például a 192.168.1.1/24 - ahol az IP-cím után elválasztva az alhálózati maszk 1-es bitjeinek a számát jelezzük.

## 5. Generációk
A hagyományos IP protokoll szerinti IP-címeket nevezzük „IPv4” címeknek is, ami a negyedik generációs (v4, version 4) internetprotokollt jelenti. Bár kezdetben jól megfelelt, az internet előre nem látott növekedése közben sok problémába ütköztek a hálózati szakemberek. Egyik ilyen az, hogy nem elégséges a kiosztott címek mennyisége. Gondot jelent, hogy nem támogatja a protokoll a mobilitást, nincs lehetőség benne korrekt titkosítás támogatására stb. Ezek megoldására jött létre az IPv6.

Aktívan létezik már az IPv6, vagyis a hatodik generációs címzésmód, melynek célja – többek között – az egyre fogyatkozó számú IPv4-címek problémájának megoldása. Az IPv6-címek 32 bit helyett 128 biten ábrázolják a címeket (ez olyan, mintha a mostani 4 helyett 16 byte-ból álló IP-címeket használnánk), ezért azokat hexadecimális formában szokás jelölni, például 3ffe:2f80:3912:1.

Az IP-cím parancssor alatti használata

az egyes számítógépek a "ping" paranccsal ellenőrzik, hogy a számítógép az adott pillanatban elérhető-e. A számítógép a másik félnek csomagokat küld, és mikor ezek visszaértek, kiszámítja, mennyi adat veszett el; ha pl. azt írja, hogy

```
küldött: 4
fogadott: 0
veszteség 100%
```
akkor nem érhető el, mert a csomagot nem küldi vissza.

## 6. IPv4 fejléc

```
  0                   1                   2                   3
  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |Version|  IHL  |Type of Service|          Total Length         |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |         Identification        |Flags|      Fragment Offset    |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |  Time to Live |    Protocol   |         Header Checksum       |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |                       Source Address                          |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |                    Destination Address                        |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |                    Options                    |    Padding    |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

```

```
                   Example Internet Datagram Header
```

* **Verzió**: Ez a mező teszi lehetővé, hogy azonos hálózatban eltérő IP verziók működhessenek, egy-egy átállás így folyamatosan mehet végbe, nem kell egyszerre az egész hálózat összes berendezését átállítani. Jelenleg évek óta tart például az IPv4-ről IPv6-ra történő migrációja az internetnek. A verzió mező értéke 4 ha IPv4, 6 amennyiben IPv6 protokollhoz tartozik a csomag. (az IPv5 egy kísérleti protokoll volt, mely végül nem terjedt el)

* **IHL**: Az IP fejléc hossza nem állandó, ez a mező hordozza a fejléc hosszára vonatkozó információt. A hosszt 32 bites szavakban adja meg, 5 és 15 között vehet fel értéket, ami minimum 20 maximum 60bájtos fejlécet jelent. (a fejléchossz az opciók változó száma miatt nem állandó)

* **Szolgálat típus (TOS)**: 8 bites mező. Eredeti tartalma három bitnyi precedencia információ, majd három jelzőbit ami gyakorlatilag prioritásnak felel meg(0: normál csomag, 7: hálózatvezérlő csomag). A három bit egyenként a késleltetésre, átbocsátásra és a megbízhatóságra vonatkozott. Egy csomag prioritizálásánál ezekkel lehetett kiválasztani, hogy a szolgáltatás mely minőségi eleme fontos a kézbesítésnél. Mivel sok eszköz figyelmen kívül hagyta egyszerűen az IHL-t, jelentését végül megváltoztatták. Ma egyszerűen szolgáltatási osztályhoz tartozást jelöl, ami a fenti információkat is magába foglalja.

* **Teljes hossz (Total Length)**: Ez a mező a csomag teljes hosszát tartalmazza bájtban megadva, beleértve a fejlécet opciókkal és az adatrészt együttesen. Maximális értéke 65535. Ez a felső korlát. Hamarosan ez igencsak szűkös lehet, de az IPv6-ban már van lehetőség ún. Jumbogram vagy Jumbo frame küldésre, mely elméleti maximális csomagmérete 1 bájt híján 4 gigabájt (!!!).

* **Azonosító (Identification)**: Erre a mezőre a célhosztnak feltétlen szüksége van ahhoz, hogy a felsőbb protokollok darabolt üzeneteit össze tudja állítani. Minden datagram szétdarabolása után az összes darab ugyanazzal az azonosítóval kerül továbbításra.

* **DF (Do not fragment!)**: Egyetlen jelzőbit, beállításával az üzenet darabolását lehet tiltani. Ilyenkor a routerek elkerülik a kiscsomagos hálózatokat.

* **MF (More Fragments)**: Szintén egyetlen bit, mely jelzi, hogy létezik még több darabja az üzenetnek. Egy darabolt üzenet minden darabjának a fejléce tartalmazza, kivéve az utolsót.

* **Darabeltolás (Fragment offset)**: Megadja, hogy a feldarabolt datagramnak a csomagban szállított része honnan kezdődik. Elengedhetetlen információ a datagram összeállításához a vételi oldalon. Az eltolást itt 8 bájtos egységekben kell értelmezni.

* **Élettartam (TTL, Time To Live)**: Ezzel a mezővel korlátozzák egy csomag hálózatban eltölthető idejét, illetve egyúttal a megoldás azt is biztosítja, hogy ne maradhassanak a hálózatban vég nélkül keringő csomagok. Kezdőértéke 255 lehet maximálisan, melyet minden router csökkent eggyel továbbításkor. Ha eléri a nullát, egyszerűen eldobja.

* **Protokoll (Protocol)**: Ez a mező jelzi, hogy a csomag milyen protokoll számára szállít. Amikor a csomagokból összeállítja az üzenetet a vételi oldal, ezalapján továbbítja a felsőbb réteg megfelelő protokolljának (ált. TCP vagy UDP). Az egyes protokollok ide vonatkozó megfeleltetést az RFC 1700 írja le.

* **Fejrész ellenőrző összeg (Header checksum)**: Csak a fejrészre vonatkozik! Az élettartam mező miatt minden alkalommal újra kell számolni.