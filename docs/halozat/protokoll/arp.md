## 1. Bevezetés
Az **ARP (Address Resolution Protocol)** egy alapvető hálózati protokoll, amelynek célja, hogy az IP-címeket MAC-címekre **(Media Access Control)** fordítsa le, hogy a hálózati eszközök helyesen tudják egymást azonosítani a helyi hálózatban (LAN).

## 2. Hogyan működik az ARP
### 2.1. ARP kérés (ARP Request) generálása
Amikor egy hálózati eszköz, például egy számítógép vagy egy router, kommunikálni szeretne egy másik eszközzel ugyanazon helyi hálózaton belül, először meg kell tudnia a cél eszköz MAC-címét. Az eszköz ismeri a cél IP-címét (például 192.168.1.10), de nem tudja, hogy melyik MAC-címhez tartozik ez az IP-cím. Ezért küld egy ARP Request üzenetet.

### 2.2. ARP kérés továbbítása (Broadcast)
Az ARP kérés egy úgynevezett broadcast üzenetként kerül továbbításra a helyi hálózaton. A broadcast azt jelenti, hogy az üzenet minden eszközhöz eljut a hálózaton belül (pl. minden eszközhöz a 192.168.1.0/24 hálózaton). Az ARP kérés tartalmazza:

    A küldő IP-címét és MAC-címét.
    A cél IP-címet, amelyhez a MAC-címet szeretné megtudni.

Az ARP kérés így néz ki:

    „Kinek a MAC-címe tartozik a 192.168.1.10 IP-címhez? Az én IP-címem 192.168.1.5, és az én MAC-címem xx:xx:xx:xx:xx
    .”

### 2.3. ARP kérés fogadása
Mivel az ARP kérés broadcast, minden eszköz megkapja azt a hálózaton. Azok az eszközök, amelyek IP-címe nem egyezik a kérésben szereplő IP-címmel, figyelmen kívül hagyják az üzenetet. Azonban az eszköz, amelynek az IP-címe megegyezik a kérésben szereplő IP-címmel (jelen esetben a 192.168.1.10), válaszolni fog az üzenetre.

### 2.4. ARP válasz (ARP Reply)
A cél eszköz (192.168.1.10) generál egy ARP Reply üzenetet, amely tartalmazza a saját MAC-címét. Ez az üzenet unicast módban kerül továbbításra, vagyis csak a kérdező eszköz (192.168.1.5) kapja meg, nem pedig az összes többi eszköz.

Az ARP válasz így néz ki:

    „Az IP-címem 192.168.1.10, és a MAC-címem yy:yy:yy:yy:yy
    .”

### 2.5. ARP válasz fogadása és ARP cache frissítése
A kérdező eszköz (192.168.1.5) megkapja az ARP választ, és elmenti az IP-címhez tartozó MAC-címet az úgynevezett ARP cache-be vagy ARP táblába. Ez egy helyi tároló, amelyben az IP- és MAC-cím párosok ideiglenesen tárolódnak, hogy a jövőbeli kommunikáció során ne kelljen ismételten ARP kérdéseket küldeni.

### 2.6. Adatküldés
Most, hogy a kérdező eszköz (192.168.1.5) tudja, melyik MAC-cím tartozik a cél IP-címhez (192.168.1.10), megkezdheti az adatküldést. A hálózati keret (például Ethernet keret) most már tartalmazni fogja a cél MAC-címet, így a csomag közvetlenül a cél eszközhöz jut.
Példa lépésenkénti ARP felfedezés

    192.168.1.5 (forrás eszköz) küld egy ARP kérést:
        „Kié a 192.168.1.10 IP-cím? Az én IP-címem 192.168.1.5, és a MAC-címem aa:bb:cc:dd:ee
        .”
    A hálózat minden eszköze megkapja a broadcast üzenetet.
    192.168.1.10 (cél eszköz) felismeri, hogy az ő IP-címéről van szó, és válaszol:
        „Az IP-címem 192.168.1.10, és a MAC-címem 11:22:33:44:55:66.”
    192.168.1.5 elmenti ezt a MAC-címet az ARP táblájába, és megkezdi az adatküldést a 192.168.1.10 eszköz felé a megadott MAC-címmel.

## 3. ARP támadások
Az ARP protokollt nem tervezték biztonságosnak, ezért különböző támadások is kihasználhatják ezt a gyengeséget. Ezek közül az egyik leggyakoribb az ARP spoofing vagy ARP mérgezés.

### 3.1. ARP Spoofing
A támadó hamis ARP válaszokat küld, amellyel meggyőzi a hálózat eszközeit arról, hogy az ő MAC-címe tartozik egy adott IP-címhez. Ezzel átirányíthatja a forgalmat, ellophatja az adatokat, vagy akár egy közbeékelődő (man-in-the-middle) támadást is végrehajthat.

### 3.2. ARP mérgezés (ARP Poisoning)
Ez a támadás az ARP táblák hamis információval való "mérgezésére" irányul, amely lehetővé teszi a támadónak, hogy az eszközök kommunikációját eltérítse vagy manipulálja.

## 4. ARP típusok
* **Normál ARP:** Ahogy fentebb említettem, az IP-címek MAC-címre fordítására szolgál.
* **Reverse ARP (RARP):** A MAC-címből IP-cím lekérdezésére szolgál, de manapság ritkán használják, mert helyette DHCP-t használnak.
* **Gratuitous ARP:** Ez egy speciális ARP üzenet, amelyet egy eszköz önmaga küld saját IP-címére vonatkozóan, hogy frissítse más eszközök ARP tábláit, vagy ellenőrizze, hogy van-e IP-cím ütközés.
* **Proxy ARP:** Egy eszköz válaszol egy másik helyett egy ARP kérésre, mintha az az eszköz lenne.

## 5. ARP cache (ARP tábla)
Az ARP táblában találhatóak az IP és MAC cím párosítások, amelyek idővel lejárnak, hogy friss adatok kerüljenek bele. Ez biztosítja, hogy ha egy eszköz MAC-címe megváltozik, akkor az új cím gyorsan átveheti a helyét.

Az ARP tehát egy nagyon fontos része a helyi hálózati kommunikációnak, hiszen segíti a hálózati eszközöket abban, hogy egymást megtalálják és adatokat küldjenek egymásnak.

## 6. ARP scan
### 6.1. Aktiv scan
#### 6.1.1. nmap
```bash
nmap -sn 192.168.1.0/24         # APR es ICMP
nmap -PR 192.168.1.0/24         # csak ARP
```

#### 6.1.2. netdiscover
```bash
netdiscover -s 30 -c 1 -F "arp or port 53" -r 192.168.2.0/24 -i eth0        # csak ARP
```

#### 6.1.3. arp-scan
```bash
arp-scan --interface=eth0 --timeout=1000 --backoff=2 --random --quiet --retry=1 --bandwidth=100000 192.168.2.0/24                  # csak ARP
```

#### 6.1.4. scapy
```python
from scapy.all import ARP, Ether, srp

def scan(ip_range):
    arp = ARP(pdst=ip_range)
    ether = Ether(dst="ff:ff:ff:ff:ff:ff")
    packet = ether/arp
    result = srp(packet, timeout=3, verbose=0)[0]

    for sent, received in result:
        print(f"IP: {received.psrc} - MAC: {received.hwsrc}")

scan("192.168.1.0/24")
```

### 6.2. Passziv scan
#### 6.2.1. tcpdump
```bash
tcpdump -i eth0 arp             # csak ARP
```

## 7. ARP Csomag Felépítése
Az ARP csomagok felépítése viszonylag egyszerű, de fontos szerepet játszik a hálózati kommunikációban. Az ARP csomagok szerkezete tartalmazza azokat az információkat, amelyek szükségesek az IP-címek és a MAC-címek közötti leképezéshez.

Az ARP csomag néhány alapvető mezőt tartalmaz, amelyek az IP-címek és MAC-címek közötti leképezéshez szükségesek. Ezek a mezők mind az ARP kérés (ARP Request), mind az ARP válasz (ARP Reply) csomagokban megtalálhatók.

1. Hardware Type (Hardver típus) [2 bájt]
    Ez a mező meghatározza, hogy milyen típusú hardvercímet használunk. Az Ethernet esetében ez az érték általában 0x0001 (ami az Ethernetet jelenti).

2. Protocol Type (Protokoll típus) [2 bájt]
    Ez a mező azt a hálózati protokollt jelöli, amelyet a cím leképezéséhez használunk. IP protokoll esetében az érték 0x0800, amely az IPv4-et jelzi.

3. Hardware Address Length (Hardver cím hossz) [1 bájt]
    Ez a mező meghatározza a hardvercím (MAC-cím) hosszát. Ethernet hálózat esetén ez az érték 6, mivel a MAC-cím 6 bájt hosszú.

4. Protocol Address Length (Protokoll cím hossz) [1 bájt]
    Ez a mező a protokollcím hosszát jelzi. IPv4 esetében ez az érték 4, mivel az IPv4-cím 4 bájt hosszú.

5. Operation (Művelet) [2 bájt]
    Ez a mező határozza meg, hogy a csomag ARP kérés (1) vagy ARP válasz (2). Ez az érték jelzi, hogy a csomag egy kérés vagy válasz.

6. Sender Hardware Address (Küldő hardver cím - MAC cím) [6 bájt]
    Ez a mező tartalmazza annak az eszköznek a MAC-címét, amely az ARP kérdést vagy választ küldi.

7. Sender Protocol Address (Küldő protokoll cím - IP cím) [4 bájt]
    Ez a mező tartalmazza annak az eszköznek az IP-címét, amely az ARP kérdést vagy választ küldi.

8. Target Hardware Address (Cél hardver cím - MAC cím) [6 bájt]
    ARP kérés esetén ezt a mezőt nullák töltenék ki (00:00:00:00:00:00), mivel még nem tudjuk a cél MAC-címet. ARP válasz esetén viszont itt lesz a cél MAC-cím.

9. Target Protocol Address (Cél protokoll cím - IP cím) [4 bájt]
    Ez a mező tartalmazza a cél IP-címet. ARP kérés esetén ez az a cím, amelyhez megpróbáljuk megtalálni a MAC-címet. ARP válasz esetén ez az a cím, amelyet a kérés során kerestünk.

### 7.1. Összegzésként egy ARP csomag mezői
| Mező név               | Méret (bájt) | Leírás                                                      |
|------------------------|--------------|-------------------------------------------------------------|
| Hardver típus          | 2 bájt       | Ethernet esetében: `0x0001`                                 |
| Protokoll típus        | 2 bájt       | IP esetében: `0x0800`                                       |
| Hardver cím hossza     | 1 bájt       | Ethernet esetében: `6`                                      |
| Protokoll cím hossza   | 1 bájt       | IPv4 esetében: `4`                                          |
| Művelet (Operation)    | 2 bájt       | `1` (kérés) vagy `2` (válasz)                               |
| Küldő MAC cím          | 6 bájt       | A küldő eszköz MAC-címe                                     |
| Küldő IP cím           | 4 bájt       | A küldő eszköz IP-címe                                      |
| Cél MAC cím            | 6 bájt       | ARP kérés esetén üres (nullák), válasz esetén a cél MAC-cím |
| Cél IP cím             | 4 bájt       | Az a cím, amelyhez keressük a MAC-címet                     |

### 7.2. Példa egy ARP kérésre
Tegyük fel, hogy egy eszköz a hálózaton ismeri a cél IP-címet (például 192.168.1.10), de nem ismeri a cél MAC-címet. Ebben az esetben egy ARP kérés csomag így nézhet ki

* Hardver típus: 0x0001 (Ethernet)
* Protokoll típus: 0x0800 (IPv4)
* Hardver cím hossza: 6 (MAC-cím 6 bájt hosszú)
* Protokoll cím hossza: 4 (IP-cím 4 bájt hosszú)
* Művelet: 1 (ARP kérés)
* Küldő MAC-cím: 00:1A:2B:3C:4D:5E
* Küldő IP-cím: 192.168.1.5
* Cél MAC-cím: 00:00:00:00:00:00 (még nem ismert)
* Cél IP-cím: 192.168.1.10

### 7.3. Példa egy ARP válaszra
Amikor a cél eszköz (192.168.1.10) válaszol az ARP kérésre, egy ARP válasz csomag így nézhet ki

* Hardver típus: 0x0001 (Ethernet)
* Protokoll típus: 0x0800 (IPv4)
* Hardver cím hossza: 6 (MAC-cím 6 bájt hosszú)
* Protokoll cím hossza: 4 (IP-cím 4 bájt hosszú)
* Művelet: 2 (ARP válasz)
* Küldő MAC-cím: 11:22:33:44:55:66
* Küldő IP-cím: 192.168.1.10
* Cél MAC-cím: 00:1A:2B:3C:4D:5E
* Cél IP-cím: 192.168.1.5

## 8. ARP parancsok linuxon
### 8.1. arp
#### 8.1.1. ARP táblázat megtekintése
Az `-n` opció megakadályozza, hogy a parancs próbáljon DNS nevet feloldani, és csak IP címeket és MAC címeket jelenít meg.

```bash
arp -n
```
#### 8.1.2. ARP bejegyzés hozzáadása
```bash
sudo arp -s [IP-cím] [MAC-cím]
```

#### 8.1.3. ARP bejegyzés törlése
```bash
sudo arp -d [IP-cím]
```

### 8.2. ip
Az `ip` parancs az újabb Linux rendszerekben preferált, mivel több funkciót is ellát a hálózati konfigurációval kapcsolatban.

#### 8.2.1. ARP táblázat megtekintése
Az `ip neighbor` parancs az ARP táblázatot is megjeleníti, és tartalmazhat kiegészítő információkat, például a kapcsolati állapotot.
```bash
ip neighbor
```

Ez a parancs részletesebb információkat nyújt az ARP tábláról. Az `-s` opció statisztikákat is tartalmaz az ARP táblázatban lévő bejegyzésekről.
```bash
ip -s neigh
```

### 8.3. netstat
Bár a `netstat` eszköz régebbi és már nem annyira elterjedt, egyes Linux disztribúciókban még mindig használható az ARP táblázat megtekintésére.
```bash
netstat -arp
```

### 8.4. ss
A `ss` parancs az újabb Linux rendszerekben használható a socketek és hálózati kapcsolatok állapotának ellenőrzésére. Általában nem tartalmazza az ARP táblát, de hasznos lehet egyes rendszerekben.
```bash
ss -a
```

## 9. Összefoglalás
Az ARP felfedezés tehát egy automatikus folyamat, amely során az eszközök megtalálják egymás MAC-címét IP-címek alapján a helyi hálózaton belül. Ez elengedhetetlen ahhoz, hogy a hálózati kommunikáció megfelelően működjön, különösen Ethernet-alapú hálózatokon.
