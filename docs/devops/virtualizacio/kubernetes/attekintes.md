## 1. Bevezetés
A Kubernetes (gyakran hivatkoznak rá a k8s rövidített formában) egy nyílt forráskódú konténer alapú alkalmazáskezelő szoftver, amellyel automatizálható az alkalmazások telepítése, skálázása és menedzselése. Eredetileg a Google mérnökei kezdték el a fejlesztését, jelenleg a Cloud Native Computing Foundation gondozza. Számos konténeres megoldást használó infrastruktúrát támogat, többek között a Dockert is.

## 2. Története
A Kubernetes elnevezés a görög κυβερνήτης szóból származik; magyarul: irányító vagy kormányos — amelyre utalás a logójában található hajókormány is. A szoftvert Joe Beda, Brendan Burns és Craig McLuckie kezdte el fejleszteni, de hamarosan csatlakozott a fejlesztéshez több Google mérnök is, mint Brian Grant és Tim Hockin. Az alkalmazás alapjaihoz a Google Borg elnevezésű projektje adta az ihletet, valamint a főbb fejlesztők közül többen dolgoztak is az említett rendszeren. A Google belső kódneve a Kubernetes fejlesztésére "Project Seven of Nine", amely utalás a Star Trek sci-fi egyik szereplőjére (a magyar változatban Hét Kilenced néven ismert), aki a Borg kollektíváról leválva egy "barátságosabb" személlyé válik. Az 1.0-ás verzió 2015. július 21-én lett kiadva. A szoftver publikálása mellett a Google a Linux Foundation konzorciummal létrehozta a Cloud Native Computing Foundation szervezetet, amely azóta is gondozza a projektet.

## 3. Architektúra
### 3.1. Kubernetes Master
A "Kubernetes Master" a klaszter fő vezérlő egysége, amely irányító funkciót lát el, az infrastruktúra menedzselése a feladata. A komponensei a következők:

* **etcd:** Egy magas rendelkezésre-állású kulcs-érték (eredeti nevén key-value data store) adatbázis, amelyben a Kubernetes állapot információi tárolódnak. Ezek az adatok reprezentálják a rendszer aktuális állapotát, amelynek segítségével a menedzselt szolgáltatások üzemeltetése és ütemezése lehetővé válik.
* **API Server:** Ez a komponens egy API amely HTTP protokoll segítségével JSON szabványú felületet biztosít a Kubernetes-hez a kliensek és komponensek számára. Feldolgozza és validálja a REST útján kapott adatokat és beállítja az objektumok státuszát az etcd adatbázisban.
* **Scheduler:** Erőforrás elosztó szoftver, ami figyeli egy újonnan létrehozott erőforrás kéréshez tartozik-e már lefoglalt erőforrás. Nyomon követi a különböző node-ok erőforrásait, tehát ismernie kell a kívánt erőforrás igényeket, a rendelkezésre álló szabad erőforrásokat és minden egyéb felhasználó által meghatározott követelményeket és szabályokat, hogy megfelelően tudja kiosztani a szükséges feladatokat a node-ok számára.
* **Controller manager:** Háttérfolyamat, amely biztosítja a Kubernetes alapvető funkcióinak a működtetését.

### 3.2. Kubernetes node
A node avagy worker node (esetleg minion néven is szokás rá hivatkozni) egy logikai egység, ami lehetővé teszi, hogy egy fizikai szerver master és worker funkciót is elláthasson. A worker node magában foglalja a Kubernetes egyik alapvető objektumait, a pod-okat, amelyek egy vagy több konténert tartalmaznak.

* **Kubelet:** A node erőforrásait monitorozza és menedzseli a pod-okat.
* **Kube-proxy:** A Kubernetes belső hálózatért felelős, hogy a kiosztott címeken elérhetőek legyenek a szolgáltatások.

## 4. Alkalmazása
A Kubernetes szoftvert elsősorban mikroszolgáltatásokon alapuló megvalósítások menedzselésére használják, mivel minden olyan képességet biztosít, amely a mikroszolgáltatási architektúrák legfontosabb problémáinak a kezeléséhez szükséges.

## 5. Példa kódok GitHub-on
[:octicons-file-code-24: Példakód][3]
[3]: https://github.com/eresseel/k8s-example