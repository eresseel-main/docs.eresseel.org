## 1. Bevezetés
Az operációs rendszer (Operating System, OS) az a szoftver, amely közvetlenül a számítógép hardverével kommunikál, és lehetővé teszi az alkalmazások futtatását és a felhasználói interakciót.

## 2. Alapvető funkciók
**Erőforráskezelés:** Az operációs rendszer kezeli a számítógép erőforrásait, például a processzort, memóriát, tárolókat és perifériákat. Ez magában foglalja az erőforrások lefoglalását, felszabadítását és megosztását a különböző felhasználók és alkalmazások között.

**Folyamatkezelés:** Az operációs rendszer feladata a folyamatok kezelése és ütemezése. Ez magában foglalja a folyamatok létrehozását, futtatását, felfüggesztését, újraindítását és leállítását. Az operációs rendszer biztosítja a folyamatok közötti kommunikációt és az erőforrásokhoz való hozzáférést.

**Memóriakezelés:** Az operációs rendszer a memóriát kezeli, beleértve a fizikai és virtuális memóriát. Ez magában foglalja a memóriaterületek lefoglalását, felszabadítását és azok közötti cserét. Az operációs rendszer gondoskodik arról, hogy az alkalmazások számára elegendő memória álljon rendelkezésre, és hatékonyan használja azt.

**Fájlrendszerek:** Az operációs rendszer biztosítja az adatok tárolását és szervezését fájlok és mappák formájában. Ez magában foglalja a fájlok létrehozását, törlését, átnevezését, másolását és mozgatását. Az operációs rendszer gondoskodik a fájlrendszer integritásáról és a hozzáférési jogosultságok kezeléséről.

**Felhasználói felület:** Az operációs rendszer biztosítja a felhasználó és a számítógép közötti kommunikációt. Ez lehet grafikus felhasználói felület (GUI), parancssor vagy egyéb felhasználói interfész.