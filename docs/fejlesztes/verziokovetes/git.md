## 1. Bevezetés
A Git egy nyílt forráskódú, elosztott verziókezelő szoftver, mely a sebességre helyezi a hangsúlyt. A fejlesztők a saját gépükön nem csak a repository-ban (tárolóban) lévő legfrissebb állapotát tárolják, hanem az egész repot.

A verziókezelői tevékenységek végrehajtása nagyon gyorsan történik, mely a Git erősségét is adja. A központi szerverrel történő hálózati kommunikáció helyett a lokális számítógépen hajtódnak végre a parancsok, így a fejlesztés offline megy végbe a workflow megváltoztatása nélkül.

Mivel minden egyes fejlesztő lényegében teljes biztonsági másolattal rendelkezik az egész projektről, ezért a szerver meghibásodásának, a tároló megsérülésének vagy bármilyen bekövetkező adatvesztésnek a kockázata sokkal kisebb, mint a központosított rendszerek által támasztott pont-hozzáférés esetében.

A Git repository minden egyes példánya – akár local, akár remote – rendelkezik a projekt teljes történetével, így egy teljes, elszigetelt fejlesztői környezetet biztosít minden fejlesztő számára, hogy szabadon kísérletezzenek új funkciók fejlesztésével mindaddig, amíg egy tiszta, publikálható verziót nem képesek előállítani.

Számos nagy volumenű projekt használja jelenleg a Gitet verziókezelő rendszerként; a legfontosabbak ezek közül: Linux-rendszermag, GNOME, Samba, X.org, Qt, One Laptop per Child core development, MediaWiki, VLC media player, Wine, Ruby on Rails és az Android platform. A Git karbantartásának felügyeletét jelenleg Junio Hamano látja el.

### 1.1. Elnevezés
A „git” nevet Linus Torvalds némi iróniával a brit angol szleng kellemetlen személyt jelentő szavából eredezteti. „Egy egoista szemétláda vagyok, és minden projektemet magam után nevezem el. Először volt a Linux és most a git.” Ez az önmarcangoló humor valójában csak fikció, hiszen Torvalds valójában külső nyomásra nevezte el a Linuxot maga után. A hivatalos Git wiki számos magyarázatot ad az elnevezésre, mint pl. „Global Information Tracker” (globális információkövető).

## 2. Jellemzők
A Git rendszerét a BitKeeper és a Monotone ihlette. Eredetileg alacsony szintű verziókövető rendszernek tervezték, amelyhez más programok adják a felhasználói felületet, pl. a Cogito vagy az StGIT. Mindazonáltal a Git projekt azóta teljes értékű revíziókövető rendszerré szélesedett, amelyet már közvetlenül is lehet használni.

Torvalds, aki jártas a nagy, szétosztott fejlesztési projektekben és a fájlrendszerekben, és gyorsan akart működő rendszert fejleszteni, ezekre az elvekre alapozta a Gitet:

* A nemlineáris fejlesztés erős támogatása. A Git támogatja a branchok készítését, összefésülésüket, és tartalmaz eszközöket a nemlineáris fejlesztési történet ábrázolására. Az alapelgondolás ugyanis az, hogy a Gitben egy változást többször kell összefésülni, mint írni.
* Elosztott fejlesztés. Hasonlóan a Darcs, BitKeeper, Mercurial, SVK, Bazaar és Monotone rendszerekhez, a Git minden fejlesztő helyi munkaváltozatában rendelkezésre bocsátja a teljes addigi fejlesztési történetet, és a változtatások másolása mindig két repository között történik. Ezeket a változtatásokat, mint külön ágakat importálják és összefésülhetőek, hasonlóan a helyben létrehozott fejlesztési branchokhoz.
 *A repositoryk publikálhatóak HTTP-n, FTP-n és rsyncen keresztül, vagy a Git protokollt használva egy socketen vagy SSH-n keresztül is. A Git tartalmaz ezen felül egy Cvs-szerveremulációt is, így a már meglévő CVS kliensek és IDE pluginok is képessé válnak a Git repository-k elérésére.
* A Subversion és az svk repositoryk közvetlenül használhatóak a git-svn híd segítségével.
* Nagy objektumok hatékony használata. Torvalds a Gitet nagyon gyors és skálázható rendszerként mutatta be. A Mozilla által végzett teljesítménytesztek megmutatták, hogy legalább egy, de bizonyos műveletek esetén akár két nagyságrendnyi sebességfölénye is van minden más verziókövető rendszerhez képest.[4]
* Kriptográfiailag hitelesített történet. A Git-történet olyan módon tárolódik, hogy a szóban forgó revízió neve függ a hozzá vezető fejlesztési történettől. Ha egyszer publikálták, akkor már nem lehetséges észrevétlenül megváltoztatni egy régi revíziót. A struktúra hasonlatos a hash-fához, de hozzáadott adatokkal az egyes csomópontokon és leveleken. (A Mercurial és a Monotone is rendelkezik ezzel a képességgel.)
* Eszközkészlet-szerű megoldás. A Git maga C-ben írt programok halmaza, számos shell-szkripttel megtűzdelve, amelyek összefűzik ezeket a C-ben írt programokat. Annak ellenére, hogy a szkriptek többségét újraírták C-ben a Microsoft Windowsra való portolás eredményeként, a struktúra megmaradt, és továbbra is könnyű az egyes komponenseket láncba rendezni az igényeknek megfelelően.
* Plugin-rendszerű összefésülési stratégiák. Az eszközkészlet-struktúra részeként a Gitnek van egy jól definiált modellje a nem teljes összefésülésre, és számos algoritmusa befejezni azt. Amennyiben ezek az algoritmusok nem alkalmasak a feladatra, úgy a felhasználót értesíti az automatikus folyamat megszakításáról, és kéri a kézi szerkesztést.
* A hulladék adatok felhalmozása, amennyiben nincs összegyűjtve. A műveletek megszakítása vagy a változtatások visszavonása használatlan kósza adatokat eredményez az adatbázisban. Ezek általában csak töredékét képezik a folyamatosan növekvő mennyiségű valós adatnak, de ezen tároló területek felszabadítása a git-gc --prune paranccsal meglehetősen lassú tud lenni.

## 3. Fogalmak
A Git hasonló egy hash-fához, azonban az egyes csomópontokon és leveleken hozzáadott adatokkal rendelkezik.

A Git célja az adott projekt menedzselése, ill. az adatok változásának nyomon követése. Ezen információk adatstruktúrákban történő tárolását repository-nak, röviden repo-nak, avagy lokális adatbázisnak nevezik.

A `working directory`, working copy vagy history az adott projektről, gyökérkönyvtáráról – amelyben a fájlok, forráskódok és mappák találhatóak – egy változatot, verziót, állapotot tartalmaz.

`Snapshot` egy adott pillanatban, időpontban a könyvtárak, fájlok aktuális állapotát, tartalmát, verzióját jelenti. A pillanatkép úgy képzelhető el, mint egy fotó a teljes kódról.

`Staged`, cached jelzővel illetik azokat a fájlokat, amelyek módosítva lettek és az állapotukról, verziójukról, tartalmukról snapshot készült.

`Commit`-nak nevezik azt a folyamatot, amely során a Git a megjelölt és a staging areaban lévő fájlokról készült snapshot-okat a .git könyvtárában, azaz a lokális adatbázisában tárolja el biztonságosan, és egy SHA-1 hash kódot generál, amellyel a snapshot-okra hivatkozik.

A Git lehetővé teszi, hogy a módosult fájlok közül egy csokorban csak azok kerüljenek eltárolásra az adatbázisában, amelyeket a fejlesztők szándékoznak. Ezért a working directory és a lokális adatbázis közé egy harmadik, index, cache, staging area szinonima nevekkel illetett átmeneti területet, pontosabban egy egyszerű fájlt alakítottak ki, amelyben információk szerepelnek arról, hogy a következő commit során mely snapshotok legyenek eltárolva. Röviden az a staging area a fájlrendszernek a commit-ra jelölt elemei snapshot-jait tartalmazza.

A working directory-t tisztának nevezik, ha a fájlokon végzett összes módosítás el van mentve a lokális Git adatbázisba, a repo-ba. Ilyenkor az utolsó commit óta nem történt változtatás az adatokon.

Branch-nek nevezik azon commit-ok összességét, melyek egy közös ágazatba lettek rendezve, és egy közös ős commit-ból erednek lineárisan egymás után fűzve.

HEAD a legutolsó commit-ra való hivatkozás az aktuális branch-ben.

A kiadott Git parancsok minden esetben az aktuális branch-re vonatkoznak. Mindig létezik egy aktív, kiválasztott aktuális branch.

## 4. Fájlok állapotai
A Git `untracked` jelzővel illeti azokat a fájlokat, mappákat, amelyekről még egyetlen snapshot sem készült (nem szerepelnek a staging area-ban, ill. nem része a lokális adatabázisnak), tehát nem követi nyomon a rajtuk végzett módosításokat. 

`Tracked` megjelöléssel szerepelnek azok az adatok, amelyek tartalmának változását a Git nyomon követi.

A tracked fájloknak 3 további állapotuk lehetséges:

* **modified:** a fájl módosult a legutóbbi commit óta, de az aktuális verziója még nincs a staging area-ban (nem készült róla snapshot), ill. a következő commitnak nem lesz része,
* **unmodified:** a fájl nem módosult a legutóbbi commit óta (tiszta),
* **staged, cached:** a fájl módosult a legutóbbi commit óta és már snapshot is készült róla (a staging area része), a következő commit során el lesz tárolva az adatbázisban.

## 5. A repository-k fajtái
Egy adott repo-nak két fajtáját különböztetik meg elhelyezkedés szerint:

* **remote:** a távoli, szerveren lévő központi repo-t jelenti, mely segítségével tartják a kapcsolatot egymással a fejlesztők; origin névvel hivatkoznak rá,
* **local:** a helyi számítógépen lévő, klónozott repo; a tényleges fejlesztés ezen történik.

Egy adott repo-nak két fajtáját különböztetik meg hozzáférhetőség szerint:

* **private:** a repository-hoz csak regisztrált és engedélyezett felhasználók férhetnek hozzá,
* **public:** a repository-hoz bárki hozzáférhet és használhatja.

## 6. Git felépítése és egy rövid ismertető
A Git 3 alapkövét és részét a `working directory`, a `cache vagy index vagy staging area` és a `local database` (.git könyvtár) jelenti.

Egy új, üres repo létrehozása után a gyökérkönyvtár .git nevezetű, rejtett mappájában helyezi el a verziókezelő a szükséges fájljait. Ezután a gyökérkönyvtárban bármilyen adatot elhelyezve a Git érzékelni fogja a változást. Például egy új, üres fájl létrehozásakor a verziókezelő jelez, hogy egy olyan fájlt talált, amely még nem része az index-nek, ezáltal úgynevezett untracked minősítéssel illeti. Ebben az esetben a rendszer nem követi nyomon a fájlon végrehajtott módosításokat, csak jelzi, hogy nincs indexelve. Ahhoz, hogy a rendszer figyelje a fájlt, s a rajta végzett változtatásokat, hozzá kell adni az index-hez. Amikor ez megtörténik, tracked minősítést kap, és egy pillanatkép készül róla (staged), mely az aktuális tartalmát jelöli. Ezután egy üres könyvtár létrehozását is untracked-ként  fogja megjeleníteni, melyet a cache-hez történő csatolás után tracked-re módosít a Git. Az első commit-olás után a fájl és könyvtár aktuális állapota (tartalma) mentésre kerül az adatbázisba.

## 7. Repository-k közötti kommunikáció
A Git-es parancsok használata során – néhányat kivéve – a verziókezelő rendszer nem hoz létre kapcsolatot a local (saját gép) és remote (központi szerver) repo között. Kommunikációra csak akkor van szükség, amikor az adatbázisok szinkronizációja történik. Ez történhet a HTTPS protokollal vagy az SSH nyilvános kulcsai segítségével. Mindkét esetben titkosított csatornán folyik a kommunikáció.

A HTTPS protokollt használva a szolgáltató weboldalán regisztrálni kell egy felhasználónév - jelszó párossal, amelyre a csatlakozás során lesz szükség. Már a klónozás alkalmával a szerverhez történő kapcsolódás alatt kérni fogja a felhasználónevet és a hozzá tartozó jelszót, s a sikeres authentikáció után megtörténik a tároló letöltése. Repo klónozása HTTPS protokoll segítségével:

```bash
git clone https://felhasznalonev@hoszt/felhasznalonev/repo-neve.git 
```

SSH protokollt használva először a sajátgépen egy privát-publikus RSA kulcspárost szükséges generálni, mely a kommunikáció alapját szolgálja. A generálás során egy passphrase-t kell megadni, amely jelszóként szolgál majd a kapcsolódás során. A generált kulcsok az operációs rendszer aktuális felhasználója főkönyvtárának a .ssh nevezetű mappájában találhatóak. A publikus kulcs .pub végződésű, s alapértelmezetten mindkettő fájlnak id_rsa a neve. A privát kulcs nem kerülhet másik fél kezébe. Az id_rsa.pub fájl tartalmát az adott Git szolgáltató oldalán történő bejelentkezés után a Beállítások menüpont alatt az SSH kulcsok-at kiválasztva fel kell venni saját kulcsként. Ezután már SSH protokollal klónozható is a saját repository a következő parancs kiadásával:

```bash
git clone git@bitbucket.org:felhasznalonev/repo-neve.git
```

A csatlakozás során a rendszer kérni fogja a passphrase-t, melyet a kulcsok generálása során kellett megadni. A helyes jelszó begépelése után a Git letölti a tárolót a remote szerverről. Egyidejűleg egy privát-publikus kulcs páros alkalmazható az összes repositoryhoz bármely szolgáltatóknál.

## 8. Branch-ek
A branch a fejlesztés egy ágát jelenti, melyet névvel szoktak ellátni. A branch-ek lehetővé teszik, hogy a fejlesztés ne csak egy szálon történjen. Az egyidejű, párhuzamos fejlesztések egymástól elszeparálva működnek. Egy új projekt létrehozásakor automatikusan létrejön egy alapértelmezett branch, a master branch.

A projekt history commit-okból épül fel, melyek egymásra hivatkoznak. Minden egyes commit-nak legalább egy, legfeljebb kettő szülője van (merge-ölés után). Branch létrehozása során ki kell jelölni egy bázis commit-ot, mely az ág kezdetét fogja jelölni, s a branch származtatása innen történik. A branch-ek egymással párhuzamosan léteznek (természetesen más névvel is ellátható, de ez az elterjedt).

A branch tulajdonképpen arra szolgál, hogy a fejlesztést különböző részekre (pl. fejlesztési fázisokra és modulokra) lehessen osztani. Ez azért fontos, mert a fejlesztők egymástól függetlenül dolgozhatnak az egyes ágakon ugyanazon repo-n. Lehetőség van branch-ek közötti váltásra és branch-ek egybefűzésére is. Az előbbi azért fontos, mert átjárhatóságot biztosít a fejlesztés különböző részei között, az utóbbi pedig a fejlesztés párhuzamosan folyó meneteit képes egybeolvasztani, ezáltal kettő vagy több eredményből egy közös keletkezik.

Az egész Git repository-nak a gyökerét az első commit jelenti. Az összes commit és branch ebből a commit-ból, vagy ennek leszármazottjaiból ered.

Minden tároló rendelkezik legalalább 1 branch-csel. A fejlesztés az ágakban történik. Branch-ek közötti váltás során a Git visszakeresi a két branch legutolsó közös commit shivatkozását (bázis vagy ős branch), s attól a commit-tól kezdve a cél branch összess módosítását leírja a fájlrendszerre.

Az ágak létrehozásakor a master branch legtöbbször a production fázist jelöli, s ebből származtatják a release, develop és hotfixes ágazatokat. A fő fejlesztés a develop ágon megy végbe, melyből modulonként 1-1 feature branch-et származtatnak. Az egyes modulok elkészülte után a develop ágba fűzik a kész feature branch-eket. A release branch-be a tesztelés alatt álló develop verziók kerülnek. Ha az adott release verzió stabil, akkor a végleges master ágba kerül befűzésre. A master branch-ben felmerült hibákat a hotfix nevezetű branch-ekkel javítják, melyeket közvetlenül a master-ből származtatnak.

## 9. Konfliktus
Konfliktus történik branch-ek merge-ölése vagy pull-olás során, ha két különböző commit egy fájl ugyanazon során történt változtatást tárolja. Ez esetben a Git nem tudja eldönteni, hogy mely módosításokat hagyja meg vagy törölje, ezért értesíti a fejlesztőt, hogy manuálisan javítsa ki a konfliktust.

Például konfliktus történik, ha 2 fejlesztő a master branch-en dolgozik, s egymás után commit-olnak úgy, hogy egy fájl egy adott sorát mind a ketten szerkesztették. Ez esetben az időben később push-oló fejlesztő kap egy figyelmeztetést (rejected), hogy a remote szerveren találhatóak olyan commit-ok az aktuális branch-ben, amelyek még nem lettek letöltve a lokális repoba, ezért még nem push-olhatóak a lokális commit-ok. A fejlesztőnek ez esetben le kell töltenie a változtatásokat (pull), s ekkor értesül majd a konfliktusról, hogy a fájl egy sorát már más is módosította. Az adott fájlt szerkeszteni kell, össze kell vágni a helyes kódsort a két commit-ból. Ezután újra kell commit-olni a változtatásokat, s majd már a feltöltés (push) művelet sikeresen végrehajtódik.

## 10. Remote és local branch-ek
Egy klónozott, sajátgépen lévő repo-ban remote és local branch-ek találhatóak. A remote branch-ek referenciák a remote repo-ban lévő branch-ekre. Amolyan "könyvjelzőként" és emlékeztetőül szolgálnak, hogy a remote repo branch-eiben milyen commit-ok szerepelnek.

A `remote` repository-kban csak a remote branch-ek léteznek, local branch-ek nem.

A `local` branch-ek a fejlesztést teszik lehetővé, s a commit-ok feltöltése a remote tárolóba a local branch-ekből történik.

A `remote` branch-ek a remote repository-nak (összes branch-ének) az állapotát tárolják, míg a local branch-ek a sajátgépen történő fejlesztést tartalmazzák.

## 11. Saját adatok beállítása
A Git commit-ok létrehozásakor eltárolja a parancsot kiadó felhasználó nevét, e-mail címét, dátumot és egyéb információkat. A következő parancsokkal a saját név és e-mail cím állíthatóak be:

```bash
git config --global user.name "Név"
git config --global user.email "e-mail"
git config --global core.editor "editor neve"
git config --global alias.addgd "checkout addgd" #alias beallitas arra hogy git addgd, ez egyenerteku az git checkout addgd-vel
```

## 12. .gitignore fájl
A .gitignore fájl tartalmazza azon, a repository-ban lévő fájlok és könyvek nevét, amelyeket a Git nem követ nyomon, nem figyeli a változásaikat, kihagyja a verzió nyomon követésből. A fájlok és mappák neveiben reguláris kifejezések is szerepelhetnek. Általában a projekt gyökérkönyvtárában helyezik el. Példa a tartalmára:

```bash
# Debug és debug mappák kihagyása
[Dd]ebug/
# az összes .txt kiterjesztésű fájl kihagyása
*.txt
# obj könyvtár kihagyása
obj/
# config.c fájl kihagyása
config.c
# lib/main.c fájlt kövesse, nem lesz a kihagyott fájlok között
!lib/main.c
```

## 13. Git parancsok
### 13.1. clone
A clone egy olyan parancs, mely segítségével egy repository lemásolható a sajátgépre.

```bash
git clone git@host:felhasznalonev/repo-neve.git repo-mas-neve
```
A Git az aktuális könyvtárban létrehoz egy repo-mas-neve nevezetű mappát, majd ebbe a könyvtárba tölti le a tároló adatait, a teljes repo-t. A repo-mas-neve paraméter elhagyható, mely esetén a repo nevével megegyező, .git kiterjesztés nélkül hozza létre a könyvtárat az aktuális mappában.

### 13.2. status
Az index tartalmának és a working directory állapotának megjelenítésére szolgál. Az untracked fájlok, ill. a modified és staged adatok listázására ad lehetőséget.

```bash
git status
```

### 13.3. add
Az add parancs a módosult adatokat az index-be helyezi, snapshot-ot készít róluk. Az összes fájl cache-be történő helyezése rekurzívan:

```bash
git add .
```

Csak a config.c fájl helyezése a cache-be:

```bash
git add config.c
```

### 13.4. commit
A módosítások, snapshot-ok eltárolására szolgál a lokális adatbázisban. Minden commit rendelkezik egy rövid szöveggel, leírással, mely arra utal, hogy milyen módosításokat tartalmaz. A commit parancs futtatása csakis az add parancs kiadása után lehetséges!

```bash
git add .
git commit -m ”a commit szövege”
```

Az add parancs elhagyható az –a kapcsolóval csakis akkor, ha az összes módosult fájl tracked minősítésű. Egyéb esetben a Git hibát ad:

```bash
git commit -am ”a commit szövege”
```
A –m kapcsoló elhagyásával az alapértelmezett szövegszerkesztő ugrik fel, és abban  írható meg a commit szövege:

```bash
git commit
```

### 13.5. checkout
A checkout parancs a branch-ek közötti váltásra szolgál. Ilyenkor a Git az aktuális branch commit-jain visszafelé haladva megkeresi a legelső, cél branch-csel ugyanarra hivatkozó commit-ot, s attól fogva a cél branch commit-jainak változtatásait leírja a fájlrendszerre. A következő példában a develop branch-ra történik váltás:

```bash
git ckeckout develop
```

### 13.6. fetch
A fetch parancs segítségével a remote repo-n lévő commit-ok importálásra kerülnek a lokális adatbázisba. A parancs csak a lokális adatbázist frissíti, a working directory-t és a staging area-t érintetlenül hagyja. A letöltött commit-ok a remote branch-ekben tárolódnak a local branch-ek helyett, ezáltal lehetőség van a szerveren történt módosításokat áttekinteni, mielőtt azok integrálva (merge) lesznek a lokális branch-be.

```bash
git fetch <remote_repo> [<branch_neve>]

git fetch origin
git fetch origin master
```

### 13.7. push
Az aktuális branch commit-jainak (snapshot-jainak) feltöltése a remote repository egy meghatározott branch-ébe. Remote repository-nak nevezzük a távoli szerveren lévő tárolót, mely segítségével tartják egymással a kapcsolatot a fejlesztők. A remote szerverre alapértelmezetten origin névvel hivatkoznak.

A fetch parancs párjaként is emlegetik, mert a fetch importálja, a push pedig exportálja a commit-okat a local és remote repository-k között.

Használata:

```bash
git push –u <remote_repo> <branch_neve>

#az aktuális branch feltöltése a remote szerver master branch-ébe:
git push –u origin master
```

A branch nevét azért szükséges megadni, mert előfordulhat, hogy jelenleg a master branch az aktuális ág, míg a módosításokat a távoli szerver production branch-ébe szeretnénk feltölteni, nem a master branch-ébe:

```bash
git checkout master #master ágba lépés
git push –u origin production #snapshot-ok feltöltése nem a master # ágba, hanem a production-be
```

### 13.8. pull
A központi szerverre felküldött változtatások, commit-ok letöltése és merge-ölése az aktuális branch-be. Az adatbázis frissítése (fetch) után a working directory-ba, a fájlrendszerre is leírja (merge) a módosításokat.

```bash
git pull [<remote_repo> <branch_neve>]

A <remote_repo> és <branch_neve> paraméterek opcionálisak. Például:
git pull
git pull origin
git pull origin master

#A git pull origin master parancs a következőt jelenti:
git fetch origin
git merge origin/master
```

A pull parancs rebase móddal is működtethető. Ez esetben az aktuális branch lineáris marad. Az adatbázis frissítése (fetch) után a working directory-ba, a fájlrendszerre is leírja (rebase) a módosításokat:

```bash
git pull --rebase [<repository> <branch_neve>]
```

### 13.9. revert
A revert parancs segítségével egy commit-tált snapshot összes módosítása visszavonható. A parancs esetében a Git nem törli a korábbi commit-ot, hanem az aktuális branch-ben létrehoz egy újat, amely visszavonja a megadott commit változtatásait. Erre az integritás megőrzése miatt van szükség.

```bash
git revert <commit_sha-1_azonosító>
```

### 13.10. merge
A merge parancs lehetővé teszi önálló fejlesztési ágak integrálását egy ágazatba. A parancs használata során annak a branch-nek a nevét kell megadni, amelyet a jelenleg aktív ágba szükséges integrálni. Tehát a merge paranccsal másik ág integrálása történik az aktuális ágba.

```bash
git merge <branch_neve>
```

### 13.11. branch
A branch parancs a branch-ek listázására, létrehozására és törlésére szolgál.
A következő parancs egy új ágat hoz létre úgy, hogy a bázisát majd az a commit képezi, amely az aktuális branch aktuális commit-ja:

```bash
git branch <branch_neve>

#Branch-ek listázása a következő módokon tehető meg:
git branch
git branch --list --all
```

Branch törlése kétféle módon lehetséges: a –d kapcsolóval biztonságosan törölhető, mert a Git jelez, ha olyan commit-okat tartalmaz, amelyek egyetlen másik ágba sem lettek merge-ölve.

```bash
git branch –d <branch_neve>
```
A –D kacsolóval oly módon törölhető egy branch, hogy lehetnek benne olyan commitok, amelyek egyetlen egy másik ágazatnak sem része.

```bash
git branch –D <branch_neve>
```

Az aktuális branch átnevezése:

```bash
git branch –m <uj_branch_nev>
```

### 13.12. diff
A diff parancs a változtatásokat mutatja meg a working directory és az index, vagy két commit, branch, esetleg fájl között.

```bash
git diff
git diff --cached
git diff <branch1> <branch2>
```

### 13.13. reset
3 kulonbozo modon lehet resetelni:

1. **soft:** Csak a HEAD-et állitja vissza eggyel, alkalmas commit üzenet megváltoztatására,
1. **mixed:** Vissza állitja a HEAD-et az előző kommitra, a tree-t vissza állitja a stage-ig,
1. **hard:** Vissza állitja a HEAD-et az előző kommitra, a tree-t vissza állitja a stage-ig, a blob-okat kimásolja a working directory-ba.

Soft reset, commit módosításra használjuk:

```bash
git reset --soft HEAD^                          #a HEAD-et visszalitja egyel korabbi allapotra
git commit --ammend -m "commit uzi javitas"     #igy lehet egy parancsba commit uzenetet javitani
```

Mixed reset, file tartalmának módosításra használjuk:

```bash
git reset --mixed HEAD^           #visszall egy korabbi commitra, es a file tartalamt visszateszi a STAGE-be file tartalmanak javitasa
git commit --ammend -m"uj uzi"    #egy lepesben commit uzenet javitasa
```

Hard reset, mindent vissza állit egy korábbi állapotra:

```bash
git reset --hard HEAD^         #vissza altunk egyel korabbi allapotra
git reset --hard HEAD@{2}      #vissza altunk egy korabbi allapotra
git reset --hard commitHash    #vissza altunk egy korabbi allapotra
git reset --hard HEAD^^        #ketto komittal visszalep az eredeti allapothoz kepest
git reset --hard v1            #visszaall arra a commitra ahol a v1 tag talalhato
```

### 13.14. tag
A Git lehetőséget ad a history fontosabb pontjainak megcímkézésére. Ezt a leggyakrabban a verziószámok megjelölésére használják.

A következő parancs ’v1.4’-es címkével látja el az aktuális commit-ot, és ’my version 1.4’ rövid leírással illeti azt.

```bash
git tag –a v1.4 –m ’my version 1.4’
```

Címkék listázása:

```bash
git tag -l
```

Adott címke részletes információi:

```bash
git show <tag_neve>
git show v1.4
```

Csak a címkék felküldése a remote repo-ra:

```bash
git push <remote> --tags
```

### 13.15. stash
A stash parancs biztosítja a working directory változtatásainak ideiglenes elmentését egy biztonságos helyre. Erre akkor lehet szükség, amikor pl. branch-váltás történik. Egyik ágról a másikra csak úgy lehet átváltani, hogy ha a working directory „tiszta”, tehát vagy nem történtek változtatások, vagy a változtatások már commit-elve lettek. A másik branchre történő átállás előtt először a módosításokat stash-elni kell – ha a fejlesztő nem szándékozik még commit-olni –, majd ezután engedélyezett a checkout parancs futtatása. Stash-elés nélkül a Git hibát ad a folyamatra.

A még nem snapshot-olt módosítások ideiglenes eltárolása:

```bash
git stash
```

A még nem snapshot-olt módosítások ideiglenes eltárolása ’message’ néven:

```bash
git stash save ”message”
```

Ideiglenesen eltárolt változtatások listázása:

```bash
git stash list
```

A stash@{1} módosítások visszaállítása a working directory-ba:

```bash
git stash apply stash@{1}
```

A legutolsó mentett stash állapot visszaállítása:

```bash
git stash pop
```

### 13.16. log
A commit logokról és a branch-ekről ad információt.

```bash
git log
git log --oneline --graph --decorate
git log --graph --parents --name-status --oneline
git log --oneline --decorate --all   #megmutatja a HEAD aktualis allapotat
git log HEAD^                        #a HEAD parentjetol irja ki a kommitokat
git log HEAD^^                       #a HEAD parent parentjere mutat
git log HEAD~10                      #A HEAD 10 parentjere mutat
```

### 13.17. rm
Fájlok törlésére szolgál a working directory-ból és az index-ből. A következő parancs kiadása után mind a fájlrendszerről, mint az index-ből eltávolításra kerül a fájl:

```bash
git rm <fajl_nev>

#Például:
git rm readme.txt
```

A --cached kapcsolóval csak az index-ből törlődik a fájl, a fájlrendszerről (working
directory) viszont nem:

```bash
git rm --cached readme.txt
```

### 13.18. mv
Fájl, könyvtár átnevezésére vagy áthelyezésére szolgál.

```bash
git mv <forras> <cel>
```

### 13.19. cat-file
A `-t` kapcsoló kiírja a megadott objektum tipusát, ami `commit, tree, blob` lehet.

A `-p` kiírja a megadott objektum tartalmát.

```bash
git cat-file -t objektumAzonosito
git cat-file -p objektumAzonosito
```

### 13.20. ls-files
Ki listázza a file-okat a stage-ben, ez nem egy konyvtar, hanem egy file, amibe beleírja az állapotot, ez gyakorlatilag a blob tartalma, ezert jön létre ez elsonek.

```bash
git ls-files -s
```

### 13.21. rev-parse
Lefordítja és megmutatja az utolsó commit hash-ét.

```bash
git rev-parse HEAD
```

### 13.22. reflog
Megmutatja a HEAD mozgásat, HEAD@{2}.

```bash
git reflog
```

### 13.23. remote
Megmutatja a távoli repo elérési útját.

```bash
git remote show origin
```

Beállítja a távoli repo url-jét.

```bash
git remote set-url origin repourl
```