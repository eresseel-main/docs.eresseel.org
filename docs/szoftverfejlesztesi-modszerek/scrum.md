## 1. Bevezetés
A „scrum” a rögbijátékban használt alakzat, amely a labda védelmét szolgálja a támadókkal szemben, akik természetesen mozognak. A Scrum agilis módszer köré komoly infrastruktúra épült ki az évek során. A módszer kézikönyve (Scrum Guide, lásd Sutherland–Schwaber, 2011) több nyelven elérhető, illetve a módszertan elsajátítása esetén többféle Scrum minősítés is szerezhető.

## 2. Alapelvek
A Scrum módszertanban egy iteratív fejlesztési ciklust sprintnek neveznek. Az adott sprint során a vizsgált időpontban még hátralevő, megvalósítandó szoftverfunkcionalitást “product backlog”-nak, vagy „sprint teendőlistának” hívják. A Scrum feltételezi, hogy nagy vonalakban ismert a sprint során elkészítendő termék terjedelme.

A Scrum kisebb fejlesztőcsapatokat (3–9 fő) tételez fel. Két szerepet különböztet meg, a „product ownert”, vagy magyarul “termékgazdát”, egy olyan felhasználót értve ez alatt, aki lényegében meg tudja fogalmazni a fejlesztendő szoftvertől elvárt funkcionalitást, illetve a „Scrum-mestert”, aki a Scrum szabályainak megértéséért és betartatásáért felelős.

A fejlesztőcsapat további tagjait a módszer nem különbözteti meg névvel. Mindenki fejlesztőnek számít függetlenül attól, hogy az adott platformhoz értő szoftverfejlesztő, üzleti elemző, tesztelő, dokumentumíró vagy a felhasználói élmény megtervezéséért felelős UX/UI designer.

A Scrum módszer sajátossága a fejlesztőcsapat önszerveződő volta. A Scrumban ennek megfelelően a csapat tagjai önként (önszerveződően) vállalják fel a sprint feladatait. Egy sprint időtartama legfeljebb egy hónap. Egy feladat időigénye pedig a sprint első részeiben legfeljebb egy munkanap.

„Napi Scrumnak”, más néven “Daily stand-up”-nak nevezett értekezleteket tartanak, ami a következő 24 óra tervezését szolgálja. A napi Scrum legfeljebb 15 percig tarthat, minden résztvevőnek három kérdésre kell válaszolnia:

* Mit sikerült elvégeznie az előző megbeszélés óta?
* Mit fog csinálni a következő megbeszélésig?
* Milyen akadályozó tényezőket lát?
* Milyen akadályozó tényezőket lát?

A sprint végén kerül sor a „sprintáttekintőre”, más néven ” sprint visszatekintésre”, vagy “retrospektivre”, ahol áttekintik az elkészült és az el nem készült részeket, megvizsgálják ennek okait és lehetséges következményeit.

Ezen a ponton lehet átbeszélni azt is, hogy miként áll a teljes termék megvalósítása. Ilyenkor felmérhetik a fejlesztőcsapat és a módszer javítását célzó intézkedéseket is. Ezek után kerülhet sor a következő sprint megtervezésére, ahol gyakran olyan funkcionalitást határoznak meg, amely a sprint eredményes befejezése esetén azonnal használható.