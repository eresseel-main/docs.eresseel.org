## 1. Bevezetés
A Java általános célú, objektumorientált programozási nyelv, amelyet a Sun Microsystems fejlesztett a ’90-es évek elejétől kezdve egészen 2009-ig, amikor a céget felvásárolta az Oracle.

A Java alkalmazásokat jellemzően bájtkód formátumra alakítják, de közvetlenül natív (gépi) kód is készíthető Java forráskódból. A bájtkód futtatása a Java virtuális géppel történik, ami vagy interpretálja a bájtkódot, vagy natív gépi kódot készít belőle, és azt futtatja az adott operációs rendszeren. Létezik közvetlenül Java bájtkódot futtató hardver is, az úgynevezett Java processzor.

A Java nyelv a szintaxisát főleg a C és a C++ nyelvektől örökölte, viszont sokkal egyszerűbb objektummodellel rendelkezik, mint a C++. A JavaScript szintaxisa és neve hasonló ugyan a Java-hoz, de a két nyelv nem áll olyan szoros rokonságban, mint azt ezekből a hasonlóságokból gondolhatnánk.

Bár a nyelv neve kezdetben Oak (tölgyfa) volt, (James Gosling, a nyelv atyja nevezte így az irodája előtt növő tölgyfáról), később kiderült, hogy ilyen elnevezésű nyelv már létezik, ezért végül Java néven vált ismertté. A Java szó a Oracle védjegye. Ennélfogva engedélye nélkül nem használható mások által kifejlesztett termékek megjelölésére; még például Java-szerű... stb. összetételben sem, mert ez a védjegyjogosult jogaiba ütközik.

## 2. Általános tudnivalók
A Java nyelvet kávézás közben találták ki, innen ered a kávéscsésze ikon. Négy fontos szempontot tartottak szem előtt, amikor a Javát kifejlesztették:

* objektumorientáltság,
* függetlenség az operációs rendszertől, amelyen fut (többé-kevésbé),
* olyan kódokat és könyvtárakat tartalmazzon, amelyek elősegítik a hálózati programozást,
* távoli gépeken is képes legyen biztonságosan futni.

### 2.1. Objektumorientáltság
A nyelv első tulajdonsága, az objektumorientáltság („OO”), a programozási stílusra és a nyelv struktúrájára utal. Az OO fontos szempontja, hogy a szoftvert „dolgok” (objektumok) alapján csoportosítja, nem az elvégzett feladatok a fő szempont. Ennek alapja, hogy az előbbi sokkal kevesebbet változik, mint az utóbbi, így az objektumok (az adatokat tartalmazó entitások) jobb alapot biztosítanak egy szoftverrendszer megtervezéséhez. A cél az volt, hogy nagy fejlesztési projekteket könnyebben lehessen kezelni, így csökken az elhibázott projektek száma.

### 2.2. A Java szerepe
A Java szoftver három igen fontos szerepet tölt be:

* mint programozási nyelv,
* mint köztes réteg (middleware),
* mint platform.

A Java legfontosabb része a Java virtuális gép (Java Virtual Machine – JVM). A JVM mindenütt jelen van (szinte mindenféle berendezés, chip és szoftvercsomag tartalmazza), így a nyelv középszintként és platformként egyaránt működik. Ugyanakkor a nyelv „platformfüggetlen” is, mert a Java virtuális gépek interpretálják a szabványos Java bájtkódot. Ez azt jelenti, hogy egy PC-n megírt Java program minimális módosítás után ugyanúgy fog futni egy javás telefonon is. Innen jön az írd meg egyszer, futtasd bárhol kifejezés. Ez jelentős költségcsökkenést eredményez, mert a kódot csak egyszer kell megírni.

### 2.3. Platformfüggetlenség (hordozhatóság)
Ez a tulajdonság azt jelenti, hogy a Java nyelven írt programok a legtöbb hardveren (megközelítőleg) ugyanúgy futnak. Ezt úgy érik el, hogy a Java fordítóprogram a forráskódot csak egy úgynevezett Java bájtkódra fordítja le. Ez azután a virtuális gépen fut, ami az illető hardver gépi kódjára fordítja. Léteznek továbbá szabványos könyvtárcsomagok, amelyek – közvetítve a kód és a gép között –, egységes funkcionalitásként teszik elérhetővé az illető hardver sajátosságait (grafika, szálak és hálózat).

Vannak olyan Java fordítóprogramok, amelyek a forráskódot natív gépi kódra fordítják le – ilyen például a GCJ –, ezzel valamelyest felgyorsítva annak futtatását. Cserébe a lefordított program elveszíti hordozhatóságát.

A Sun Microsystems licence ragaszkodik a különböző Java kivitelezések egymással való összeférhetőségéhez (felcserélhetőségéhez?). Egyes cégek, mint például a Microsoft, mégis platformfüggő sajátságokat adtak a nyelvhez, amire a Sun keményen reagált: beperelte a Microsoftot (az amerikai bíróság 20 millió dollár kártérítésre és a sajátos tulajdonságok visszavonására kötelezte a céget).

Válaszként a Microsoft kihagyta a Java rendszert a jövőbeli termékekből és Windows-változatokból. Ez azt jelenti, hogy az Internet Explorer webböngésző alapváltozataiból hiányzik a Java. Így abban az olyan weboldalak, amelyek Java-t használnak, nem fognak helyesen megjelenni. A Windows-felhasználók e problémáját megoldva a Sun és más cégek ingyenesen letölthetővé tették a JVM rendszert azon Windows-változatok számára, amelyekből a virtuális gép hiányzik.

A hordozhatóság megvalósítása technikailag nagyon bonyolult. E közben a Java esetében is sok vita volt. Az „írd meg egyszer, futtasd bárhol” szlogenből „írd meg egyszer, keress hibát mindenhol” lett. 2016-ra a hordozhatóság nem okoz tovább problémát, mivel maga a Java is nyílt szabványokra épül, pl. openGL v. Open POSIX vagy az RFC-k, a Java minden jelentősebb platformon elérhető (Linux, Unix, Windows, más rendszerek pl. AS/400).

Java 2016-ban is sikeres a szerver oldalon a servlet, a JSP és Enterprise JavaBeans, JDBC technológiákkal, integrációs lehetőségeivel, nyelvi eszközeivel, jvm nyelveivel és a nyílt forráskódú közösség tudására is építve.

### 2.4. Biztonságos távoli futtatás
A Java rendszer volt az első, amely lehetővé tette a távoli gépeken való futtatást sandboxban (homokozóban). Egy kisalkalmazás futtatható a felhasználó gépén (letöltve a Java kódot egy HTTP kiszolgálóról). A kód egy biztonságos környezetben fut, amely nem engedi meg "rossz szándékú" kód futtatását. A gyártók kiadhatnak olyan tanúsítványokat, amelyeket digitálisan aláírnak, ezzel a nevüket adva ahhoz, hogy a kisalkalmazás biztonságos. Így azok a felhasználó felügyelete alatt léphetnek ki a biztonságos környezetből.

## 3. A nyelv
### 3.1. A klasszikus „Helló Világ!” Javában

A következő egyszerű program kiírja azt, hogy „Helló Világ!” az alapértelmezett kimeneti eszközre (ami általában a konzol, de lehet egy fájl vagy bármi más is).

```java
public class HelloVilag {
    public static void main(String[] args) {
        System.out.println("Helló Világ!");
    }
}
```

### 3.2. Vezérlés
#### 3.2.1. Ciklusok
##### 3.2.1.1. while
A while egy olyan ciklus, amely a belsejében lévő utasításokat mindaddig ismétlődően végrehajtja, ameddig a megadott feltétel igaz.

```java
while (logikai kifejezés) {
    utasítás(ok)
}
```

##### 3.2.1.2. do – while
A do…while ciklus hasonlóan a while ciklushoz, addig hajtja végre a belsejében lévő utasításokat, ameddig a feltétel igaz. A while és a do…while között annyi a különbség, hogy a while az utasítások lefuttatása előtt kiértékeli feltételt, így ha már az első alkalommal a feltétel hamis, a belsejében lévő utasítások egyszer sem futnak le. A do…while ezzel ellentétben viszont csak az utasítások lefuttatása után értékeli ki a kifejezést, tehát ebben az esetben egyszer mindenképpen végrehajtja a belsejében lévő utasításokat.

```java
do {
    utasítás(ok)
} while (logikai kifejezés);
```

##### 3.2.1.3. for
A for ciklus általános alakja a következő:

```java
for (inicializáló kifejezés(ek) ; ciklusfeltétel(ek) ; léptető kifejezés(ek) ) {
    utasítás(ok)
}
```

##### 3.2.1.4. for – each
A Java 1.5 verziótól kezdve for ciklussal iterálhatóak a tömbök és a java.lang.Iteratable interface implementációi a következő szintaxissal:

```java
for (elem : tömb) {
    utasítás(ok)
}
```

Például:
```java
for (String s : new String[]{"1","2","3"}) {
    System.out.println(s);
}
```

#### 3.2.2. Feltételes utasítások
```java
if (logikai kifejezés) {
    utasítás(ok)
}

if (logikai kifejezés) {
    utasítás(ok)
} else {
    utasítás(ok)
}
```

Elegendő else if utasításokkal bármilyen komplex ha-akkor szerkezetet ki lehet építeni.
```java
if (logikai kifejezés) {
    utasítás(ok)
} else if (logikai kifejezés) {
    utasítás(ok)
} else if (logikai kifejezés) {
    utasítás(ok)
} else {
    utasítás(ok)
}
```

Az előbbi szerkezet kiváltható, ha ugyanazt az egész, felsorolható ill. string típusú (Java 1.7 óta) kifejezést kell kiértékelni több esetben is. Így kevesebb karakter felhasználásával (rövidebb a kód), átláthatóbb megvalósítást kapunk.

```java
switch (egész kifejezés) {
    case konstans egész kifejezés:
         utasítás(ok)
         break;
    …
    default:
         utasítás(ok)
         break;
}
```

#### 3.2.3. Kivételkezelés
```java
try {
    utasítás(ok)
} catch (kivételtípus) {
    utasítás(ok)
} catch (kivételtípus) {
    utasítás(ok)
} finally {
    utasítás(ok)
}
```

A Java nyelvben a kivételtípusok osztályok, és közöttük is fennáll típushierarchia. Éppen ezért, ha több catch-ágat használunk egy blokkban, akkor mindig a speciálisabb típust kell korábban feltüntetni, mert a catch ágak kiértékelése fentről lefelé halad.

Egy try után kötelező legalább egy catch vagy egy finally ágat tenni. A catch ágakból több is lehet, de egy try blokk végén csak egy finally lehet.

A könnyebb try-catch blokkot, úgy lehet, hogy a "|" (Alt Gr + W billentyű) megnyomva teszünk két kivételt egy helyre.

```java
try {
    utasítás(ok)
} catch (kivételtípus | kivételtípus) {
    utasítás(ok)
}
```

#### 3.2.4. Feltétel nélküli ugróutasítások
A Java nem támogatja a goto utasítást, mivel ennek használata spagettikódot eredményezhet. Nagyon ritkán mégis szükség van a goto-ra, a Java lehetővé tesz alternatív megoldásokat, ami a címkézhető continue és break utasítás. A goto fenntartott szó és nem használható azonosítóként.

##### 3.2.4.1. Korai kilépés a ciklusokból
A Java nyelv két utasítást is ad a ciklusból való kilépéshez. A

```java
continue;
```

utasítás megszakítja a folyamatban levő ismételgetést és egy újabbat kezd (ugyanúgy viselkedik, mint a ciklus elejére ugró goto).

Hasonlóan, a

```java
break;
```

utasítás teljesen kilép a ciklusból, és több ismételgetést nem hajt végre. A hatás ugyanaz, mint egy goto utasítás a cikluson kívülre.

A Java break és continue utasításai sokkal hatásosabbak, mint a C és C++ hasonló nevű utasításai, mert képesek egy többszintű ciklusból is kilépni (csak annyi a teendő, hogy megcímkézzük a ciklust és hozzátoldjuk a break vagy continue utasításokhoz. Ugyanezt csak goto utasítással lehet elérni C-ben és C++-ban).

Példa:
```java
kulso: while (true) {
    belso: while (true) {
        break;             // kilépés a legbelső ciklusból
        break belso;       // ugyancsak kilépés a legbelső ciklusból
        break kulso;       // kilépés a legkülső ciklusból
    }
}
```

##### 3.2.4.2. Korai kilépés az eljárásokból
A

```java
return;
```

utasítás befejez egy eljárást.

A
```java
return aErtek;
```

visszaad a hívó eljárásnak egy értéket (aErtek) is visszatéréskor.

### 3.3. Alapvető adattípusok
A nyelv egyszerű adattípusai, más szóval primitív típusai a következők:

| Változó típusa | Leírás | Példa |
|---------------|---------------------------------------------|------------------------------------------------|
| byte          | 8 bites előjeles egész                      | `byte largestByte = 127;`                      |
| short         | 16 bites előjeles egész                     | `short largestShort = 32767;`                  |
| int           | 32 bites előjeles egész                     | `int largestInteger = 2147483647;`             |
| long          | 64 bites előjeles egész                     | `long largestLong = 9223372036854775807L;`     |
| float         | 32 bites egyszeres lebegőpontosságú         | `float largestFloat = 3.4028235E38f;`          |
| double        | 64 bites kétszeres lebegőpontosságú         | `double largestDouble = 1.7976931348623157E308;`|
| char          | 16 bites Unicode-karakter                   | `char aChar = 'S';`                            |
| boolean       | logikai érték (igaz / hamis)                | `boolean aBoolean = true;`                     |

A tömb és a karakterlánc nem egyszerű típusok, hanem objektumok. A long és float változók inicializálásánál külön meg kell adni, hogy a begépelt szám melyik típusba tartozik: az egész szám literál végére L vagy f betűt írunk. A forráskódba begépelt, ilyen megjelölés nélküli egész számokat integer-ként, a lebegőpontos számokat pedig double-ként kezeli.

### 3.4. Karakterek
A Java a 16 bites Unicode kódolást (az UTF-16-ot) használja. Ez tartalmazza a szabványos ASCII-karaktereket, de ugyanakkor tartalmazza más nyelvek karakterkészletét is (pl: görög, cirill, kínai, arab stb.). A Java programok mindezeket a karakterkészleteket képesek használni, habár a legtöbb szerkesztőprogram csak a hagyományos ASCII karakterkészletet támogatja.

### 3.5. Interfészek és osztályok
A Java egyik fontos tulajdonsága, hogy lehetővé teszi interfészek létrehozását, amiket az osztályok megvalósíthatnak. Példa egy interfészre:

```java
public interface Torolheto {
   public void torol();
}
```

Ez az interfész csak annyit határoz meg, hogy minden, ami törölhető, biztosan rendelkezik `torol()` eljárással. Ennek a fogalomnak több haszna is van, mint például:

```java
public class Fred implements Torolheto {
    @Override
    public void torol() {
        //Itt kötelező megvalósítani a torol() eljárást
    }
}
```

Más osztályban lehetséges a következő:

```java
public void torolMindent(Torolheto[] lista) {
     for (int i = 0; i < lista.length; i++)
          lista[i].torol();
}
```

Léteznek továbbá jelölő interfészek, amelyeknek az implementálása nem jár metódus megvalósításával, csak egy bizonyos tulajdonsággal ruházzák fel az őket implementáló osztályt. Ilyen pl. a Serializable interfész. Az interfészek között is fennállhat öröklődési reláció. Ilyenkor ugyanúgy minden átöröklődik, ahogyan az osztályok esetében.

Egy absztraktként megjelölt osztálynak lehet nem megvalósított (csak deklarált, de nem implementált) metódusa. Példa:

```java
// Kötelező kulcsszó az abstract, ekkor az osztálynak lehet absztrakt metódusa, de nem lehet példánya
public abstract class Elvont {
    private int adat;
    public Elvont(int adat) {
        this.adat = adat;
    }
    public int getAdat() {
        return adat;
    }
    // Kötelező kulcsszó az abstract, ekkor nem szabad implementálni a metódust
    public abstract void manipulal();
}
```

Ennek értelmében az interfész egy olyan osztály, amely teljesen absztrakt, mert nem lehet megvalósított metódusa, ez alól kivételt képeznek a Java 8-ban bevezetett defult metódusok. Egy absztrakt osztálynak illetve egy interfésznek nem létezhetnek példányai, mert akkor futásidőben nem lenne adott a konkrét viselkedése (a hívott metódus törzse). Egy kivétel azonban mégis létezik, a névtelen osztály. Ez az absztrakt osztály (típus) egyszeri példányosítása, az absztrakt részének a példányosítás helyén történő kötelező megvalósításával. Példa:

```java
...
Torolheto obj = new Torolheto() {
    @Override
    public void torol() {
        //Itt kötelező megvalósítani a torol() eljárást
    }
};
...
obj.torol(); // Ez itt így már érvényes
...
```

Ha egy osztály implementál egy vagy több interfészt, akkor az az(ok) által előírt (deklarált) minden metódust kötelezően meg kell valósítania (implementálnia kell), kivéve, ha az illető osztály absztrakt. Ekkor a megörökölt, de nem implementált metódusok az osztály meg nem valósított részét képezik. Példa:

```java
public abstract class TorolhetoElvont extends Elvont implements Torolheto {
    // A következő kikommentezett rész mind megöröklődik
    /*
    private int adat;
    public Elvont(int adat) {
        this.adat = adat;
    }
    public int getAdat() {
        return adat;
    }
    // Kötelező kulcsszó az abstract, ekkor nem szabad implementálni a metódust
    public abstract void manipulal();
    public abstract void torol();
    */
    // Célszerű létrehozni konstruktort, amely lehetővé teszi a megörökölt adat inicializálását
    public TorolhetoElvont(int adat) {
        super(adat);
    }
}
```

Lehetséges az öröklési lánc megszakítása; azaz egy osztály mondhatja magáról, hogy végleges. Ekkor belőle nem lehet örököltetni. Példa:

```java
public final class Vegleges {
   ...
}
```

### 3.6. Ki- és bemenet
A következő kódrészlet bemutatja egy karakter beolvasását a felhasználótól, majd ennek kiíratását:

```java
public static void main(String[] args) throws java.io.IOException {
   char a;
   System.out.println("Üdvözlöm! Kérem írjon be egy betűt.");
   a = (char) System.in.read();
   System.out.println("A beütött betű: " + a);
}
```

### 3.7. Objektumorientált programozás megvalósítása
A származtatott osztály megadása extends segítségével:

```java
public class Alaposztaly {
   protected int i;
   public void eljaras(){
      i++;
   }
}

public class Szarmaztatott extends Alaposztaly {
   //eljaras felulirasa
   @Override
   public void eljaras() {
       i+=2;
   }
}
```

A származtatáskor az alaposztály minden elemét átvette a származtatott osztály, de az `eljaras()` metódusát felüldefiniáltuk.

Alaposztály konstruktor meghívása a `super` segítségével

```java
public class Szarmaztatott extends Alaposztaly {
   private int masikValtozo;
   //Konstruktor
   public Szarmaztatott(int i){
       //Alaposztaly konstruktoranak atadjuk a parametert
       super(i);
       masikValtozo = i;
   }
}
```

Származtatáskor kizárólag egyetlen ősosztályt adhatunk meg, viszont tetszőleges számú interfészt implementálhatunk. Így elkerülhető egyrészt a leszármazási láncban a kör (egy osztály tranzitívan önmagától származzon), illetve a megvalósított részek ütközése. C++-ban egy osztálynak több közvetlen őse is lehet, de ekkor egyértelműen jelölni kell, hogy melyik közvetlen ős melyik megvalósított részét használjuk fel, viszont a kör ott sem lehetséges.

### 3.8. Láthatósági körök
A Java nyelv négyféle láthatóságot támogat a típusok, adattagok és tagfüggvények körében. A felsorolás a legszigorúbbtól halad a legmegengedőbb felé:

* private – privát, azaz csak a definiáló osztály belsejében látható,
* nincs kulcsszó – (angolul default vagy package-private) félnyilvános, azaz a definiáló csomag belsejében látható,
* protected – védett, azaz a definiáló osztály leszármazottaiból látható,
* public – nyilvános, azaz mindenhol látható.

### 3.9. Generikus osztályok
A java 5-től kezdve megjelent az osztályok paraméterezhetővé tétele. A szintaxis hasonlít a C++ nyelv Standard Template Library (STL)-hez.

```java
List<Integer> list = new ArrayList<Integer>();
```

A java 7-től megjelent a diamond operátor, amely egyszerűsíti a szintaxist. Az értékadás jobb oldalán nem kell újból megismételni az osztály paraméterezést.

```java
Map<String,Integer> map = new HashMap<>();
```

Létezik továbbá generikus osztály paraméterezés is Javaban. Erre a következőben mutatunk példákat:

```java
List<?> l1; // bármilyen osztály lista típus, de azonos osztályok listája
List<? super Number> l2; // lehet: List<Number> vagy List<Object>.
List<? extends Number> l3;//elemei lehetnek Number-ek vagy bármilyen Number bármilyen kiterjesztése
```

### 3.10. Lambda kalkulus megvalósítása
A Java 8-cal bekerült a Java nyelvi elemei közé a Lambda kalkulus, amit funkcionális nyelvekből vettek át.

## 4. A Java története
A Java nyelv története összeforrt a Sun Microsystems Java fordítójával és virtuális gépével és az ezekhez kapcsolódó fejlesztői programcsomaggal (Java SDK vagy újabban JDK – Java Development Kit), amely a Java nyelv és ahhoz kapcsolódó szabványok standard implementációjának tekinthető. A nyílt szabványt képviselő, de zárt forráskódú Java eszközök miatt sok kritikát kapott a Sun a Free Software Foundationtól. Valószínűleg ennek is köszönhető, hogy a Sun Microsystems 2007-ben a Java SE (Standard Edition), Java ME (Micro Edition) és a Java EE (Enterprise Edition) GPL licenc alatt nyílt forráskódúvá, azaz szabad szoftverré teszi, ahogy ez már részben meg is történt a Java EE esetében, nem GPL-kompatibilis licenccel.

## 5. Példa kódok GitHub-on
[:octicons-file-code-24: Példakód][3]
[3]: https://github.com/eresseel/java-example