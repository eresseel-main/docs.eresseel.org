## 1. Bevezetés
A Vagrant egy nyílt forráskódú szoftvertermék virtuális szoftverfejlesztői környezetek létrehozására és karbantartására, pl.: VirtualBox, KVM, Hyper-V, Docker konténerek, VMware és AWS. Megpróbálja leegyszerűsíteni a szoftverkonfiguráció-kezelését a virtualizáció fejlesztési termelékenység növelése érdekében. A Vagrant Ruby nyelven íródott, de ökoszisztémája néhány más nyelven is támogatja a fejlődést.

## 2. Előzmények
A Vagrant először Mitchell Hashimoto személyes mellékprojektként indította el 2010 januárjában. A Vagrant első verziója 2010 márciusában jelent meg. 2010 októberében az Engine Yard bejelentette, hogy szponzorálni fogják a Vagrant projektet. Az első stabil verzió, a Vagrant 1.0 2012 márciusában jelent meg, pontosan két évvel az eredeti verzió megjelenése után. 2012 novemberében Mitchell megalakította a HashiCorp nevű szervezetet a Vagrant teljes munkaidős fejlesztésének támogatására; A Vagrant továbbra is megengedően licencelt ingyenes szoftverként maradt fenn. A HashiCorp jelenleg kereskedelmi kiadások létrehozásán dolgozik, és szakmai támogatást és képzést biztosít a Vagrant számára.

A Vagrant eredetileg VirtualBox-hoz kötődött, de az 1.1-es verzió támogatja az egyéb virtualizációs szoftvereket, például a VMware-t és a KVM-et, valamint az olyan szerverkörnyezeteket, mint az Amazon EC2. A Vagrant Ruby által írt projektekben is használható nyelven íródott, de más programozási nyelveken, például PHP, Python, Java, C# és JavaScript is használható. Az 1.6-os verzió óta a Vagrant natívan támogatja a Docker konténereket, amelyek bizonyos esetekben helyettesíthetik a teljesen virtualizált operációs rendszert.

## 3. Architektúra
A Vagrant a "Provisioners"-t és a "Providers"-t használja építőelemként a fejlesztői környezetek kezeléséhez. A szolgáltatók olyan eszközök, amelyek lehetővé teszik a felhasználók számára a virtuális környezetek konfigurációjának testreszabását. A Puppet és a Chef a két legszélesebb körben használt szolgáltató a Vagrant ökoszisztémában (az Ansible 2014 óta elérhető). A szolgáltatók azok a szolgáltatások, amelyeket a Vagrant használ virtuális környezetek beállítására és létrehozására. A VirtualBox, a Hyper-V és a Docker virtualizáció támogatása a Vagrant-tal érkezik, míg a VMware és az AWS bővítményeken keresztül támogatott.

A Vagrant a virtualizációs szoftver tetején ül, mint csomagolóanyag, és segíti a fejlesztőt, hogy könnyen kapcsolatba lépjen a szolgáltatókkal. Automatizálja a virtuális környezetek konfigurációját a Chef vagy Puppet segítségével, és a felhasználónak nem kell közvetlenül más virtualizációs szoftvert használnia. A gép- és szoftverkövetelmények a „Vagrantfile” nevű fájlba vannak írva, hogy végrehajtsák a szükséges lépéseket a fejlesztésre kész doboz létrehozásához. A „Box” a Vagrant-környezetek formátuma és kiterjesztése (.box), amelyet egy másik gépre másolnak, hogy ugyanazt a környezetet replikálják.

## 4. Telepítés Linuxra
```bash
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update && sudo apt install vagrant
```

## 5. Parancsok
### 5.1. Vagrant konfiguráció inicializálása
```bash
vagrant init hashicorp/bionic64
```

### 5.2. Vagrant indítása
```bash
vagrant up --provider virtualbox <GEPNEV>
```

### 5.3. Vagrant leállítása
```bash
vagrant up halt <GEPNEV>
```

### 5.4. Vagrant törlése
```bash
vagrant destroy -f <GEPNEV>
```

## 6. Példa kódok GitHub-on
[:octicons-file-code-24: Példakód][3]
[3]: https://github.com/eresseel/vagrant-example