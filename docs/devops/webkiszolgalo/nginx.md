## 1. Bevezetés
Az Nginx egy webszerver, amely fordított proxyként, terheléselosztóként, levelezőproxyként és HTTP-gyorsítótárként is használható. A szoftvert Igor Sysoev hozta létre és 2004-ben adták ki nyilvánosan. Az Nginx egy ingyenes és nyílt forráskódú szoftver, amelyet a 2-klauzulas BSD licenc feltételei szerint adnak ki. A webszerverek nagy része gyakran használja az Nginx-et terheléselosztóként.

Az azonos nevű céget 2011-ben alapították, hogy támogatást és fizetős Nginx Plus szoftvert biztosítson. 2019 márciusában a céget az F5, Inc. 670 millió dollárért felvásárolta.

## 2. Előzmények
Igor Sysoev 2002-ben kezdte el az Nginx fejlesztését. Eredetileg az Nginxet a C10k probléma igényeinek kielégítésére szolgált, megoldására fejlesztették ki, és több webhely beleértve a Rambler keresőmotort és a portált, amelyekhez naponta 500 millió kérést szolgált ki.

Az Nginx Inc.-t 2011 júliusában Sysoev és Maxim Konovalov alapította, hogy kereskedelmi termékeket és támogatást nyújtson a szoftverhez.

A cég székhelye a kaliforniai San Francisco, jogilag a Brit Virgin-szigeteken található.

2011 októberében az Nginx, Inc. 3 millió dollárt vett fel a BV Capitaltól, a Runa Capitaltól és az MSD Capitaltól.

A vállalat kereskedelmi támogatási lehetőségeket jelentett be az Nginx-et termelésben használó vállalatok számára. Az Nginx 2012 februárjában kereskedelmi támogatást kínált, 2013 augusztusában pedig fizetett Nginx Plus előfizetést. A támogatási csomagok a telepítésre, a konfigurációra, a teljesítmény javítására stb. összpontosítanak. A támogatás magában foglalja a proaktív értesítéseket a főbb változásokról, biztonsági javítások, frissítések és javítások. Az Nginx, Inc. tanácsadói szolgáltatásokat is kínál, hogy segítse az ügyfeleket az egyéni konfigurációban vagy további funkciók hozzáadásával.

2013 októberében az Nginx, Inc. egy 10 millió dolláros B sorozatú befektetési kört gyűjtött össze a New Enterprise Associates vezetésével. Ebben a körben szerepeltek a korábbi befektetők, valamint Aaron Levie vezérigazgatója és alapítója, a Box.com. 2014 decemberében az Nginx egy 20 millió dolláros sorozat B1 kört gyűjtött össze a New Enterprise Associates vezetésével az e.ventures (korábban BV Capital), a Runa Capital, az Index Ventures és az Nginx saját vezérigazgatója, Gus Robertson részvételével.

2017 szeptemberében az Nginx bejelentette egy API-kezelő eszközét, az NGINX Controller-t, amely az API-átjárójukra, az NGINX Plusra épül. 2017 októberében az Nginx, Inc. bejelentette az általánosan elérhető Nginx Amplify SaaS-t, amely felügyeleti és elemzési képességeket biztosít az Nginx számára.

2018 júniusában az Nginx, Inc. 43 millió dollárt gyűjtött össze a C sorozatú finanszírozásból a Goldman Sachs által vezetett körben, „az alkalmazások modernizálásának és a vállalatok digitális átalakulásának felgyorsítása érdekében”.

2019.március 11-én az F5 Networks 670 millió USD-ért megvásárolta az Nginx, Inc.-t.

2019.december 12-én arról számoltak be, hogy a rendőrség razziát tartott az Nginx Inc. moszkvai irodájában, és Sysoevet és Konovalovot őrizetbe vették. A razziát egy házkutatási parancs alapján hajtották végre, amely a Rambler Nginx feletti szerzői jogi követeléséhez kapcsolódik – amely azt állítja, hogy a kód birtokában van minden jog, mivel azt akkor írták, amikor Sysoev a cég alkalmazottja volt. 2019. december 16-án a Rambler 46,5 százalékát birtokló orosz állami hitelező, a Sberbank rendkívüli ülést hívott össze a Rambler igazgatótanácsával, és felkérte a Rambler vezetőségét, hogy kérjék az orosz bűnüldöző szervektől a büntetőeljárás leállítását, és kezdjenek tárgyalásokat a Rambler igazgatótanácsával. Nginx és F5 között.

2022.január 18-án bejelentették, hogy Igor Sysoev elhagyja az Nginxet és az F5-öt.

## 3. Népszerűség
2022 júniusában A W3Tech webszervereinek száma az összes webhelyen az Nginxet helyezte az első helyre 33,6%-kal. A második az Apache 31,4%-kal, a Cloudflare Server pedig a harmadik 21,6%-kal. 2022 márciusában a Netcraft becslése szerint az Nginx a legforgalmasabb webhelyek 22,01%-át szolgálta ki, az Apache pedig 23,04%-kal megelőzte. A Cloudflare 19,53%-kal és a Microsoft Internet Information Services 5,78%-kal zárta a legforgalmasabb webhelyek első négy szerverét. A Netcraft néhány egyéb statisztikája azt mutatja, hogy az Nginx megelőzi az Apache-t.

A Docker használatáról szóló 2018-as felmérés szerint az Nginx volt a leggyakrabban alkalmazott technológia a Docker konténerekben. Az OpenBSD 5.2-es verziójában (2012. november) az Nginx az OpenBSD alaprendszer részévé vált, alternatívát kínálva az Apache 1.3 rendszerhez, amelyet le akartak váltani, de később az 5.6-os verzióban (2014. november).) eltávolították az OpenBSD saját `httpd` javára.

## 4. Jellemzők
Az Nginx könnyen konfigurálható statikus webtartalom kiszolgálására vagy proxyszerverként való működésre.

Az Nginx telepíthető dinamikus tartalom kiszolgálására is a hálózaton FastCGI, SCGI WSGI szkriptkezelők, modulok segítségével alkalmazásszerverek vagy Phusion Passenger is szolgálhat, és szoftveres terheléselosztóként.

Az Nginx szálak helyett, aszinkron eseményvezérelt megközelítést használ a kérések kezelésére. Az Nginx moduláris eseményvezérelt architektúrája nagy terhelés mellett is kiszámítható teljesítményt tud nyújtani.

### 4.1. HTTP proxy és webszerver funkciók
* Több mint 10 000 egyidejű kapcsolat kezelésének képessége alacsony memóriaigény mellett (~2,5 MB 10 000 inaktív HTTP-kapcsolatonként )
* Statikus fájlok, indexfájlok kezelése és automatikus indexelés
* Fordított proxy gyorsítótárazással
* Terheléselosztás sávon belüli állapotellenőrzéssel
* TLS/SSL és SNI, OCSP OpenSSL-en keresztül
* FastCGI, SCGI, uWSGI támogatás gyorsítótárazással
* gRPC támogatás 2018 márciusa óta, 1.13.10 verzió.
* Név és IP-cím alapú virtuális szerverek
* IPv6 kompatibilis
* WebSockets 1.3.13 óta, beleértve a fordított proxy szerepét és a WebSocket alkalmazások terheléselosztását.
* HTTP/1.1 frissítés (101 kapcsolóprotokoll), HTTP/2 protokoll támogatás
* URL átírása és átirányítása

### 4.2. Mail proxy funkciók
* TLS/SSL támogatás
* STARTTLS támogatás
* SMTP, POP3 és IMAP proxy
* Hitelesítést igényel külső HTTP szerverrel vagy hitelesítési szkripttel

Az egyéb funkciók közé tartozik a végrehajtható és konfiguráció frissítése az ügyfélkapcsolatok elvesztése nélkül,  és a modul alapú architektúra, amely harmadik féltől származó modultámogatással is rendelkezik.

A fizetős Plus termék további funkciókat is tartalmaz, például fejlett terheléselosztást és hozzáférést a teljesítményfigyeléshez szükséges mérőszámok bővített csomagjához.

## 5. Nginx vs Nginx Plus
Az Nginxnek két verziója létezik: az Nginx nyílt forráskódú és az Nginx Plus.

Az Nginx Open Source egy ingyenes és nyílt forráskódú szoftver.

Az Nginx Plus előfizetéses modellként kerül értékesítésre. Az Nginx Open Source mellett olyan funkciókat is kínál, mint az aktív állapotellenőrzés, a cookie-kon alapuló munkamenet-megmaradás, a DNS-szolgáltatás-felderítés integráció, a Cache Purging API, az AppDynamic, a Datalog, a Dynatrace New Relic beépülő modulok, az Active-Active HA konfigurációval. szinkronizálás, Key-Value Store, menet közben, nulla állásidő-frissítéssel az upstream konfigurációkkal, valamint kulcsérték-tárolók az Nginx Plus API és a webalkalmazások tűzfalának (WAF) dinamikus moduljával.