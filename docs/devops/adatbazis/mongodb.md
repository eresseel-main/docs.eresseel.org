## 1. Bevezetés
A MongoDB nyílt forráskódú dokumentumorientált adatbázis szoftver, amelyet a MongoDB inc. (korábbi nevén 10gen) fejleszt. A NoSQL adatbázisszerverek közé tartozik. A dokumentumokat JSON-szerű formátumban tárolja (BSON). A MongoDB-t olyan nagyobb felhasználók is használják, mint az MTV Networks, a Craigslist és a FourSquare. A legnépszerűbb NoSQL adatbázis szoftver. 

## 2. Története
A MongoDB fejlesztését 2007-ben kezdték a 10gen-nél, amikor a cég egy platform szolgáltatás fejlesztésén dolgozott. 2009-ben a szoftvert nyílt forráskódúvá tették önálló termékként.

Az 1.4-es verzió 2010 márciusi kiadásával a fejlesztő csapat éles üzemre késznek tartja a terméket.

## 3. Főbb tulajdonságok
Egy rövid összefoglaló a MongoDB főbb tulajdonságairól:

* Ad hoc lekérdezések
    * A MongoDB támogatja a keresést mező alapján, érték-tartomány alapján vagy reguláris kifejezéssel. A lekérdezések visszaadhatják a dokumentum egy meghatározott részét és tartalmazhatnak javascript funkciókat is.
* Indexek
    * A MongoDB lehetővé teszi, hogy a dokumentum bármelyik mezője alapján indexet készítsünk.
* Replikáció
    * A MongoDB támogatja a master-slave replikációt. Ebben az esetben a master hajthat végre írás műveleteket, a slave szerverek másolják az adatokat, olvasásra biztonsági mentésre használhatóak. A slave adatbázisok képesek új master adatbázist választani, ha a master meghibásodik.
* Terheléselosztás
    * A MongoDB horizontálisan skálázható sharding használatával.[6] A fejlesztőnek kell shard kulcsot választania, amely meghatározza, hogyan lesz elosztva gyűjtemény adathalmaza.
* Fájl tároló
    * A MongoDB-t lehet elosztott fájlrendszerként is használni, ezt GridFS-nek hívják.[7]
* Aggregáció
    * MapReduce algoritmusok használhatóak kötegelt feldolgozáshoz és aggregációra. Ezeket a programokat javascript nyelven kell megírni.