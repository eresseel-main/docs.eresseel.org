## 1. Bevezetés
Az extrém programozás (XP) egy szoftverfejlesztési módszertan, amelynek célja a szoftver minőségének fejlesztése és a változó vevői igényekhez való reagálás javítása. Az agilis szoftverfejlesztés egyik típusaként rövid fejlesztési ciklusokban támogatja a gyakori "kiadásokat", amelyek célja a termelékenység javítása és az ellenőrző pontok bevezetése, ahol új ügyféligényeket lehet elfogadni.

Az extrém programozás további elemei: páros programozás vagy széles körű kódáttekintés, az összes kódegység tesztelése, a szolgáltatások programozásának elkerülése, amíg azok valóban nem szükségesek, lapos menedzsment szerkezet, kódegyszerűség és érthetőség, az ügyfél igényeinek időbeli változására számító, a probléma jobb megértése, valamint a vevő és a programozók közötti gyakori kommunikáció. A módszertan neve onnan jött, hogy a hagyományos szoftverfejlesztési gyakorlatok jótékony elemeit "extrém" szintre kell vinni. Például a kódáttekintést előnyös gyakorlatnak tekintik. Ezt extrém szinten véve a kód folyamatosan áttekinthető, azaz ez a páros programozás gyakorlata.

## 2. Története
Kent Beck fejlesztette ki az extrém programozást a Chrysler átfogó kompenzációs rendszer (C3) bérszámfejtési projektje során. Beck 1996 márciusában vált a C3 projektvezetőjévé. Elkezdte finomítani a projektben alkalmazott fejlesztési módszertant, és könyvet írt a módszertanról (Extreme Programming Explained, megjelent 1999. október). A Chrysler 2000 februárjában, hét év után, amikor a Daimler-Benz megvásárolta a társaságot, visszavonta a C3 projektet.

Sok extrém programozási gyakorlat létezik egy ideje; a módszertan a jól bevált gyakorlatokat extrém szintre emeli. Például az „először teszt fejlesztésben, a tesztek megtervezése és megírása megelőz minden egyes programsor hozzáadást” gyakorlatot már a NASA „Mercury-programján, az 1960-as évek elején alkalmazták. A teljes fejlesztési idő lerövidítése érdekében néhány hivatalos tesztdokumentumot (például elfogadási teszteléshez) fejlesztettek ki a tesztelésre kész szoftverrel párhuzamosan (vagy röviddel azelőtt). A NASA független tesztcsoportja formai követelmények és logikai korlátok alapján írhatja ki a tesztelési eljárásokat, mielőtt a programozók megírják a szoftvert és integrálnák azt a hardverrel. Az XP ezt a koncepciót extrém szintre állítja, automatizált teszteket (néha a szoftvermodulokon belül) írva, amelyek még a szoftverkódolás inkább kisebb részeinek működését is validálják, mint a nagyobb szolgáltatások tesztelését.

### 2.1. Eredet
Két nagyobb hatás befolyásolta a szoftverfejlesztést az 1990-es években:

* Belsőleg, az objektum-orientált programozás felváltotta a procedurális programozást, a néhány fejlesztő által kedvelt programozási paradigmát.
* Külsőleg az internet és a dot-com fellendülése hangsúlyozta a piacra lépést és a vállalati növekedést, mint versenyképes üzleti tényezőket.

A gyorsan változó követelmények rövidebb termék-életciklusokat igényeltek, és gyakran ütköztek a szoftverfejlesztés hagyományos módszereivel.

A Chrysler átfogó kompenzációs rendszer (C3) azzal indult, hogy meghatározzák az objektumtechnológiák felhasználásának legjobb módját. A kutatás tárgyaként a Chrysler bérszámfejtési rendszereit használva, a Smalltalk nyelvvel, az adat-hozzáférési réteg pedig a GemStone volt. Chrysler behozta Kent Becket aki egy kiemelkedő Smalltalk-gyakorló volt, hogy végezzen teljesítményhangolást a rendszeren, de szerepe kibővült, mivel a fejlesztési folyamat számos problémájára rámutatott. Megragadta a lehetőséget, hogy javasoljon és végrehajtson néhány változtatást a fejlesztési gyakorlatban - gyakori munkatársával, Ward Cunninghammel végzett munkája alapján, Beck leírja a módszerek korai koncepcióját:

„*Amikor először kértek fel, hogy vezessek egy csapatot, megkértem őket, hogy olyan apró dolgokat tegyenek meg, amikről azt hittem, hogy érzékenyek, mint a tesztelés, és az áttekintés. Másodszor sokkal többről volt szó. Azt gondoltam: "A fene egye meg a torpedókat, legalább ez egy jó cikk lesz." és megkértem a csapatot, hogy vegyenek előre minden olyan dolgot, amikről úgy gondoltam, hogy alapvetőek, és hagyjanak ki minden mást.*”


Beck meghívta Ron Jeffriest a projektbe, hogy segítsen fejleszteni és finomítani ezeket a módszereket. Jeffries ezt követően edzőként járt el, hogy a C3 csapatba belenevelje a gyakorlatokat.

Az XP mögött meghúzódó alapelvekkel és gyakorlatokkal kapcsolatos információkat az eredeti wikiről, Cunningham WikiWikiWebjéről folytatott megbeszélések útján terjesztették a szélesebb világban. Különböző közreműködők megvitatták és kibővítették az ötleteket, ami néhány spin-off módszertan eredményezett (lásd agilis szoftverfejlesztés). Az XP fogalmakat évekig magyarázták egy hipertext rendszertérkép segítségével, az XP webhelyén 1999 körül.

Beck egy könyvsorozatot készített az XP-ről, kezdve saját Extreme Programming Explaineddel (1999, ISBN 0-201-61641-6), ötleteit sokkal nagyobb közönségnek terjesztve. A sorozat szerzői különféle szempontokat tekintettek át, amikor megvizsgálták az XP-t és gyakorlatait. A sorozat egy olyan könyvet tartalmazott, amely kritikus volt a gyakorlatokkal szemben.

### 2.2. Jelenlegi helyzet
Az XP az 1990-es évek végén és a 2000-es évek elején jelentős érdeklődést váltott ki a szoftverközösségek körében, mivel az elfogadását számos, az eredetétől radikálisan eltérő környezetben jelent meg.

Az eredeti gyakorlatok által megkövetelt magas fegyelem gyakran az út mentén haladt, és e gyakorlatok némelyikét, például a túl merevnek ítélt gyakorlatokat, az egyes helyszíneken elavulttá nyilvánították, gyengítették, vagy akár befejezetlenül hagyták. Például egy adott projekt nap végi integrációs tesztet, hét végi ütemtervre lehet változtatni, vagy egyszerűen egy közösen elfogadott időpontban történő tesztelésre lehet redukálni. Egy ilyen nyugodtabb ütemtervvel az emberek kevésbé érzik, hogy mesterséges tüskéket készítenek, csak azért, hogy a nap végi teszteket teljesíthessék. Egy kevésbé szigorú ütemterv lehetővé teszi komplex funkciók fejlesztését egy néhány napos periódus alatt.

Eközben más agilis fejlesztési gyakorlatok nem álltak meg, és 2019-re az XP tovább fejlődik, és a gyakorlati tapasztalatokból további tanulságok elsajátítását más gyakorlatok felhasználásával valósítja meg. Az Extreme Programming Explained második kiadásában (2004. november), öt évvel az első kiadás után, Beck további értékeket és gyakorlatokat adott hozzá, és megkülönböztette az elsődleges és a következményes gyakorlatokat.

A fenntartható szoftverfejlesztés elmélete megmagyarázza, hogy miért sikeresek az extrém programozási csapatok, a csapatok szétszakítása ellenére is.

## 3. Koncepció
### 3.1. Célok
Az Extreme Programming Explained az extrém programozást olyan szoftverfejlesztési tudományágként írja le, amely arra szervezi az embereket, hogy termelékenyebben készítsenek magasabb színvonalú szoftvereket.

Az XP megkísérli csökkenteni a követelmények változásainak költségeit, mivel nem egy hosszú, hanem több rövid fejlesztési ciklust használ. Ebben a doktrínában a változások a szoftverfejlesztési projektek természetes, elkerülhetetlen és kívánatos szempontjai, és ezeket a stabil követelmények meghatározásának megkísérlése helyett meg kell tervezni.

Az extrém programozás számos alapvető értéket, alapelvet és gyakorlatot vezet be az agilis programozási keret mellett.

### 3.2. Tevékenységek
Az XP négy alapvető tevékenységet ír le, amelyeket a szoftverfejlesztési folyamat során hajtanak végre: kódolás, tesztelés, meghallgatás és tervezés. E tevékenységek mindegyikét alább ismertetjük.

### 3.3. Kódolás
Az XP támogatói úgy érvelnek, hogy a rendszerfejlesztési folyamat egyetlen igazán fontos terméke a kód - szoftver utasítások, amelyeket a számítógép értelmezhet. Kód nélkül nincs működő termék.

A kódolás felhasználható a legmegfelelőbb megoldás kitalálására. A kódolás segíthet a programozási problémákkal kapcsolatos gondolatok közlésében. Ha egy programozó összetett programozási problémával foglalkozik, vagy nehezen tudja megmagyarázni a megoldást a programozó társak számára, akkor egyszerűsített módon kódolhatja, és a kód segítségével mutathatja meg, hogy mit gondol. A kód, ezen álláspontjának támogatói szerint, mindig világos és tömör, és egynél több módon nem értelmezhető. Más programozók visszajelzést adhatnak erről a kódról, gondolataik kódolásával is.

### 3.4. Tesztelés
A tesztelés az extrém programozás központi eleme. Az extrém programozás megközelítése az, hogy ha egy kis tesztelés kiküszöböl néhány hibát, akkor sok tesztelés kiküszöböl még több más hibát.

* Az egységtesztek meghatározzák, hogy egy adott szolgáltatás a kívánt módon működik-e. A programozók annyi automatizált tesztet írnak, amennyiről csak azt gondolják,hogy "megtörheti" a kódot. Ha az összes teszt sikeresen lefut, akkor a kódolás befejeződött. Minden megírt kóddarabot tesztelünk, mielőtt továbbmegyünk a következő funkcióra.
* Az elfogadási tesztek igazolják, hogy a programozók által megértett követelmények megfelelnek-e az ügyfél tényleges követelményeinek.

A rendszerszintű integrációs tesztelést először arra ösztönözték, mint egy nap végi tevékenységet az összeférhetetlen interfészek korai felismerésére, az újracsatlakozásra, mielőtt az egyes szakaszok nagymértékben eltérnek a koherens funkcionalitástól. A rendszerszintű integrációs tesztelést azonban lecsökkentették heti vagy még ritkább végrehajtásra, a rendszer összeköttetéseinek stabilitásától függően.

### 3.5. Meghallgatás
A programozóknak meg kell hallgatniuk, hogy mi szükséges az ügyfeleknek a rendszerhez, milyen üzleti logikára van szükség. Meg kell értenie ezeket a szükségleteket ahhoz, hogy visszajelzést adjon az ügyfeleknek a probléma megoldásának vagy meg nem oldásának technikai szempontjairól. Az ügyfél és a programozó közötti kommunikációt a tervezési játékban tovább tárgyalják.

### 3.6. Tervezés
Az egyszerűség szempontjából természetesen azt lehet mondani, hogy a rendszerfejlesztéshez nem kell több, mint a kódolás, tesztelés és figyelés. Ha ezeket a tevékenységeket jól hajtják végre, akkor az eredménynek mindig egy működő rendszernek kell lennie. A gyakorlatban ez nem fog működni. Tervezés nélkül is hosszú utat lehet elérni, de egy adott időpontban elakadhat. A rendszer túlságosan összetetté válik, és a rendszeren belüli függőségek már nem egyértelműek. Ezt el lehet kerülni, ha olyan tervezési struktúrát hoz létre, amely a rendszer logikáját szervezi. A jó tervezés elkerüli a rendszeren belüli sok függőséget; ez azt jelenti, hogy a rendszer egyik részének megváltoztatása nem érinti a rendszer többi részét.

### 3.7. Értékek
Az extrém programozás kezdetben négy értéket ismert el 1999-ben: kommunikáció, egyszerűség, visszajelzés és bátorság. Új értéket, a tiszteletet adta az Extreme Programming Explained második kiadása. Ezt az öt értéket az alábbiakban ismertetjük.

### 3.8. Kommunikáció
A szoftver rendszerek felépítéséhez le kell kommunikálni a rendszerkövetelményeket a rendszer fejlesztőivel. A formális szoftverfejlesztési módszertanban ezt a feladatot dokumentációval hajtják végre. Az extrém programozási technikák olyan módszereknek tekinthetők, amelyek segítségével gyorsan fejleszthetők és terjeszthetők az intézményi ismeretek egy fejlesztői csapat tagjai között. A cél az, hogy minden fejlesztő láthassa a rendszert, amely megegyezik a rendszer felhasználóinak nézetével. Ennek érdekében az extrém programozás az egyszerű tervezést, a közös metaforákat, a felhasználók és a programozók együttműködését, a gyakori verbális kommunikációt és a visszajelzést támogatja.

### 3.9. Egyszerűség
Az extrém programozás a legegyszerűbb megoldásra ösztönöz. További funkciók később hozzáadhatók. Ezen megközelítés és a hagyományosabb rendszerfejlesztési módszerek közötti különbség az, hogy a holnap, a jövő héten vagy a jövő hónap helyett a mai igények megtervezésére és kódolására összpontosítunk. Ezt néha úgy foglalják össze, hogy a "Nem lesz szükséged rá" (You aren't gonna need it [YAGNI]) megközelítés. Az XP támogatói elismerik azt a hátrányt, hogy holnap több erőfeszítést igényelhet a rendszer megváltoztatása; állításuk szerint ezt inkább ellensúlyozza az az előnye, hogy nem fektetünk be olyan lehetséges jövőbeli követelményekbe, amelyek megváltozhatnak, mielőtt azok relevánssá válnak. A bizonytalan jövőbeli követelmények kódolása és megtervezése azzal a kockázattal jár, hogy forrásokat költenek valamire, amelyre esetleg nincs szükség, miközben esetleg késleltetik a kritikus feladatokat. A "kommunikáció" értékéhez kapcsolódóan a tervezés és a kódolás egyszerűségének javítania kell a kommunikáció minőségét. Egy egyszerű kialakítás és egy nagyon egyszerű kód a csapatban lévő legtöbb programozó számára könnyen érthető.

### 3.10. Visszajelzés
Az extrém programozásban a visszajelzés a rendszerfejlesztés különböző dimenzióival kapcsolatos:

* **Visszajelzés a rendszerről:** egységtesztek írásával, vagy időszakos integrációs tesztek futtatásával a programozók közvetlen visszajelzéseket kapnak a rendszer állapotáról a változások végrehajtása után.
* **Visszajelzés az ügyféltől:** A funkcionális teszteket (más néven elfogadási teszteket ) az ügyfél és a tesztelők írják. Konkrét visszajelzést kapnak rendszerük jelenlegi állapotáról. Ezt a felülvizsgálatot két-három hetente egyszer tervezik be, így az ügyfél könnyen irányíthatja a fejlesztést.
* **Visszajelzés a csapattól:** Amikor az ügyfelek új követelményekkel állnak elő a tervezési játékban, a csapat közvetlenül becsüli meg a végrehajtáshoz szükséges időt.

A visszajelzés szorosan kapcsolódik a kommunikációhoz és az egyszerűséghez. A rendszer hibáival könnyen kommunikálhat egy egységteszt írásával, amely bizonyítja, hogy egy bizonyos kóddarab hibásan fog működni. A rendszer közvetlen visszajelzése arra készteti a programozókat, hogy újrakódolják ezt a részt. Az ügyfél rendszeresen tesztelheti a rendszert a funkcionális követelményeknek, azaz felhasználói történeteknek megfelelően. Kent Becket idézve: "Az optimizmus a programozás foglalkozási veszélye. A visszajelzés a kezelés."

### 3.11. Bátorság
Számos gyakorlat testesíti meg a bátorságot. Az egyik a parancsolat, hogy mindig tervezzünk és a mai napra kódoljunk, és ne holnapra. Ez egy olyan erőfeszítés, amely elkerüli a tervezés megbotlását, és bármi más végrehajtásához szükséges erőfeszítést. A bátorság lehetővé teszi a fejlesztőknek, hogy jól érezzék magukat, ha szükségük van a kód refaktorálására. Ez azt jelenti, hogy felül kell vizsgálni a meglévő rendszert és módosítani kell annak érdekében, hogy a jövőbeni változások könnyebben végrehajthatók legyenek. A bátorság másik példája az, ha tudjuk, mikor dobjuk ki a kódot: bátorság az elavult forráskód eltávolítása, függetlenül attól, hogy mekkora erőfeszítést igényelt a forráskód létrehozása. A bátorság kitartást is jelent: a programozók egész nap megragadhatnak egy komplex problémánál, majd másnap gyorsan megoldják a problémát, de csak akkor, ha kitartóak.

### 3.12. Tisztelet
A tisztelet érték magában foglalja a mások iránti tiszteletet és az önbecsülést. A programozók soha nem hajthatnak végre olyan változtatásokat, amelyek megszakítják az összeállítást, megbuktatják a meglévő egységteszteket, vagy egyéb módon késleltetik társaik munkáját. A tagok tisztelik saját munkájukat azáltal, hogy mindig a kiváló minőségre törekszenek, és a refaktorálás révén a legjobb megoldás kialakítására törekednek.

A négy korábbi érték elfogadása a csapat többi tagjának tiszteletét eredményezi. A csapat egyik tagjának sem szabad meg nem értettnek vagy figyelmen kívül hagyottnak éreznie magát. Ez biztosítja a magas szintű motivációt, és ösztönzi a csapat iránti és a projekt célja felé mutatott lojalitást. Ez az érték a többi értéktől függ, és a csapatmunka felé irányul.

### 3.13. Szabályok
Az XP-re vonatkozó szabályok első változatát Don Wells 1999-ben tette közzé az XP weboldalán. A tervezés, kezelés, tervezés, kódolás és tesztelés kategóriáiban 29 szabály van megadva. A tervezést, az irányítást és a tervezést kifejezetten arra hívják fel, hogy megakadályozzák az olyan követeléseket, amiket az XP nem támogat.

Az XP szabályok egy másik változatát Ken Auer javasolta az XP/Agile Universe 2003-ban. Úgy érezte, hogy az XP-t a szabályok határozzák meg, nem pedig a gyakorlatai (amelyek több variáció és kétértelműség alá esnek). Két kategóriát határozott meg: „Elkötelezettségi szabályok”, amelyek leírják azt a környezetet, amelyben a szoftverfejlesztés hatékonyan megvalósulhat, és „Játékszabályok”, amelyek meghatározzák a tevékenységeket és a szabályokat az Elkötelezettségi Szabályok keretében.

Íme néhány szabály (hiányos):

**Coding**

* Az ügyfél mindig elérhető
* Először az egységtesztet írd meg
* Egyszerre csak egy pár integrálja a kódot
* Hagyd az optimalizálást végére
* Nincs túlóra

**Tesztelés**

* Minden kódnak egységtesztekkel kell rendelkeznie
* Az összes kódnak meg kell felelnie az összes egységtesztnek, mielőtt kiadható lenne.
* Ha egy hibát találtak, akkor a hiba elhárítását megelőzően tesztek jönnek létre (a hiba nem logikai hiba, hanem egy olyan teszt, amelyet nem írtak meg)
* Az elfogadási teszteket gyakran futtatják, és az eredményeket közzéteszik

### 3.14. Alapelvek
Az XP alapját képező alapelvek a fent leírt értékekre épülnek, és célja a rendszerfejlesztési projekt döntéseinek elősegítése. Az alapelvek célja, hogy konkrétabbak legyenek, mint az értékek, és gyakorlati helyzetben könnyebben iránymutatásokká váljanak.

### 3.15. Visszajelzés
Az extrém programozás a visszajelzést a leghasznosabbnak tekinti, ha azt gyakran és azonnal elvégzik. Hangsúlyozza, hogy a cselekvés és a visszajelzés közötti minimális késleltetés kritikus jelentőségű a tanulás és a változások végrehajtása szempontjából. A hagyományos rendszerfejlesztési módszerektől eltérően, az ügyféllel való kapcsolat gyakrabban jön elő. Az ügyfél világos betekintést nyújt a fejlesztés alatt álló rendszerbe, és visszajelzést adhat, és szükség szerint irányíthatja a fejlesztést. Az ügyféltől kapott gyakori visszajelzésekkel a fejlesztő téves tervezési döntését észreveszik és gyorsan kijavítják, mielőtt a fejlesztő sok időt töltene annak megvalósítására.

Az egységteszt hozzájárul a gyors visszajelzés elvéhez. Kód írásakor az egységteszt futtatása közvetlenül visszajelzést ad arról, hogy a rendszer hogyan reagál a végrehajtott változásokra. Ez magába foglalja nemcsak az egységteszt futtatását, amely teszteli a fejlesztő kódját, hanem az összes egységteszt futtatását a teljes szoftver ellen, egy automatizált folyamat használatával, amelyet egyetlen parancs indíthat el. Ilyen módon, ha a fejlesztő által végrehajtott változások a rendszer valamelyik olyan részében hibát okoznak, amelyről a fejlesztő keveset vagy semmit sem tud, az automatikus egység-tesztcsomag azonnal felfedi a hibát, figyelmeztetve a fejlesztőt a változásokkal való összeegyeztethetetlenségére, a rendszer többi részét, valamint a változásuk eltávolításának vagy módosításának szükségességét. A hagyományos fejlesztési gyakorlatok szerint az automatizált, átfogó egységteszt-készlet hiánya azt jelentette, hogy egy ilyen kódváltoztatást, amelyet a fejlesztő ártalmatlannak tartott, a helyén maradt volna, és csak az integrációs tesztelés során jelenik meg - vagy ami még rosszabb, csak a használat során. Óriási feladat volt annak meghatározása, hogy melyik kódváltoztatás okozta a problémát, az összes fejlesztő által az integrációs tesztelést megelőző hetekben vagy akár hónapokban elvégzett változtatások között.

### 3.16. Feltételezve az egyszerűséget
Arról szól, hogy minden problémát úgy kezelnek, mintha „rendkívül egyszerű” megoldás lenne. A hagyományos rendszerfejlesztési módszerek azt mondják, hogy tervezzünk a jövőre és kódoljunk az újrafelhasználhatóságért. Az extrém programozás elutasítja ezeket az ötleteket.

Az extrém programozás támogatói szerint a nagy változások egyszerre történő végrehajtása nem működik. Az extrém programozás növekményes változtatásokat hajt végre: például lehet, hogy egy rendszernek három hetente kis kiadásai jelennek meg. Amikor sok apró lépést megtesznek, az ügyfélnek nagyobb ellenőrzése van a fejlesztési folyamat és a fejlesztés alatt álló rendszer felett.

### 3.17. Átfogó változás
Az átfogó változás elve abban áll, hogy nem a változásokkal szemben kell dolgozni, hanem át kell ölelni őket. Például, ha az egyik iteratív találkozón kiderül, hogy az ügyfél igényei drámaian megváltoztak, a programozóknak ezt átfogóan kell megvizsgálni, és meg kell tervezniük a következő iteráció új követelményeit.

## 4. Gyakorlatok
Az extrém programozásról leírták, hogy 12 gyakorlattal rendelkezik, négy területre csoportosítva:

**Finom skálájú visszajelzés**

* Páros programozás
* Tervezési játék
* Tesztvezérelt fejlesztés
* Egész csapat

**Folyamatos fejlődés**

* Folyamatos integráció
* Refaktorálás vagy tervezés fejlesztése
* Kis kiadások

**Közös megértés**

* Kódolási szabványok
* Kollektív kódtulajdon
* Egyszerű kialakítás
* Rendszer metafora

**A programozó jóléte**

* Fenntartható ütem

## 5. Vitatott szempontok
Az XP gyakorlatát erősen vitatják. A szélsőséges programozás támogatói azt állítják, hogy azáltal, hogy a helyszíni ügyfél kérése informálisan megváltozik, a folyamat rugalmassá válik, és megtakarítja a formális általános költségeket. Az XP kritikusai azt állítják, hogy ez költséges átdolgozást és a projekt hatóköréből kinyúlhat, ami korábban megállapodott vagy finanszírozott volt.

A változáskezelő táblák arra utalnak, hogy lehetséges konfliktusok vannak a projekt célkitűzéseiben és a több felhasználó között. Az XP gyorsított módszerei bizonyos mértékben attól függnek, hogy a programozók képesek-e egységes kliens nézőpontot feltételezni, így a programozó a kódolásra koncentrálhat, nem pedig a kompromisszumos célok és korlátozások dokumentálására. Ez akkor is érvényes, ha több programozó szervezet vesz részt, különösen azok a szervezetek, amelyek a projektek részarányáért versenyeznek.

Az extrém programozás más, esetlegesen ellentmondásos szempontjai a következők:

* A követelményeket inkább automatikus elfogadási tesztek formájában fejezik ki, mint specifikációs dokumentumokkal.
* A követelményeket fokozatosan határozzák meg, ahelyett, hogy mindezt előre megpróbálnák megszerezni.
* A szoftverfejlesztőknek általában párokban kell dolgozniuk.
* Nincs nagy tervezés az elején. A tervezési tevékenység nagy része menet közben, fokozatosan zajlik, kezdve "a lehető legegyszerűbb dologgal, ami esetleg működhet", és csak akkor ad hozzá komplexitást, ha a teszt sikertelensége miatt szükségessé válik. A kritikusok ezt egy "rendszer megjelenéshez való hibakeresés"sel hasonlítják össze, és attól tartanak, hogy ez több áttervezési erőfeszítést fog eredményezni, mint csupán az újratervezés, amikor a követelmények megváltoznak.
* A vevői képviselőt csatolják a projekthez. Ez a szerep a projekt egyetlen kudarcpontjává válhat, és néhány ember úgy gondolta, hogy ez egy stresszforrás. Ugyancsak fennáll annak veszélye, hogy a nem műszaki képviselő mikrogazdálkodást próbáljon diktálni műszaki szoftver jellemzői és az architektúra felhasználásával.

A kritikusok számos lehetséges hátrányra rámutattak, beleértve az instabil követelményekkel kapcsolatos problémákat, a felhasználói konfliktusok dokumentált kompromisszumainak hiányát és az átfogó tervezési specifikáció vagy dokumentum hiányát.

### 5.1. Skálázhatóság
A ThoughtWorks észszerű sikert igényelt az elosztott XP-projektekben, akár hatvan embert foglalkoztatva.

2004-ben az XP fejlődéseként bevezették az ipari extrém programozást (IXP). Célja, hogy nagy és megosztott csoportokban is létrejöjjön a munkaképesség. Most 23 gyakorlattal és rugalmas értékekkel rendelkezik.

### 5.2. Elkülöníthetőség és válaszok

2003-ban Matt Stephens és Doug Rosenberg közzétették az Extreme Programming Refactored: The Case Against XP című kiadványt, amely megkérdőjelezte az XP folyamat értékét, és javaslatot tett annak javítására. Ez hosszú vitát váltott ki a cikkekben, az internetes hírcsoportokban és chat oldalakon. A könyv alapvető érve az, hogy az XP gyakorlatai egymástól függnek, de kevés gyakorlati szervezet hajlandó / képes alkalmazni az összes gyakorlatot; ezért az egész folyamat kudarcot vall. A könyv más kritikákat is felvet, és negatív módon hasonlítja az XP „kollektív tulajdonosi” modelljét a szocializmushoz.

Az XP bizonyos szempontjai megváltoztak az Extreme Programming Refactored megjelenése óta. Az XP most alkalmazza a gyakorlatok módosítását, mindaddig, amíg a szükséges célok teljesülnek. Az XP egyre inkább általános kifejezéseket használ a folyamatokra. Egyesek szerint ezek a változások érvénytelenítik a korábbi kritikákat; mások azt állítják, hogy ez csak a folyamat enyhítése.

Más szerzők megkísérelték egyeztetni az XP-t a régebbi módszerekkel, hogy egységes módszertant képezzenek. Ezek közül az XP-k közül néhányat megkíséreltek kicserélni, például a vízesésmodellt. Példa: Projekt életciklusai: vízesés, gyors alkalmazásfejlesztés (RAD) és Az Mind. A JPMorgan Chase & Co. megpróbálta kombinálni az XP-t a CMMI számítógépes programozási módszerével és a Six Sigmaval. Megállapították, hogy a három rendszer jól megerősíti egymást, jobb fejlődéshez vezetve, és nem mondanak egymásnak ellent.

## 6. Kritika
Az extrém programozás kezdeti fellángolása és az ellentmondásos tételek, mint például a páros programozás és a folyamatos tervezés, különös kritikát vontak, például az alábbiaktól: McBreen, Boehm és Turner, Matt Stephens és Doug Rosenberg. Számos kritikát azonban az agilis gyakorlók úgy gondolják, hogy félreértik az agilis fejlesztést.

Különösen az extrém programozást vizsgálta meg és kritizálta Matt Stephens és Doug Rosenberg Extreme Programming Refactored. című írása.

A kritika a következőket tartalmazza:

* A módszertan csak annyira hatékony, mint az érintett emberek, az Agile ezt nem oldja meg
* gyakran használják eszközként az ügyfelek pénzének megszerzésére a kézbesíthető termék meghatározásának hiánya miatt
* a struktúra és a szükséges dokumentáció hiánya
* csak senior szintű fejlesztőkkel működik
* nem elégséges a szoftver tervezése
* megköveteli a gyakori találkozók megrendezését, óriási költségekkel az ügyfelek számára
* túl sok kulturális változást igényel az elfogadáshoz
* nehezebb szerződéses tárgyalásokat eredményezhet
* nagyon hatástalan lehet; ha a kód egy területére vonatkozó követelmények különböző iterációk révén megváltoznak, lehet, hogy ugyanazt a programozást többször is el kell végezni. Míg ha egy tervet követnének, akkor egyetlen kódterületet várhatóan egyszer írnak meg.
* lehetetlen reális becsléseket készíteni az árajánlathoz szükséges munkára vonatkozóan, mivel a projekt elején senki sem ismeri a teljes alkalmazási körét/követelményeit
* a részletes követelménydokumentáció hiánya miatt növekedhet a hatókör kicsúszásának a kockázata
* Az agilis funkció vezérelt; a nem funkcionális minőségi jellemzőket nehéz bemutatni felhasználói történetekként.
