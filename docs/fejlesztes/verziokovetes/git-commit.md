## 1. Bevezetés
Ha bármely véletlenszerű Git repo naplójában nézelődünk, akkor valószínűleg úgy találjuk, hogy annak végrehajtási üzenetei többé-kevésbé rendetlenek. Itt van egy remek elrententő példa:

```git
$ git log --oneline -5 --author cbeams --before "Fri Mar 26 2009"

e5f4b49 Re-adding ConfigurationPostProcessorTests after its brief removal in r814. @Ignore-ing the testCglibClassesAreLoadedJustInTimeForEnhancement() method as it turns out this was one of the culprits in the recent build breakage. The classloader hacking causes subtle downstream effects, breaking unrelated tests. The test method is still useful, but should only be run on a manual basis to ensure CGLIB is not prematurely classloaded, and should not be run as part of the automated build.
2db0f12 fixed two build-breaking issues: + reverted ClassMetadataReadingVisitor to revision 794 + eliminated ConfigurationPostProcessorTests until further investigation determines why it causes downstream tests to fail (such as the seemingly unrelated ClassPathXmlApplicationContextTests)
147709f Tweaks to package-info.java files
22b25e0 Consolidated Util and MutableAnnotationUtils classes into existing AsmUtils
7f96f57 polishing
```

Hasonlítsuk össze ugyanabból az adattárból érkező commit üzenetet ami a 7 szabálynak megfelő formátumban íródott:

```git
$ git log --oneline -5 --author pwebb --before "Sat Aug 30 2014"

5ba3db6 Fix failing CompositePropertySourceTests
84564a0 Rework @PropertySource early parsing logic
e142fd1 Add tests for ImportSelector meta-data
887815f Update docbook dependency and generate epub
ac8326d Polish mockito usage
```

Az előbbi hossza és formája változó, utóbbi tömör és következetes.
Az előbbi történik alapértelmezés szerint, az utóbbi soha nem véletlenül történik.

Bár sok tároló naplója úgy néz ki, mint az előbbi, vannak kivételek. A Linux kernel és maga a Git remek példák.

E repo-k közreműködői tudják, hogy egy jól kialakított Git üzenet a legjobb módja annak, hogyan kell kommunikálni más társfejlesztőkkel. A diff megmondja, hogy mi változott, de csak a commit üzenet tudja megmondani, hogy miért. Peter Hutterer irja ezt is:

> Egy kódrész kontextusának helyreállítása pazarló. Nem tudjuk elkerülni teljesen, ezért erőfeszítéseinknek a lehető legnagyobb mértékű csökkentésére kell fókuszálnunk. A commit üzenetek pontosan ezt képesek megtenni, és ennek eredményeként az elkötelezett üzenet megmutatja, hogy a fejlesztő jó-e.

A legtöbb programnyelv jól bevált konvencióval rendelkezik arról, hogy mi képezi az idiomatikus stílust, azaz a névadást, a formázást stb-it. Természetesen vannak variációk ezekben az egyezményekben, de a legtöbb fejlesztő egyetért abban, hogy egyet kell választani és ragaszkodni kell ahhoz.

A csapatnak az commit üzenethez való hozzáállásának egységesnek kell lennie. A hasznos verziótörténet létrehozása érdekében a csapatoknak először meg kell állapodniuk egy olyan elkötelezett üzenet-megállapodásban, amely legalább a következő három dolgot meghatározza:

**Stílus**. Jelölési szintaxis, margók, nyelvtan, nagybetűs írásjelek. Egyértelművé kell kell tenni ezeket a dolgokat. A végeredmény egy rendkívül következetes napló lesz, amelyet öröm lesz olvasni.

**Tartalom**. Milyen információt kell tartalmaznia a commit üzenet törzsének (ha van ilyen)? Mit ne tartalmazzon?

**Metaadatok**. Hogyan kell hivatkozni a nyomkövetési azonosítókra, a lekérési számokra stb.?

Szerencsére vannak jól megalapozott egyezmények arról, hogy hogyan kell készteti az idiomatikus Git üzeneteket. Szerencsére, nem kell újra feltalálni a kereket. Csak az alábbi hét szabályt kell betartani, és máris profi módon lehet commit üzeneteket írni.

## 2. Git üzenet hét szabálya

1. Válasszuk el a tárgyat a szöveg törzset egy üres sorral,
1. Korlátozzuk a tárgy sort 50 karakterre,
1. Kezdjük nagybetűvel a tárgy sort,
1. Ne fejezzük be a tárgy sort ponttal,
1. Használjuk a felszólító módot a tárgy sorban,
1. Kb 72 karakter szélesre tördeljük a szöveg törzset
1. Használjuk a szöveg törzset magyarázásra, hogy mit és miért csináltunk.

```
Összefoglaljuk a változásokat legfeljebb 50 karakterben

Szükség esetén részletesebb magyarázó szöveg. Kb 72 karakter szélesre tördeljük
a szöveg törzset. Bizonyos összefüggésekben az első sor a commit üzenet tárgya
és a szöveg többi része a törzs. Az összefoglalót a törzstől elválasztó üres
sor után kell kezdeni (kivéve ha teljesen kihagyod a szöveg törzset). Különféle
eszközök, például a `log`, a `shortlog` és a `rebase` összezavarodhat, ha a
kettőt együtt futtatja.

Magyarázzuk el azt a problémát, amit megoldottunk. Koncentráljunk arra, hogy
miért ezt a változtatást hajtottuk végre. Vannak-e ennek mellékhatásai vagy
más értelmetlen következményei? Itt a magyarázat.

További bekezdések az üres sorok után következnek.

 - A pontokba szedés is rendben van

 - Általában kötőjelet vagy csillagot használnak a pontokbe szedéshez,
   előtte egyetlen szóközzel, üres sorokkal, de a konvenciókkal itt
   variálhat

Ha issue tracker használunk, tegyünk hivatkozásokat rájuk,
mint ez:

Megoldva: #123
Lásd még: #456, #789
```

### 2.1. Válasszuk el a tárgyat a szöveg törzset egy üres sorral
Leírás a `git commit` kézi könyvéből:

> Bár nem szükséges, de célszerű a commit üzenetet egyetlen rövid (50 karakternél kevesebb) sorral kezdeni, amely összefoglalja a változást, majd egy üres sor után, elkezdeni egy alaposabb leírást. A commit üzenetének első üres soráig a szöveget commit címként kezelik, és ezt a címet az egész Git-ben használják. Például a Git-format-patch a commit üzenetet e-mail-re alakítja.

Először is, nem minden commithoz szükséges tárgy és szöveg törzs. Néha egyetlen sor is elég, különösen, ha a változás olyan egyszerű, hogy nincs szükség további magyarázatra. Például:

```
Javítva a gépelési hiba, a felhasználói útmutató bevezetésében
```

Semmi többet nem kell mondani; ha az olvasó kíváncsi arra, hogy mi volt a gépelési hiba, akkor egyszerűen csak egy pillantást vethet magára a változásra, azaz használhatja a `git show` vagy a `git diff` vagy `git log -p`.

Ha ilyesmi commit üzenetet akarunk irni, akkor használható a `git commit -m` parancs:

```git
git commit -m"Javítva a gépelési hiba, a felhasználói útmutató bevezetésében"
```

Ha azonban a commit üzenet kis magyarázatot és magyarázatot igényel, akkor szöveg törzset az üres sor után kell írni. Például:

```git
Derezz a vezérlő program

Az MCP gonosznak bizonyult, és világuralomra akar törni.
Ez az elkötelezettség Tron korongját dobja az MCP-be
és visszaforgatja a sakkjátszmát.
```

A szöveg törzsel történő üzenetküldés nem olyan egyszerű az `-m` opcióval. Jobb, ha az üzenetet egy megfelelő szövegszerkesztőbe írjuk. Ha még nem rendelkezik a Git használatához szükséges szerkesztővel a parancssorban, olvassa el a Pro Git ezen szakaszát.

Mindenesetre a tárgy és a szöveg törzs elválasztása megtérül a napló böngészése során. Itt van a teljes naplóbejegyzés:

```git
$ git log
commit 42e769bdf4894310333942ffc5a15151222a87be
Author: Laszlo Beres <beres.laszlo1990@gmail.com>
Date:   Fri Jan 01 00:00:00 1982 -0200

  Derezz a vezérlő program
  
  Az MCP gonosznak bizonyult, és világuralomra akar törni.
  Ez az elkötelezettség Tron korongját dobja az MCP-be
  és visszaforgatja a sakkjátszmát.
```

És most `git log --oneline`, amely csak a tárgyat írja ki:

```git
$ git log --oneline
42e769 Derezz a vezérlő program
```

Vagy a, `git shortlog` amely felhasználó szerint csoportosítja a commit üzenetet, ismét csak a tárgyat mutatja meg:

```git
$ git shortlog
Laszlo Beres (1):
    Derezz a vezérlő program

Alan Bradley (1):
    Introduce security program "Tron"

Ed Dillinger (3):
    Rename chess program to "MCP"
    Modify chess program
    Upgrade chess program

Walter Gibbs (1):
    Introduce protoype chess program
```

Számos olyan összefüggés van a Gitben, ahol van különbség a tárgy és a szöveg törzs között - de egyik sem működik megfelelően a kettő közötti üres sor nélkül.

### 2.2. Korlátozzuk a tárgy sort 50 karakterre
Az 50 karakter nem kemény határ, csak ökölszabály. A tárgysorok ilyen hosszúságú megtartása biztosítja azok olvashatóságát, és arra kényszeríti a szerzőt, hogy egy pillanatra gondolkodjon el a legtömörebb módon, hogy elmagyarázza, mi történik.

> Tipp: Ha nehezen tudjuk összefoglalni, akkor túl sok változtatást hajtottunk végre egyszerre. Törekedjünk atomi commitra.

### 2.3. Kezdjük nagybetűvel a tárgy sort
Ez olyan egyszerű, mint amilyennek hangzik. Kezdjük az összes tárgy sort nagybetűvel.

Például:

* Gyorsoljunk fel 88 mérföldre

Ahelyett:

* ~~gyorsoljunk fel 88 mérföldre~~

### 2.4. Ne fejezzük be a tárgy sort ponttal
Az írásjelek utólagos követése a tárgy sorokban felesleges.

Példa:

* Nyissuk ki az ajtót

Ahelyett:

* ~~Nyissuk ki az ajtót.~~

### 2.5. Használjuk a felszólító módot a tárgy sorban
Az imperatív mód csak annyit jelent, hogy "úgy beszélt vagy írt, mintha parancsot vagy utasítást adna". Néhány példa:

* Takarítsd ki a szobádat
* Csukd be az ajtót
* Kivinni a szemetet

Mind a hét szabály, amelyről most olvasunk, az imperatív módban van megírva.

A felszólítás kissé durván hangozhat, ezért nem gyakran használjuk. De tökéletes a Git commit tárgy soraibak. Ennek egyik oka az, hogy Git maga is használja, amikor commit-ot hoz létre az nevünkben.

Például a használat során létrehozott alapértelmezett üzenet a git merge követően:

```git
Merge branch 'myfeature'
```

> Ne feledjük: Az imperatívum használata csak a tárgyban fontos. A szöveg törzsében enyhíthetjük ezt a korlátozást.

### 2.6. Kb 72 karakter szélesre tördeljük a szöveg törzset
A Git soha nem tördeli be automatikusan a szöveget. Amikor egy commit üzenet törzsét írjuk, ügyelnünk kell rá, és manuálisan be kell tördelni a szöveget.

Az ajánlás az, hogy ezt 72 karakternél hosszabb nem lehet, hogy a Gitnek bőven legyen helye a szöveg behúzására, miközben mindent 80 karakter szélesen tart.

Itt egy jó szövegszerkesztő segíthet. Könnyen konfigurálható például a Vim, hogy 72 karakteres szöveget tördeljen, amikor Git commit-ot írunk. Hagyományosan azonban az IDE-k szörnyűek abban, hogy intelligens támogatást nyújtanak a git commit üzenetek szöveg tördeléséhez.

### 2.7. Használjuk a szöveg törzset magyarázásra, hogy mit és miért csináltunk.
A Bitcoin Core commit üzenetei remek példa arra, hogy elmagyarázza, mi változott és miért:

```git
commit eb0b56b19017ab5c16c745e6da39c53126924ed6
Author: Pieter Wuille <pieter.wuille@gmail.com>
Date:   Fri Aug 1 22:57:55 2014 +0200

   Simplify serialize.h's exception handling

   Remove the 'state' and 'exceptmask' from serialize.h's stream
   implementations, as well as related methods.

   As exceptmask always included 'failbit', and setstate was always
   called with bits = failbit, all it did was immediately raise an
   exception. Get rid of those variables, and replace the setstate
   with direct exception throwing (which also removes some dead
   code).

   As a result, good() is never reached after a failure (there are
   only 2 calls, one of which is in tests), and can just be replaced
   by !eof().

   fail(), clear(n) and exceptions() are just never called. Delete
   them.
```