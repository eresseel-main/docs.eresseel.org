## 1. Bevezetés
A Prometheus egy ingyenes szoftveralkalmazás, amelyet eseményfigyelésre és riasztásra használnak. Valós idejű mutatókat rögzít egy idősoros adatbázisban, amely HTTP - lekérési modell segítségével készült, rugalmas lekérdezésekkel és valós idejű riasztással. A projekt Go nyelven íródott, és az Apache 2 License alatt van licencelve, a forráskód pedig elérhető a GitHubon. A Cloud Native Computing Foundation, valamint a Kubernetes és az Envoy közös projektje.

## 2. Előzmények
A Prometheust-ban fejlesztették ki a SoundCloudon 2012-ben, amikor a vállalat felfedezte, hogy meglévő mérőszámai és megfigyelési megoldásai (StatsD és Graphite használatával) nem elégségesek az igényeik kielégítésére. Konkrétan azonosították azokat az igényeket, amelyeknek a Prometheus kielégítésére készült, beleértve a többdimenziós adatmodellt, a működési egyszerűséget, a méretezhető adatgyűjtést és a hatékony lekérdezési nyelvet, mindezt egyetlen eszközben. A projekt kezdettől fogva nyílt forráskódú volt, és a Boxever és a Docker felhasználók is elkezdték használni, annak ellenére, hogy nem jelentették be kifejezetten. A Prometheust a Google-nél használt Borgmon megfigyelőeszköz ihlette.

2013-ra a Prometheust a SoundCloud gyártásfigyelésére vezették be. A hivatalos nyilvános bejelentésre 2015 januárjában került sor.

2016 májusában a Cloud Native Computing Foundation után második inkubált projektjeként fogadta el a Prometheust. Az ezt bejelentő blog bejegyzés azt állította, hogy az eszközt számos vállalat használja, köztük a DigitalOcean, az Ericsson, a CoreOS, a Weaveworks, a Red Hat és a Google.

A Prometheus 1.0 2016 júliusában jelent meg. A későbbi verziók 2016-ban és 2017-ben jelentek meg, ami a Prometheus 2.0-hoz vezetett 2017 novemberében.

2018 augusztusában a Cloud Native Computing Foundation bejelentette, hogy a Prometheus projekt befejeződött.

## 3. Felépítése
A Prometheus tipikus megfigyelési platformja több eszközből áll:

* A megfigyelt gazdagépen általában több exportőr fut a helyi mutatók exportálásához.
* Prometheus a mérőszámok központosítására és tárolására.
* Alertmanager riasztások kiváltására ezeken a mérőszámokon alapulóan.
* Grafana műszerfalak gyártásához.
* A PromQL az irányítópultok és riasztások létrehozására használt lekérdezési nyelv. 

### 3.1. Adattárolási formátum
A Prometheus-adatokat metrikák formájában tárolják, és minden metrikának van neve, amelyet a hivatkozásra és a lekérdezésre használnak. Minden metrika tetszőleges számú kulcs=érték párral (címkével) használható. A címkék tartalmazhatnak információkat az adatforrásról (melyik szerverről származnak az adatok) és egyéb alkalmazás-specifikus lebontási információkat, például a HTTP állapotkódot (a HTTP-válaszokhoz kapcsolódó metrikákhoz), a lekérdezési módszert (GET versus POST), végpontot stb. A Prometheus adatmodelljét többdimenziósnak nevezik, hogy tetszőleges címkék listáját adhatjuk meg, és ezek alapján valós időben lekérdezhetünk.

### 3.2. Adatgyűjtés
A Prometheus idősorok formájában gyűjt adatokat. Az idősorok lekérési modellen keresztül épülnek fel: a Prometheus szerver egy adott lekérdezési gyakorisággal lekérdezi az adatforrások listáját (néha exportőröknek is nevezik). Mindegyik adatforrás az adott adatforrás metrikáinak aktuális értékeit szolgálja ki a Prometheus által lekérdezett végponton. A Prometheus szerver ezután összesíti az adatokat az adatforrások között. A Prometheus számos mechanizmussal rendelkezik az adatforrásként használható erőforrások automatikus felfedezésére.

### 3.3. PromQL
A Prometheus saját lekérdezési nyelvet kínál, a PromQL-t (Prometheus Query Language), amely lehetővé teszi a felhasználók számára az adatok kiválasztását és összesítését. A PromQL-t kifejezetten úgy alakították ki, hogy konvencionálisan működjön az idősoros adatbázissal, és ezért idővel kapcsolatos lekérdezési funkciókat biztosít. A példák közé tartozik a rate() függvény, az azonnali vektor és a tartományvektor, amely sok mintát biztosíthat minden egyes lekérdezett idősorhoz. A Prometheus négy egyértelműen meghatározott metrikatípussal rendelkezik, amelyek körül a PromQL komponensei forognak. A négy típus az

* Mérés
* Számláló
* Hisztogram
* Összegzés

### 3.4. Figyelmeztetések és figyelés
A riasztások konfigurációja a Prometheusban adható meg, amely olyan feltételt ad meg, amelyet egy adott ideig fenn kell tartani a riasztás aktiválásához. Amikor riasztások aktiválódnak, a rendszer továbbítja azokat az Alertmanager szolgáltatáshoz. Az Alertmanager logikát tartalmazhat a riasztások elnémításához, valamint e-mailbe, Slack-be vagy értesítési szolgáltatásokba, például a PagerDuty-ba történő továbbításukhoz. Néhány más üzenetküldő rendszer, például a Microsoft Teams konfigurálható az Alertmanager Webhook Receiver segítségével külső integráció mechanizmusaként. A Prometheus Alerts is használható riasztások fogadására közvetlenül az Android készülékeken, még akkor is, ha az Alert Managerben nincs szükség célkonfigurációra. 

### 3.5. Irányítópultok
A Prometheus nem dashboard megoldásnak készült. Bár használható konkrét lekérdezések grafikonjaira, ez nem egy teljes értékű dashboard megoldás, és össze kell kötni a Grafana-val az irányítópultok létrehozásához; ezt hátrányként említették a további beállítási bonyolultság miatt. 

### 3.6. Interoperabilitás
A Prometheus a fehérdobozos megfigyelést részesíti előnyben. Javasoljuk, hogy az alkalmazások tegyenek közzé (exportálják) belső mérőszámokat, amelyeket a Prometheus rendszeresen gyűjt. Egyes exportőrök és ügynökök különféle alkalmazásokhoz rendelkezésre állnak a mutatók biztosítására. A Prometheus támogat néhány felügyeleti és adminisztrációs protokollt, hogy lehetővé tegye az átmenet interoperabilitását: Graphite, StatsD, SNMP, JMX és CollectD.

A Prometheus a platform elérhetőségére és az alapvető műveletekre összpontosít. A mutatókat általában néhány hétig tárolják. A hosszú távú tárolás érdekében a mérőszámok streamelhetők távoli tárolási megoldásokhoz.

### 3.7. Az OpenMetrics szabványosítása
Törekednek arra, hogy a Prometheus kiállítási formátumot egy OpenMetrics néven ismert szabványba tegyék. Egyes termékek elfogadták a formátumot: az InfluxData TICK csomagja, InfluxDB, Google Cloud Platform, és DataDog.