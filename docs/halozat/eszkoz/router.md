## 1. Bevezetés
Az útválasztó, forgalomirányító vagy angol kifejezéssel: router[1] a számítógép-hálózatokban egy útválasztást végző eszköz, amelynek a feladata a különböző hálózatok – például egy otthoni vagy irodai hálózat és az internet, vagy egyes országok közötti hálózatok, vagy vállalaton belüli – összekapcsolása, az azok közötti adatforgalom irányítása.

A számítógépes hálózatok működésének leírására több elméleti modell is létezik, az általánosan elterjedt OSI (Open Systems Interconnection) modell réteges struktúrájában a router a harmadik – hálózati – rétegben helyezkedik el. Útvonalválasztási döntéseinek alapját az ezen rétegbeli – általában IP- – címek adják.

## 2. Működése
A számítógépes hálózatok forgalma különböző típusú adatcsomagokban zajlik. Ezen csomagok utaznak a feladótól a címzettig, akár több eszközön is keresztül, például az internet esetében. Útjuk során minden érintett eszköznek ismernie kell, hogy merre továbbítsa a fogadott csomagot, hogy az eljusson a címzettig, és döntéseket kell hoznia, amennyiben például több útvonal is ismert. A routerek végzik ezen csomagok megfelelő irányba való továbbítását, és végzik ezen döntéseket. A mai routerek nagy része az IP protokoll-alapú hálózatok forgalmát irányítják, de több más protokoll kezelésére is alkalmasak lehetnek. IP protokoll esetén egymás és a hálózatok azonosítására a harmadik rétegbeli IP-címet alkalmazzák.

## 3. Típusai
### 3.1. Szolgáltatói (ISP – Internet Service Provider)
Az internetre csatlakozást mindig valamilyen szolgáltatón keresztül lehet megvalósítani. A szolgáltatók által üzemeltetett hálózatokat és a szolgáltatókat magukat is routerek kötik össze, általában ezeket a hálózatokat nevezhetjük az internet gerincének.

### 3.2. Vállalati, nagyvállalati
A cégek és vállalkozások mai alapvető követelménye, hogy az internetre csatlakozzanak. Ehhez is routereket használnak, azonban nagyobb vállalatok esetében szükséges lehet a hálózat tagolása, akár logikailag adminisztratív szempontból, akár fizikailag elhatárolódott, országos vagy akár kontinens méretű kiterjedés esetén. Ebben az esetben a külön egységek külön helyi hálózatokkal (LAN) rendelkeznek, melyeket routerekkel lehet összekötni, így lehetővé téve a kommunikációt közöttük.

### 3.3. SOHO (Small Office, Home Office), Otthoni Irodai (kisvállalati)
Kisebb cégek illetve otthoni felhasználók Internetre való csatlakozásához használatosak ezen routerek, melyek teljesítménye is ennek megfelelően jóval kisebb. Alapvető feladatuk a belső, saját hálózat Internetre való csatlakoztatása. Egy 2013-as vizsgálat szerint a SOHO routerek nagy részének biztonsága hagy kívánnivalót maga után. A helyi hálózat felől mind a 13 vizsgált készülék feltörhető volt.

## 4. Története
Az első routert egy William Yeager nevű kutató alkotta meg 1980 januárjában a Stanford Egyetemen.[4] Feladata a számítógéptudományi részleg, a villamosmérnöki részleg és az orvosi központ hálózatának összekapcsolása volt. Az általa készített router egy DEC PDP11/05 volt, egy módosított Portable C fordítóban írt egyedi operációs rendszerrel.

## 5. Útvonalválasztó protokollok
A routerek útvonaldöntéseket hoznak a hálózati réteg címei alapján. Minden interface-ük más-más alhálózatra vagy alhálózatokra csatlakozik, melyeket az IP-cím és hálózati maszk határoz meg. Az útvonalválasztó protokollok határozzák meg az útvonalválasztás szabályait, valamint biztosítják az egyes routerek között az útvonalakra vonatkozó információcserét. Ilyen protokollok például:

* **TCP/IP routing** : EIGRP, OSPF, BGP, RIP, ISIS
* **Novell routing** : Novell RIP, EIGRP, NLSP

Az útvonalválasztó protokollokat többféle módon is besorolhatjuk, ilyenek többek között az alábbi kategóriák:

* Statikus/Dinamikus
* Single-path/Multipath
* Distance vector/Link State

## 6. Mai routerek
Néhány a legnagyobb router gyártók közül (nagyvállalati, szolgáltatói): Avaya, Cisco, Juniper, Huawei, Fujitsu, Foundry, NEC. Néhány a SOHO routerek gyártói közül: D-Link, Linksys (a Cisco leányvállalata), ZyXEL, 3Com, Trendnet, Netgear, TP-LINK. A mai számítógépek általában maguk is használhatók routerként, ha több hálózati kártya van bennük. A Unix-alapú operációs rendszerek esetén a pf (BSD) illetve az iptables (Linux) a szokványos eszköz, Windows alatt az Internet Connection Sharing szoftver képes ellátni az útvonalválasztási feladatokat.