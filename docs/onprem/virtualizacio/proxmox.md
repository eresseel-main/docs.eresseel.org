## 1. Bevezetés
A Proxmox Virtual Environment (röviden: Proxmox VE, PVE vagy proxmox) egy szerver virtualizációra optimalizált nyilt forráskódú Debian alapú Linux disztribúció. Lehetővé teszi a virtuális gépek és konténerek egyszerű telepítését és kezelését web konzol és command line felülettel. A programcsomag két LXC és OpenVZ konténerek, valamint a KVM alapú virtualizáció kezelését támogatja.

A Proxmox márkanév nem jelent semmit egyszerűen ez volt egy szabad domain név.

## 2. A kezdetek
A Proxmox fejlesztését Dietmar és Martin Maurer kezdte el, amikor felismerték, hogy az OpenVZ és a KVM nem rendelkezik mentési rendszerrel és praktikus grafikus felhasználói felülettel. Az első kiadás 2018 áprilisában jelent meg. 

## 3. Szolgáltatások
Virtualizáció, web és parancssoros interfésszel, a tárolás (storage), hálózati beállítások széles választékával. Lehetővé teszi az alkalmi és ütemezett mentési és karbantartási munkák elvégzését. Legalább három host esetén támogatja a HA (nagy megbízhatóságú) cluster építését. 

### 3.1 Storage (tárolási) modell
Helyi tárolás LVM, directory, ZFS és hálózati iSCSI, Fibre Channel, NFS, GlusterFS, CEPH. 

### 3.2. High-availability (nagy megbízhatóságú) cluster
A Proxmox képes több, akár eltérő architektúrájú számítógépek között nagy megbízhatóságú cluster kezelésére. A 2.0 verzió óta Corosync alapú kommunikációs rendszer biztosítja. 

### 3.3. Live Migration
Az egyes virtuális gépek egyenként állíthatóak be HA vagy nem HA üzemmódra. Ha egy node meghibásodik akkor a HA üzemmódú guestek automatikusan újraindulnak egy másik node-n. A virtuális gépek a live migration szolgáltatás segítségével használat közben költöztethetőek a node-k között.

## 4. Proxmox beállítások
### 4.1. Ingyenes tároló beállítássa
```bash
rm -rf /etc/apt/sources.list.d/pve-enterprise.list
echo "deb http://download.proxmox.com/debian/pve buster pve-no-subscription" | tee /etc/apt/sources.list.d/pve-no-subscription.list
```
### 4.2. Automata frissites telepítése
```bash
apt install unattended-upgrades -y

vim /etc/apt/apt.conf.d/50unattended-upgrades
  --> "o=Debian,a=stable";
  --> "o=Debian,a=stable-updates";
```
### 4.3. Nyelvi tamogatas helyreállítása
```bash
dpkg-reconfigure -plow locales
```
### 4.4. Időszinkon beállítása
```bash
vim /etc/systemd/timesyncd.conf
  --> NTP=hu.pool.ntp.org
  --> FallbackNTP=0.debian.pool.ntp.org 1.debian.pool.ntp.org 2.debian.pool.ntp.org 3.debian.pool.ntp.org
```

## 5. LXC parancsok
### 5.1. LXC konténer létrehozása
```bash
pct create 101 local:vztmpl/alpine-3.12-default_20200823_amd64.tar.xz --net0 name=eth0,bridge=vmbr0,ip=dhcp --memory=512 --hostname=alpine --storage local-zfs
```

### 5.2. LXC management
#### 5.2.1. Konténerek listázása
```bash
pct list
```
#### 5.2.2. Konténerbe belépés
```bash
pct enter 100
```
#### 5.2.3. Konténer törlése
```bash
pct destroy 100
```
#### 5.2.4. Beállítások listázása
```bash
pct config 101
```
#### 5.2.5. Boot start beállítása
```bash
pct set 101 -onboot 1
```

### 5.3. LXC snapshot
#### 5.3.1. Snapshot létrehozása
```bash
pct snapshot 100 test01
```
#### 5.3.2. Snapshot listázása
```bash
pct listsnapshot 100
```
#### 5.3.3. Snapshot törlése
```bash
pct delsnapshot test01
```
#### 5.3.4. Snapshot visszaállítása
```bash
pct rollback 100 test01
```

### 5.4. LXC backup
#### 5.4.1. Backup készítése, max 5 backup lehet egyszerre, alapból csak 1 lehet
```bash
vzdump 100 --mode snapshot --compress zst --maxfiles 5
```
#### 5.4.2. Backup visszaállítása
```bash
pct restore 100 local:backup/vzdump-lxc-100-2020_09_14-10_14_51.tar.zst --storage local-zfs
```

## 6. Template kezeles
### 6.1. Template frissítése
```bash
pveam update
```
### 6.2. Template listázása
```bash
pveam available
```
### 6.3. Template letöltése
```bash
pveam download local <template neve>
```
### 6.4. Letöltött template listázása
```bash
pveam list local
```

## 7. VM-ek kezelése
### 7.1. VM-ek listázása
```bash
qm list
```
### 7.2. Listázza a VM-ek beállításait
```bash
qm config 101
```
### 7.3. Megmutatja a VM alapállapotát
```bash
qm status 101
```
### 7.4. Listázza a VM snapshotjait
```bash
qm snapshot 101 test1
```
### 7.5. Listázza a VM-ekről készült snapshotot
```bash
qm listsnapshot 101
```
### 7.6. Backupot csinál a VM-ről
```bash
vz dump 101 --mode suspend
```

## 8. CEPH telepitese
### 8.1. CEPH inicializálása
```bash
pveceph install --version nautilus
```
### 8.2. Monitor hozzáadása, minden gépen ki kell adni a saját IP címével
```bash
pveceph createmon --mon-address 172.20.1.11
```

## 9. Cluster-ből node törlése
### 9.1. Cluster állapota
```bash
pvecm status
```
### 9.2. Cluster-ből kiszedi a node-ot
```bash
pvecm delnode pve02 
```
### 9.3. Szavazati jog törlése
```bash
pvecm expected
```
