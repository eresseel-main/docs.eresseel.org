## 1. Bevezetés
A Packer egy nyílt forráskódú eszköz azonos gépképek létrehozására többféle platformra egyetlen forrásból származó konfigurációból. Működik minden nagyobb operációs rendszeren, és nagy teljesítményű, gépi képeket készít több platformra párhuzamosan. A csomagoló nem helyettesíti a konfigurációt menedzsmentet, mint pl a Chef vagy a Puppet. Valójában képek készítésekor a Packer képes rá, hogy használjon olyan eszközöket, mint a Chef vagy a Puppet, hogy szoftvert telepítsen a képre.

A gépkép egyetlen statikus egység, amely tartalmaz egy előre konfigurált operációs rendszert és telepített szoftvert, amellyel gyorsan hozhat létre újat futó gépek. A gépi képformátumok minden platformon változnak. Néhány példa tartalmazza az AMI -ket az EC2-hez, VMDK/VMX fájlok VMware-hez, OVF exportálás VirtualBoxhoz stb.

## 2. Packer használata
Az előre készített gépi képeknek sok előnye van, de a legtöbbnek nem sikerült profitálni ebből, mert a képek létrehozása és kezelése túl fárasztó volt. Vagy nem léteztek eszközök a gépi képek létrehozásának automatizálására vagy túl magas volt a tanulási görbéjük. Az eredmény az, hogy a Packer előtt gépi képek létrehozása veszélyeztette a műveleti csapatok agilitását, és ezért a hatalmas előnyök ellenére nem használják.

A Packer mindezt megváltoztatja. A Packer automatizálja a létrehozását bármilyen típusú gépképnek. Felöleli a modern konfigurációkezelést arra ösztönzi a felhasználót, hogy olyan keretrendszert használjon, mint a Chef vagy a Puppet a telepítéshez és konfigurálja a szoftvert a Packer által készített képeken belül.

Más szóval: a Packer előre elkészített képeket hoz a modern korba, felszabadítva a kiaknázatlan lehetőségeket és új lehetőségeket nyit meg.

## 2.1. A Packer használatának előnyei

* **Szupergyors infrastruktúra kiépítés**. A Packer képek lehetővé teszik az indítást másodpercek alatt teljesen kiépített és konfigurált gépeken. Ez nem csak a termelésben, hanem a fejlesztésben is előnyös, hiszen a fejlesztő virtuális gépek másodpercek alatt is elindíthatók, anélkül jellemzően sokkal hosszabb rendelkezésre állási időre van szükség.

* **Több szolgáltató hordozhatósága**. Mivel a Packer azonos képeket készít ezért több platformon futtatható, pl. a termelést AWS-ben, a staging/QA-t privát felhőben mint az OpenStack, és az asztali virtualizációs megoldások fejlesztése, mint pl. VMware vagy VirtualBox. Minden környezet azonos gépképet futtat, végső hordozhatóságot biztosítva.

* **Javított stabilitás**. A Packer telepíti és konfigurálja az összes szoftvert a gép a kép készítésekor. Ha hibák vannak ezekben a szkriptekben, korán elkapják őket, nem pedig néhány perccel a gép után elindított.

* **Nagyobb tesztelhetőség**. A gépkép elkészítése után az a gépkép gyorsan beindítható, és smoke teszttel ellenőrizhető. Ennek köszönhetően, biztos lehetsz benne, hogy a kép megfelelően fog működni.

A Packer rendkívül egyszerűvé teszi mindezen előnyök kihasználását.

## 3. Használati esetek
### 3.1. Folyamatos szállítás
A Packer könnyű, hordozható és parancssori vezérlésű. Ez teszi a tökéletes eszköz a folyamatos szállítási folyamat közepére. Packer használható új gépképek generálására több platformon mindegyiken váltson Chef/Puppet-re.

Ennek a folyamatnak a részeként az újonnan létrehozott képek elindíthatók tesztelve, ellenőrizve, hogy az infrastruktúra-módosítások működnek. Ha a tesztek sikeresek, akkor lehet biztos abban, hogy a kép működni fog a telepítéskor. Ez új szintet hoz a stabilitásan és tesztelhetőségben az infrastruktúra változásaikor.

### 3.2. Dev/Prod Parity
A Packer segít abban, hogy a fejlesztés, a színpadra állítás és a gyártás hasonló maradjon. Packer-rel egyidejűleg több platformra is lehet készíteni képeket. Tehát ha az AWS-t használja gyártásra és a VMware-t (talán a Vagrant segítségével) fejlesztés számára, akkor AMI és VMware gépet is generálhat a Packer ugyanabban az időben ugyanabból a sablonból.

Keverje össze ezt a fenti folyamatos a szállítási esettel, és máris homogén rendszert kapsz a fejlesztéstől kezdve egészen a gyártásig.

### 3.3. Készülék/demó létrehozása
Mivel a Packer konzisztens képeket hoz létre párhuzamosan több platformhoz, így van alkotáshoz tökéletes készülékek és eldobható termékbemutatók. Ahogy a szoftver változik, automatikusan létrehozhat készülékeket előre telepített szoftverrel. A potenciális felhasználók ezután elkezdhetik használni a szoftvert az általuk választott környezetbe telepítve.

Soha nem volt még ilyen egyszerű a komplex követelményeket támasztó szoftvercsomagolás.

## 4. Telepítés Linuxra
```bash
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update && sudo apt install packer
```

## 5. Parancsok
### 5.1. Packer konfiguráció inicializálása
```bash
packer init .
```

### 5.2. Packer sablon formázása és érvényesítése
```bash
packer fmt .
packer validate .
```

### 5.3. Packer image építése
```bash
packer build docker-ubuntu.pkr.hcl
packer build -var "docker_username=*****" -var "docker_password=*****" docker-ubuntu-with-nginx.pkr.hcl
```

### 5.4. AWS autentikáció beállítása
```bash
export AWS_ACCESS_KEY_ID="<YOUR_AWS_ACCESS_KEY_ID>"
export AWS_SECRET_ACCESS_KEY="<YOUR_AWS_SECRET_ACCESS_KEY>"
```

### 5.5. Kép létrehozása változó fájllal
```bash
packer build --var-file=3.aws-ubuntu-with-variables.pkrvars.hcl
```

### 5.6. Saját image beállítása a Vagrantba
```bash
mkdir output2
cp package.box ./output2
vagrant box add new-box name-of-the-packer-box.box
vagrant init new-box
vagrant up
```

## 6. Példa kódok GitHub-on
[:octicons-file-code-24: Példakód][3]
[3]: https://github.com/eresseel/packer-example