## 1. Bevezetés
A busztopológia (avagy sín) olyan hálózati topológia, amelyben kliensek egy csoportja egy megosztott kommunikációs vonalon (ezt nevezik busznak) keresztül van összekapcsolva. A busz architektúráknak számos megjelenési formája ismert, ide tartoznak az alaplapok buszrendszerei, más számítógépek különféle (memória, periféria, bővítő stb.) buszai, és néhány Ethernet hálózat is.

A busz hálózati architektúra a legegyszerűbb módszer több kliens összekapcsolására, de gyakran problémát jelenet, hogy a kliensek egy időben akarnak ugyanazon a buszon keresztül kommunikálni, ezért egy busz architektúra esetében egy külön rendszer szükséges az ütközések elkerülésére (ez leggyakrabban a Csma/cd – vivő érzékelés többszörös hozzáféréssel rendszer) vagy kell egy kitüntetett busz mester, amely vezérli a busz forgalmát és a megosztott használatot.

Egy valódi buszhálózat passzív: a busszal összekapcsolt számítógépek csak figyelik a buszon érkező jelzéseket, nem kell továbbküldeniük a jeleket. Ennek ellenére több aktív architektúra is leírható így, mivel ugyanazokat a funkciókat valósítja meg, mint egy „busz”. Például a kapcsolt Ethernet egy logikai busz hálózaton működik, de ez fizikailag nem busz. Tulajdonképpen a számítógép hardvere is nagyon absztrakt szinten tekinthető a szoftver busz egy különleges esetének.

Annak ellenére, hogy a kapcsolt Ethernet hálózatok elterjedtebbek a passzív Ethernet hálózatoknál, a busztopológia mégsem terjedt el a kábelalapú hálózatok között. Ugyanakkor csaknem minden vezeték nélküli hálózat passzív busztopológiájú.

## 2. Jellemzői
### 2.1. Előnyök
* Egyszerűen megvalósítható és bővíthető.
* Jól megfelel ideiglenes hálózatnak (gyorsan beállítható).
* Általában a legolcsóbb a megvalósítása.
* Egy csomópont meghibásodása nincs hatással a többiekre.

### 2.2. Hátrányok
* Nehézkes az adminisztráció, a hibakeresés.
* A csomópontok száma, és a közöttük lévő kábelhosszak korlátosak.
* Egy kábel szakadás megbéníthatja a teljes hálózatot.
* Hosszabb működés esetén a karbantartási költségek magasabbak lehetnek.
* Egy új csomóponttal (számítógép) való bővítés teljesítményromláshoz vezethet.
* Alacsony biztonság (minden számítógép a busz teljes adatforgalmát "látja").
* Egy vírus a rendszerben az összes gépre szétterjedhet (jobb viszont, mint a csillag vagy gyűrű).
* Megfelelően le kell zárni a buszt (egy hurok mindig zárt).
* Korszerűtlen.
