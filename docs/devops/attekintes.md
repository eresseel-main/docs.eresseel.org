## 1. Bevezetés
A DevOps a „development” (azaz fejlesztés, Dev) és az „operations” (üzemeltetés, Ops) szavakból tevődik össze. Az emberek, folyamatok és a technológia egyesülését jelenti, célja az, hogy folyamatosan értéket lehessen teremteni az ügyfeleknek.

A DevOps a felszínen tulajdonképpen egy szervezetben a fejlesztői és az üzemeltetői teamek a hagyományosnál szorosabb együttműködését jelenti, de a részletekért érdemes ennél mélyebbre ásnunk.

Főleg az agilis fejlesztési módszerek elterjedése miatt egyre több szervezet találkozik azzal a problémával, hogy a gyors fejlesztési iterációk miatt nehezebbé válik a felhasználóktól érkező problémák feldolgozása. A gyors válaszidő nyilván minden szoftverház saját igénye, de ezek a gyors változások nagy terhet rónak a funkcionális csapatokra, ami végső soron egyre több bugot és zavart eredményez.

Erre a helyzetre válaszként egyre több szervezet mozdul a DevOps irányába, ami a fejlesztés (Development) és az üzemeltetés (Operations) viszonyának javítását takarja. Ebben a struktúrában a fejlesztői team a ciklus elejétől eszközökkel támogatja az üzemeltetést (deploy scriptek, automatikus diagnosztikai eszközök, terhelés-, és performanciatesztelési eszközök) mely a ciklus előtt, közben, és után is hasznos visszajelzéseket küld.

A rövid ciklusok megjelenésével és az egyre erősebb időbeli megkötésekkel a fejlesztés, a QA, és az üzemeltetés ilyen szigorú felosztása leginkább csak gátat szab az agilitásnak; a DevOps ezeket a falakat próbálja meg lerombolni (de legalábbis könnyebben átjárhatóvá tenni). Ennek egyik fő jellemzője, hogy mindenki elsősorban a végfelhasználói élményre koncentrál, és arra, hogy az miként befolyásolja az üzleti igényeket. A DevOps nem egy új eszközkészlet, sokkal inkább egy új folyamat — ha úgy tetszik, új kultúra.

## 2. Alapelvek
### 2.1. Három alapvető érvet lehet felhozni a DevOps mellett fejlesztői szempontból
* Csökken a stresszes helyzetek száma. Sokkal kevesebb hívást fogunk kapni az éjszaka közepén, hogy valamit sürgősen javítani kell egy éles, produktív rendszerben. Ez főleg azért van, mert ezekre a helyzetekre már proaktívan fel van készülve mindenki, jóval még mielőtt katasztrofálissá válna a helyzet.
* A kód az egész úton látható a fejlesztő számára. A régi modellben a fejlesztő végzett a kóddal, "átdobta a falon" a QA-nak, aki később szintén átdobta egy újabb falon az éles rendszert üzemeltető csapatnak. Így akár az is előfordulhatott, hogy a fejlesztő által elengedett kód az út végén már nem teljesen hasonlított a kiindulási állapothoz. A DevOps modell alatt a fejlesztők végig szemmel tudják tartani a saját kódjukat a tesztelésen át az élesítésig.
* Több releváns munka. A legtöbb emberre, így a fejlesztőkre is igaz, hogy nagyobb örömüket lelik az olyan munkában, ami valamilyen relevanciával bír a való világra nézve is. A hagyományos modellben a szoftverfejlesztők egy meglehetősen izolált térben dolgoznak, főleg kitalált szcenáriókon, amikről aztán csak az éles használat alatt derül ki, hogy valójában durva és rossz közelítések voltak. A DevOps modellben minden szcenárió valós.

### 2.2. Régi szokások
A DevOps bevezetésekor sokszor régi, rossz beidegződéseket kell kiölni a szervezetből, ami mindig problémás és nehézkes. Ennek egyik tipikus példája, hogy a javított bugok számát egyenesen arányosítjuk a szoftver minőségével. Ehelyett érdemes inkább a folyamat-bugokra koncentrálni: hol van a folyamatban az a pont, ami előidézte a szóban forgó hibát? Például biztosan tökéletesen ugyanaz a kód található-e a fejlesztők saját, helyi környezetében, mint ami aztán a QA-ba és a produktív rendszerbe kerül? Vagy esetleg ugyanaz a kód másképp viselkedik a különböző (fejlesztői, teszt, éles) környezetekben?

Itt (is) nagyon jól jön az automatizáció: biztosítani kell mind a környezetek, mind pedig a kódbázis szinkronját, enélkül ugyanis nehéz megmondani, hogy a hiba kód, adat, vagy környezet-konfiguráció jellegű.

### 2.3. Partnerség vagy ujjal mutogatás
Talán a legjelentősebb változás, amit egy fejlesztő a mindennapi munkájában tapasztal a DevOps kapcsán az a más csapattagokkal történő gyakoribb interakció. A fejlesztőknek proaktívan kell fellépniük a hibák javítását illetően (pl. üzemeltetési logok folyamatos monitorozásával) ahelyett, hogy a supporttól várják a bejövő bugokat. Ha felmerül a probléma, egymásra mutogatás helyett partnerekként kell közösen dolgozni a megoldáson (itt is érződik, hogy a korábban említett falak nagyon megkönnyítik a felelősség áthárítását).

Természetesen ehhez az egészhez elengedhetetlen a megfelelő menedzseri támogatás. A vezetőknek példákon keresztül kell vezetniük a csapataikat, és tréningeket kell biztosítaniuk a DevOps módszertan megismerésére.