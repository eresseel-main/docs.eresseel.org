## 1. Bevezetés
A K3s egy teljesen megfelelő Kubernetes disztribúció, amely az alábbi fejlesztésekkel rendelkezik:

* Egyetlen bináris fájlba csomagolt verzió.
* Könnyűsúlyú háttértár alapértelmezés szerint a sqlite3 alapú tárolási mechanizmusként. Elérhető még az etcd3, MySQL és Postgres is.
* Egyszerű indítóval ellátott, amely kezeli a TLS és opciókhoz kapcsolódó bonyolultságot.
* Alapértelmezetten biztonságos beállításokkal, amelyek megfelelőek a könnyűsúlyú környezetekhez.
* Egyszerű, de erőteljes "minden beépítve" funkciók hozzá lettek adva, például:
    * helyi tároló szolgáltató
    * szolgáltatás terheléselosztó
    * Helm vezérlő
    * Traefik ingress vezérlő.

* A Kubernetes irányító síkjának összes komponense egyetlen bináris fájlban és folyamatban van. Ez lehetővé teszi a K3s számára a tanúsítványok automatikus kezelését és a bonyolult klaszterműveletek automatizálását.
* A külső függőségeket minimalizálták (csak egy modern kernel és cgroup mount szükséges). A K3s csomagolja a szükséges függőségeket, ideértve:
    * containerd
    * Flannel (CNI)
    * CoreDNS
    * Traefik (Ingress)
    * Klipper-lb (Service LB)
    * Beágyazott hálózati irányító
    * Beágyazott local-path-provisioner
    * Gazda segédprogramok (iptables, socat, stb.)

## 2. Architektúra
Ezen az oldalon leírásra kerül egy magas rendelkezésre állású K3s szerverklaszter architektúrája, valamint az, hogy hogyan különbözik egyetlen csomópontból álló szerverklasztertől.

Emellett bemutatásra kerül, hogyan regisztrálódnak az ügynökcsomópontok a K3s szerverekkel.

* Egy szervercsomópontot úgy definiálunk, mint egy olyan gépet, amelyen a k3s server parancs fut, a vezérlő sík és adattároló komponenseit a K3s kezeli.
* Egy ügynökcsomópontot úgy definiálunk, mint egy olyan gépet, amelyen a k3s agent parancs fut, anélkül, hogy bármilyen adattároló vagy vezérlő sík komponenst tartalmazna.
* Mind a szerverek, mind az ügynökök futtatják a kubeletet, a konténer futásidejét és a CNI-t. További információkért tekintse meg az "Haladó beállítások" dokumentációt az ügynökmentes szerverek futtatásával kapcsolatban.

### 2.1. Egyetlen szerver konfiguráció beágyazott adatbázissal
Az alábbi diagram egy olyan példát mutat be, ahol egyetlen csomópontú K3s szerver található beágyazott SQLite adatbázissal.

Ebben a konfigurációban minden ügynökcsomópont ugyanahhoz a szervercsomóponthoz van regisztrálva. Egy K3s felhasználó a Kubernetes erőforrásokat a szervercsomóponton keresztül manipulálhatja, a K3s API hívásával.

### 2.2. Magas rendelkezésre állású K3s
Egyetlen szerverklaszter kielégíthet különböző felhasználási eseteket, de olyan környezetekben, ahol a Kubernetes vezérlő síkjának rendelkezésre állása kritikus, lehetőség van a K3s futtatására magas rendelkezésre állású (HA) konfigurációban. Egy HA K3s klaszter a következőkből áll:

* Beágyazott adatbázis
    * Három vagy több szervercsomópont, amelyek a Kubernetes API-t szolgáltatják, valamint más vezérlő sík szolgáltatásokat futtatnak
    * Beágyazott etcd adattároló (ellentétben az egyetlen szerverklaszterekben használt beágyazott SQLite adattárolóval)
* Külső adatbázis
    * Két vagy több szervercsomópont, amelyek a Kubernetes API-t szolgáltatják, valamint más vezérlő sík szolgáltatásokat futtatnak
    * Egy külső adattároló (például MySQL, PostgreSQL vagy etcd)

### 2.3. Rögzített regisztrációs cím az ügynökcsomópontok számára
A magas rendelkezésre állású szerverkonfigurációban minden csomópont regisztrálhat a Kubernetes API-val egy rögzített regisztrációs címmel, ahogy azt az alábbi diagram is mutatja.

A regisztráció után az ügynökcsomópontok közvetlenül kapcsolatot létesítenek az egyik szervercsomóponthoz.

### 2.4. Az ügynökcsomópontok regisztrációja
Az ügynökcsomópontok egy websocket kapcsolaton keresztül regisztrálódnak, amit a `k3s agent` hoz létre, és a kapcsolatot egy kliensoldali terheléselosztó fenntartja, amely az ügynökfolyamat részeként fut. Kezdetben az ügynök a helyi terheléselosztón keresztül csatlakozik a felügyelőhöz (és a kube-apiserverhez) a 6443-as porton. A terheléselosztó karbantart egy elérhető végpontok listáját, amelyekhez kapcsolódhat. Az alapértelmezett (és kezdetben egyetlen) végpontot a `--server` címmel megadott hosztnév alapján generálják. Miután csatlakozik a klaszterhez, az ügynök lekéri a kube-apiserver címek listáját a Kubernetes szolgáltatás végpontjainak listájából az alapértelmezett névtéren belül. Ezeket a végpontokat hozzáadják a terheléselosztóhoz, amely fenntartja a stabil kapcsolatot az összes szerverrel a klaszterben, és biztosítja a kapcsolatot a kube-apiserverhez, amely tolerálja az egyes szerverek meghibásodásait.

Az ügynökcsomópontok a csomópont-klaszter titkosítókulccsal regisztrálnak a szerverhez, valamint a csomópont számára véletlenszerűen generált jelszóval, amelyet a `/etc/rancher/node/password` könyvtárban tárolnak. A szerver a csomópontok jelszavait a Kubernetes titkosítókulcsokként tárolja, és a későbbi próbálkozásoknak ugyanazt a jelszót kell használniuk. A csomópont jelszótitkosítások a kube-system névtérben tárolódnak a <host>.node-password.k3s sablonnal. Ez azért történik, hogy megvédjék a csomópontazonosítók integritását.

Ha az ügynök `/etc/rancher/node` könyvtára eltávolításra kerül, vagy egy meglévő névvel szeretnél újra csatlakozni egy csomóponthoz, a csomópontot törölni kell a klaszterből. Ez a régi csomópontbejegyzést és a csomópont jelszótitkot tisztítja, és lehetővé teszi a csomópont számára, hogy újra csatlakozzon a klaszterhez.

Ha gyakran újrahasználod a hosztnév, de nem tudod eltávolítani a csomópont jelszótitkosításokat, egyedi csomópontazonosító automatikusan hozzáadható a hosztnévhez a K3s szerverek vagy ügynökök `--with-node-id` jelzővel történő indításával. Amikor engedélyezve van, a csomópontazonosító is tárolódik a `/etc/rancher/node/` könyvtárban.

## 3. Telepítés
```bash
curl -sfL https://get.k3s.io | sh -
# Check for Ready node, takes ~30 seconds
sudo k3s kubectl get node
```