## 1. Bevezetés
A Groovy egy objektumorientált programozási nyelv a Java platformhoz. Hasonló tulajdonságokkal rendelkezik, mint a Python, a Ruby, a Perl és a Smalltalk.

A Groovy szintaxisa Java-szerű, a blokkok határait kapcsos zárójelek jelzik. A Java-kód általában szintaktikailag helyes Groovy-ban is. Dinamikusan fordul Java bájtkóddá és együtt tud működni lefordított Java-kóddal és csomagolt Java programkönyvtárakkal is.

## 2. Történet
James Strachan először 2003 augusztusában tett említést a blogján a Groovy fejlesztéséről. Több verzió is kijött 2004 és 2006 között. Amikor elkezdődött a szabványosítás a JCP kereteiben, akkor megváltozott a verziószámozás és 2007. január 2-án (kedden) megszületett az „1.0” verzió. Ezt követően több bétát és release-jelöltet is kiadtak 1.1 verziószámmal, majd 2007. december 7-én befejezték az 1.1 verziót és rögtön át is nevezték 1.5-nek, mert már nagyon sok változást tartalmazott az 1.0-hoz képest.

2009 év elején publikálták az 1.6 verziót , majd nyáron Strachan a következőt írta a blogjában

    „Őszintén mondhatom, hogy ha 2003-ban valaki megmutatta volna nekem Martin Odersky, Lex Spoon és Bill Venners Programming in Scala című könyvét, akkor valószínűleg soha nem alkotom meg a Groovy-t.”

Még ugyanazon a nyáron kijött az 1.7 verzió is.

## 3. Jellemzők
Ránézésre a Groovy hasonlít a Javára. Nem az összes, de sok .java fájl egyszerűen átnevezhető .groovy fájllá és működni fog Groovy kódként. Vannak azonban kivételek, tehát nem mondható el, hogy a Groovy nyelv a Java nyelv kiterjesztése. Elmondható viszont, hogy a Groovy nyelv bír olyan tulajdonságokkal, amik a Javából hiányoznak.

A két nyelv hasonlósága miatt Java-programozók elég könnyen meg tudják tanulni a Groovy nyelvet, mert a Java szintaxisból kiindulva fokozatosan szokhatnak hozzá a Groovy specialitásaihoz.

A Java nyelvből hiányzó, de a Groovy nyelvben meglévő tulajdonságok között szerepel a statikus és a dinamikus típusolás (a def kulcsszóval), a closure-ök, az operátor túlterhelés, a natív szintaxis a listáknál és az asszociatív tömböknél, a reguláris kifejezések natív támogatása, a polimorf iterációk, a stringekbe ágyazott kifejezések, a helper metódusok és a biztonságos navigációs operátor (?.), ami automatikusan ellenőrzi, hogy a változó nem null értékű-e (például variable?.method() vagy variable?.field).

A Groovy megengedi egyes elemek elhagyását, amik a Javában kötelezőek, így a Groovy kód tömörebb tud lenni. A Groovy szintaxisa gyakran tömörebb, mint a Javáé. Például egy iteráció a Standard Java 5 verziótól ilyen:

```java
for (String it : stringArray)
    if (it.contains("tehén"))
        System.out.println(it);
```

Groovy-ban ugyanez így is kifejezhető:
```groovy
stringArray.findAll{it.contains("tehén")}.each{println it}
```

A Groovy beépített DOM szintaxissal támogat több jelölőnyelvet, például az XML-t és a HTML-t. Ez a feature lehetővé teszi különböző heterogén adatok definiálását és manipulálását egységes és tömör szintaxissal.

A Java nyelvtől eltérően a Groovy forráskód futtatható interpretált (tehát nem kompilált) szkriptnyelvként, ha tartalmaz osztálydefiníción kívüli kódot vagy ha egy osztálydefiníció tartalmaz main metódust vagy implementálja a Runnable vagy a GroovyTestCase interface-ek egyikét.

Eltérően a Ruby nyelvtől, a Groovy szkriptek a Perl nyelvhez hasonlóan teljes mértékben parse-olódnak és lefordulnak a futtatás előtt. (Ezt a futtató környezet „rejtve” teszi és a lefordított változatot nem menti el a folyamat során.)

A GroovyBeans a JavaBeans Groovy-féle változata. A Groovy implicit módon generálja getter és a setter metódusokat. Például a következő kódban implicit generálódnak a setColor(String color) és a getColor() metódusok; és az utolsó két sor, amely látszólag közvetlenül manipulálja az adatmezőt, tulajdonképpen a megfelelő metódusokat hívja meg.

```groovy
class AGroovyBean {
  String color
}

def myGroovyBean = new AGroovyBean()

myGroovyBean.setColor('babarózsaszín')
assert myGroovyBean.getColor() == 'babarózsaszín'

myGroovyBean.color = 'türoszi bíbor'
assert myGroovyBean.color == 'türoszi bíbor'
```

A listák és az asszociatív tömbök kezelésére a Groovy-ban egyszerű és konzisztens szintaxis van. A Java szintaxishoz képest mindenképp.

```groovy
def movieList = ['Dersu Uzala', 'Ran', 'Seven Samurai'] // deklarál egy (tömbnek tűnő) listát
assert movieList == 'Seven Samurai'
movieList = 'Casablanca'  // hozzáad egy elemet a listához
assert movieList.size() == 4

def monthMap = [ 'January' : 31, 'February' : 28, 'March' : 31 ]  // deklarál egy asszociatív tömböt
assert monthMap['March'] == 31
monthMap['April'] = 30  // hozzáad egy kulcs-érték párt az asszociatív tömbhöz
assert monthMap.size() == 4
```

## 4. Példa kódok GitHub-on
[:octicons-file-code-24: Példakód][3]
[3]: https://github.com/eresseel/groovy-example