## 1. Bevezetés
Az angol repeater szakkifejezés magyarul ismétlőt, jelismétlőt jelent.

A jelismétlő egy olyan csomóponti eszköz, amely a bemenetére érkező jel információtartalmát változtatás nélkül megjeleníti a kimenetén. A jelismétlő rendeltetésétől függően a bemeneti jelet

* erősíti
* újragenerálja
* transzponálja.

A repeatereknek sok típusa van, felépítésük az egyszerű erősítőkapcsolástól az összetett logikai áramkörök kombinációjáig terjed.

A „repeater” kifejezést először a távíró jelek átvitele kapcsán használták, ahol egy olyan elektromechanikus eszközre utalt, amely távíró jelek újragenerálását végzi. A kifejezés később a távbeszélő és a digitális adatátvitel területén is elterjedt.

Ma leginkább számítógépes hálózat kiépítésénél használják, ha az áthidalni kívánt távolság miatt az adott közegben futó jelek erősen torzulnának.

Rádiófrekvenciás átjátszóállomásokat magaslati pontokon szoktak kiépíteni, ami lehetőséget ad arra, hogy akár kis teljesítményű kézirádióval is nagy távolságú rádiós összeköttetést lehessen megvalósítani.

## 2. Repeaterek osztályozása
Az ismétlés iránya alapján egy repeater lehet

* egyirányú
* időben változó irányú
* folytonos kétirányú

A feldolgozott jel típusa alapján lehet

* analóg
* digitális

A kiépítés alapján lehet

* vezetékes
* rádiófrekvenciás
* optikai, Fiber Optic Repeater

## 3. A repeater mint hálózati eszköz
Az ismétlő a jelek újragenerálására használt hálózati készülék. Az ismétlő újragenerálja az átvitel közbeni csillapítás miatt eltorzult digitális jeleket. Repeatereket a hálózat azon pontjára kell telepíteni, ahol a jel torzulásából még nem keletkezik adatvesztés.

Azok a repeaterek amelyek fizikai jellel dolgoznak, anélkül hogy bármi módon megpróbálnák értelmezni az átvitelre kerülő adatokat, az OSI modell 1. szintjének, a fizikai rétegnek felel meg. Mivel az ilyen repeaterek nem értelmezik újra a jelet, így nem lép fel késleltetés.

Az Ethernet jelismétlők az OSI modell 2. szintjén végzik a jelismétlést. A beérkezett adatkeretet beolvassák egy pufferbe, majd a puffer tartalmát kiírják a kimenetre. Ezáltal történik egy késleltetés, ami függ a hálózat átviteli sebességétől, valamint a jelismétlő feldolgozási sebességétől.

A WiFi jelismétlők az OSI modell 4. szintjének felelnek meg, mivel TCP-IP csomagokat továbbítanak, valamint a továbbítást ellenőrzik, visszaigazolást kérnek a célállomástól, hibás átvitel esetén újraküldik, valamint meghatározott számú hibás újraküldés után törlik a csomagot. Szükség esetén az adatcsomagot több darabra bontják.

Repeaterek önmagukban, mint hálózati eszközök nem elterjedtek, leginkább egy összetett hálózati eszköz részeként fordulnak elő:

HUB-ok, switch-ek, router-ek: mindegyik portján a jel újragenerálódik. Több jelismétlőt tartalmaznak, továbbá egy beágyazott rendszert futtató számítógépet is tartalmaznak, ami a forgalomirányítást végzi.

Wi-Fi jelismétlők: a jelismétlésen kívül forgalomirányítást is végezhetnek, vagy akár új alhálózatot is képesek létrehozni. Jelismétlőn kívül egy beágyazott rendszert futtató számítógépet is tartalmaz, ami a forgalomirányítást végzi.

Maga az ismétlő egység nem végez intelligens forgalomirányítást.
