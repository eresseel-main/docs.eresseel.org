## 1. Bevezetés
A Command Injection sérülékenység akkor fordul elő, amikor egy webalkalmazás lehetővé teszi, hogy a felhasználó bemeneti adatait közvetlenül végrehajtsa a szerver operációs rendszerében futó parancsok részeként. Ha az adatbevitel nem megfelelően van ellenőrizve vagy szűrve, egy támadó kártékony parancsokat fűzhet hozzá, amelyeket a szerver végrehajt.
A cél a szerverre történő parancs végrehajtása a webalkalmazáson keresztül.

## 2. Low Security Level (Alacsony biztonsági szint)
Az alacsony biztonsági szinten nincs bemeneti validáció vagy szűrés, így a parancsok közvetlenül injektálhatók.
```bash
127.0.0.1; ls
127.0.0.1; cat /etc/passwd
127.0.0.1; touch hacked.txt
```

- 127.0.0.1: Az eredeti parancsot teljesíti.
- ; ls: Egy második parancsot injektál, amely a szerver könyvtárainak listázását végzi.

Az alacsony biztonsági szinten a parancsok szabadon futtathatók, mert nincs semmilyen bemeneti ellenőrzés.

## 3. Medium Security Level (Közepes biztonsági szint)
A közepes szinten az alkalmazás bizonyos szűréseket alkalmaz, például eltávolít bizonyos speciális karaktereket (;, &&), hogy megakadályozza a parancs láncolását.
```bash
127.0.0.1 | ls
```

Egyéb technikák a szűrés megkerülésére:
- Hexadecimal kódolás: A speciális karaktereket hexadecimális vagy Unicode formában használhatod.

A közepes szinten a védelem gyengébb, és kis kreativitással még mindig megkerülhető.

## 4. High Security Level (Magas biztonsági szint)
A magas szinten az alkalmazás erősebb szűrést használ, és csak érvényes IP-címeket fogad el. Ezenkívül gyakran alkalmazza az escape mechanizmusokat és bemeneti tisztítást.
```bash
127.0.0.1 || cat /etc/passwd
```