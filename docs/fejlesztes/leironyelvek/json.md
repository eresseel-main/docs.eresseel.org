## 1. Bevezetés
A JSON (JavaScript Object Notation) egy kis méretű, szöveg alapú szabvány, ember által olvasható adatcserére. A JavaScript szkriptnyelvből alakult ki egyszerű adatstruktúrák és asszociatív tömbök reprezentálására (a JSON-ban objektum a nevük). A JavaScripttel való kapcsolata ellenére nyelvfüggetlen, több nyelvhez is van értelmezője.

A JSON formátumot eredetileg Douglas Crockford specifikálta és az RFC4627 írja le. A JSON hivatalos mime-típusa application/json. A JSON fájlok kiterjesztése pedig .json.

A JSON-t legtöbbször egy szerver és egy kliens számítógép közti adatátvitelre használják (legtöbbször AJAX technológiával), az XML egyik alternatívájaként. Általánosságban strukturált adatok tárolására, továbbítására szolgál.

### 1.1. Története
Douglas Crockford volt az első, aki meghatározta és népszerűsítette a JSON formátumot.

A JSON-t a State Software, egy többek között Crockford alapította cég használta körülbelül 2001-től. A JSON.org honlap 2002-ben indult el. 2005 decemberében a Yahoo! elkezdte némely webes szolgáltatásait JSON-ban kínálni. A Google a GData webes protokolljának 2006 decemberében kezdett JSON feedeket kínálni.

Habár a JSON alapja a JavaScript szkriptnyelv egy részhalmaza (konkrétan az ECMA-262 3rd Edition—December 1999 standard) és gyakran használják a JavaScript programokban, a JSON programnyelvfüggetlen. JSON adatok értelmezésére és generálására igen sok programozási nyelv ad kész eszközöket. A JSON weboldalán található egy összefoglaló lista arról, hogy mely nyelvekhez vannak JSON-könyvtárak.

## 2. Szintaxis
### 2.1. A JSON alap adattípusai
* **Szám**: Lehet lebegőpontos és egész. A C és Java számaihoz hasonló. A különbség az, hogy oktális és hexadecimális formátum itt nem * használható.
* **String**: A karakterlánc nulla vagy több, idézőjelek közé zárt Unicode karakter, szükség szerint visszaper-jellel kivédve. A karakter egy hosszúságú karakterláncnak felel meg. A karakterlánc nagyban hasonlít a C vagy Java karakterláncaihoz.
* **Boolean**: Értéke true (igaz) vagy false (hamis).
* **Tömb**: A tömb értékek rendezett halmaza. A tömb [ (nyitó szögletes zárójel)-lel kezdődik és ] (záró szögletes zárójel)-lel zárul. Az értékeket , (vessző)-vel választjuk el egymástól. Az értékeknek nem kell azonos típusúnak lenniük.
* **Objektum**: Az objektum név–érték párok rendezetlen halmaza. Egy objektum { (nyitó kapcsos zárójel)-lel kezdődik és } (záró kapcsos zárójel)-lel zárul. A ':' karakter választja el a kulcsot és az értéket. A név–érték párok , (vessző)-vel tagoltak. A neveknek sztringeknek kell lenniük és különbözniük egymástól. Érték lehet idézőjelek közé írt karakterlánc, szám, logikai igaz, logikai hamis null, objektum vagy tömb. A struktúrák egymásba ágyazhatók.
* **null**: Üres érték.

Üres hely szabadon hozzáadható a strukturáló karakterek ( [ { : , ) előtt és után. Egy JSON file objektumok és tömbök leírását tartalmazza.

### 2.2. JSON példakód
```json
{
  "vezetekNev": "Kovács",
  "keresztNev": "János",
  "kor": 25,
  "cim":
  {
      "utcaHazszam": "2. utca 21.",
      "varos": "New York",
      "allam": "NY",
      "iranyitoSzam": "10021"
  },
  "telefonSzam":
  [
    {
      "tipus": "otthoni",
      "szam": "212 555-1234"
    },
    {
      "tipus": "fax",
      "szam": "646 555-4567"
    }
  ]
}
```
