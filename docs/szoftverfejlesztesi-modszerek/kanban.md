## 1. Alapelvek
A Kanban is egy agilis módszertani eszköz, a Scrum után talán a második leggyakrabban emlegetett a szoftverfejlesztés területén. A Kanban is meglehetősen adaptív módszer, ami azt jelenti, hogy relatíve kevés a követendő szabály.

A két módszer közötti egyik különbség, hogy a Kanban megengedőbb a Scrum-hoz képest. Például, nincsenek meghatározott szerepkörök. Nincs időkeret közé szorított iteráció, azaz mi választhatjuk ki, hogy mikor tervezünk, mikor javítunk a folyamatunkon, és bocsátunk ki termékverziót.

Egy másik jellemző különbség a munkafolyamat-tábla használatában van. A Kanban-ban a munkafolyamat lépései korlátozottak. Egy adott munkafázisban bármely pillanatban csak annyi elem lehet, amennyit előzetesen meghatározunk.

Az agilis projektmenedzsmentet alkalmazók általában nem korlátozzák magukat egyetlen eszközre. Sok Kanban csapat tart napi megbeszéléseket, ami tipikusan Scrum-módszer.