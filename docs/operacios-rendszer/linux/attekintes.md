## 1. Bevezetés
A Linux operációs rendszer, a szabad és a nyílt forráskódú szoftverek egyik legismertebb példája.

A „Linux” elnevezés szigorú értelemben véve a Linux-rendszermagot jelenti, amelyet Linus Torvalds kezdett el fejleszteni 1991-ben. A köznyelvben mégis gyakran a teljes Unix-szerű operációs rendszerre utalnak vele, amely a Linux-rendszermagra és az 1983-ban, Richard Matthew Stallman vezetésével indult GNU projekt keretében született alap programokra épül. A Linux pontosabb neve ebben az értelemben GNU/Linux.

A „Linux” kifejezést használják Linux-disztribúciókra (terjesztések) is, ám ilyenkor általában a disztribúció nevét is hozzáteszik. Egy-egy disztribúció olyan összeállítás, amely az alaprendszeren túl bizonyos szempontok alapján összeválogatott és testre szabott programokat tartalmaz.

A Linux a szerverek és személyi számítógépek mellett – elsősorban nyíltságának köszönhetően – megtalálható sok összetett elektronikus eszközben, így hálózati eszközökben (például routerek), hordozható eszközökben (például mobiltelefonok, okostelefonok, PDA-k, hordozható hanglejátszók, órák), háztartási gépekben, szórakoztató elektronikai berendezésekben (például asztali DVD-lejátszók, videójáték-konzolok, set-top-boxok) is. Bizonyos területeken (például webszerverek, szuperszámítógépek esetében) a legmeghatározóbb operációs rendszernek számít, ám az utóbbi években személyi számítógépekre (asztali és hordozható gépek) is egyre szélesebb körben telepítenek valamilyen Linux disztribúciót. Az egyre szélesebb elterjedtség köszönhető részben az Ubuntunak, részben pedig a netbookok elterjedésének.

## 2. Előzmények
### 2.1. A GNU projekt
Világszerte sok programozót inspirált az 1983-ban indított GNU Projekt, amelyet Richard M. Stallman (RMS) indított útjára. A szabad szoftveres megmozdulás célja az volt, hogy szabadon felhasználható, minőségi szoftvereket készítsen és terjesszen. Elkezdődött a GNU operációs rendszer, a GNU Hurd fejlesztése is, de a kilátások szerint nem lehetett a megjelenésére számítani néhány éven belül (2015-ben jelent meg).

### 2.2. Az 1980-as évek PC-s operációs rendszerei
A 80-as évek elejétől az egyik meghatározó PC-s operációs rendszer a DOS (Disk Operating System) volt. Az IBM a zárt forráskódú MS-DOS operációs rendszerrel szállította személyi számítógépeit, melyek később elárasztották a világot. Az akkori Apple Macintosh gépek (a szintén zárt forrású MacOS operációs rendszerrel) jobbak voltak az IBM PC-knél, de magasabb áruk és a perifériák alacsony választéka meggátolta őket a szélesebb körű elterjedésben.

### 2.3. A UNIX operációs rendszer

Egy másik, a 80-as években használt operációs rendszer a Unix volt. Jó tulajdonságai ellenére főként az intézmények, kutatási központok, iskolák operációs rendszere volt, részben az ára miatt, részben mert eredetileg sem otthoni felhasználásra, hanem nagyszámítógépekre tervezték. A UNIX operációs rendszer forráskódja zárt volt, ahhoz néhány kivételezett intézményen kívül mások nem férhettek hozzá.

### 2.4. A MINIX operációs rendszer
A szakemberek természetesen megkísérelték a UNIX előnyeit átültetni az olcsóbb PC-re. Az egyik ilyen próbálkozás 1987-ben a MINIX volt, megalkotója Andrew S. Tanenbaum Hollandiában élő, USA állampolgárságú professzor és kernelszakértő. A MINIX fejlesztése a nulláról kezdődött, azaz készítője nem egy meglevő operációs rendszert használt fel alapjául. Tanenbaum a rendszert oktatási céllal írta, azt szerette volna, ha tanítványai ezen a MINIX-en tanulják meg az operációs rendszerek működésének, felépítésének alapjait. A MINIX rendszert az Intel 8086 mikroprocesszoraira fejlesztette, amelyek abban az időben elárasztották a világ számítástechnikai piacát.

A MINIX nem volt kimagaslóan jó operációs rendszer, de elvitathatatlan előnye, hogy nyílt volt a forráskódja. Bárki, aki megvásárolta Tanenbaum Operációs rendszerek című könyvét, hozzájuthatott a mintegy 12 000 sor MINIX kódhoz (a könyv második kiadásakor már több mint 27 000 sor), amely egy működő operációs rendszer forráskódja volt.

Ez volt az első alkalom, hogy a nagyközönség elolvashatta egy operációs rendszer forrását, hiszen a többi rendszer forráskódját a gyártók szigorúan védelmezték a nyilvánosságtól. Tanenbaum munkájának köszönhetően programozók és egyetemisták tömegei tanulmányozhatták a MINIX operációs rendszerek működését. Ennek hatására ugrásszerűen megnőtt a téma népszerűsége, egyre-másra alakultak olyan levelezési listák, ahol az érdeklődők az operációs rendszerek készítésének kérdéseiről beszélgethettek. Az egyik olvasó Linus Torvalds, svéd nemzetiségű finn egyetemista volt.

### 2.5. A Linux születése
1991-ben Linus a Helsinki Egyetem számítástechnikai tudományok karának másodéves hallgatója volt. Linus autodidakta hacker volt, saját operációs rendszert szeretett volna írni, mivel a GNU Hurd-re várhatóan néhány évet várni kellett volna. A 21 éves egyetemista a Intel 80386-os processzor védett módú (protected mode), feladat-váltó (task-switching) lehetőségeit szerette volna felfedezni. Ez körülbelül 1991 nyarának elején lehetett, a pontos dátumra maga Linus sem emlékszik. Egy biztos: egy e-mail tanúsága szerint 1991. július 3-án már a POSIX szabvány után érdeklődött az interneten, így ekkor már futhatott nála egy kezdetleges rendszer. A program fejlesztése a Tanenbaum-féle Minix alatt történt, eleinte assembly nyelven.

1991.augusztus 25-én Linus egy mostanra már legendássá vált levelet küldött a MINIX hírcsoportba. A levél magyar nyelvű fordítása:

„Üdv minden Minix-felhasználónak odaát! Egy (ingyenes) operációs rendszert csinálok (csak hobbiból, nem lesz olyan nagy és profi, mint a gnu) a 386-os (486-os) AT-klónokhoz. Április óta érlelem, és lassan elkészül. Szeretnék visszajelzéseket arról, hogy mi tetszik és mi nem tetszik a Minixben az embereknek, mivel az én operációs rendszerem némileg hasonlít rá (többek között (gyakorlati okokból) azonos a fájlrendszer fizikai kiosztása).

A bash (1.08) és a gcc (1.40) programokat már átültettem, és úgy tűnik, működik a dolog. Ez azt jelenti, hogy pár hónapon belül valami hasznosat fogok kapni, és kíváncsi lennék, milyen funkciókat szeretnének legtöbben. Minden javaslatot szívesen veszek, azt viszont nem ígérem, hogy meg is valósítom őket :-)

- Linus (torvalds@kruuna.helsinki.fi)

Ui.: Igen! Nincs benne Minix-kód és többszálú fs-sel rendelkezik. Nem hordozható (a 386 feladatváltást használja stb.), és lehet, hogy soha nem is fog az AT-merevlemezeken kívül bármi mást támogatni, minthogy nekem csak ez van :-(.”

Így kezdődött a Linux története. Linus levele számos programozót inspirált. Andrew S. Tanenbaum – a MINIX operációs rendszer atyja – Hollandiában élő egyetemi tanár, operációsrendszer-kutató, egyik későbbi levelében azt írta, hogy a Linux elavult, mert monolitikus, ezért a professzor nem jósolt neki nagy jövőt. A levélből óriási vita lett. Ennek ellenére a Linux az elmúlt 21 évben hihetetlen fejlődésen ment keresztül, és még mindig monolitikus, pontosabban hibrid (távol áll a mikrokerneltől, de betölthető-eltávolítható modulokra osztható a funkciók többsége).

### 2.6. „GNU/Linux”
A „Linux” valójában csupán a kernel (rendszermag) neve. A Linux kernelen alapuló rendszerek szinte minden esetben a GNU projekt által fejlesztett alapprogramokat használják (mint például az operációs rendszer alapkönyvtára, a „libc”,vagy az alapvető Unix/Linux programok, mint például a „echo” vagy az „ls”).

A Linux operációs rendszer tehát leggyakrabban a Linux kernelből valamint a GNU rendszerkönyvtárakból és rendszerprogramokból áll. Stallman szerint ezért az operációs rendszer helyes elnevezése „GNU/Linux operációs rendszer”. Bár ennek jogosságával kevesen vitáznak, a köznyelvben elterjedt a Linux elnevezés.

Az FSF alapítvány azokat a Linux-disztribúciókat, amelyek GNU programokat használnak GNU variánsoknak nevezik, és azt kérik, hogy ezekre az operációs rendszerekre GNU/Linux- vagy Linux alapú GNU rendszer névvel utaljanak.Ennek ellenére nem csak a nagyközönség nevezi egyszerűen Linuxnak a rendszert, hanem a nagy és jelentős Linux disztribútorok is (pl.: SuSE Linux). Van azonban néhány disztribúció akik a GNU/Linux elnevezést használják, ezek közül talán a legjelentősebb a Debian. A lelkes szakértői berkeken kívül azonban szinte senki nem használja a GNU/Linux kifejezést. Ráadásul arról is megoszlik a vélemény, hogy egyáltalán helyes-e ez az elnevezés. Linus Torvalds például elhatárolódik a GNU/Linux kifejezéstől, mivel a Linux nem egy GNU projekt.

### 2.7. Jogi kérdések
#### 2.7.1. Informatika
Maga a Linux kernel és a legtöbb GNU szoftver a GNU General Public License (GPL) licenc alatt jelenik meg. A GPL kötelezi azt, aki bármilyen Linux disztribúciót árusít, hogy a Linux kernel forráskódját (vagy a rajta végzett bármilyen módosítást) elérhetővé tegye, hogy így bárki megtekinthesse, tovább módosíthassa, és ugyanolyan feltételekkel továbbadhassa.1997-ben Linus Torvalds kijelentette: "A Linux GPL licenc alá helyezése volt a legjobb dolog, amit valaha tettem."

A Linux más fő összetevői más licenceket is használnak. Sok könyvtár a GNU Lesser General Public License (LGPL) licencet használja (ami a GPL licencnek egy engedékenyebb változata), az X Window System rendszer pedig a MIT licencet használja.

Torvalds nyilvánosan kijelentette, hogy a Linux kernelt nem fogja a GPL 2 licencről a 2007 közepén megjelent GPL 3-ra változtatni, az új licenc egyes részleteit kifogásolva, melyek lehetetlenné tennék a kernel felhasználását Digital Rights Management-et („digitális jogkezelést”, tkp. másolásvédelmet) használó környezetben.Ezen kívül megjegyzi, hogy van egy nagyon praktikus, gyakorlati oka is: mivel több ezer ember is hozzájárult a Linux kernel fejlesztéséhez - és tulajdonképpen ezért ők is licenc tulajdonosai -, ezért nem lehet csak úgy átállni egyik licencről a másikra azok megkérdezése nélkül.

Egy 2001-es Red Hat Linux 7.1-ről készült tanulmány szerint a disztribúció 30 millió sornyi kódot tartalmazott. A COCOMO segítségével kiszámították, hogy körülbelül ennek létrehozása egy embernek 8000 évbe telne, és ha ezt hagyományos módszerrel egy kereskedelmi cég fejlesztené, az körülbelül 1,34 milliárd amerikai dollárba (2009-es árfolyamon) kerülne az Egyesült Államokban. A Red Hat Linux 7.1 kódjának legnagyobb része (71%) C programozási nyelven íródott, de emellett sok más nyelvet is használtak, például a C++ (15%), Perl, Python, Fortran és számos shell script nyelvet. A Red Hat Linux 7.1-ben található szoftverek alig több, mint a fele van GPL licenc alá helyezve. Egész pontosan a copyleftet használó szoftverek (tehát a GPL és LGPL) aránya 63%. A Linux kernel maga 2,4 millió sornyi kódot jelent, ami az egésznek a 8%-át teszi ki.

Egy későbbi tanulmány ugyanezzel a módszerrel vizsgálta a Debian GNU/Linux 4.0-t. Ez a disztribúció közel 283 millió sornyi kódot tartalmazott, melynek kifejlesztése hagyományos módszerekkel 7,37 milliárd amerikai dollárba (2009-es árfolyamon) került volna.

2003 márciusában az SCO csoport, a UNIX legfőbb jogtulajdonosa nagy összegű jogi támadásba kezdett a Linuxot támogató IBM ellen, arra hivatkozva, hogy a Linux kernel egyes verzióinak forráskódjában UNIX-ból származó kódok és megoldások találhatók. Ezen kívül több mint 1500 Linuxot használó vállalatot figyelmeztetett a jogdíjak elmaradására. Nem sokkal később a Novell beperelte az SCO-t, azt állítva, hogy a UNIX és UnixWare jogok az ő tulajdonukban vannak, ebben a perben 2007. augusztus 10-én Dale Kimball szövetségi bíró hozott ítéletet, mely kimondja, hogy a jogtulajdonos egyértelműen a Novell. Ezzel az ítélettel az SCO IBM elleni pere a jogalapját vesztette. Az SCO közleményt adott ki, melyben sajnálatosnak tartották, hogy a bíróság a lényeges pontokban a Novellnek adott igazat, de hangsúlyozták, hogy az 1995 után az SCO-nál kifejlesztett technológiák a cég tulajdonában maradnak az elsőfokú ítélet szerint is, és természetesen fellebbeznek. A per egyik eredménye az Open Source Risk Management (OSRM) megalakulása, amely a nyílt forráskódú fejlesztők és végfelhasználók szervezett jogi védelmének ellátására jött létre. Az SCO azóta gyakorlatilag csak papíron létező vállalat, és sokak szerint a Microsoftnak a Linux terjedésében vívott harcában képezett „élő pajzsot”.

#### 2.7.2. A Linux védjegy
Az Amerikai Egyesült Államokban a Linux lajstromozott védjegy, Linus Torvalds javára áll oltalom alatt. 1994.augusztus 15-én egy bizonyos William R. Della Croce Jr. jelentette be a Linux megjelölést a saját neve alatt védjegyként való lajstromozásra az USPTO-nál. Ezután "jogdíj" (licenciadíj) fizetését követelte a Linux disztribúciók fejlesztőitől. Torvalds és néhány szervezet fizetés helyett fellépett Della Croce védjegye ellen, azt állítva, hogy rosszhiszemű védjegybejelentés történt. 1997-ben zárult le az ügy. Ezután Torvalds a Linux védjegyet saját nevére lajstromoztatta. Kijelentette, hogy csak azért, nehogy valaki engedély nélkül használhassa. Azóta a Linux védjegy használatával kapcsolatos ügyeket a Linux Mark Institute intézi. 2005-ben az Egyesült Államok védjegyekkel foglalkozó szervezete felszólította az LMI-t, hogy tegyen aktív lépéseket a védjegy megőrzéséért. Ennek eredményeképpen az LMI levélben szólította fel a különböző Linux-disztribúciókat forgalmazó cégeket, hogy fizessenek díjat a Linux névjegy használatáért. Természetesen számos Linux-disztribútor felháborodott.

## 3. A Linux kernel
A Linux kernel a Linux operációs rendszer magja. Unix-szerű, monolitikus kernel, melyet Linus Torvalds informatikus alkotott meg 1991-ben, azóta egy csoport fejleszti a világ minden tájáról. Eredetileg az Intel 386-os processzorára fejlesztette, hogy tanulmányozhassa az akkoriban újdonságnak számító 386 védett üzemmódját, de később számos más architektúrára is kiterjesztették. Jelenleg a következő géptípusokon érhető el:

* Alpha AXP
* Sun SPARC
* Motorola 68000
* PowerPC
* ARM-architektúra
* Hitachi SuperH
* IBM S/390
* System z9
* MIPS
* HP PA-RISC
* Intel IA-64
* Intel x86
* AMD x86-64
* AXIS CRIS
* Renesas M32R
* Atmel AVR32
* Renesas H8/300
* NEC V850
* Tensilica Xtensa
* Analog Devices Blackfin

A legtöbb támogatott architektúra 32 és 64 bites változata is natívan támogatott, amennyiben az létezik. Alapvetően C nyelven íródott. Forráskódja szabad szoftver, jelenleg GNU GPL v2 licenc alatt adják ki.

## 4. Fejlesztés
A legfontosabb különbség a Linux és más népszerű operációs rendszerek között az, hogy a Linux kernel és a komponensek ingyenesek és nyílt forrásúak. A Linux nem az egyetlen ilyen operációs rendszer, de a legismertebb és legszélesebb körben használt. Számos szabad- és nyílt forrású szoftver licencelése a copyleft licencen alapul, melynek lényege, hogy a jog adta eszközöket nem az adott szellemi termék terjesztésének gátlására, hanem a megkötések kiküszöbölésére használják fel, így garantálva a felhasználás szabadságát a módosított változatokra nézve is. A legismertebb szabad szoftver licenc a GNU GPL, amely egy copyleft licenc. A Linux kernel és a legtöbb GNU projektből származó szoftver ezt a licencet használja.

### 4.1. Linux disztribúciók
Egy disztribúció adott készítőtől vagy gyártótól származó összeállítás, amely egy GNU/Linux alaprendszert, és ahhoz tartozó, bizonyos szempontok szerint válogatott és testre szabott programokat (és gyakran egyéni kernelt) tartalmaz. Ilyen szempont lehet például a célközönség, a felhasználás célja, a célgép hardverkonfigurációja, kezelhetőség, biztonság stb.

A disztribúciók sokfélesége kompatibilitási problémákhoz vezet, azaz nem biztos, hogy ha egy program működik az egyiken, akkor működni fog a másikon is. Ennek az az oka, hogy a különböző disztribúciók részben különböző bináris formátumokat, indexelést, … használnak. Különböző standardokkal (Linux Standard Base és Filesystem Hierarchy Standard) igyekeznek segíteni ezen.

Főbb nemzetközi fejlesztésű disztribúciók:

* Arch Linux
* Debian
* Elementary OS
* Fedora, Red Hat Linux, CentOS
* Gentoo
* Kali Linux
* KDE neon
* Linux Mint
* Mageia
* Manjaro
* Slackware
* SuSE
* Ubuntu, Kubuntu, Lubuntu, Xubuntu, Ubuntu Budgie, Ubuntu MATE
* Zorin

## 5. Programozás Linux alatt
A legtöbb Linux disztribúció számos programnyelvet támogat. A Linux alá írt alkalmazásokhoz és magához az operációs rendszerhez való eszközök mind megtalálhatóak a GNU toolchain (eszköztár vagy eszközcsomag) összeállításban. A GNU toolchain programozáshoz összeállított alkalmazások csomagja. Ez tartalmazza a GNU Compiler Collection (GCC) (fordító) és GNU build system (rendszerépítő) alkalmazásokat. A GCC-vel többek közt Ada, C, C++, Java és Fortran nyelvű programokat lehet fordítani. A Linux kernel úgy van megírva, hogy GCC-vel lefordítható legyen. Elérhetőek kereskedelmi fordítók is Linux alá, ilyen többek között az Intel C++ Compiler, Sun Studio, és az IBM XL C/C++ Compilera. A BASIC nyelvet a Gambas, FreeBASIC és XBasic alkalmazásokkal le lehet fordítani.

A legtöbb disztribúció emellett PHP, Perl, Ruby, Python és egyéb dinamikus nyelveket is támogatnak. Habár ez nem általános, de a Linux támogatja a C#-ot is a Mono projekt (melyet a Novell és a Scheme támogat), valamint a Microsoft által készített .NET Core segítségével. Számos Java virtuális gép (Java Virtual Machine) és fejlesztői eszköz fut Linuxon, beleértve az eredeti Sun Microsystems JVM (HotSpot) alkalmazást, az IBM J2SE RE-t és egyéb nyílt forrású alkalmazásokat, mint például a Kaffe.

Két fő keretrendszer létezik a grafikus alkalmazások fejlesztéséhez: a GNOME és KDE. Ezek a projektek GTK+ és Qt widget toolkit-on alapulnak, amelyek szintén függetlenül használhatók nagyobb keretrendszerekhez. Mindkettő támogat számos programozási nyelvet és számos IDE elérhető ezekhez pl. Anjuta, Code::Blocks, Eclipse, KDevelop, Lazarus, MonoDevelop, NetBeans, QtCreator, és Omnis Studio míg a régóta létező szerkesztők, mint pl. a Vim és az Emacs szintén népszerűek. 