## 1. Beveztés
A **DuckDuckGo Dorking** a DuckDuckGo keresőmotor speciális keresési operátorainak használatát jelenti a célzott információgyűjtéshez. Bár DuckDuckGo kevésbé fejlett a keresési operátorok terén, mint a Google, néhány operátor és technika segítségével itt is eredményesen végezhetünk kereséseket.

DuckDuckGo előnyei között szerepel a felhasználók adatvédelmének védelme, mivel nem követi a kereséseket és nem tárolja az IP-címeket. Ezért a DuckDuckGo dorking kifejezetten népszerű lehet olyan helyzetekben, ahol az anonimitás is fontos szempont.

## 2. DuckDuckGo keresési operátorok
Az alábbi operátorokat használhatod a DuckDuckGo keresőmotorban célzott keresésekhez:

### 2.1. **`site:`**
   - Meghatározott weboldalon belüli keresés.
   - **Példa**: `site:example.com` – Csak az example.com weboldalon található tartalmak jelennek meg.

### 2.2. **`filetype:`**
   - Keresés adott fájlformátumokkal.
   - **Példa**: `filetype:pdf` – PDF fájlok keresése.

### 2.3. **`intitle:`**
   - Az oldal címében lévő szavak keresésére.
   - **Példa**: `intitle:"index of"` – Az "index of" szót tartalmazó oldalak keresése.

### 2.4. **`inurl:`**
   - URL-ben lévő specifikus szavak keresése.
   - **Példa**: `inurl:admin` – Adminisztrációs oldalak keresése.

### 2.5. **`-` (kizárás)**
   - Egyes szavak kizárása a keresésből.
   - **Példa**: `site:example.com -login` – Kizárja a bejelentkezési oldalakat az example.com weboldalon belüli keresés során.

### 2.6. **Logikai operátorok**
   - `AND`, `OR` és `NOT` használata a keresési eredmények szűrésére.
   - **Példa**: `admin AND login` – Csak azokat az oldalakat jeleníti meg, amelyekben mindkét szó szerepel.

---

## 3. DuckDuckGo Dorking példák
### 3.1. Bejelentkezési oldalak keresése
   ```text
   site:example.com inurl:login
```

### 3.2. Érzékeny fájlok keresése
   ```text
   filetype:env site:example.com
```

### 3.3. Speciális szavakat tartalmazó fájlok keresése
   ```text
   intitle:"index of" confidential
```

### 3.4. Nyilvános dokumentumok keresése
   ```text
   filetype:pdf site:gov
```

### 3.5. Weboldalak szűrése bizonyos kifejezések alapján
   ```text
   site:example.com "admin panel" -login
```
