## 1. Bevezetés
Az Ethernet a számítógépes hálózatok egy specifikációja, a kiterjesztéseivel együtt. A LAN (helyi hálózat), a MAN (városi hálózat) és a WAN (nagy kiterjedésű hálózat) kialakításának műszaki tartalmát írja le. Manapság az elnevezés már összeforrt a számítógépes helyi hálózattal, így legtöbbször ennek a szinonimájaként használatos.

Az eredeti változat a DEC, az Intel és a Xerox cég által kidolgozott, alapsávú LAN-ra vonatkozó specifikáció volt. Az IEEE 802.3 szabvány a napjainkban is használatos megoldások alapjának tekinthető, amit azóta az igényeknek megfelelően időről időre újabb kiegészítésekkel bővítenek.

Az Ethernet hálózatok az ütközések feloldására a CSMA/CD protokollt használják. Számos kábeltípuson (koax, csavart érpár stb.) működnek, legalább 10 Mbps sebességgel.

## 2. Története
Az Ethernetet az 1970-es évek elején fejlesztették ki a Xerox PARC kutatási központjában adatkommunikációs megoldásként. Az ihletet a Hawaii egyetemen ALOHANET néven megalkotott vezeték nélküli rendszer adta. Bob Metcalfe és David Boggs 1976-ban tervezte és valósította meg az első helyi hálózatot a Xerox Palo Altó-i kutatási központjában.

A nevét az éterről (angolul: ether), a 19. századi fizikusok által feltételezett, az elektromágneses sugárzások terjedésére szolgáló könnyű közegről kapta.

Az Ethernet esetén a közeg nem vákuum, hanem egy speciális koaxiális kábel volt, amely akár 2,5 km hosszú is lehetett (500 méterenként jelismétlővel). A kábelre csavarozott adóvevőkkel legfeljebb 256 gépet lehetett csatlakoztatni. A központi kábelnek, amelyhez a gépek csatlakoztak, sokcsatlakozós kábel (multidrop cable) volt a neve, és 2,94 Mbit/s-os sebességgel tudott üzemelni.

Bob Metcalfe 1979-ben kilépett a Xerox cégtől, és megalapította a 3Comot, amely Ethernet-kompatibilis eszközöket kezdett gyártani PC-khez. Az Ethernet olyan sikeres volt, hogy a DEC, az Intel és a Xerox megállapodott egy közös, 10 Mbit/s-os adatátviteli sebességet megvalósító Ethernet-szabványban, amely a DIX szabvány (a cégek kezdőbetűiből: DEC, Intel, Xerox) nevet kapta, és 1980-ban került publikálásra The Ethernet, A Local Area Network. Data Link Layer and Physical Layer Specifications címmel, amit Blue booknak (magyarul: Kék könyv) is szokás nevezni. Ebből jött létre 1983-ban a IEEE 802.3 szabvány, amely Ethernet II néven is ismert.

Az Ethernet azóta is fejlődik, és noha az eredetileg kifejlesztett szabvány műszaki megoldásait a napjainkban alkalmazott változat már jócskán meghaladja, az Ethernet elnevezés annyira elterjedtté vált, hogy ezt szokás használni, amikor a számítógépes hálózatokról beszélünk. A korai időszakban alkalmazott speciális koaxkábel helyett a későbbiekben készült hétköznapi koaxkábeles, majd pedig a manapság is széleskörűen alkalmazott sodort érpáras rézkábel, valamint optikai szálas megoldás is.

Napjainkban a kezdeti 10 Mbit/s sebesség többszöröse érhető el, a 100 Mbit/s és 1000 Mbit/s (azaz 1 Gbit/s) sebességek mindennaposak, de a közelmúltban készült szabványok (pl. IEEE 802.3cn) már 400 Gbit/s sebességet is definiálnak.

## 3. Más hálózati szabványok
A bizottság a 802.3 jelű Ethernet-szabvány létrehozása mellett a vezérjeles sínét (token bus, 802.4) és a vezérjeles gyűrűét (token ring, 802.5) is megalkotta. Ezek egymással nem kompatibilisek, de nem műszaki okok miatt: a General Motors az autógyártásban a 802.4 szabványhoz ragaszkodott, az IBM pedig a token ringet favorizálta. Mára e két utóbbi jelentőségét vesztette, annak ellenére, hogy az IBM dolgozik egy gigabites változaton (802.5v).

## 4. Az Ethernet (802.3) család
Az Ethernet állomásai a közvetítő közeggel (kábel) való állandó kapcsolatot kihasználva folyamatosan figyelik a csatornát. Bele tudnak hallgatni abba, így ki tudják várni, amíg felszabadul, és leadhatják a saját üzenetüket anélkül, hogy ezzel egy másik állomásé sérülne, tehát elkerülhető a torlódás. Ha ütközést tapasztalnak, akkor zavarni kezdik a csatornát, hogy figyelmeztessék a küldőket, ezután véletlenszerűen meghatározott hosszúságú ideig várnak, majd adni kezdenek. Ha további ütközések történnek, az eljárás ugyanez, de a véletlen hosszú várakozás idejét a kétszeresére növelik. Így időben szétszórják a versenyhelyzeteket, esélyt adva arra, hogy valaki adni tudjon.

A DIX Ethernet és a 802.3 Ethernet adatkereteket küld a kábelen. Ezek kicsit eltérő formátumúak, de mindkettő a manchesteri kódolást használja, míg a token ring (802.5) a differenciál manchesteri kódolást.

### 4.1. A klasszikus Ethernet
A megnevezés első száma az átviteli sebességet jelöli, az ezt követő Base az alapsávú átvitelre utal. A következő szám koaxiális kábel esetén annak hosszát adja meg 100 méterre kerekítve.

A klasszikus Ethernet-kábelek leggyakoribb típusai:

| Megnevezés | Kábel            | Maximális szegmenshossz | Csomópont/szegmens | Megjegyzés                         |
|------------|------------------|-------------------------|--------------------|------------------------------------|
| 10Base5    | vastag koaxiális | 500 m                   | 100                | az eredeti kábel; mára idejétmúlta |
| 10Base2    | vékony koaxiális | 185 m                   | 30                 | nincs szükség elosztóra            |
| 10Base-T   | sodrott érpár    | 100 m                   | 1024               | a legolcsóbb rendszer              |
| 10Base-F   | optikai          | 2000 m                  | 1024               | épületek között                    |

#### 4.1.1. A 10Base5
A vastag Ethernet (thick Ethernet) esetében a sárga kerti öntözőcsőre hasonlító kábelen a lehetséges csatlakozási pontok 2,5 méterenként meg vannak jelölve. (A szabvány a sárga színt nem írja elő, de javasolja.) Ezekre az állomások csatlakoztatása úgynevezett vámpír csatlakozóval történik: egy vékony tüskét nyomnak a koaxiális kábel központi vezetékébe. A csatlakozó közvetlenül kapcsolódik egy adó-vevő egységhez, az pedig egy speciális kábellel a számítógépben lévő csatolókártyához.

A forgalmazás alapsávú (baseband), 10 Mb/s-os jelekkel történik. Létezett a szélessávú változat, a 10Broad36, de idővel eltűnt a piacról.

#### 4.1.2. A 10Base2
A vékony Ethernet (thin Ethernet) esetében a kábel jobban hajlítható, vékonyabb, és gyári BNC csatlakozókat és T elosztókat használ. A BNC csatlakozókról a jel egy – korlátozott hosszúságú – koaxiális kábelen keresztül jut a számítógép csatolókártyájához, ami tartalmazza a szükséges adó-vevő áramköröket is.

#### 4.1.3. A 10Base-T
Az irodai környezetben szokásos, csavart érpárokat használó megoldás. A számítógépek közvetlenül egy elosztóhoz csatlakoznak.

#### 4.1.4. A 10Base-F
Optikai csatolás van az egységek között, ezért mind biztonsági szempontból, mind zavarvédelmi szempontból kedvezőbb az előbbieknél, viszont jóval drágább. Tipikusan épületek közötti kapcsolat kiépítéséhez használatos.

### 4.2. A kapcsolt Ethernet
#### 4.2.1. A gyors Ethernet (802.3u)
1992-ben összehívták a bizottságot, hogy készítsenek egy új szabványt egy gyorsabb LAN-ra, megtartva a 802.3 minden egyéb előírását. Egy másik elképzelés viszont arról szólt, hogy teljesen át kell alakítani mindent, és biztosítani kell a valós idejű forgalmat, valamint a digitális hangátvitelt. A nevet – üzleti okokból – meg akarták tartani. A bizottság az első változatot fogadta el, és elkészítette a 802.3u-t. Az el nem fogadott javaslat hívei elkészítették a saját szabványukat, a 802.12-t, ami nem terjedt el.

A gyors Ethernet eredeti kábelezése:

| Megnevezés  |      Kábel       | Maximális szegmenshossz  |               Megjegyzés                |
|:-----------:|:----------------:|:------------------------:|:---------------------------------------:|
| [100Base-T4](/halozat/adatatvitel/vezetekes/100Base-T4/)  | sodrott érpár    | 100 m                    | 3-as kategóriájú UTP                    |
| [100Base-TX](/halozat/adatatvitel/vezetekes/100Base-TX/)  | sodrott érpár    | 100 m                    | duplex 100 Mb/s (5-ös kategóriájú UTP)  |
| [100Base-FX](/halozat/adatatvitel/vezetekes/100Base-FX/)  | fényvezető szál  | 20.000 m                 | nagy távolságra, duplex 100 Mb/s        |

#### 4.2.2. A gigabites Ethernet (802.3.z)
A gyors Ethernet szabványát követően 1995-ben a 802-es bizottság egy még gyorsabb Ethernet tervein kezdett dolgozni. A célkitűzések a következők voltak: tízszer nagyobb sebesség, kompatibilitás az eddigi Ethernetekkel. A végső szabvány, a 802.3z eleget tett a feltételeknek.

A gigabites Ethernet – eltérően a klasszikustól – pont-pont felépítésű. A legegyszerűbb topológiánál két számítógép van összekapcsolva, de gyakoribb az a megoldás, amikor egy kapcsoló vagy elosztó köt össze több számítógépet, illetve további elosztókat vagy kapcsolókat. Egy Ethernet-kábel végén minden esetben csak egy-egy eszköz található.

A gigabites Ethernet két működési módot támogat: a duplex és félduplex működést. „Normális” esetnek a duplex módot tekintik, aminél a forgalom mindkét irányban egy időben folyhat. Ezt akkor használják, ha egy központi kapcsolót vagy a periférián lévő gépekkel, vagy más kapcsolókkal kötnek össze. Ekkor minden adatot pufferelnek, így bármelyik gép és kapcsoló tetszés szerinti időben küldheti el az adatait (kereteit). Az adónak nem kell figyelnie a csatorna forgalmát, mert a versengés kizárt, ami feleslegessé teszi a CSMA/CD protokoll használatát. Mivel a kábel egy gépet és egy kapcsolót köt össze, így csak ez a gép adhat a kapcsoló felé, tehát a duplex megoldás miatt az esetleges ellenirányú adatküldés biztosan sikeres lesz. A lehetséges legnagyobb kábelhosszt a jel erőssége határozza meg, nem pedig egy zajlöket adóhoz való visszajutásának az ideje. A kapcsolóknak módjukban áll keverni és egyeztetni a sebességeket, és automatikusan konfigurálhatják a hálózatot is, hasonlóan a gyors Ethernethez.

Ha a számítógépek nem kapcsolóhoz, hanem elosztóhoz csatlakoznak, akkor a félduplex módot használják. Az elosztó nem puffereli a beérkező kereteket. A kapcsoló belül villamosan összeköti az összes vonalat, hasonlóan a klasszikus Ethernetnél alkalmazott megoldáshoz. Az ütközések nem kizárhatók, tehát szükséges a CSMA/CD protokoll használata. Mivel a legrövidebb keretet (64 byte) százszor gyorsabban lehet elküldeni, a maximális távolság is százszor kisebb, azaz 25 méteres lesz, hogy megmaradjon az a sajátosság, hogy az adás még a legrosszabb esetben is addig tart, amíg a zajlöket visszaér az adóhoz. Egy 2500 méter hosszú kábel esetében 1 Gb/s sebesség mellett az adó már régen végzett egy 64 byte-os keret adásával, amikor az még a kábel hosszának a tizedét sem tette meg (a visszaútról nem is beszélve).

A bizottság – érthetően – elfogadhatatlannak tartotta a 25 méteres távolságot, ezért bevezette a vivőkiterjesztés (carrier extension) és a keretfűzés (frame bursting) funkciót. Így a kábelhossz 200 méterre terjeszthető ki.

Ellentmondásos, hogy egy fölhasználó a gigabites Ethernet megvalósítása mellett dönt, de elosztókkal köti össze a gépeket, ezzel tulajdonképpen a klasszikus Ethernetet szimulálva. Tény, hogy az elosztók valamivel olcsóbbak a kapcsolóknál, és a gigabites Ethernet-illesztőkártyák elég drágák. Ha ezt a relatív drágaságot az olcsóbb elosztókkal akarják ellensúlyozni, akkor ez egyben drasztikusan csökkenti a hálózat teljesítményét. A bizottságnak viszont a visszafelé kompatibilitás célként való kitűzése, előírása – és szokása – miatt meg kellett engednie ezt a lehetőséget a 802.3z szabványban.

A gigabites Ethernet kábelezése:

|  Megnevezés  |      Kábel       | Maximális szegmenshossz  |                         Megjegyzés                          |
|:------------:|:----------------:|:------------------------:|:-----------------------------------------------------------:|
| 1000Base-SX  | fényvezető szál  | 550 m                    | többmódusú fényvezető szál (50 vagy 62,5 mikron)            |
| 1000Base-LX  | fényvezető szál  | 5.000 m                  | egy- vagy többmódusú fényvezető szál (50 vagy 62,5 mikron)  |
| 1000Base-CX  | 2 pár STP        | 25 m                     | árnyékolt, sodrott érpár                                    |
| 1000Base-T   | 4 pár UTP        | 100 m                    | szabványos 5-ös kategóriájú UTP                             |
