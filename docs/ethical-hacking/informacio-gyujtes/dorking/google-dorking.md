## 1. Beveztés
A **Google Dorking** (más néven **Google Hacking**) a Google keresőmotor fejlett, speciális keresési technikáit alkalmazza az interneten fellelhető érzékeny vagy rejtett információk felfedezésére. A "Dorking" kifejezés azokra a speciális keresési lekérdezésekre vonatkozik, amelyeket a Google keresőjében használhatunk a nyilvános weboldalak és adatbázisok mélyebb, célzott átvizsgálására.

A Google Dorking a biztonsági szakemberek és hackerek által használt technika, amely segíthet a sebezhetőségek, adatvédelmi problémák és más fontos információk felfedésében. Fontos azonban, hogy a Google Dorking etikus és jogi határokat ne lépje túl – az adatok hozzáférése, különösen a személyes vagy védett adatoké, jogszerűtlen tevékenység lehet.

## 2. Google Dorking működése
A Google Dorking során a felhasználó speciális keresési operátorokat alkalmaz a Google keresőjében, hogy olyan eredményeket találjon, amelyek más módon nehezen érhetők el. A keresési operátorok lehetővé teszik az oldal struktúrájának, a fájlformátumoknak, az oldalon belüli információknak és más specifikus tényezőknek a célzott keresését.

### 2.1. Google keresési operátorok
A Google Dorking hatékonyságát különféle operátorok növelik, amelyek kifejezetten segítenek az információk szűrésében:

1. **`site:`**
   - Meghatározott weboldalon belüli keresésre.
   - **Példa**: `site:example.com` – csak az example.com oldalán belüli találatokat fogja visszaadni.

2. **`filetype:`**
   - Keresés a specifikus fájlformátumokkal rendelkező dokumentumok között.
   - **Példa**: `filetype:pdf` – csak PDF fájlokat keres.

3. **`intitle:`**
   - Az oldal címében lévő szavak keresésére.
   - **Példa**: `intitle:"index of"` – az "index of" kifejezést tartalmazó oldalakat keresi.

4. **`inurl:`**
   - Keresés olyan URL-ekben, amelyek a megadott kifejezést tartalmazzák.
   - **Példa**: `inurl:admin` – adminisztrációs felületeket keres.

5. **`intext:`**
   - Olyan oldalakat keres, amelyek tartalmában szerepel a megadott szó.
   - **Példa**: `intext:"confidential"` – csak azokat az oldalakat jeleníti meg, amelyek tartalmazzák a "confidential" szót.

6. **`cache:`**
   - A Google által tárolt weboldalak gyorsítótárának keresésére.
   - **Példa**: `cache:example.com` – az example.com oldal gyorsítótárában lévő változatot mutatja.

7. **`related:`**
   - Keresés olyan oldalakon, amelyek hasonlóak egy adott URL-hez.
   - **Példa**: `related:example.com` – olyan oldalakat keres, amelyek kapcsolódnak az example.com oldalhoz.

8. **`link:`**
   - Keresés olyan oldalakon, amelyek hivatkoznak egy adott URL-re.
   - **Példa**: `link:example.com` – azokat az oldalakat listázza, amelyek hivatkoznak az example.com oldalra.

9. **`-` (kizárás)**
   - Egyes szavak kizárása a keresésből.
   - **Példa**: `site:example.com -admin` – kizárja az admin oldalak találatait az example.com weboldalon belül.

## 3. Google Dorking használata
### 3.1. **Adatgyűjtés és információ feltárása**
A Google Dorking különösen hasznos lehet a nyilvánosan elérhető információk gyors gyűjtésében. Az adatvédelmi hiányosságok, mint például a nem megfelelően védett dokumentumok, vagy a helytelenül konfigurált webhelyek és fájlok, könnyen felfedezhetők a dorking segítségével. Példák:
- **Érzékeny dokumentumok keresése**: `filetype:pdf confidential` – Ez a keresés a "confidential" szót tartalmazó PDF fájlokat hozza elő.
- **Bejelentkezési oldalak keresése**: `inurl:login` – Különböző bejelentkezési oldalak keresése.

### 3.2. Vulnerability scanning (Sebezhetőségek keresése)
A Google Dorking segíthet a weboldalak biztonsági réseinek és sebezhetőségeinek feltérképezésében. Például egy nem megfelelően védett adminisztrációs felület, amit bárki hozzáférhet:
- `inurl:admin intitle:login` – A Google a különböző adminisztrációs oldalakra mutató linkeket találhatja meg, amelyek bejelentkezési felületeket tartalmaznak.

### 3.3. Weboldalak és fájlok indexelése
A Google Dorking segíthet olyan weboldalak keresésében, amelyek esetleg érzékeny információkat tartalmaznak, például:
- **Jelszavak, titkos kulcsok**: `filetype:env password` – .env fájlok, amelyek érzékeny adatokat tartalmazhatnak.

## 4. Példák a Google Dorking technikák használatára
### 4.1. Weboldalak bejelentkezési felületeinek keresése
```text
   inurl:login intitle:"admin"
```

### 4.2. Titkos dokumentumok keresése
```text
filetype:pdf confidential
```

### 4.3. Nyilvános fájlok keresése egy adott weboldalon
```text
site:example.com filetype:txt
```

### 4.4. Adminisztrációs felületek keresése
```text
inurl:admin
```
