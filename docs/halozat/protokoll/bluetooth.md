## 1. Bevezetés
A Bluetooth (ejtsd: blútúsz, IPA: [bluːtuːθ]) rövid hatótávolságú, adatcseréhez használt, nyílt, vezetéknélküli szabvány. Alkalmazásával számítógépek, mobiltelefonok (telefonkihangosítók), fejhallgatók és egyéb készülékek között automatikusan létesíthetünk kis hatótávolságú rádiós kapcsolatot, amihez a készülékek kis teljesítményű rádióhullámot használnak.

## 2. Jellemzői
Az 1.2-es verzió 1 Mbps-os, a 2.0-s Bluetooth pedig 3 Mbps-os adatátviteli sebességet tesz lehetővé a világszerte szabadon elérhető 2,4 gigahertzes frekvenciasávban. Európában és az Egyesült Államokban a 2,402 GHz és 2,480 GHz közötti 79 db 1 MHz-es sávban, Japánban a 2,472 és 2,497 GHz közötti 23 db 1 MHz-es sávban működik. Az adatcsatorna ebben a sávban másodpercenként 1600-szor változik véletlenszerűen („szórt spektrumú frekvenciaugrás”). Egy hálózatban egy időben 1 „mester” eszközhöz legfeljebb 7 másik eszköz csatlakozhat. Az egymáshoz csatlakozott eszközök ún. personal-area network-öt (PAN), más szóval piconet-et hoznak létre, ami például az egy szobában lévő eszközök által alkotott hálózatot jelenti (vagy az autóban a mobiltelefon és a fejhallgató közötti kicsiny hálózatot).

A Bluetooth alacsony energiafogyasztása miatt különösen alkalmas hordozható eszközök számára.

## 3. Név eredete (kék fog)
A név Harald Blåtand, azaz I. (Kékfogú) Harald dán király nevének angol változata, aki 958-tól, illetve 976-tól 986-ig volt Dánia és Norvégia uralkodója és a hagyomány szerint nagyon szerette az áfonyát, amitől gyakran kék lett a foga. Harald arról volt nevezetes, hogy egyesítette a lázongó dán, norvég és svéd törzseket. Ehhez hasonlóan a Bluetooth-t is arra szánták, hogy egyesítsen és összekössön olyan különböző eszközöket, mint a számítógép, a mobiltelefon, vagy a fejhallgató. A Bluetooth logója a H és B betűknek megfelelő skandináv rúnákat, a Haglazt és a Berkanant idézi.

## 4. Alkalmazása
* Vezetéknélküli hálózatok kialakítására asztali és hordozható számítógépek között, illetve csak asztali gépek között kis területen, ha nincs szükség nagy sávszélességre
* Számítógép-perifériák csatlakoztatására nyomtatók, billentyűzetek, egerek esetében
* Fájlok és adatok átvitelére és szinkronizálására személyi digitális asszisztensek (PDA-k, mobiltelefonok és a számítógép) között
* Egyes digitális zenelejátszók, fényképezőgépek és számítógép között
* Autóskészletek és fülhallgatók csatlakoztatására mobiltelefonokhoz
* Orvosi és GPS-készülékek

## 5. Serial Port Profile
A Serial Port Profile (SPP), az ETSI TS 07.10 specifikáción alapuló és RFCOMM protokollt használó profil. Soros kábelt emulál, hogy egy egyszerű vezeték nélküli helyettesítést biztosítson a már meglévő RS-232 alapú kommunikációs alkalmazásokhoz, hasonló vezérlőjeleket alkalmazva. Biztosítja az alapot a DUN, FAX, HSP és AVRCP profiloknak.

## 6. Biztonsági tanácsok
A Bluetooth természeténél fogva lehetővé teszi az ilyen képességű eszközök automatikus kapcsolódását egymáshoz, és a kétirányú adatcserét. Ennek árnyoldala, hogy személyes adatainkhoz olyanok is hozzáférhetnek, akiknek nem akartuk ezt megengedni.

Néhány könnyen betartható biztonsági tanács:

* Csak akkor engedélyezzük a Bluetooth használatát az eszközben (például telefonban), amikor használni akarjuk, és használat után tiltsuk azt le
* Használjunk hosszú, nehezen kitalálható számkódot az eszközök párosításához (használjunk 8-jegyű vagy még hosszabb számot - az „1234” kód nem jó)
* Párosítás után az eszköz legyen „rejtett” (hidden) állapotban, így is működni fog a már párosított másik eszközzel
* Utasítsunk vissza minden ismeretlen kapcsolódási kísérletet
* Engedélyezzük a titkosítást (encryption)
* Időnként nézzük meg a párosított eszközök listáját, nincs-e köztünk olyan, amit nem mi állítottunk be
* Frissítsük a mobilunk firmware-szoftverét a legújabb verzióra a gyártó honlapjáról

## 7. Verziók
### 7.1. Bluetooth 1.0 és1.0B
E verziók számos problémával küzdöttek és számos gyártónak nehézséget okozott az egymással kompatibilis készülékek előállítása. A kapcsolat felállítása során kötelező volt sugározni a Bluetooth hardvercímet (Bluetooth Hardware Device Address, BD_ADDR). Ez protokollszinten tette lehetetlenné az anonimitást, ami nagy visszalépést jelentett az előzetes tervekhez képest.

### 7.1. Bluetooth 1.1
Főként az 1.0B hibáinak javításait tartalmazza. Lehetővé vált a titkosítatlan csatornák használata.

### 7.2. Bluetooth 1.2
Ez a változat visszafelé kompatibilis az 1.1-gyel. A főbb újdonságok:

* Magasabb adatátviteli sebesség a gyakorlatban
* A vett jel erősségének kijelzése (Received Signal Strength Indicator, RSSI)
* Adaptív frekvenciaváltás (Adaptive Frequence Hopping, AFH), amely a rádiós interferenciák kiküszöbölését segíti azzal, hogy váltáskor elkerüli a zsúfolt frekvenciákat.

### 7.3. Bluetooth 2.0
Ez a verzió visszafelé kompatibilis az előzőekkel.

* Háromszor (bizonyos esetekben akár tízszer) gyorsabb adatátvitel. Adatátviteli sebesség: 3 Mbit/s.
* Alacsonyabb energiafogyasztás. Az EDR (Enhanced Data Rate) segítségével felére csökkentette a fogyasztást.

### 7.4. Bluetooth 2.1
Egyszerűbbé tette a párosítást és javított a biztonságon.

### 7.5. Bluetooth 3.0
A Bluetooth-ba a belopja magát a 802.11-es szabvány, vagyis tulajdonképpen a Wi-Fi. A Bluetooth 3.0 működését valahogy úgy kell elképzelni, hogy a szűk sávszélességet igénylő feladatok (például párosítás, kapcsolat felépítés) az eddigiekhez hasonló Bluetooth kapcsolatra épülnek, ha pedig érkezik egy broadband igényű kérés (például egy nagy állomány másolása), akkor az az adatforgalom átterelődik a Wi-Fi-re. Mindez persze úgy történik, hogy a felhasználónak ne kelljen a Wi-Fi beállításokkal törődnie. HS (High-Speed) elméleti maximális sebessége: 24 Mbit/s.

### 7.6. Bluetooth 4.0
Az újdonság a Bluetooth 3.0-tól eltérően nem csak a sebességre, hanem emellett a korábbiaknál alacsonyabb fogyasztásra és a rendkívül széles felhasználási körre fókuszál. A 100 méteres hatósugarú újdonság kétfajta változatban létezik: a High-Speed, valamint az alacsony fogyasztású Bluetooth-t is támogatja, és elsősorban olyan masinákban fellelhető, ahol a Bluetooth már ott van (például mobiltelefonok, hordozható számítógépek). Az úgynevezett "single mode low energy" kivitel ezzel szemben a Bluetooth számára mindeddig ismeretlen területeket hódítja meg magának, így például lépésszámlálókba, kulcstartókba, hőmérőkbe, vagy akár keréknyomás mérőkbe is beköltözhet. Ha például a kulcstartónk és a mobilunk is támogatja ezt a szabványt, akkor a telefonunk kijelzőjén megjelenhet egy apró felirat, hogy valahol elhagytuk (nincs a közelben) a kulcsunkat. Az 1 Mbit/s-os sebességű Bluetooth Low Energy (korábban Wibree) alacsony energiafogyasztással kecsegtet, olyannyira, hogy egyetlen parányi gombelemmel akár több mint egy évig is működhet egy készülék.

### 7.7. Bluetooth 4.1
A 4.1-es szabvány a következő javításokat hozta a 4.0-shoz képest:

1. 4G (LTE) frekvenciával való zavaróhatás kiszűrése;
2. Intelligens csatlakozás: a Bluetooth eszköz igény szerint fel-le csatlakozik a mester eszközre, így energiát takarít meg;
3. Jobb adatátvitel: a Bluetooth 4.1-es eszközök hubként és végpontként is tudnak működni, ami az IoT technológia terjedését segíti (az okos eszközök önállóan egymással is tudnak kommunikálni).


### 7.8. Bluetooth 4.2
A 2014. december 2-án bevezetett protokoll főleg az IoT terjedését segíti. Főbb javítások:

* Jobb energiagazdálkodás, titkosítás, adatcsomag-kezelés (a sebesség 27-ről 251 bps-re nőtt);
* A személyi adatok jobb védelme, le lehet tiltani a felhasználói szokásokat figyelő szolgáltatásokat (pl. Apple iBeacon);
* IP-támogatási profil (IPSP) (az IPv6 támogatása)

### 7.9. Bluetooth 5
Tehát nem 5.0 a jelölés, hasonlóan a korábbi verziókhoz, hanem egyszerűen 5. A Bluetooth SIG hivatalosan egy 2016. június 16-i médiaeseményen mutatta be az új szabványt, amely 2018-ban jelent meg a mobileszközökben, és jobb támogatást nyújt az IoT-nek.

Változások (a Bluetooth 4.2-höz képest):

* Kétszeres sebesség (2 Mbit/s);
* Négyszeres hatótáv (240 m);
* Nyolcszoros adattovábbítási kapacitás. Ez az IoT technológiát segíti, ahol sok eszközt kell egyszerre kezelni;
* Internetelérés nélküli, helyfüggő szolgáltatások támogatása (problémamentes navigálás a reptereken, raktárkészletek nyomon követése, sürgősségi hívások kezelése, a gyengén látókat segítő „okos város” infrastruktúrák kialakítása stb.)
* „Dual Audio”: audio hangfolyam lejátszása két eszközön egy időben - ez két különböző audio forrással is megtehető

Megjegyzés: a hatótávolság növelésével az elérhető sebesség a maximálisnál kisebb lehet.

### 7.10. Bluetooth 5.2
A Bluetooth Special Interest Group (SIG) közzétette a Bluetooth továbbfejlesztett változatát, a Bluetooth LE Audio-t. A Bluetooth 5.2-es verzióját 2020. január 7-én adták ki.

Az LE Audio segítségével több eszköz is képes az adatok megosztására.

A hallássérült emberek jobb hangélményt kapnak.

A Bluetooth 5.2-es verziójának technikai specifikációi a Bluetooth-on keresztül új funkciókat adnak hozzá, nevezetesen:

1. Enhanced Attribute Protocol (EATT), az Attribute Protocol (ATT) továbbfejlesztett változata. 2. LE teljesítményszabályozás 3. LE izokron csatornák.

#### 7.10.1. Továbbfejlesztett attribútum protokoll (EATT)
Az ATT továbbfejlesztett változatát és a Generic Attribute profil továbbfejlesztését Enhanced Attribute Protocol (EATT) néven jelentették be.

Az új protokollal a végponttól végpontig tartó késleltetés csökken és emiatt az alkalmazások reakciókészsége javul.

Az EATT csak titkosított kapcsolaton keresztül használható, így az EATT biztonságilag előnyösebb a meglévő ATT-vel szemben.

#### 7.10.2. LE teljesítményszabályozás
A csatlakoztatott eszközöknél az átviteli teljesítmény optimalizálása érdekében az LE teljesítményszabályozás fontos szerepet játszik. A Bluetooth LE vevőkészülékek képesek figyelni a jel erősségét, és dinamikusan kérhetik az átviteli teljesítményszint módosítását, így automatikusan kompromisszumot kötnek a jelminőség és az alacsony energiafelhasználás között.

A LE teljesítményszabályozás néhány előnye:

* A források energiafogyasztásának csökkenése.
* A vevőjel megbízhatóságának növelése.
* A meglévő és a készülő vezeték nélküli eszközökkel kapcsolatos fejlesztések.

#### 7.10.3. LE izokron csatornák
Az alacsony energiaigényű izokron csatornák segítségével javul a hallókészülékek hangminősége. A hang több eszközhöz való csatlakoztatása és sugárzása csak az izokron csatornáknak köszönhetően lehetséges.

#### 7.10.4. LE Audio
Az LE Audio a Bluetooth 5.2-ben bevezetett LE Isochronous Channels funkció után válhat lehetővé. Ez a funkció Bluetooth LE izokronos adatátvitelt tesz lehetővé.

Az LE a „Low Energy” (alacsony energia) rövidítése. Ahogy a neve is mutatja, az LE az adatokat (hangot) alacsony energiájú spektrumon továbbítja az eszközök között. Így a Bluetooth minőségének megőrzése érdekében új tömörítési algoritmust használ.

#### 7.10.5. LC3 - Alacsony komplexitású kommunikációs kodek
A LE Audio LC3 kodekje egy alacsony energiaigényű és kiváló minőségű audiokodek.

„A kiterjedt hallgatói tesztek azt mutatták, hogy az LC3 még 50%-kal alacsonyabb bitsebesség mellett is jobb hangminőséget biztosít a Classic Audio-hoz mellékelt SBC codec-hez képest” - mondta Manfred Lutzky, a Fraunhofer IIS kommunikációs audióért felelős vezetője.
