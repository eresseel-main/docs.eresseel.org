## 1. Bevezetés
Az Ubuntu egy Debian alapú Linux-disztribúció 6 hónapos kiadási ciklusokkal, a legfrissebb szoftverekkel, hibajavításokkal és integrációs munkákkal, amelyek visszakerülnek a Debianba.

Az Ubuntu szó afrikai eredetű, jelentése „emberiesség másokkal szemben"; "azért vagyok, mert mi vagyunk”. Ezen a néven jött létre egy közösség ingyenes és nyílt forráskódú szoftverek fejlesztésére és terjesztésére. Ők alkották meg az Ubuntu Linuxot, amely Debian alapokon nyugszik. Az Ubuntu Linux ezt a filozófiát szeretné elterjeszteni a szoftver világban. Fejlesztői között több Debian- és GNOME-híresség is található. A disztribúció frissességét és az új dolgokra való reagálását jól jelzi, hogy ez volt az első olyan Linux-disztribúció, amely a GNOME 2.8-at használta.

Az Ubuntu jelenleg a GNOME felületet használja alapértelmezetten, de régebben, a 11.04-es verziótól a 17.04-esig a saját fejlesztésű Unity volt alkalmazva. Egyéb felhasználói felületekkel is elérhető, melyek fejlesztése kisebb-nagyobb mértékben különálló projektként fut, mint például az Ubuntu KDE-s testvére, a Kubuntu vagy az LXQt-re alapozott Lubuntu disztribúció. Mindegyik terjesztés azonos verzióit újratelepítés nélkül konvertálhatjuk át, vagy akár egy időben mindegyik telepítve lehet. Ezt azért tehetjük meg, mert mindegyik disztribúció ugyanazon az archívumon alapul.

A projekt mögött anyagilag Mark Shuttleworth és cége, a Canonical Ltd áll. Ez többek között a legfontosabb fejlesztők támogatását, az infrastruktúra fenntartását, illetve üzleti tevékenységet (céges támogatás) takar. 

## 2. Történet és fejlesztési folyamat
Az Ubuntu első kiadása 2004. október 20-án jelent meg, mint egy ideiglenes Debian GNU/Linux fork. A projekt célja az volt, hogy az Ubuntu minden 6. hónapban ki tudjon adni egy új kiadást, aminek az eredménye egy gyakran frissített rendszer. Az új Ubuntu kiadások mindig tartalmazzák a legfrissebb GNOME-ot, mert úgy van időzítve, hogy a legfrissebb GNOME után egy hónappal jelenik meg az Ubuntu. A kontraszt az előző generációs Debian forkokhoz - pl. MEPIS, Xandros, Linspire, Progeny és Libranet - képest, hogy tartalmaz jó néhány zárt forrású kiegészítőt, részeként az Ubuntu üzleti modelljének, ami kizárja a „Debián filozófiát”, mely túlnyomóan szabad szoftvert használ.

Az Ubuntu csomagok általában a Debian unstable csomagjain alapulnak: mindkét disztribúció a Debian .deb formátumú csomagjait használja, és az APT és Synaptic eszközöket a telepített csomagok kezeléséhez. Az Ubuntu minden csomag fejlesztést közvetlenül visszajuttat a Debian-hoz, bár ez nem minden esetben jelenti azt hogy az Ubuntu és a Debian csomagok 'binárisan kompatibilisek' (azaz Debian-ra nem feltétlenül telepíthetünk minden probléma nélkül Ubuntu csomagokat, illetve ez fordítva is igaz). Sok Ubuntu fejlesztő egyben valamely kulcsfontosságú Debian csomag karbantartója is. Ian Murdock, a Debian alapítója azonban kritizálta az Ubuntut a csomagok Debiannal való inkompatibilitása miatt, és a Debian azon fejlesztőit akik szerint az Ubuntu már túlságosan eltávolodott a Debiantól ahhoz, hogy kompatibilis maradhasson.

Az Ubuntu fejlesztését jelenleg a Canonical Ltd. finanszírozza. 2005. július 8-án Mark Shuttleworth és a Canonical Ltd. bejelentette az Ubuntu alapítvány létrehozását, 10 millió dollár alaptőkével. Az alapítvány célja az Ubuntu fejlesztésének mindenkori támogatása, de 2006 óta nem fejtett ki aktív tevékenységet. Az alapítvány célja hogy az Ubuntunak a Canonical-tól független pénzügyi alapja legyen, ezzel biztosítani a disztribúció fennmaradását hosszú távon, abban az esetben is, ha a Canonical esetleg nem vesz részt a fejlesztésben.

2007.május 1-jén a Dell bejelentette, hogy Ubuntuval előtelepített asztali PC-ket, és laptopokat fog árusítani 2007. május 24-től. Ezek a számítógépek csak Észak-Amerikában kerültek forgalomba, de nem sokkal később, 2007. augusztus 8-án az Egyesült Királyságban, Franciaországban, és Németországban is hozzáférhetők lettek.

2007 júliusában az Ubuntu Live 2007 konferencián Mark Shuttleworth bejelentette, hogy a 2008 áprilisában megjelenő Ubuntu 8.04 lesz a következő LTS (Long Term Support - hosszú ideig támogatott) kiadás, illetve hogy kétévente fognak LTS kiadásokat megjelentetni.

## 3. Képességek
Az Ubuntu a könnyű használhatóságra koncentrál, beleértve a széleskörűen elterjedt adminisztratív feladatokra használt sudo eszközt. Az Ubuntu a Live CD-ről futtatott környezetből is feltelepíthető, anélkül hogy a telepítés megkezdése előtt újra kellene indítani a számítógépet. Az Ubuntu több szoftvert tartalmaz a fogyatékkal élők támogatására, és kiemelt célnak tartja a lehető legtöbb nyelvre történő honosítást, hogy minél több emberhez tudjon eljutni. Az 5.04-es kiadástól kezdve az UTF-8 az alapértelmezett karakterkódolás.

Az alapvető rendszereszközökön és alkalmazásokon kívül az Ubuntu előre telepített alkalmazásokat is tartalmaz: a LibreOffice irodai programcsomagot, a Firefox böngészőt, az Empathy azonnali üzenetküldőt, és a Shotwell fénykép-katalogizáló és -szerkesztő programot, ezen kívül számos logikai- és kártyajáték is megtalálható benne. Az Ubuntuban alapbeállításban minden hálózati port zárva van, bár néhányan használnak tűzfalat is, a bemenő és kimenő kapcsolatok megfigyelésére.

Az Ubuntu telepítés után egy teljes munkakörnyezetet nyújt további programok telepítése nélkül is. A live CD lehetővé teszi, hogy a felhasználók még a telepítés előtt meggyőződjenek arról, hogy a hardverük kompatibilis-e az Ubuntuval. A live CD használható a rendszer telepítésére is, amely az internetről letölthető. A live CD elindításához 256 MB RAM szükséges, és a rendszer installálás után 4 GB tárterületet foglal el. Letölthető egy ún. alternatív installáló CD (12.10-es verzióig) azok számára, akik gyengébb hardverrel rendelkeznek, vagy összetett particionálásra, pl. LVM vagy RAID használatára van szükségük.

Az Ubuntu 7.04 kiadásával a telepítési folyamat megváltozott. Az új telepítő támogatja a Windowsról való migrálást. Az új migrációs eszköz képes a felhasználó könyvjelzőinek, asztali háttérképének, és egyéb beállításainak importálására (ezek az importált beállítások a Live CD használatakor még nem érhetők el). 

## 4. A csomagok osztályozása, és támogatottsága
Az Ubuntuban a szoftvereket a licencük, és támogatottságuk szerint négy osztályba sorolják, ezeket komponenseknek (components) nevezik.

A következő négy komponens létezik:

|                        | Szabad szoftver     | Nem szabad szoftver |
| ---------------------- | ------------------- | ------------------- |
| Hivatalosan támogatott | Main                | Restricted          |
| Nem támogatott         | Universe            | Multiverse          |

"Szabad" szoftver ebben az esetben azokat a programokat jelenti, amelyek teljesítik az Ubuntu által támasztott feltételeket, amelyek nagyrészt hasonlóak a Debian Szabad Szoftver Irányelvekhez. Ez alól kivétel a Main komponens: "tartalmazhat bináris firmware-eket, és bizonyos betűtípusokat (ezeket a Main szabad komponensei használják), amelyek módosítása az alkotók engedélye nélkül tilos", de ezek közül csak olyanokat, amelyek szabadon terjeszthetők.

A nem szabad szoftverek általában hivatalosan nem támogatottak (Multiverse), illetve csak azok amelyek nagyon fontos funkciót látnak el, és nem pótolhatók szabad szoftverekkel (ezek a Restricted komponenshez tartoznak). Ilyenek például a videokártya meghajtóprogramok. Ezen programok forráskódjához az Ubuntu készítőinek nincs közvetlen hozzáférésük, ezért csak korlátozottan tudnak hivatalos támogatást nyújtani.

A Main és a Restricted komponensekben minden olyan szoftver megtalálható, amely egy általános célú Linux rendszerhez szükséges. A Universe és a Multiverse több választható szoftvert biztosít adott feladatok elvégzésére.

## 5. Nem szabad szoftverek az Ubuntuban
Az Ubuntunak létezik minősítési rendszere a harmadik féltől származó programok számára. Az Ubuntu által minősített szoftvernek megfelelően kell működnie Ubuntu alatt. A legtöbb olyan program, amely a nem-szabad operációs rendszerek használói számára megszokott programok, nem kompatibilis az Ubuntuval.

Néhány olyan szoftver, amelyet nem tartalmaz az Ubuntu:

* Olyan szoftverek, amelyek a régiókóddal ellátott DVD-k lejátszását teszik lehetővé. Ennek oka a libdvdcss nyílt forráskódú DVD-lejátszó függvénykönyvtár megkérdőjelezhető jogi helyzete a világ néhány országában. (Megjegyzés: ez a függvénykönyvtár szükséges a régiókóddal ellátott DVD lejátszásához még a megfelelő régióban is.)
* Kódoló, és dekódoló függvénykönyvtárak nem szabad média formátumú médiatípusokhoz, mint pl. a Windows Media.
* Néhány népszerű böngésző-beépülőmodul, mint az Adobe Shockwave-je (nem létezik Linux verzió), és a Flash. (A Flash installálását a "flashplugin-nonfree" multiverse csomaggal mégis megtehetjük, mert ez csomag a Flash végfelhasználói licencszerződését betartva az Adobe weboldaláról tölti le a linuxos telepítőt, és futtatja).

## 6. Csomagkezelő (APT)
Az `apt-get` parancs egy nagyon hatékony eszköz, amely alapértelmezés szerint olyan disztribúciókban érkezik, mint például a Debian, a LinuxMint, az Ubuntu felelős a csomag könyvtár kezeléséért, és mint ilyen, az operációs rendszer csomagjainak telepítéséért, eltávolításáért, frissítéséért és kereséséért a sorból parancsok.

Az `apt-get` származéka. Fő funkciója a tároló frissítése után a gyorsítótár segítségével telepíteni kívánt csomagok megtalálása. De nem csak a telepítésre kereshetünk, hanem ellenőrizhetjük, hogy mely csomagok vannak telepítve, vagy a csomagokra vonatkozó információkat, például a telepített verziót, vagy ha elérhető-e frissítés. Számos disztribúcióban szerepel, mint az `apt-get`.

### 6.1. Csomaglista megjelenitése
Felsorolhatjuk a rendszer számára elérhető összes csomagot.

```bash
apt-cache pkgnames esseract-ocr-epo pipenightdreams
```

### 6.2. Keresés név szerint
A rövid leírást tartalmazó csomag kereséséhez telepítés nélkül, az `apt-cache` parancsot használjuk a keresési opcióval együtt. Felsorolja az összes csomagot, amely megfelel a keresésének, például ha FTP szervert akarunk telepíteni, akkor szükségünk van a `vsftpd` csomagra.

```bash
apt-cache keresés vsftpd
```

De abban az esetben, ha csak a nevét akarjuk leírás nélkül, akkor használjuk.

```bash
apt-cache pkgnames vsftpd
```

### 6.3. Információ a csomagról
Ha további információkat szeretnénk kapni a csomagról, annak verziójának vagy méretének a megadott KB-ban.

```bash
apt-cache show netcat
```

### 6.4. Csomagokhoz szükséges függőségek ellenőrzése
Ebben az esetben a `showpkg`-t használjuk, és felsorolja az összes függőséget, amely megfelelő működéséhez szükséges, függetlenül attól, hogy telepítve vannak-e vagy sem.

```bash
apt-cache showpkg vsftpd
```
A szakaszban felsoroljuk a függőségeket, ha van ilyen, akkor megmondja nekünk a verziót, ellenkező esetben null lesz.

### 6.5. Gyorsítótár statisztikája
Tudjuk, hogy az `apt-cache` parancs a betöltött gyorsítótár függvénye. Hasznos tudni, hogy a gyorsítótárban van, ehhez statisztikákat használunk.

```bash
apt-cache stats
```

### 6.6. Csomagrendszer frissítése
A frissítés használatakor szinkronizáljuk a csomaglistáinkat a hivatalos lerakatban, így ha rendelkezésre áll az ssh új verziója, frissíthetjük.

```bash
sudo apt-get update
```

### 6.7. Operációs rendszer frissítése a csomagok legújabb verziójával
A netes tároló alapján frissíti a saját telepített csomagjainkat, ha van frissebb.

```bash
apt-get upgrade 
```

### 6.8. A rendszer frissítése, és szükség esetén új csomagok telepítése
Frissítéskor a csomagok új verzióit és azok függőségét telepítjük, bizonyos esetekben nem telepítünk egy új függőséget igénylő csomagot. Ebben az esetben a `dist-upgrade` használjuk az új függőségek frissítésére és telepítésére, ha szükséges, legyen óvatos ezzel a lehetőséggel Termelési környezetben.

```bash
sudo apt-get dist-upgrade
```

### 6.9. Csomag telepítése és frissítése
Ezzel elkerüljük a teljes rendszer frissítését, és kis sávszélességet takarítunk meg abban az esetben, ha csak egyetlen csomagot akarunk telepíteni.

```bash
sudo apt-get install nethogs goaccess
```

### 6.10. Telepítés csomagok helyettesítő karakter segítségével
Szabványos kifejezések használatával telepíthetünk több csomagot, amelyek ugyanazt a szót tartalmazzák, például * ssh * install kliens, szerver és kiegészítők.

```bash
sudo apt-get install '* ssh *'
```

### 6.11. Csomag telepítése frissítés nélkül
A `–no-upgrade` opcióval telepítjük a csomag alapváltozatát, a frissítést nem.

```bash
sudo apt-get install packageName --no-upgrade
```

### 6.12. Csomag telepítése speciális verzióval
Telepíthetünk egy csomag egy adott verzióját is, ez a gyakorlat például a java vagy az NGINX számára.

```bash
sudo apt-get install vsftpd=2.3.5-3ubuntu1
```

### 6.13. Csomag törlése beállítások nélkül
Törölhetjük, de megőrizzük az összes konfigurációs fájlt arra az esetre, ha a jövőben telepítenünk kell.

```bash
sudo apt-get remove vsftpd
```

### 6.14. Csomag törlése beállításokal együtt
A csomag eltávolításához az összes fájlt és függőséget a `purge` opcióval használjuk.

```bash
sudo apt-get purge vsftpd
# vagy
sudo apt-get remove --purge vsftpd
```

### 6.15. Lemezterület felszabadítása
Amikor letölt egy csomagot, a .deb fájlt gyorsítótárként tárolja, törölheti az összes csomagot a `clean` opcióval.

```bash
sudo apt-get autoclean
```

### 6.16. Felesleges csomagok törlése
```bash
apt-get autoremove 
```

### 6.17. Teljes rendszer frissítése
```bash
apt update && apt upgrade -y && apt full-upgrade -y && apt dist-upgrade -y && apt autoremove --purge && apt autoclean -y
```

## 7. Csomagkezelő (DPKG)
A Dpkg egy alap csomagkezelő rendszer a Debian Linux családhoz, a `.deb` csomagok telepítésére, eltávolítására, tárolására és információkkal szolgál.

Alacsony szintű eszköz, és vannak olyan kezelőeszközök, amelyek segítik a felhasználókat a távoli adattárakból történő csomagok beszerzésében és/vagy a komplex csomagkapcsolatok kezelésében.

Ez egy nagyon népszerű, ingyenes, hatékony és még inkább hasznos parancssori csomagkezelő rendszer, amely a `dpkg` csomagkezelő rendszer kezelőfelülete.

A Debian vagy annak származékai, például az Ubuntu és a Linux Mint felhasználóinak ismerniük kell ezt a csomagkezelő eszközt.

Ez is egy népszerű parancssori front-end csomagkezelő eszköz a Debian Linux család számára, hasonlóan működik, mint az APT, és sok összehasonlítás történt a kettő között, de mindenekelőtt mindkettő kipróbálása megértheti, hogy melyik működik jobb.

Eleinte a Debian és származékai számára készült, de most már az RHEL családra is kiterjed.

A Synaptic egy grafikus felhasználói felület csomagkezelő eszköz az APT-hez, amely a GTK+-ra épül, és jól működik azoknak a felhasználóknak, akik esetleg nem akarják beszennyezni a kezüket egy parancssorban. Ugyanazokat a szolgáltatásokat valósítja meg, mint az apt-get parancssori eszköz.

### 7.1. Csomag telepítése
```bash
dpkg -i programNeve
```