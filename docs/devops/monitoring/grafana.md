## 1. Bevezetés
A Grafana egy többplatformos nyílt forráskódú analitikai és interaktív vizualizációs webalkalmazás. Támogatott adatforrásokhoz csatlakoztatva diagramokat, grafikonokat és figyelmeztetéseket biztosít az interneten. A Grafana Enterprise licencelt verziója további képességekkel szintén elérhető önálló telepítésként vagy fiókként a Grafana Labs felhőszolgáltatásban. Beépülő rendszerrel bővíthető. A végfelhasználók összetett megfigyelési irányítópultokat hozhatnak létre az interaktív lekérdezéskészítők segítségével. A Grafana-t TypeScript és Go nyelveken írják.

Vizualizációs eszközként a Grafana a stack-ek figyelésének népszerű összetevője, kombinálva használják, gyakran olyan idősoros adatbázisokkal mint az InfluxDB, Prometheus és a Graphite; olyan megfigyelő platformok, mint a Sensu, Icinga, Checkmk, Zabbix, Netdata, és egyéb adatforrások. A Grafana felhasználói felülete eredetileg a Kibana 3. verzióján alapult.

## 2. Előzmények
A Grafana-t Torkel Ödegaard adta ki először 2014-ben az Orbitz egyik projektjeként. Az olyan idősoros adatbázisokat célozta meg, mint az InfluxDB, OpenTSDB és Prometheus, de úgy fejlődött, hogy támogassa a relációs adatbázisokat, mint például a MySQL, a PostgreSQL és a Microsoft SQL Server.

2019-ben a Grafana Labs 24 millió dollárt kapott.

A 2020-ban további 50 millió dollárt kapott.

A GrafanaCon 2020 konferenciát 2020. május 13-14-re tervezték Amszterdamban de a COVID-19 világjárvány miatt kétnapos online élő közvetítéssé változtatták.

A k6 2021-ben vásárolta meg a Grafana-t.

2021-ben a Grafana Labs 220 millió dolláros finanszírozást biztosított. 