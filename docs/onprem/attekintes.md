## 1. Bevezetés
Az On premises, más néven "on-premise" módszer a szoftverek telepítésének egyik módszere. Az on-prem használatával a számítógépes programokat közvetlenül a felhasználó számítógépére telepítik CD-k vagy USB-meghajtók segítségével. A telepítő bárhol lehet az interneten.

Sok vállalat az on-prem lehetőséget választja, mert ehhez nincs szükség harmadik fél hozzáférésére, a tulajdonosok fizikai ellenőrzést biztosítanak a szerver hardver és szoftver felett, és nem követelik meg, hogy hónapról hónapra fizessenek a hozzáférésért.
