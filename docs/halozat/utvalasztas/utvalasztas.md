## 1. Bevezetés
Az útválasztás, hálózati forgalomirányítás vagy routing (még mint: routeing, route-olás, routolás) az informatikában annak kiválasztását jelenti, hogy a hálózatban milyen útvonalon haladjon a hálózati forgalom. Útválasztásról beszélhetünk telefonhálózatok, elektronikus adathálózatok (mint amilyen az internet) vagy akár közúti hálózat esetében is. Ebben a cikkben a csomagkapcsolt számítógép-hálózatokkal foglalkozunk.

Csomagkapcsolt hálózatokban az útválasztás határozza meg a csomagtovábbítást (packet forwarding), azaz a logikai címzéssel ellátott csomagok átvitelét a forrás irányából a cél irányába, köztes hálózati csomópontokon (node-okon) keresztül; ez utóbbiak tipikusan útválasztók (routerek), hálózati hidak (bridge-ek), átjárók (gatewayek), tűzfalak (firewallok) vagy hálózati kapcsolók (switchek). Több hálózati kártyával rendelkező, általános célú számítógépek is képesek csomagokat továbbítani és útválasztást végezni, bár specializált hardver hiányában ezt kisebb teljesítménnyel végezhetik. Az útválasztás általában egy útválasztó tábla (routing table) alapján történik, ami különböző hálózati célállomások felé vezető útvonalak leírását tartalmazza. Az útválasztó memóriájában tartott útválasztó tábla felépítése fontos eleme az útválasztás hatékonyságának. A legtöbb útválasztó algoritmus két cím között egyetlen hálózati útvonalat használ, de léteznek többutas forgalomirányítási (multipath routing) technikák, melyek több alternatív útvonalat használhatnak.

A szűkebb értelemben vett útválasztást gyakran kontrasztba állítják az egyszerű áthidalással (bridging). Az útválasztó azzal a feltételezéssel él, hogy a hálózati címek strukturáltak, és a hasonló címek a hálózaton egymáshoz közel találhatók. Mivel a strukturált címek lehetővé teszik, hogy az útválasztó tábla egyetlen bejegyzése a hálózaton lévő eszközök egy csoportjára vonatkozzon, a strukturált címzés (szűkebb értelemben vett útválasztás) jóval hatékonyabb a strukturálatlan címzésnél (bridge-elés), és az interneten a címzés általánosan elterjedt formájává vált; a bridginget LAN-okban használják tovább.

## 2. Címzéstípusok
Az egyes útválasztási sémák eltérnek egymástól a használt címzés jellege szerint:

* **unicast**: egy csomagot egy meghatározott hálózati csomóponthoz juttat el;
* **broadcasting (üzenetszórás)**: egy csomagot egy hálózat minden csomópontjához juttat el;
* **multicast**: egy csomagot az erre érdeklődést mutató hálózati csomópontok csoportjához juttat el;
* **anycast**: egy csomagot eljuttat egy csoportba tartozó csomópontok valamelyikének, általában a forráshoz legközelebbinek.

Az interneten az üzenettovábbítás leggyakoribb formája az unicast.
