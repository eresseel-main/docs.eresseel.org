## 1. Bevezetés
A Docker egy számítógépes program, amely operációs rendszer szintű virtualizációt végez.

A Docker egy alkalmazást és függőségeit virtuális tárolóba tudja csomagolni, amely bármely Linux, Windows vagy macOS számítógépen futhat. Ez lehetővé teszi, hogy az alkalmazás számos helyen futhasson, például lokálisan vagy Cloud-ban (decentralizált számítástechnika, elosztott számítástechnika és számítási felhő) vagy privát felhőben. Amikor Linuxon fut, a Docker a Linux kernel erőforrás-izolációs szolgáltatásait (például cgroups és kernel névterek) és egy unióképes fájlrendszert (például OverlayFS) használja, hogy lehetővé tegye a konténerek egyetlen Linuxon belüli futtatását. A macOS rendszeren futó Docker, Linux virtuális gépet használ a tárolók futtatásához.

Mivel a Docker-tárolók könnyűek, egyetlen szerver vagy virtuális gép több tárolót is futtathat egyszerre. Egy 2018-as elemzés megállapította, hogy a Docker tipikus használati esetei állomásonként nyolc konténer futtatását jelentik, és az elemzett szervezetek negyede 18 vagy több tárolót futtat. Egyetlen egylapos számítógépre is telepíthető, mint például a Raspberry Pi.

A Linux kernel névterek támogatása többnyire elkülöníti az alkalmazás nézetét az operációs környezettől, beleértve a folyamatfákat, a hálózatot, a felhasználói azonosítókat és a csatolt fájlrendszereket, míg a kernel cgroupjai erőforrás-korlátozást biztosítanak a memória és a CPU számára. A 0.9-es verzió óta a Docker saját összetevőt tartalmaz ("libcontainer segítségével absztrahált virtualizációs interfészek használatára") a közvetlenül a Linux kernel által biztosított virtualizációs lehetőségek használatára.

A Docker magas szintű API -t valósít meg, hogy olyan könnyű konténereket biztosítson, amelyek elszigetelten futtatják a folyamatokat. A Docker konténerek szabványos folyamatok, így lehetséges a kernelfunkciók használata a végrehajtásuk figyelésére – ideértve például a strace-hez hasonló eszközök használatát a rendszerhívások megfigyelésére és közbenjárására.

## 2. Összetevők
A Docker szoftver, mint szolgáltatási ajánlat három összetevőből áll:

* **Szoftver**: A Docker démon, úgynevezett ```bash
dockerd`, egy állandó folyamat, amely kezeli a Docker-tárolókat és kezeli a tárolóobjektumokat. A démon figyeli a Docker Engine API-n keresztül küldött kéréseket. A Docker kliensprogram, az úgynevezett ```bash
docker`, parancssori felületet (CLI) biztosít, amely lehetővé teszi a felhasználók számára a Docker démonokkal való interakciót.

* **Objektumok**: A Docker-objektumok különböző entitások, amelyeket egy alkalmazás összeállítására használnak a Dockerben. A Docker-objektumok fő osztályai a képek, tárolók és szolgáltatások. 

    * A **Docker-tároló** szabványos, beágyazott környezet, amely alkalmazásokat futtat. Egy tárolót a Docker API vagy CLI segítségével kezelnek.
    * A **Docker-kép** egy csak olvasható sablon, amelyet konténerek készítésére használnak. A képeket alkalmazások tárolására és szállítására használják. <23>
    * A **Docker-szolgáltatás** lehetővé teszi a konténerek méretezését több Docker-démon között. nevezik, amely Az eredményt rajnak együttműködő démonok halmaza, amelyek a Docker API-n keresztül kommunikálnak. <23> 

* **Nyilvántartások**: A Docker-nyilvántartás a Docker-képfájlok tárháza. A Docker-kliensek a regisztrációs adatbázisokhoz csatlakoznak, hogy letöltsék ("lehúzzák") a képeket vagy töltsék fel ("push") az általuk készített képeket. A nyilvántartások lehetnek nyilvánosak vagy magánjellegűek. A fő nyilvános nyilvántartás a <Docker Hub>(https://hub.docker.com/). A Docker Hub az alapértelmezett beállításjegyzék, ahol a Docker képeket keres. A Docker-nyilvántartások lehetővé teszik az eseményeken alapuló értesítések létrehozását is.

## 3. Telepítés
A Dockert a legegyszerűbben az alábbi parancsal lehet telepíteni Linux OS-ekre:
`wget -qO- https://get.docker.com | bash`

## 4. Dockerfile példa
```bash
dockerfile
# syntax=docker/dockerfile:1
FROM ubuntu:18.04           # kiindulo image
COPY . /app                 # ${PWD}-bol masolas az /app folderbe
RUN make /app               # make parancs futtatasa az /app folderben
CMD python /app/app.py      # python parancsal futtatom az app.py filet amig a kontenert nem allitom le
```
## 5. Alap parancsok
A leggyakrabban használt parancsok.
### 5.1. Docker verzió ellenőrzése
```bash
docker --version
```
### 5.2. Docker image letöltése
```bash
docker pull alpine:latest
```
### 5.3. Docker images listázása
```bash
docker images
```
### 5.4. Konténer indítása interaktív módban
```bash
docker run -it alpine:latest sh
```
### 5.5. Konténer indítása háttérben futással
```bash
docker run -d alpine:latest
```
### 5.6. Futó konténerek listázása
```bash
docker ps
```
### 5.7. Minden konténer listázása (leállított is)
```bash
docker ps -a
```
### 5.8. Futó konténerbe belépés
```bash
docker exec -it <CONTAINER_ID> bash
```
### 5.9. Futó konténer logjainak megnézése
```bash
docker logs <CONTAINER_ID>
```
### 5.10. Futó konténer információinak listázása
```bash
docker inspect <CONTAINER_ID>
```
### 5.11. Konténer leállítása
```bash
docker stop <CONTAINER_ID>
```
### 5.12. Konténer törlése
```bash
docker rm <CONTAINER_ID>
```
### 5.13. Dockerfile építése
```bash
docker build -t container_name:latest .
```