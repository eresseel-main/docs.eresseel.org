## 1. Bevezetés
A Graylog, Inc. egy naplókezelő szoftvercég a texasi Houstonban. Fő termékük egy naplókezelő szoftver, amelyet Graylognak is hívnak.

## 2. Előzmények
A Graylogot, korábban Torch 2009-ben alapította Lennart Koopmann, és nyílt forráskódú projektként indult Hamburgban, Németországban. A központ a texasi Houstonban van.

2014 októberében a Mercury megkezdte első befektetését a Graylogba. Az e.ventures és a Mercury Asset Management későbbi befektetők. A többi befektető a Crosslink Capital, a Draper Associates és a High-Tech Gründerfonds volt.

A Graylog 2016-ban adta ki első kereskedelmi ajánlatát, amellyel elérhetővé tette vállalati termékét. 2018-ra a Graylog több mint 35 000 telepítésre nőtt világszerte.

A Graylog rendszeresen részt vesz olyan konferenciákon, mint a DEF CON, DerbyCon, Black Hat USA, NolaCon és a Security BSides.