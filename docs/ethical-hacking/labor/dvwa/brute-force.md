## 1. Bevezetés
Ez a leírás a DVWA Low, Medium és High biztonsági szintjeinek brute force támadására vonatkozóan. A cél egy HTTP bejelentkezési oldal brute force támadása.

## 2. Low Security Level (Alacsony biztonsági szint)
A Low biztonsági szinten a DVWA minimális védelmet kínál, így a brute force támadás könnyen végrehajtható, mivel nincsenek olyan mechanizmusok, mint például a próbálkozások számának korlátozása vagy az automatikus zárolás. Indítsd el a Hydra-t a következő parancs használatával.
```bash
hydra -l admin -P /usr/share/wordlists/rockyou.txt http-get://127.0.0.1:4280/DVWA/vulnerabilities/brute/
```

- -l admin: Az adminisztrátori felhasználónevet adja meg.
- -P /usr/share/wordlists/rockyou.txt: A jelszólistát adja meg (például a rockyou.txt fájlt).
- http-get://127.0.0.1/DVWA/vulnerabilities/brute/: A DVWA bejelentkezési oldalának URL-je.

Ebben a szintben a brute force támadás gyorsan végrehajtható, és valószínűleg sikeres lesz.

## 3. Medium Security Level (Közepes biztonsági szint)
A Medium biztonsági szinten a DVWA néhány alapvető védelmi mechanizmust tartalmaz, például a próbálkozások számának korlátozását rövid időn belül. Ha túl sok sikertelen próbálkozás történik, akkor a rendszer blokkolja a további próbálkozásokat, így a brute force támadás lassabb lesz. A rendszer itt már lassítja a támadást, de a brute force még mindig végrehajtható a megfelelő beállításokkal.
```bash
hydra -l admin -P /usr/share/wordlists/rockyou.txt -t 4 -w 3 http-get://127.0.0.1:4280/DVWA/vulnerabilities/brute/
```
- -t 4: Négy szálat (thread) használ a gyorsabb végrehajtáshoz.
- -w 3: 3 másodperces késleltetést ad hozzá minden próbálkozás után a rate-limiting megkerülésére.

## 4. High Security Level (Magas biztonsági szint)
A High biztonsági szinten a DVWA erősebb védelmi mechanizmusokat alkalmaz, mint például a fiók zárolása után több sikertelen próbálkozás és CAPTCHA. A brute force támadás sokkal nehezebbé válik, mivel a rendszer figyeli és blokkolja a túl sok próbálkozást, és a CAPTCHA megjelenhet.
Ha a CAPTCHA megjelenik, a brute force támadás automatizálása nem lesz egyszerű. A CAPTCHA megkerüléséhez például OCR (Optical Character Recognition) technikák vagy kézi beavatkozás szükséges.

Alternatív megoldás lehet, ha a támadást az alap bejelentkezési mechanizmuson kívüli más sérülékenységek kiaknázásával kombinálod.
