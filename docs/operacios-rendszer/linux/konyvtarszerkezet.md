## 1. Bevezetés
A fájlokat valahogyan rendszerezni kell, és erre a legelfogadhatóbb megoldást a hierarchikus könyvtárszerkezet biztosítja. A hierarchikus szerkezet azt jelenti, hogy a könyvtárak tartalmazhatnak fájlokat és alkönyvtárakat is. A könyvtárszerkezet alapját a gyökérkönyvtár (root directory) adja.

Ebben a könyvtárszerkezetben az egyes fájlokra úgy tudunk hivatkozni, hogy meg kell adnunk, mely könyvtárakon keresztül érhetjük el a könyvtárszerkezet gyökerétől kiindulva. Ezt nevezik a fájl teljes (vagy abszolút) elérési útjának. Az elérési út egyes tagjait a „/” jel választja el egymástól, a legelső „/” jel a hierarchia legtetején lévő úgynevezett gyökér-könyvtárat jelöli, amelyből a többi elágazik.

Nagyon kényelmetlen lenne, ha minden fájl elérését csak a gyökértől tudnánk meghatározni, ezért kialakították a munka-könyvtár fogalmát (working directory, lekérdezhető a pwd utasítással). Így elég a munka-könyvtár alatt lévő könyvtárak nevét felsorolni a bennük lévő fájlok eléréséhez. Ez a relatív elérési út.

A fájlokra való hivatkozáskor nem kell tudnunk azt, hogy milyen fizikai eszközön (floppy, CD-ROM, merevlemez, stb.) helyezkednek el, elég a „könyvtár hierarchia”-beli helyüket megadnunk. A különböző fizikai (vagy logikai) eszközök (pl. floppy, merevlemez partíciója, stb.) a könyvtárszerkezetből érhetők el a csatlakoztatási könyvtárukon (mount point) keresztül. Ennek érdekében automatikusan, vagy manuálisan csatlakoztatni kell a különböző eszközöket a könyvtárszerkezethez (mount utasítás). A különböző eszközökön különböző fájlrendszerek lehetnek, de ezt a csatlakoztatás után nem észleli a felhasználó. Pontosabban nem észleli a fájlrendszer típusát, csak bizonyos tulajdonságait (pl. Cd-rom csak olvasható). A különböző eszközöket az operációs rendszer leállítása folyamán le kell választani a könyvtárszerkezetről (umount utasítás). Ez szintén automatikusan vagy manuálisan kell, hogy megtörténjen. Az automatizmushoz a „/etc/fstab” nevű fájlt kell megfelelően megszerkeszteni. Ez a fájl tartalmazza azokaz az információkat, amelyek az automatikus csatlakoztatáshoz, illetve lecsatlakoztatáshoz szükségesek (az eszköz neve, az eszköz fájlrendszerének típusa, a könyvtárszerkezet csatlakoztatási pontja, stb.).

A Linux gyökérkönyvtárában helyezkednek el a rendszerkönyvtárak és a felhasználók „home” könyvtárai. A rendszerkönyvtárakban a Linux rendszer működését segítő alkalmazások találhatóak, míg a /home könyvtárban lévő könyvtárak a felhasználók bejelentkező könyvtárai. A könyvtáraknak azt a sorozatát, amelyik a gyökérkönyvtárból kiindulva elérhető egy könyvtár, a könyvtár elérési útvonalának hívjuk (path). A fájl neve és az elérési útvonal együttesen adják meg a fájl telje elérési útvonalát.

## 2. A Linux alapvető könyvtárai a gyökérkönyvtárban

* `bin`: A futtatható parancsok könyvtára (binaries). A `/bin` az alaprendszerhez szükséges programokat tartalmazza.  Több bin könyvtár is található ezen kívül, például a `/usr/bin` és a `/usr/sbin`. Ezekbe a felhasználó által telepített programok kerülnek. Bár ez nem törvényszerű, de általában a bin könyvtárakban a minden felhasználó által elérhető állományok kerülnek az sbin könyvtárakba pedig olyan rendszereszközök, melyeket például csak rendszergazdák használnak.
* `/boot`: A rendszer indításához szükséges állományok és a rendszermag helye (grub, vmlinuz, stb.).
* `/cdrom`: Ez egy szimbolikus link, általában a /media/cdrom könyvtárra. Ez utóbbi alá csatolódik be a CD meghajtó egység.
* `/dev`: A rendszerhez csatlakozott speciális eszközfájlok, csatolható különleges állományok (devices).
* `/etc`: Ez a könyvtár a gyűjtőhelye a különböző programok globális konfigurációs fájljainak. Itt minden konfigurációs állomány egyszerű szövegfájlba van mentve, ezért két nagy előnye van:

    * az állományok akkor is egyszerűen elérhetők, ha a rendszer egyébként használhatatlan,
    * gyakran rengeteg megjegyzés segít eligazodni a sorok közt.

* `/home`: Rendszer felhasználóinak saját könyvtáraik. Például `/home/tesztelek` könyvtár a tesztelek felhasználóhoz tartozik. Az adott könyvtáron belül a felhasználónak korlátlan joga van.
* `/lib`: Az induláshoz szükséges osztott rendszerkönyvtárak (libraries). Ezek szerepe megegyezik a Windows rendszereken használt dll fájlokéval. Továbbá tartalmazza a rendszerhez csatolható modulokat, meghajtó-programokat.
* `/lost+found`: Ez a könyvtár nem is a Linux, mint inkább a fájlrendszer része. Ez a könyvtár a naplózó fájlrendszer (ext3 ext4) okán került oda ahova. Minden egyes ext partíció gyökér könyvtárban van egy lost+found könyvtár. 
Arra szolgál, hogy a rendszer fájlrendszer sérülés esetén ide állítja helyre a sérült állományokat. Szerencsére erre valószínűleg soha nem lesz szükségünk.
* `/media`: Általában a `/media` könyvtár alá kerülnek befűzésre a cserélhető médiák. Pl.  CD/DVD eszközök, pendirve illetve a floppy.
* `/mnt`: Ide kerülnek (általában) befűzésre a fix partíciók.
* `/opt`: Idegen forrásból származó (tehát nem a rendszer fejlesztői által készített csomagból) felhasználói programok opcionális telepítési helye. Ritkán használják.
* `/proc`: Itt találhatóak az éppen futó műveletek, fájlként leképezve, sorszámozva, illetve információk a rendszerről: processzorról, memóriáról, stb.
* `/root`: Ez a rendszeradminisztrátor saját home könyvtára. Igazából a / könyvtártól kezdve a root-nak mindenhova joga van írni - olvasni, de praktikusabb, ha van neki is egy saját tárhelye, mint minden felhasználónak. Azért nem a /home/ könyvtár alatt van a `/root` könyvtár, mert az gondot okozna akkor, ha a `/home` más fizikai tárolón van és probléma esetén nem lehet felcsatolni.
* `/sbin`: Hasonló szerepe van mind a /bin könyvtárnak azzal a kivétellel, hogy olyan rendszereszközök kerülnek ide, melyeket csak a rendszergazdák használnak.
* `/sys`: A `/sys` egy szintetikus fájlrendszer, a `/proc` párja, amely egy csomó információt szolgál a kernel állapotáról.
* `/tmp`: A futó programoknak szükségük van/lehet átmeneti fájlokra. Ezek kerülnek ide. Ez a másik olyan könyvtár, amely alapértelmezettben írható minden felhasználó számára.
* `/usr`: Ez egy „gyűjtő” könyvtár. Csak olvasható. Ez alatt található „majdnem minden”. Pl.:
Telepített programok nagy része, hagyományból ide szoktunk forrásokat pakolni (`/usr/src`), és azt leforgatni. Itt találhatók a dokumentációk (`/usr/doc`), itt találhatók az ikonok nagy része, stb.
* `/var`: Ez szintén egy gyűjtő könyvtár. Itt találhatók a naplófájlok (`/var/log/maillog`) egyes programok hosszabb ideig tárolt, mégis átmeneti fájljai, (`/var/cache/apt/archives`) alapértelmezettben a felhasználói levél fiókok (`/var/mail/user`).