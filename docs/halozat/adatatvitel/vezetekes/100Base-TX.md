## 1. Bevezetés
A 100Base-TX a gyors Ethernet 5-ös kategóriájú csavart érpárokkal való kábelezési megoldása.

A gyors Ethernet (fast Ethernet) szabványt, a 802.3u-t 1995 júniusában fogadta el az IEEE.

A régi keretformátum megtartása, az interfészek változatlansága, a bitidő 100ns-ról 10ns-ra való csökkentése miatt néhány áldozatot meg kellett hozni. Műszaki szempontból lehetséges lett volna a 10Base-5 vagy a 10Base-2 megőrzése, és annak ellenére, hogy a kábelhosszak lecsökkennek, az ütközéseket lehetett volna észlelni. A 10Base-T kábelezés előnyei azonban annyira elsöprőek voltak, hogy a gyors Ethernet kábelezését erre a megoldásra alapozták: a gyors Ethernet elosztókat és kapcsolókat használ, a sok bosszúságot okozó vámpír-csatlakozók és a BNC-csatlakozók nem megengedettek.

## 2. Kábelezés
A kábelezés tervezésekor három lehetséges kábelt vett a bizottság figyelembe:

* 3-as kategóriájú sodrott érpár (lásd: 100Base-T4)
* 5-ös kategóriájú sodrott érpár
* Fényvezető szál vagy kábel (lásd: 100Base-FX).

A 3-as kategóriájú érpár okozta a legnagyobb gondot. Azt adottságnak tekintették, hogy minden épületben 3-as kategóriájú érpárok (legalább 4), vagy jobbak rendelkezésre álltak. Azt az előnyt, hogy ezeket a meglévő kábelezéseket használják fel a gyors Ethernethez, senki sem volt hajlandó feladni.

A 3-as kategóriájú érpár hátránya volt az a tény, hogy a 200 Mbaudos jeleket (Manchester kódolással) nem tudta 100 méterre továbbítani, márpedig a 10Base-T szabvány szerint ez követelmény.

Az 5-ös kategóriájú kábelezés, azaz a 100Base-TX alkalmazásakor a fenti probléma nem jelentkezik, mivel a szükséges sávszélesség rendelkezésre áll.

Állomásonként mindössze 2 sodrott érpár szükséges, egy az elosztó felé, egy az elosztó felől. A közvetlen bináris kódolás helyett itt a 4B/5B sémát használják, amit az FDDI-nél is, így a kompatibilitás megoldott. Az 5 órajelből álló csoportokban kétféle szimbólum használható, így összesen 32 kombinációhoz jutunk. Ezek közül 16-ot használnak a 0000, 0001, 0010, …, 1111 négyes csoportok átvitelére. A fennmaradó 16 kombináció közül néhány vezérlési funkciót lát el, például a kerethatár kijelölését. A felhasznált kombinációk kiválasztásánál elsődleges szempont volt, hogy a szinkronizáláshoz sok jelátmenet legyen bennük. A 100Base-TX rendszer duplex: az állomások egy időben adhatnak és vehetnek, 100Mb/s-os sebességgel.

A 100Base-TX-et és 100Base-T4-et emlegetik 100Base-T összefoglaló névvel.
