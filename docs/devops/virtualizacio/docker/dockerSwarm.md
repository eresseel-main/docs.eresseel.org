## 1. Bevezetés
A **Docker Swarm** natív fürtözési funkcionalitást biztosít a Docker-tárolókhoz, amely a Docker-motorok egy csoportját egyetlen virtuális Docker-motorrá alakítja. A Docker 1.12-es és újabb verzióiban a Swarm mód a Docker Engine-hez van integrálva. A `docker swarm` CLI segédprogram segítségével a felhasználók Swarm-tárolókat futtathatnak, felfedezési tokeneket hozhatnak létre, csomópontokat listázhatnak a fürtben stb. A `docker node` CLI segédprogram segítségével a felhasználók különféle parancsokat futtathatnak a raj csomópontjainak kezeléséhez, például listázhatják a raj csomópontjait, frissíthetik a csomópontokat, és eltávolíthatják a csomópontokat a rajból. A Docker a Raft konszenzus algoritmussal kezeli a rajokat. Raft szerint a frissítés végrehajtásához a Swarm csomópontok többségének meg kell állapodnia a frissítésről.

## 2. Telepítés
### 2.1. Szerver gépre
```bash
docker swarm init --advertise-addr <MANAGER-IP>
```
### 2.2. Helyi gépre
```bash
docker swarm init --advertise-addr 127.0.0.1
```
## 3. docker-compose.yml példa
```docker-compose
version: "3.9"
services:
  redis:
    image: redis:latest
    deploy:
      replicas: 2           # 2 redis konténert indít
```
## 4. Alap parancsok
A leggyakrabban használt parancsok.
### 4.1. Docker Swarm indormáció
```bash
docker info
```
### 4.2. Manager token listázása
```bash
docker swarm join-token manager
```
### 4.3. Worker token listázása
```bash
docker swarm join-token worker
```
### 4.4 Manager node hozzáadása a Swarmhoz
```bash
docker swarm join \
  --token  SWMTKN-1-49nj1cmql0jkz5s954yi3oex3nedyz0fb0xx14ie39trti4wxv-8vxv8rssmk743ojnwacrr2e7c \
  192.168.99.100:2377
```
### 4.5. Worker node hozzáadása a Swarmhoz
```bash
docker swarm join \
  --token  SWMTKN-1-2iy7hgk0tec1yu3xyiw0zid8tuyor94x3em176qwbkvadh8mzj-ctxus47py68c5ksrygjz6q96v \
  192.168.99.100:2377
```
### 4.6. Docker node listázása
```bash
docker node ls
```
### 4.7. Swarm stack deploy
```bash
docker stack deploy --compose-file docker-compose.yml <stack_name>
```
### 4.8. Swarm stack listázása
```bash
docker stack ls
```
### 4.9. Swarm stack-en belüli futó konténerek listázása
```bash
docker stack ps
```
### 4.10. Swarm stack törlése
```bash
docker stack rm
```
### 4.11. Swarm stack-en belüli szolgáltatások listázása
```bash
docker stack services
```
### 4.12. Futó konténerek listázása
```bash
docker service ls
```