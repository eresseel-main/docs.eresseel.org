## 1. Bevezetés
A szoftverfejlesztés hajnalán úgy vélték, hogy a fejlesztés lineáris folyamat, aminek az első szakaszaiban kell a megfelelő figyelmet és technikákat alkalmazni, és akkor biztos lesz a siker. Számtalan sikertelen szoftverfejlesztési projekt bebizonyította, hogy ez tévedés.

Ha ön 40+ éves, talán van olyan élménye, amikor a pályája kezdetén a cégénél szoftverfejlesztésbe kezdtek, ami teljes kudarccal végződött. Mire a szoftver elkészült, kiderült ugyanis, hogy nem is arra volt az ügyfélnek szüksége, amin a fejlesztők hónapokat dolgoztak. A karrierem szoftverfejlesztő cégnél kezdtem, és jól emlékszem ezekre. Feltételezem, hogy a sok fejlesztési kudarc is hozzájárult ahhoz, vagy inkább kikényszerítette a megoldást: az agilis módszertant.

## 2. Kiáltvány
2001-ben a szoftveripar nagyjai együttesen előálltak egy „kiáltvánnyal az agilis szoftverfejlesztésért”. A kiáltvány honlapján található 12 „agilis alapelv” a következő:

* Legfontosabbnak azt tartjuk, hogy az ügyfél elégedettségét a működő szoftver mielőbbi és folyamatos szállításával vívjuk ki.
* Elfogadjuk, hogy a követelmények változhatnak akár a fejlesztés vége felé is. Az agilis eljárások a változásból versenyelőnyt kovácsolnak az ügyfél számára.
* Szállíts működő szoftvert néhány hetenként vagy havonként, lehetőség szerint a gyakoribb szállítást választva!
* Az üzleti szakértők és a szoftverfejlesztők dolgozzanak együtt mindennap, a projekt teljes időtartamában!
* Építsd a projektet sikerorientált egyénekre! Biztosítsd számukra a szükséges környezetet és támogatást, és bízz meg bennük, hogy elvégzik a munkát!
* Az információátadás fejlesztési csapaton belüli leghatásosabb és leghatékonyabb módszere a személyes beszélgetés.
* A működő szoftver az előrehaladás elsődleges mércéje.
* Az agilis eljárások a fenntartható fejlesztést pártolják. Fontos, hogy a szponzorok, a fejlesztők és a felhasználók folytonosan képesek legyenek tartani egy állandó ütemet.
* A műszaki kiválóság és a jó terv folyamatos szem előtt tartása fokozza az agilitást.
* Elengedhetetlen az egyszerűség, azaz az elvégzetlen munkamennyiség maximalizálásának művészete.
* A legjobb architektúrák, követelmények és rendszertervek az önszerveződő csapatoktól származnak.
* A csapat rendszeresen mérlegeli, hogy miképpen lehet emelni a hatékonyságot, és ehhez hangolja és igazítja a működését.

## 3. Alapelvek
Tekintsük át az agilis módszertan alapelveit, melyeket ha a vezetéstudomány szemszögéből közelítünk meg, rájöhetünk, hogy nem minden új a nap alatt.

### 3.1. Az ügyféllel legyen megfelelő a kapcsolattartás
Az agilis fejlesztés sajátossága, hogy szoros kapcsolatot tételez fel a szoftver felhasználója és fejlesztői között. Másképp fogalmazva, a fejlesztés csak akkor lehet eredményes, ha az érintettek közötti kommunikáció megfelelő mélységű.

### 3.2. Egy terv legyen hihető (reális)
Az innovatív (kockázatosabb, bizonytalanabb) feladatokat (projekteket) csak olyan időtávra érdemes részletesen megtervezni, amelyben a tervtől való eltérés mértéke várhatóan nem lesz elfogadhatatlanul nagy. Ez a tervezési alapelv a klasszikus projektmenedzsment eszköztárából sem hiányzik. Ami újszerűnek tekinthető az agilis világban, az a tervezés mindenhatóságába vetett hit megkérdőjelezése.

### 3.3. Önszerveződéssel javítsuk a csapat teljesítményét
Az agilis munkavégzés további sajátossága, hogy a munkatársak maguk vállalkoznak egyes feladatok elvégzésére. Ez sem nevezhető új ötletnek, hiszen az önszerveződő csapatok előnyei már régóta ismeretesek.

Az önszerveződő csapatok jellegzetessége, hogy a csapat tagjai maguk döntik el, mely munkafázissal foglalkoznak, az egyes tagok képesek többféle munka ellátására, és maguk határozzák meg a jutalmazás és munkakövetés módját.

Az önszerveződő csapatokban dolgozók csapatkohéziója erősebb, és így termelékenysége gyakran meghaladja a szokványos munkaszervezéssel vezetett csapatokét. Nem meglepő tehát, hogy az agilis fejlesztési módszerek eredményesebbnek és hatékonyabbnak bizonyulnak, amennyiben az elvégzendő feladat természete lehetővé teszi annak önszerveződő csapat általi elvégzését.

Nyilvánvalóan a csapat tagjainak száma és személyisége is befolyásolja azt, hogy ez a fajta vezetési módszer alkalmazható-e. 10 főnél nagyobb létszámú csapatoknál gondokba ütközhet a kommunikáció. A csapat tagjainak képesnek kell lenniük arra, hogy elvégezzék a vállalt feladatot, és elég ambiciózusnak ahhoz, hogy kellően kezdeményezők legyenek.

### 3.4. A feladatok nyomon követése alapuljon tényeken, és legyen transzparens
Mint korábban láttuk, a Scrum módszer előírja a „napi Scrum” alkalmazását, ami tulajdonképpen az előrehaladás kontrollja.

A csapat minden tagja láthatja, hogy a többiek mit végeztek el egy nap alatt. Az átláthatóságot segíti az is, hogy a csapat tagjai egy helyiségben dolgoznak. Az előrehaladást és a hátralevő feladatokat lehetőleg grafikusan, mindenki által jól látható helyen teszik közzé. Nincs kizárólag a feladatok követésével foglalkozó munkatárs, mindenkinek egyszerre kell dolgoznia és a nyomon követéssel foglalkoznia.

### 3.5. A vizualizáció, a nyomon követés eszköze
Az agilis fejlesztések további jellegzetessége a vizualizáció, ami legtöbbször egy nyomon követést szolgáló tábla alkalmazását jelenti. Ez a módszer – feltéve, hogy a táblán a valósággal megegyező adatok szerepelnek – egyfajta demokratikus hozzáállást tükröz: mindenki számára egyértelmű, hogy ki mit végzett el, és mi lesz a következő lépés.

Ez tulajdonképpen a projekt követésének olyan eszköze, ami éppen a nyilvánossága miatt egyfajta kényszert is jelent a projekttagok számára, ugyanis nemcsak a vezető látja az elvégzett munkát, hanem a csapat összes tagja azonos képet kap.

### 3.6. Tartsunk önreflexiót
Az agilis módszertan jellegzetessége a gyakori visszacsatolás, az önreflexió. A Scrum sprintjei végén előírt visszatekintés célja a munkavégzés módszerének javítása. A tanulás végső soron javítja a csapat (szervezet) eredményességét és hatékonyságát. A tapasztalatok kiértékelése szintén régóta része a projektmenedzsment technikai repertoárjának. Az agilis módszertanban ezek gyakorisága nagyobb a megszokottnál.

Az agilis módszertan leginkább a szoftverfejlesztés során terjedt el, de az elvei átvihetők más feladatokra is. Gondolja át, hogy az ön területén miként látja alkalmazhatónak, és milyen lépéseket tesz az agilis szemlélet megvalósítása érdekében!

### 3.7. User story, epic, spike
A user story-k rövid “történetek”, amiket a megrendelők, felhasználók szeretnének viszontlátni a termékben. Ezek jó esetben egymástól független követelmények, amik több formában is leírhatóak. Formáktól függetlenül ezek a leírások tesztelhetővé is teszik az adott elemet, illetve célt is meghatároznak neki (pl. én mint – azt szeretném, hogy – annak érdekében).

Az epicek nagyobb “csomagok”, melyek általában több, ritkábban egy user story-ra bonthatóak. Ezek szerepe az iterációt megelőző tervezéseken igazán hangsúlyos, melyek többek közt segítenek a minimálisan elvárt funkcionalitás (MVP) meghatározásában elindulni.

Természetesen előfordulhat, hogy a megrendelői elvárások megvalósításához belső feladatokat kell ellátni, esetleg körül kell járni egy-egy témát, hogy azt milyen módszerrel, technológiával lehet megvalósítani. A csapat ezekre spike-okat definiálhat, melyek megvalósítása természetesen energiát igényel.

A csapat általi becslés szinte bármilyen dimenzióban történhet, story pontokban, időben, kavicsokban. A lényeg, hogy a csapat egymáshoz képest viszonyítani tudja a feladatok méretét és azokról döntést tudjon hozni, hogy mi férhet bele egy adott iterációba.

### 3.8. Agilis szoftverfejlesztési módszerek
Az agilis kiáltvány után számos módszertani megközelítés látott napvilágot (Feature Driven Development, Scrum, Test Driven Development, Kanban, Crystal Clear), melyek közül a Scrum módszertan tett szert a legnagyobb ismertsége.