## 1. Bevezetés
A Terraform egy nyílt forráskódú, infrastruktúra, mint kód szoftvereszköz, amelyet a HashiCorp hozott létre. A felhasználók ennek az eszköznek a segítségével határozzák meg és biztosítják az adatközponti infrastruktúrát. A HashiCorp Configuration Language (HCL) néven ismert deklaratív konfigurációs nyelv vagy opcionálisan JSON file-ban kell deklarálni az erőforrásokat.

## 2. Tervezés
A Terraform külső erőforrásokat (például nyilvános felhő infrastruktúrát, privát felhő infrastruktúrát, hálózati eszközöket, szoftvereket szolgáltatásként és platformot szolgáltatásként) kezel "szolgáltatókkal". A HashiCorp kiterjedt listát tart fenn a hivatalos szolgáltatókról, és integrálható a közösség által fejlesztett szolgáltatókkal is. A felhasználók erőforrások deklarálásával léphetnek kapcsolatba a Terraform szolgáltatókkal vagy adatforrások hívásával. Ahelyett, hogy kötelező parancsokat használna az erőforrások kiépítéséhez, a Terraform deklaratív konfigurációt használ a kívánt végső állapot leírására. Miután a felhasználó meghívja a Terraformot egy adott erőforráson, a Terraform CRUD műveleteket hajt végre a felhasználó nevében a kívánt állapot elérése érdekében. Az infrastruktúra mint kód modulokként írható, elősegítve az újrafelhasználhatóságot és a karbantarthatóságot.

A Terraform számos felhő-infrastruktúra szolgáltatót támogat, mint például az Amazon Web Services, a Microsoft Azure, az IBM Cloud, a Serverspace, a Google Cloud Platform, DigitalOcean, Oracle Cloud Infrastructure, Yandex.Cloud,VMware vSphere és OpenStack.

A HashiCorp 2017-ben elindított Terraform Module Registry-t tart fenn. 2019-ben a Terraform bevezette a Terraform Enterprise nevű fizetős verziót a nagyobb szervezetek számára.

## 3. Terraform működése
A Terraform felhőplatformokon és egyéb szolgáltatásokon hoz létre és kezel erőforrásokat az alkalmazásprogramozási felületeiken (API-k) keresztül. A szolgáltatók lehetővé teszik, hogy a Terraform gyakorlatilag bármilyen platformmal vagy szolgáltatással működjön elérhető API-val.

A HashiCorp és a Terraform közösség már több ezer szolgáltatót írt fel sok különböző típusú erőforrás és szolgáltatás kezeléséhez. Az összes nyilvánosan elérhető szolgáltató megtalálható a Terraform Registry-ben, beleértve az Amazon Web Services (AWS), Azure, Google Cloud Platform (GCP), Kubernetes, Helm, GitHub, Splunk, DataDog és még sok más szolgáltatást.

Az alapvető Terraform munkafolyamat három szakaszból áll: 

* **Írás**: Te határozod meg az erőforrásokat, amelyek több felhőszolgáltató és szolgáltatás között is lehetnek. Létrehozhatsz például egy konfigurációt egy alkalmazás virtuális gépeken való üzembe helyezéséhez egy Virtual Private Cloud (VPC) hálózaton biztonsági csoportokkal és terheléselosztóval.
* **Terv**: A Terraform végrehajtási tervet készít, amely leírja az infrastruktúrát, amelyet a meglévő infrastruktúra és konfigurációja alapján létrehoz, frissít vagy megsemmisít.
* **Alkalmazás**: Jóváhagyáskor a Terraform a javasolt műveleteket a megfelelő sorrendben hajtja végre, figyelembe véve az erőforrás-függőségeket. Például, ha frissíti egy VPC tulajdonságait, és módosítja a virtuális gépek számát abban a VPC-ben, a Terraform újra létrehozza a VPC-t a virtuális gépek méretezése előtt. 