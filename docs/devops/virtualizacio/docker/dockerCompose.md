## 1. Bevezetés
A **Docker Compose** egy többtárolós Docker-alkalmazások meghatározására és futtatására szolgáló eszköz. YAML fájlokat használ az alkalmazás szolgáltatásainak konfigurálásához, és egyetlen paranccsal végrehajtja az összes tároló létrehozását és elindítását. A `docker-compose` CLI segédprogram lehetővé teszi a felhasználók számára, hogy parancsokat futtassanak egyszerre több tárolón, például képek készítéséhez, tárolók méretezéséhez, leállított tárolók futtatásához stb. A képkezeléssel vagy a felhasználói interaktív beállításokkal kapcsolatos parancsok nem relevánsak a Docker Compose alkalmazásban, mert egy tárolóhoz szólnak. A docker-compose.yml fájl egy alkalmazás szolgáltatásainak meghatározására szolgál, és különféle konfigurációs lehetőségeket tartalmaz.

## 2. Telepítés
### 2.1. Ubuntu-ra telepítés
```bash
sudo apt-get update
sudo apt-get install docker-compose-plugin
docker compose version
```
### 2.2. Manuális telepítés
```bash
DOCKER_CONFIG=${DOCKER_CONFIG:-$HOME/.docker}
mkdir -p $DOCKER_CONFIG/cli-plugins
curl -SL https://github.com/docker/compose/releases/download/v2.12.2/docker-compose-linux-x86_64 -o $DOCKER_CONFIG/cli-plugins/docker-compose
chmod +x $DOCKER_CONFIG/cli-plugins/docker-compose
sudo chmod +x /usr/local/lib/docker/cli-plugins/docker-compose
docker compose version
```

## 3. docker-compose.yml példa
```docker-compose
version: "3.9"              
services:
  web:                      # indit egy web nevű konténert
    build: .                # a ${PWD} mappában levő Dockerfilet megépíti mielőtt elindítja a konténert
    ports:
      - "8000:5000"         # a konténer 5000-es portját kinyitja a host 8000 portjára
  redis:
    image: "redis:alpine"   # indít egy redis nevű konténert a redis image-ből
```

## 4. Alap parancsok
A leggyakrabban használt parancsok.
### 4.1. Docker Compose verzió ellenőrzése
```bash
docker compose version
```
### 4.2. Konténer indítása háttérben futással
```bash
docker compose up -d
```
### 4.3. Futó konténerek listázása
```bash
docker compose ps
```
### 4.4. Konténer leállítása
```bash
docker compose stop <CONTAINER_ID>
```