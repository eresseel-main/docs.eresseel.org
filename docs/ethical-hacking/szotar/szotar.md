## 1. Beveztés
A wordlist (szólista) egy fájl vagy adatbázis, amely szavakat tartalmaz egy adott célra. Leggyakrabban informatikai és kiberbiztonsági területeken használják. Itt van néhány példa arra, mire használják a wordlist-eket:

* **Jelszótörés**: A wordlistek gyakran jelszótöréshez használt eszközök (pl. brute force támadások vagy szótári támadások) alapjául szolgálnak. A program ezeket a szavakat próbálja ki jelszóként egy adott rendszer ellen.
* **Nyelvfeldolgozás**: Szólistákat használnak természetes nyelvfeldolgozás (NLP) során is, például szövegelemzéshez vagy fordítási rendszerek fejlesztéséhez.
* **Automatizált tesztelés**: Szólisták alkalmazhatók webes alkalmazások vagy más szoftverek biztonsági tesztelésére, például azzal, hogy különböző jelszavakat, felhasználóneveket vagy URL-eket próbálnak ki.

A wordlist-ek tehát kulcsfontosságú eszközök a biztonságtechnikai tesztek során, de más területeken is hasznosak lehetnek, ahol fontos a szavak rendszerezett és gyors feldolgozása.

## 2. Hasznos linkek wordlist-ekhez
### 2.1. Alapertelmezett jelszó listák Kali-ban
* /usr/share/wordlists/*
* /usr/share/seclists/     # Ennek beápített parancsa is van: `seclists`

### 2.2. Neten található wordlistek
* https://github.com/topics/wordlist
* https://github.com/maverickNerd/wordlists
* https://github.com/xajkep/wordlists
* https://github.com/Karanxa/Bug-Bounty-Wordlists
* https://github.com/sindresorhus/awesome        # folyamatosan frissitik
* https://github.com/n0kovo/n0kovo_subdomains
* https://github.com/berzerk0/Probable-Wordlists
* https://github.com/danielmiessler/SecLists     # folyamatosan frissitik
* https://crackstation.net/crackstation-wordlist-password-cracking-dictionary.htm
* https://github.com/cr0hn/nosqlinjection_wordlists
* https://github.com/thoughtworks/dadoware
* https://github.com/mmatje/br-wordlist
* https://github.com/assetnote/wordlists         # folyamatosan frissitik
* https://github.com/trickest/wordlists          # folyamatosan frissitik
* https://blog.g0tmi1k.com/2011/06/dictionaries-wordlists/
* https://github.com/first20hours/google-10000-english
