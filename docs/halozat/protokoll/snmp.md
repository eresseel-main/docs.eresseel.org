## 1. Bevezetés
Az SNMP (Simple Network Management Protocol) egy hálózatfelügyeleti protokoll, amelyet az informatikában használnak különböző hálózati eszközök, például routerek, switchek, szerverek, nyomtatók és más hálózati berendezések kezelésére és monitorozására.

Az SNMP három fő komponensből áll:
* **SNMP Manager:** Ez egy központi felügyeleti rendszer, amely kommunikál a hálózati eszközökkel (ügynökökkel), és gyűjti az adatokat tőlük, illetve küld utasításokat nekik.

* **SNMP Agent:** Ez az ügynök az eszközön fut, és lehetővé teszi, hogy a SNMP Manager lekérdezze az eszköz állapotát vagy konfigurálja azt. Az ügynök az eszköz erőforrásaira és állapotára vonatkozó adatokat ad vissza.

* **MIB (Management Information Base):** Ez egy adatbázis, amely meghatározza az eszközök által nyújtott kezelhető objektumokat, például az interfészek állapotát, CPU-kihasználtságot vagy más operációs paramétereket.

Az SNMP egyszerű és skálázható, és elsősorban arra használják, hogy automatikusan monitorozzák a hálózatot és kezeljék a hálózati eseményeket vagy hibákat.

## 2. Az SNMP (Simple Network Management Protocol) felépítése az alábbi fő komponensekből áll
## 2.1. SNMP Manager
* **Feladat:** Ez egy központi rendszer vagy alkalmazás, amely az SNMP protokollt használja a hálózati eszközök adatainak gyűjtésére és azok kezelésére. A SNMP Manager a hálózati eszközökkel kommunikálva monitorozza és adminisztrálja azokat.
* **Példák:** Hálózati menedzsment rendszerek, például Nagios, SolarWinds, vagy PRTG Network Monitor.

## 2.2. SNMP Agent
* **Feladat:** Az ügynök egy szoftverkomponens, amely az eszközön fut, és lehetővé teszi, hogy a SNMP Manager lekérdezze az eszköz állapotát, valamint kezelje az eszközt. Az ügynök az eszközön található információkat egy SNMP MIB (Management Information Base) adatbázis segítségével szolgáltatja.
* **Példák:** Az eszközön futó ügynök szoftverek, például a Net-SNMP vagy a vendor-specifikus ügynökök, mint az HP vagy Cisco ügynökei.

## 2.3. Management Information Base (MIB)
* **Feladat:** Ez egy adatbázis, amely az SNMP által kezelt objektumok hierarchikus reprezentációját tartalmazza. A MIB definíciókat és strukturált információkat tartalmaz, amelyeket a SNMP Manager lekérdezhet. A MIB fájlok meghatározzák, hogy az ügynök milyen információkat szolgáltathat és hogyan érhetők el.
* **Példák:** MIB fájlok, mint például a IF-MIB (Interfész MIB) vagy a SYSAPPL-MIB.

## 2.4. Protokoll
* **Feladat:** Az SNMP protokoll az üzenetek és parancsok formátumát és működését definiálja, amelyeket az SNMP Manager és az SNMP Agent között küldenek. Az SNMP protokoll különböző verziókban létezik, mint például SNMPv1, SNMPv2c, és SNMPv3, amelyek mindegyike különböző funkciókat és biztonsági szinteket kínál.
* **Példák:** SNMPv1, SNMPv2c, SNMPv3.

## 2.5. Üzenettípusok
* **Feladat:** Az SNMP protokoll különböző üzenettípusokat használ az információk lekérdezésére és kezelésére. A legfontosabb üzenettípusok a következők:
* **GET:** Lekérdezi az eszközön tárolt adatokat.
* **SET:** Beállít egy új értéket az eszközön.
* **GETNEXT:** Lekérdez egy következő adatot az MIB-ből.
* **TRAP:** Az ügynök által küldött értesítés, amely informálja a menedzsert egy eseményről vagy hibáról.

Ezek az elemek együtt működnek az SNMP rendszerében, lehetővé téve a hálózati eszközök hatékony kezelését és monitorozását.
