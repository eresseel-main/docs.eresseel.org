## 1. Bevezetés
A kliens-szerver (magyarul: ügyfél-kiszolgáló) kifejezést először az 1980-as években használták olyan számítógépekre (PC-kre), amelyek hálózatban működtek. A ma ismert modell a 80-as évek végén vált elfogadottá. A kliens-szerver szoftverarchitektúra egy sokoldalú, üzenetalapú és moduláris infrastruktúra, amely azért alakult ki, hogy a használhatóságot, rugalmasságot, együttműködési lehetőségeket és bővíthetőséget megnövelje a centralizált, nagyszámítógépes, időosztásos rendszerekhez képest.

## 2. Kliens
A kliens (angolul client) olyan számítógép, amely hozzáfér egy szolgáltatáshoz, amelyet egy számítógép-hálózathoz tartozó másik gép nyújt. A kifejezést először önálló programmal nem rendelkező végkészülékekre, illetve terminálokra alkalmazták, amelyek legfontosabb szerepe az volt, hogy a hálózaton keresztül kapcsolatba lépjenek az időosztással működő nagyszámítógépekkel, és elérhetővé tegyék azok szolgáltatásait.

### 2.1. Jellemzői
* Kéréseket, lekérdezéseket küld a szervernek
* A választ a szervertől fogadja
* Egyszerre általában csak kis számú szerverhez kapcsolódik
* Közvetlenül kommunikál a felhasználóval, általában egy GUI-n (Graphical User Interface = grafikus felhasználói felület) keresztül

## 3. Kiszolgáló
A kiszolgáló vagy szerver (angolul server) olyan (általában nagy teljesítményű) számítógépet, illetve szoftvert jelent, amely más gépek számára a rajta tárolt vagy előállított adatok felhasználását, a kiszolgáló hardver erőforrásainak (például nyomtató, háttértárolók, processzor) kihasználását, illetve más szolgáltatások elérését teszi lehetővé.

### 3.1. Jellemzői
* Passzív, a kliensektől várja a kéréseket
* A kéréseket, lekérdezéseket feldolgozza, majd visszaküldi a választ
* Általában nagyszámú klienshez kapcsolódik egyszerre
* Általában nem áll közvetlen kapcsolatban a felhasználóval

A kiszolgálókat többféleképpen csoportosíthatjuk, például:

* a funkciójuk szerint, például webkiszolgálók, FTP-kiszolgálók, adatbázis-kiszolgálók;
* a kiszolgált kör alapján, például internetes kiszolgálók, intranetes kiszolgálók;
* a teljesítményük alapján.

A kliens-szerver olyan architektúra, amely elválasztja egymástól a klienst és a szervert, és az esetek nagy többségében egy számítógép-hálózaton alakítják ki. A hálózat klienseit és szervereit más néven csomópontnak (angolul node) is nevezhetjük. A kliens-szerver architektúra legalapvetőbb formájában mindössze kétfajta csomópont van, a kliens és a szerver. Ezt az egyszerű architektúrát kétszintűnek (angolul two-tier) hívják.

Bonyolultabb architektúrák is léteznek amelyek 3 különböző típusú csomópontból állnak: kliensből, alkalmazás szerverből (application server), valamint adatbázis szerverből (database server). Ezt háromszintű (three-tier) architektúrának hívják, és a leggyakrabban alkalmazott a kliens-szerver megoldások közül. Amelyek kettőnél több szintet tartalmaznak, többszintű (multi-tiered) és n-szintű (n-tiered) architektúrának is nevezzük.

A háromszintű kiépítésben az alkalmazásszerverek azok, amelyek kiszolgálják a kliensek kéréseit, és az adatbázisszerverek az alkalmazásszervereket szolgálják ki adatokkal. Ennek a rendszernek nagy előnye a bővíthetőség.

A többszintű kiépítés előnye, hogy egyensúlyozza és elosztja a feldolgozásra váró adatmennyiséget és munkát a több és gyakran redundáns, specializált csomópont között. Ez javítja a rendszer teljesítményét és a megbízhatóságát is, hiszen a feladatok párhuzamosan több szerveren is elvégezhetőek. Hátránya, hogy nagyobb az adatátviteli forgalom a hálózaton, és hogy nehezebben programozható, illetve tesztelhető egy kétszintű architektúránál, mert több eszközt kell összehangolni a kliensek kéréseinek kiszolgálásához.

## 4. A kliens-szerver architektúra előnyei
A kliens-szerver architektúra a legtöbb esetben lehetővé teszi, hogy a feladatokat elosszuk olyan számítógépek között, amelyek csak a hálózaton keresztül érintkeznek egymással, ami megkönnyíti a karbantartás elvégzését. Megoldható például, hogy javítsunk, frissítsünk, áthelyezzünk vagy akár kicseréljünk egy szervert anélkül, hogy klienseire ez bármilyen hatással lenne. Ezt a változtatásoktól való függetlenséget információelrejtésnek vagy angolul encapsulationnek nevezik.

Az összes adat a szerver(ek)en tárolódik, amelyek általában sokkal erőteljesebb biztonsági ellenőrzéssel rendelkeznek, és jobban tudják szabályozni az erőforrásokhoz és adatokhoz való hozzáférést.

Mivel az adattárolás centralizált, könnyebb frissíteni az adatokat, mint amennyire ez egy P2P-rendszerben lehetséges lenne. Utóbbi architektúrában több száz vagy ezer részt vevő gépen kell megoldani az adatok megváltoztatását, ami időigényessége mellett a hibák előfordulásának lehetőségét is megnöveli.
