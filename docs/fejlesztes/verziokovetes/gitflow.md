## 1. Bevezetés
A Gitflow Workflow egy Git munkafolyamat, amely segíti a folyamatos szoftverfejlesztést és a DevOps-os gyakorlatok megvalósítását. Először Vincent Driessen adta ki és tette népszerűvé. A Gitflow Workflow szigorú elágazási modellt határoz meg a projekt kiadása köré. Ez megbízható keretet biztosít a nagyobb projektek kezeléséhez.  

Gitflow ideális olyan projektekhez, amelyek menetrend szerinti kiadási ciklust és folyamatos szállítást biztosít. Ez a munkafolyamat nem ad hozzá új fogalmakat vagy parancsokat a Feature Branch Workflow-nál megkövetelteken túl. Ehelyett nagyon specifikus szerepeket rendel a különféle ágakhoz, és meghatározza, hogyan és mikor kell kölcsönhatásba lépniük. A feature ágak mellett az egyes ágakat használja a kiadások előkészítésére, karbantartására és rögzítésére. Természetesen kilehet aknázni a Feature Branch Workflow összes előnyét: pull kéréseket, elszigetelt kísérleteket és hatékonyabb együttműködést.

A Gitflow valójában csak egy elvont ötlet a Git munkafolyamatról. Ez azt jelenti, hogy megmondja, hogy milyen branchet kell létrehozni, és hogyan kell ezeket merge-elni. Az alábbiakban érintjük a különböző branch-ek céljait. A git-flow eszközkészlet egy tényleges parancssori eszköz, amelynek telepítési folyamata van. A git-flow telepítési folyamata egyszerű. A git-flow csomagjai több operációs rendszeren érhetők el.

* Linux rendszeren az alábbi parancsot kell kiadni a telepítéshez: `sudo apt install git-flow`
* OSX rendszeren az alábbi parancsot kell kiadni a telepítéshez:  `brew install git-flow`.
* Windows rendszeren le kell tölteni és telepíteni kell a git-flow szoftvert [INNEN](https://github.com/nvie/gitflow/wiki/Windows).

A git-flow telepítése után telepíteni kell a használhathoz a GIT projektben `git flow init` parancs kiadásával. A `git flow init` parancs az alapértelmezett `git init` parancs kiterjesztése, és nem változtat semmit a repository-ban, csak az ágak létrehozását könnyíti meg.

### 1.1. Gitflow teljes áramlása

* Létrejön egy master és develop ág,
* Release ág a develop ágból jön létre,
* Feature ágak develop ágból jön létre,
* Amikor a feature ágon a fejlesztés véget ér, merge-elik a develop ágba,
* Ha a release ág elkészült, akkor merge-elik develop és master ágra,
* Ha egy problémát fedeznek fel a kiadott kódban akkor létrehoznak egy hotfix ágat a master ágból,
* Amint hotfix elkészült, merge-elik master és developpal.

## 2. Develop és Master branch
![Master és Develop](./master-develop.svg)
Egyetlen master ág helyett ez a munkafolyamat két ágat használ a projekt fejlesztésének rögzítésére. A master fiók tárolja a hivatalos kiadási előzményeket, a develop fiók pedig a szolgáltatások integrációs ágaként szolgál. A master ágat kényelmesen lehet verziószámmal megjelölni Release esetén.

Az első lépés az hogy a master ágat ki kell egészíteni egy develop ággal. Ennek egyszerű módja az, ha egy fejlesztő létrehoz egy üres develop branchet a helyi számítógépen, majd utána a szerverre tolja:

```git
git branch develop
git push -u origin develop
```

Ez az ág tartalmazza a projekt teljes történetét, míg master egy rövidített verziót. Más fejlesztőknek most klónozniuk kell a központi adattárat, és létre kell hozniuk egy követési ágat a develop számára.

A git-flow kiterjesztésű könyvtár használatakor a `git flow init` egy meglévő repo-n létrehozza a develop ágat:

```git
$ git flow init


Initialized empty Git repository in ~/project/.git/
No branches exist yet. Base branches must be created now.
Branch name for production releases: [master]
Branch name for "next release" development: [develop]


How to name your supporting branch prefixes?
Feature branches? [feature/]
Release branches? [release/]
Hotfix branches? [hotfix/]
Support branches? [support/]
Version tag prefix? []


$ git branch
* develop
 master
```

## 3. Feature branch
![Feature](./feature.svg)
Minden új feature-nek a helyi gépen kell lennie, amelyet biztonsági mentés/együttműködés céljából a központi adattárba lehet push-olni. Ahelyett, hogy master ágról származtatnánk le a feature ágat használjuk a develop-ot szülő ágként. Amikor egy feature fejlesztése befejeződött, újra lehet merge-elni a develop ágra. A feature ágaknak soha nem szabad közvetlenül kölcsönhatásba lépniük master ággal.

A develop ágról indított feature ágakat minden szempontból Feature Branch Workflow-nak nevezzük. De a Gitflow munkafolyamat nem áll meg itt.

### 3.1. Feature ág létrehozása
Git-flow kiterjesztés nélkül:
```git
git checkout develop
git checkout -b feature_branch
```

A git-flow kiterjesztés használatával:
```git
git flow feature start feature_branch
```

Innentől kezdve a munkát, úgy lehet folytatni mint általában.

### 3.2. Feature ág befejezése
Amikor elkészült a fejlesztési munka a következő lépés az, hogy egyesíti kell a feature_branch-et a develop ággal.

Git-flow kiterjesztés nélkül:
```git
git checkout develop
git merge feature_branch
```

A git-flow kiterjesztés használatával:
```git
git flow feature finish feature_branch
```

## 4. Release branch
![Release](./release.svg)
Miután a develop ágra elegendő feature került egyesítésre egy kiadáshoz (vagy közeledik egy előre meghatározott kiadási dátum), release ágat kell készíteni a develop ágből. Ennek az ágnak a létrehozása elindítja a következő kiadási ciklust, így ez után nem lehet új funkciókat hozzáadni - ebben a branchben csak hibajavítások, dokumentáció-generálás és egyéb kiadásorientált feladatok lehetnek. Miután készen áll a szállításra, a release branchet egyesítik a master ággal és megcímkézik egy verziószámmal. Ezután verzió számot kell növelni, és vissza kell merge-elni a develop ágra.

A dedikált ág használata a kiadások előkészítéséhez lehetővé teszi, hogy az egyik csapat csiszolja az aktuális kiadást, míg egy másik csapat folytatja a következő kiadás funkcióinak kidolgozását. Ez a fejlesztési módszer jól definiált fázisokat hoz létre (például könnyű azt mondani, hogy "Ezen a héten készülünk a 4.0-s verzióra").

A release ágak készítése egy másik egyszerű elágazási művelet. A feature ágakhoz hasonlóan a release ágak a develop ágon alapulnak.

### 4.1. Release ág létrehozása
Git-flow kiterjesztés nélkül:
```git
git checkout develop
git checkout -b release/0.1.0
```

A git-flow kiterjesztés használatával:
```git
$ git flow release start 0.1.0
Switched to a new branch 'release/0.1.0'
```

### 4.2. Release ág befejezése
Miután a kiadás készen áll a szállításra, össze kell olvasztani a master és develop ággal, majd az release ág törölni. Fontos, hogy visszacsatlakozzon, develop-ra, mert lehetséges, hogy kritikus frissítéseket adtak az release ághoz, és hozzáférhetőnek kell lenniük az új funkciók számára.

Git-flow kiterjesztés nélkül:
```git
git checkout master 
git merge release/0.1.0
```

A git-flow kiterjesztés használatával:
```git
git flow release finish '0.1.0'
```

## 5. Hotfix branch
![Hotfix](./hotfix.svg)
A karbantartást vagy az “hotfix” ágakat a gyártási kiadások gyors javításához használják. A hotfix ágak nagyon hasonlítanak a release és a feature ágakhoz, kivéve, hogy azok a master helyett develop-ról vannak leágaztatva. Ez az egyetlen elágazás, amely közvetlenül a master-ről van leágaztatva. Amint a javítás befejeződött, akkor kell összevonni a master és develop (vagy az aktuális release ágat). Ezután frissített kell az új verzió számra.

A hibajavítások külön fejlesztési vonalon mennek, így a csapat megoldhatja a problémákat anélkül, hogy megszakítaná a munkafolyamat hátralévő részét, vagy megvárná a következő kiadási ciklust. A karbantartási ágakra úgy kell gondolni, mint olyan ad-hoc release ágakra, amelyek közvetlenül működnek a master ágon.

### 5.1. Hotfix ág létrehozása
Git-flow kiterjesztés nélkül:
```git
git checkout master
git checkout -b hotfix_branch
```

A git-flow kiterjesztés használatával:
```git
$ git flow hotfix start hotfix_branch
```


### 5.2. Hotfix ág befejezése
Git-flow kiterjesztés nélkül:
```git
git checkout master
git merge hotfix_branch
git checkout develop
git merge hotfix_branch
git branch -D hotfix_branch
```

A git-flow kiterjesztés használatával:
```git
$ git flow hotfix finish hotfix_branch
```