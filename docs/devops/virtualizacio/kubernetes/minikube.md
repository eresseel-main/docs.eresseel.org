## 1. Bevezetés
A minikube gyorsan létrehoz egy helyi Kubernetes-fürtöt macOS, Linux és Windows rendszeren. A minikube egy helyi Kubernetes, amely arra összpontosít, hogy megkönnyítse a tanulást és a Kubernetes számára történő fejlesztést.

Mindössze egy Docker (vagy hasonlóan kompatibilis) tárolóra vagy egy Virtual Machine környezetre van szüksége, a Kubernetes pedig egyetlen paranccsal elérhető: `minikube start`

## 2. Amire szükséged lesz

* 2 vagy több CPU,
* 2 GB szabad memória,
* 20 GB szabad lemezterület,
* Internet kapcsolat,
* Tároló- vagy virtuálisgép-kezelő, például: Docker , QEMU , Hyperkit , Hyper-V , KVM , Parallels , Podman , VirtualBox vagy VMware Fusion/Workstation.

## 3. Telepítés
Telepítsd a legújabb minikube stabil kiadását Debian csomagból:
```bash
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
sudo dpkg -i minikube_latest_amd64.deb
``` 

Telepítsd a legújabb `kubectl` stabil kiadását apt repozitoriból:
```bash
sudo apt-get update
sudo apt-get install -y ca-certificates curl apt-transport-https
sudo curl -fsSLo /etc/apt/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
echo "deb [signed-by=/etc/apt/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubectl
```

## 4. Minikube indítása
Egy rendszergazdai hozzáféréssel rendelkező terminálról (de nincs rootként bejelentkezve) futtassd:
```bash
minikube start
minikube start --driver=docker
minikube start --driver=virtualbox
``` 

Alapértelmezett illesztőprogram beállítása:
```bash
minikube config set driver docker
minikube config set driver virtualbox
```

Kiegészítők listázása, hozzáadása és törlése:
```bash
minikube addons list
minikube addons enable <name>
minikube addons disable <name>
```

## 5. Lépjél kapcsolatba a klaszterrel
Ha már telepítve van a `kubectl`, akkor használhatod az új fürt eléréséhez:
```bash
kubectl cluster-info
kubectl get po -A
```

## 6. A kubectl automatikus kiegészítésének engedélyezése bash-ben
```bash
echo 'source <(kubectl completion bash)' >>~/.bashrc
```

## 7. Kezeld a fürtöt
A Kubernetes szüneteltetése a telepített alkalmazások befolyásolása nélkül:
```bash
minikube pause
```

Szüneteltetett példány szüneteltetésének feloldása:
```bash
minikube unpause
```

A klaszter leállítása:
```bash
minikube stop
```

Módosítsd az alapértelmezett memóriakorlátot (újraindítás szükséges): 
```bash
minikube config set memory 9001
```

Hozzon létre egy második fürtöt, amely egy régebbi Kubernetes-kiadást futtat: 
```bash
minikube start -p aged --kubernetes-version=v1.16.1
```

Az összes minikube-fürt törlése:
```bash
minikube delete --all
```