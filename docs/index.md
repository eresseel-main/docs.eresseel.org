# Üdvözöllek az oldalon!

2015 óta foglalkozok informatikával. Sokféle pozicióban dolgoztam rövidebb hosszabb ideig. Azért készítettem ezt a weboldalt, hogy megkönnyítsem a saját munkámat és megtudjam osztani másokkal a saját tapasztalataimat. A munkám során használt alkalmazásoknak a leírását és használatát olvashatod itt.

Kellemes böngészést és tanulást.
