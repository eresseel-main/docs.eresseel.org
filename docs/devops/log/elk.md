## 1. Bevezetés
Az Elastic Stack az Elastic csoport tagja, nyílt forráskódú termékeinek amelyek célja, hogy segítsenek a felhasználóknak bármilyen típusú forrásból és bármilyen formátumban adatokat gyűjteni, és valós időben keresni, elemezni és megjeleníteni. A termékcsoport korábban ELK Stack néven volt ismert a csoport alaptermékei:

* Elasticsearch,
* Logstash
* Kibana

Ezeket a termékeket idővel átkeresztelték Elastic Stack névre. Ezt követően egy negyedik termék, a Beats került a stackbe. Az Elastic Stack a helyszínen telepíthető, vagy szoftverként szolgáltatásként (SaaS) elérhetővé tehető. Az Elasticsearch támogatja az Amazon Web Services (AWS), a Google Cloud Platform és a Microsoft Azure szolgáltatást. 

## 2. Elastic Stack alaptermékei
Az Elastic céget 2012-ben Amszterdamban alapították, hogy támogassa az Elasticsearch és a kapcsolódó kereskedelmi termékek és szolgáltatások fejlesztését.

A következők az Elastic Stack alaptermékei, azok funkcióival együtt:

### 2.1. Elasticsearch
Az Elasticsearch egy RESTful elosztott keresőmotor, amely az Apache Lucene tetejére épült, és Apache licenc alatt került kiadásra. Java-alapú, és képes adatokat feldolgozni, valamint dokumentumfájlokat keresni és indexelni különféle formátumokban.

### 2.2. Logstash
A Logstash egy adatgyűjtő motor, amely több forrásból származó adatokat egyesíti, adatbázis-normalizálást kínál, és elosztja az adatokat. A terméket eredetileg naplóadatokra optimalizálták, de kibővítette a hatókört úgy, hogy minden forrásból adatokat gyűjtsön.

### 2.3. Kibana
A Kibana egy nyílt forráskódú adatvizualizációs és feltáró eszköz, amely nagy mennyiségű streaming és valós idejű adatkezelésre specializálódott. A szoftver jeleníti meg az összetett adatfolyamokat grafikus ábrázolással.

### 2.4. Beats
A Beats olyan adatszállítók, amelyek a szerverekre vannak telepítve ügynökként, amelyek különböző típusú működési adatokat küldenek az Elasticsearch-nek közvetlenül vagy a Logstash-en keresztül, ahol az adatok javíthatók vagy archiválhatók. 

## 3. Elastic Stack alkalmazása
Az Elastic Stack meredekebb tanulási görbét mutat, mint néhány hasonló termék, valamint több beállítást kínál, részben nyílt forráskódú jellegének köszönhetően.

Az alábbiakban az Elastic Stack népszerű használati esetei találhatók: 


* **Nagy adat**. A nagy mennyiségű strukturálatlan, félig strukturált és strukturált adatkészlettel rendelkező vállalatok használhatják az Elastic Stacket adatműveleteik futtatásához. A Netflix, a Facebook és a LinkedIn a sikeres vállalatok példái a stack használatával.
* **Alkalmazások összetett keresési követelményekkel**. Bármely összetett keresési követelményekkel rendelkező alkalmazás nagy előnyt jelent, ha az Elastic Stack-et használja a speciális keresések mögöttes motorjaként.
* **Egyéb kiemelkedő használati esetek**. Az Elastic Stack infrastruktúra-metrikákban és konténerfigyelésben, naplózásban és naplózási elemzésben, alkalmazásteljesítmény-figyelésben, térinformatikai adatok elemzésében és megjelenítésében, biztonsági és üzleti elemzésekben, valamint nyilvános adatok kombinálásában használatos.

## 4. Elastic Stack használata
Az Elastic Stack használatához a felhasználóknak először le kell tölteniük a három nyílt forráskódú szoftverterméket.

* Elasticsearch
* Logstash
* Kibana

A fájlok kicsomagolása után a felhasználók beállíthatják ezeket a programokat a helyi rendszerükön.

Az ELK-Stack használatának megkezdése után ezek az összetevők együttesen telepíthetők a naplóadatok összesítésére, indexelésére és keresésére, folyamatok átalakítására és adatvizualizációk előállítására. 