## 1. Információ gyűjtés
### 1.1. Domain név információ Linux alatt
Itt nagyon egyszerű mert van rá beépített parancs. Ez a `whois`.
```bash
whois google.com
```

### 1.2. Domain név információ Windows alatt
Itt nincs rá beépített parancs, így marad a böngészős megoldás. Nyisd meg a kedvenc böngésződet és írd be a `whois` szót. A találati listából én a https://whois.domaintools.com/ választottam. Itt írd be azt a domaint amelyikről infót szeretnél megtudni.

### 1.3. Mindkettő OS-en működik
#### 1.3.1. nslookup
Egy másik program az `nslookup`, itt kapsz egy interaktív terminált kapsz. Ha bővebb infót akarsz akkor állítsd be a `set type=any`-t, és próbáld újra.

Ezután írd be a `server nameszerver_vagy_ip_cím`-et. Ilyenkor plusz információt kaphatsz.

Zóna adatok lekérdezéséhez az alábbi parancsot kell kiadni: `ls -d example.com`.
