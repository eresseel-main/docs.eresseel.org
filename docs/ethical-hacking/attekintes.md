## 1. Bevezetés
A hacker szó hallatán elsőre mindenkinek valamilyen negatív dolog jut eszébe. Gyakran társul hozzá a bűnöző kifejezés, azonban a szó mai jelentése olyan szakembereket jelöl, akik egy adott rendszerben hibákat kutatnak, fedeznek fel.

Nemzetközi non-profit szervezetek is erőteljesen dolgoznak azon, hogy a hacker kifejezésnek pozitív csengése legyen. Sajnos az évek alatt számos negatív eseményeket társítottak a megnevezéshez.

## 2. Etimológia
Az angolszász „hacker” szó műszaki/informatikai értelmében az 1960-as és 70-es években keletkezett az amerikai egyetemeken. A „hacker” szó eredetileg a „hack” (üt, vág, feldarabol) szóból eredt, és olyan személyt jelentett aki vagy dolgokat kivág és feldarabol (jellemzően fát), vagy aki ehhez eszközöket, szerszámokat készít. A „hack” főnevet először a 60-as évek környékén (MIT) használták „kreatív tréfa” értelemben, emellett ugyanezen környezetben a „hacker” jelentett olyan személyt, aki „szerszámkészítőként kísérletezik vagy készít software-eket, és aki a programozást magáért a programozás élményért szereti”. 1975 magasságára becsülik azt a jelentést, mely szerint „valaki, aki engedély nélküli hozzáférést szerez számítógépes rendszerhez”, de a szónak megmaradtak az előző jelentései is.

A napjainkban használt jelentései nagyrészt a fentiekből származnak, illetve keveredik bennük a 2000-es évek filmjeiben és sajtójában megjelent szóhasználat:

* személy, aki mestere a programozásnak és a problémák számítógépekkel történő megoldásának;
* személy, aki számítógép segítségével engedély nélkül hozzáfér korlátozott adatokhoz;
* személy, aki rosszindulatú számítógépes támadást hajt végre (mivel ez a jelentés újkori, gyakran a „cracker” [valaki, aki feltör, megtör valamit] szóval helyettesítik, de inkább csak azok, akik az eredeti szóhasználatot ismerik);
* számítógép-biztonsági szakember;
* illetve informatikához nem kapcsolódó jelentésben olyan tapasztalatlan vagy tehetségtelen személy, aki egy (gyakran sport) tevékenységgel próbálkozik („a tennis hacker”).

Mivel a jelentések egy része pozitív, más része negatív, gyakran konfliktust okoz a szó használata, illetve hiányos kontextusból nem egyértelmű, hogy melyik jelentésében használják, emiatt (pontos) használata gyakran viták forrása.

## 3. A hacker típusok
### 3.1. Klasszikus hacker
A klasszikus értelemben vett hackert nagyon hosszasan lehetne körülírni, de alapvetően jellemzi a jó szakértelem, a kíváncsiság, a humor, a kreativitás és a korlátok ledöntésének élvezete. Jó kiindulási pont lehet a pontosabb megismerésükhöz az Eric S. Raymond által szerkesztett „Jargon File”, amely egyben bemutatja azt a kultúrát, amellyé fejlődött a lelkes számítógép programozók és számítógépes rendszerek megalkotóinak a közössége egészen az 1960-as évektől indulva. A 70-es évektől kezdődően a számítógépek szélesebb társadalmi körben való terjedésének köszönhetően már a lelkes számítógép felhasználók és hobbi programozók is kezdtek bekapcsolódni a közösségbe – ami korábban inkább az akadémikusokból állt össze. Számos, ma már evidensnek tekinthető műszaki megoldás és elképzelés az ilyen attitűddel dolgozó embereknek köszönhető, akik gyakran lehetetlennek tűnő feladatokat oldottak meg. Az évtizedek előrehaladtával aztán a „hacker” megnevezés már nem is feltétlenül korlátozódott a tisztán számítógépes közegre. Az élet számos egyéb részeire, mint az elektronika, a zene, vagy szinte bármilyen más terület ahol a hacker mentalitásból fakadó viselkedési forma hasznosulni tud, ott megjelent. Hogy szinte bármi lehet hackelés, az a Richard Stallman programozó „On Hacking” című írásában található felsorolásból is látható, amiből rögtön az első példa, amikor valaki – többnyire gyerekként – a mozgólépcsőn az ellentétes irányban sétál. Manapság már egészen hétköznapi kifejezés a „life hack”, amely rendkívül frappáns megoldást nyújt egy sokszor látszólag bonyolult problémára, ami az esetek többségében ráadásul könnyen tanulható.

Akinek ilyen beállítottsága van, annak lehetősége van hackerspace közösségeket látogatni, amiknek a célja helyet és adott esetben olyan infrastruktúrát szolgáltatni, amihez otthoni környezetben nem mindenki jut hozzá.

### 3.2. Biztonsági rendszerhez kapcsolódó hacker
A sajtóban és híradásokban említett hackereket, akiknek az érdeklődése elsősorban a számítógépes rendszerek és hálózatok biztonsági réseire koncentrálódik, tudásuk, céljuk és módszereik alapján tudjuk osztályokba sorolni. Az elnevezéseket az amerikai vadnyugati (western) filmekben megjelenő motívumból kölcsönözték, ahol gyakran a kalapok színéből lehetett következtetni, hogy ki a "jó fiú" (általában fehér vagy világos kalapot viselt), és ki a "rossz" (többnyire sötét vagy fekete kalapot viselt).

### 3.3. Fehér Kalapos Hacker
Fehér kalapos hackernek (angolul: white hat hacker) nevezzük azt a kiemelt tudással rendelkező informatikai szakembert; aki tudását arra használja fel, hogy megbízás alapján vagy állandó jelleggel biztonsági hibákra világít rá. Ezáltal elkerülve és megelőzve a fekete kalapos hackerek betörési kísérleteit. A fehér kalapos hackerek csoportjába tartozik az úgynevezett etikus hacker, illetve a Penetration Tester. A fehér kalapos hacker vagy etikus hacker egy adott számítógép rendszer vagy számítógép-hálózat biztonsági tesztelését végzik hibakeresési céllal. A Penetration Testerek megpróbálják a „veszélyes hibákat” felderíteni és kiaknázni, ezáltal bizonyítva az adott hibában rejlő veszélyeket a rendszer üzemeltetőjének.

### 3.4. Fekete Kalapos Hacker
Fekete kalapos hackernek (angolul: black hat hacker) nevezzük azt; – valójában ő sorolható a crackerek csoportjába is mivel nagy az átfedés a motivációk tekintetében –; aki tudásával visszaélve, jogosulatlanul lép be számítógépes rendszerekbe illetve számítógép-hálózatokba, haszonszerzés vagy károkozás céljából. Az általa végzett tevékenység nagy általánosságban illegális.

Motivációja sokrétű lehet: pénzszerzés az adott információ birtoklása által, avagy a puszta kíváncsiság: „Meg tudom-e csinálni?” „Okosabb vagyok-e, mint azok, akik a rendszerért felelősek?”. De ebben az esetben a kíváncsiságot nem kíséri önmérséklet: a fekete kalapos hacker nem kér engedélyt a tesztelésre; a sikeres behatolás végén nem értesíti az üzemeltetőket, hanem igyekszik kihasználni az illegálisan megszerzett információkat. A csoportjába tartoznak azok az ipari kémek, akik technológiai fejlesztések után kutatva törnek be hálózatokba. Gyakran hallani nagyobb multinacionális cégek vagy akár kormányszervek ellenében elkövetett „hackertámadásról”; és az sem ritka, hogy az elkövetést egy másik ország titkosszolgálatához vagy egy hozzá kapcsolódó csoporthoz kötik. Ilyen esetekben előállhat az a helyzet is, hogy az etikus és etikátlan közti határvonal elmosódik; mert ha az adott tevékenységgel egy potenciálisan háborús helyzet elkerülése a cél; akkor az más megvilágításba helyezi a történéseket.

### 3.5. Szürke Kalapos Hacker
Gyakorlatilag a fekete kalapos és a fehér kalapos hacker közti átmenet; aki ha biztonsági résre bukkan, akkor kihasználja azt, majd az esetek többségében értesíti a rendszer üzemeltetőit. A nagy különbséget az jelenti, hogy a fehér kalapos hackerrel ellentétben a megtámadott rendszer üzemeltetőjével nincs előre egyeztetett megbízás; így alapvetően illegális tevékenységnek minősül, tehát nem ajánlott magatartás. Vannak példák olyan esetekre, amikor a hibát jelentő hacker pénzjutalmat kért és kapott is, de mindenképpen rizikós vállalkozás. Sorolható az internetes trollokhoz is, akiknek egy része a szakértelmét felhasználva; pusztán szórakozásból okoz felfordulást például közösségi oldalak üzenőfalain, annak biztonsági hibáit kihasználva.

## 4. Az etikus hacker tízparancsolata
Benned is felmerülhet ezek után a kérdés, hogyan lehet a legális hackelés keretein belül részt venni bug bounty programokban. Ehhez összegyűjtöttünk számotokra néhány pontot, ami segíthet ennek elérésében.

Íme az etikus hacker tízparancsolata:

1. Regisztrálj Hackerként platformunkon és válassz a publikus bug bounty programok közül. Regisztrációd során el kell fogadnod a tevékenységre vonatkozó szabályokat (Tesztelési szabályzat, Jogi nyilatkozat), ezek elolvasását ne mulaszd el.

2. Szerződéses kapcsolatban állunk a programot indító szervezetekkel, így biztos lehetsz abban, hogy minden az ő tudtukkal és egyetértésükkel történik. Sőt, a tesztelési irányelveket ők saját maguk határozzák meg a programjukhoz; így biztos lehetsz benne, hogy az oldalunkon található összes aktív hibavadász programban való részvétel legális.

3. Csak a cég / program által megengedett domaineket teszteld a megengedett módszerekkel!

4. A talált sérülékenységeket soha ne használd fel személyes indíttatásból. Oldalunkon keresztül tudod kitölteni a bug riportot, amit mi megosztunk a céggel. Külön ne hozd nyílvánosságra a sérülékenységet!

5. Dokumentáld, hogy pontosan mit és mikor teszel! Rögzítsd, hogyan jöttél rá a hibára (legjobb, ha videóra veszed, ha erre van mód vagy készíts képernyőfotókat). Fájlok csatolására is lehetőséged van a bug riport készítése közben, úgyhogy ezeket mind töltsd fel.

6. Figyelj arra, hogy a tesztelés alatt használt programokat / eszközöket / scripteket megfelelően állítsd be – pl. Recaptcha tesztelésekor kerüld el az email bombázást több száz levéllel. Ez az ügyfél rendszereinek túlterhelését és esetleges leállását okozhatja; ezáltal az etikus hackerekbe vetett hit és bizalom is csorbulhat az ügyfelek részéről.

7. Ne próbáld meg eltüntetni a nyomaid! Bár ügyfeleinkkel közvetlen kapcsolatba nem kerülsz, bizonyos programok esetén (mint a helyszíni vizsgálat) kiléted nem tudod elrejteni. Ha valaki esetleg keres a cégtől bátran hivatkozz arra, hogy rajtunk keresztül végzed a tesztelést.

8. Óvakodj a személyes adatok megismerésétől, ezek után szándékosan ne kutakodj, és semmiképpen ne mentsd el!

9. Teszteléskor a lehető legkisebb bizonyítást (Proof of Concept) használd a biztonsági rés érvényesítéséhez. Azokban az esetekben, amikor ez hatással lehet a rendszerekre vagy a felhasználókra, először kérj engedélyt. Esettől függetlenül, ne károsítsd vagy hagyd a rendszert egy sérülékenyebb állapotban, mint ahogy a hiba felderítése előtt volt.

10. Minden olyan sérülékenység felderítése elkerülendő, ami a weboldal / webapplikáció elérhetőségét befolyásolja (DoS sérülékenységek) vagy irreális brute force-olással jár.


## 5. Hacker-ként hivatkozott fogalmak
### 5.1. Script kiddie
A script kiddie kifejezés a „szkript” (egyfajta számítógépes programnyelv amely általában feladatok automatizálását valósítja meg, jelen értelmében mások által előre megírt kis programok összessége) és a „kiddie” (kiskölyök) szavakból áll, ami gyakorlatilag leírja ezeket a személyeket: általában olyan, komoly szaktudással nem rendelkező fiatal gyerekről van szó, aki hackerek által megalkotott eljárások alapján mások által megírt programokat („szkripteket”) használ arra, hogy vandálkodjon (például mások hálózati kapcsolatát vagy gépének működését megzavarja, gépekre betörve az ott levő tartalmakat törölje vagy megváltoztassa), számítógépekhez illegális hozzáférést szerezzen és annak erőforrásait engedély nélkül használja. Motivációját leggyakrabban az elismerés utáni vágy hajtja, amelyhez nem társul az általános etikai elvek tiszteletben tartása. Gyakran olyan eszközöket (programokat) használ, melyek működésével nincs tisztában, emiatt többnyire azzal sincs tisztában, hogy mit okozhat a használatával.

### 5.2. Elite Hacker
Szociális státusz, a legnagyobb elismerés a hackerek között. Csak a legjobbakat nevezik elitnek. A legújabb módszerek valószínűleg ezekben a körökben bukkannak fel először. Innen származik a l33t internetes szleng kifejezés is.

### 5.3. Neophyte
A neophyte, vagy „n00b”, „newbie” kifejezést a kezdő, még meglehetősen tudatlan hackerekre használják.

### 5.4. Blue hat
A kéksapkások azok a személyek, akik nem tartoznak közvetlen számítógépes biztonság körökbe, hanem a különböző rendszereket még megjelenés előtt tesztelik.

### 5.5. Hacktivista
A hacktivisták számítógépes hackelés segítségével promotálnak bizonyos nézeteket, foglalnak állást politikai kérdésekben, stb. A legismertebb ilyen csoport az Anonymous.
