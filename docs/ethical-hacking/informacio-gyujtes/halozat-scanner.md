## 1. Beveztés
A hálózati scanner egy szoftver vagy eszköz, amelyet hálózati infrastruktúrák feltérképezésére, vizsgálatára és elemzésére használnak. Segítségével az adminisztrátorok azonosíthatják a hálózaton található eszközöket, azok nyitott portjait, sebezhetőségeit, valamint hálózati forgalmat is elemezhetnek.

## 2. Fő funkciók és célok
1. **Eszközök azonosítása**:
   - Meghatározza, hogy milyen eszközök vannak a hálózaton (pl. számítógépek, szerverek, nyomtatók, IoT-eszközök).
   - Információkat gyűjt az IP-címekről, MAC-címekről és az operációs rendszerekről.

2. **Portok és szolgáltatások vizsgálata**:
   - Felderíti a nyitott portokat (pl. 22 - SSH, 80 - HTTP).
   - Ellenőrzi, hogy mely szolgáltatások futnak az adott portokon.

3. **Sebezhetőségek feltárása**:
   - Olyan potenciális biztonsági réseket keres, amelyek kihasználhatók támadások során.
   - Értesít a frissítések vagy javítások hiányáról.

4. **Teljesítmény elemzése**:
   - Segít azonosítani a hálózati torlódásokat vagy problémákat.
   - Rendszeres ellenőrzések során követhetővé teszi a hálózat egészségi állapotát.

---

## 3. Példák a hálózati szkennelésre
### 3.1. Aktív IP szkennelés
Az aktív IP szkennelés során közvetlen kérések küldése történik a hálózati eszközök felé (pl. ICMP, ARP). Ez gyors és pontos módja a hálózati eszközök feltérképezésének.

#### 3.1.1. nmap
```bash
nmap -sn 192.168.1.0/24         # ARP és ICMP alapú szkennelés
nmap -PR 192.168.1.0/24         # Csak ARP
```

#### 3.1.2. netdiscover
```bash
netdiscover -s 30 -c 1 -F "arp or port 53" -r 192.168.2.0/24 -i eth0        # Csak ARP
```

#### 3.1.3. arp-scan
```bash
arp-scan --interface=eth0 --timeout=1000 --backoff=2 --random --quiet --retry=1 --bandwidth=100000 192.168.2.0/24                  # Csak ARP
```

#### 3.1.4. scapy
```python
def scan(ip_range):
    arp = ARP(pdst=ip_range)
    ether = Ether(dst="ff:ff:ff:ff:ff:ff")
    packet = ether/arp
    result = srp(packet, timeout=3, verbose=0)[0]

    for sent, received in result:
        print(f"IP: {received.psrc} - MAC: {received.hwsrc}")

scan("192.168.1.0/24")
```

### 3.2. Passzív IP szkennelés
A passzív szkennelés a hálózati forgalmat figyeli anélkül, hogy közvetlen kéréseket küldene. Ez kevésbé észlelhető, de nem mindig teljeskörű.

#### 3.2.1. tcpdump
```bash
tcpdump -i eth0 arp             # Csak ARP figyelése
```

### 3.3. Port szkennelés
A portszkennelés a nyitott portokat térképezi fel a hálózati eszközökön. Segítségével azonosíthatók a futó szolgáltatások és a lehetséges támadási felületek.

### 3.3.1. nmap
```bash
nmap -sS -T5 -Pn -f --source-port 80 --data-length 24 --randomize-hosts --spoof-mac 0 192.168.2.100
```

### 3.4. Hálózati szolgáltatások vizsgálata
A szolgáltatások vizsgálata segít azonosítani az eszközök által kínált funkciókat (pl. webszolgáltatások, nyomtatók).

### 3.3.1. avahi-browse
```bash
avahi-browse -a                                 # Összes szolgáltatás megjelenítése
avahi-browse -d _http._tcp                      # Keresés egy meghatározott domainben
avahi-browse -r _services._dns-sd._udp          # Szolgáltatások nevének feloldása
avahi-browse -c                                 # Cache-ből való lista kiírása
```
