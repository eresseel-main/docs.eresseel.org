## 1. Bevezetés
Virtualbox segítségével x86 és x64 alapú hardverrel kompatibilis rendszereket tudunk virtualizálni. Alapvetően egy ingyenes, nyílt forráskódú programról van szó, viszont van egy bővítés hozzá Extension Pack névvel ami otthoni használatra ugyanúgy ingyenesen használható és kapunk vele pl.: USB 3.0, webkamera támogatást. Virtualizációhoz mindenképp ajánlott hardveres virtualizáció megléte és bekapcsolása. A virtuális gépek létrehozásuk után az alábbi kép formátumok közül valamelyikkel rendelkezik.

* VDI (virtualbox lemezkép)
* VHD (virtuális lemezkép)
* VMDK (virtuális gép lemez)
* HDD (parallels merevlemez)
* QCOW (QEMU Copy-On-Write)
* QED (QEMU enhanced disk)

A szoftver segítségével, több operációs rendszert (VM – Virtal Machine) tudunk telepíteni ugyanarra a hardverre anélkül, hogy partícionálnunk kéne a merevlemezünket (bár az új VM telepítése közben ezeket a metódusokat is végrehajthatjuk). A telepített operációs rendszer (VM) egy kép fájlt fog alkotni a lemezünkön. Saját hardveres és szoftveres beállításokkal, mégis a fő operációs rendszertől elszigetelve fog futni, az adatok nem fognak átszivárogni.

### 1.1. Előnyei
* Nagy előnye ha valamit ki szeretnénk próbálni, de nem az éles rendszeren,
* A fő operációs rendszer és a VM-ek teljesen el vannak szigetelve egymástól, így a vírusok vagy egyéb kártevő programok nem tudjákmegfertőzni a fő operációs rendszert vagy egy másik VM-et,
* A telepített rendszer hordozható,
* Az operációs rendszerek közötti váltás ideje lerövidül,
* ISO fájlból is telepíthetünk,
* Adott állapot menthető,
* Hardveres konfiguráció eltérhet (hálózat),
* VM rendszer összeomlása a fő operációs rendszert nem érinti,
* A VM-ek között megoszthatunk adatokat (A megosztott mappát felcsatolhatjuk).

### 1.2. Támogatott rendszerek (32 és 64 bites változatok is)
* Windows,
* Linux,
* Solaris,
* FreeBSD, OpenBSD,
* IBM OS/2,
* MAC OS X.

## 2. Telepítés
A telepítést két féleképpen lehet elvégezni:

1. Letöltöd az operációs rendszerednek megfelő csomagot [INNEN](https://www.virtualbox.org/wiki/Downloads),
2. Repo-bol telepited fel, így automatizálható a frissítés.

```bash
sudo echo "deb https://download.virtualbox.org/virtualbox/debian $(lsb_release -sr) contrib" | sudo tee /etc/apt/sources.list.d/virtualbox.list
sudo apt-key add oracle_vbox_2016.asc
sudo apt-key add oracle_vbox.asc
sudo wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
sudo wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -
sudo apt-get update 
sudo apt-get install virtualbox-6.1 virtualbox-ext-pack
```

## 3. Hálózatok
VirtualBox-ban többféle hálózati beállítás közül lehet választani. Ezek között a különbség az, hogy az elkészített virtuális gép melyik belső hálózat tagja és mely gépekkel képes kommunikálni. Több künönböző hálózattípus érhető el. A hálózati beállításokba kell először belépni, ami elérhető a megfelelő virtuális gépet kiválasztva, a **"Konfigurálás"** gombra kattintva és azon belül a **"Hálózat"** menüben.

Összesen 4 különböző hálózati kártyát lehet csatlakoztatni a virtuális géphez. Amíg a **"Hálózati adapter engedélyezése"** checkbox nincs bejelölve, addig az a hálózati kártya nem létezik a virtuális gép számára. Ha pedig lenyitjuk a **"Haladó"** beállításokat, akkor ott kivehető a pipa a **"Kábel bedugva"** checkbox-ból, és ebben az esetben a virtuális kábel nem lesz bedugva a virtuális gép hálózati kártyájába.

### 3.1. NAT
A NAT hálózattal a virtuális gép egy külön belső hálózatba kerül, ahol ő maga rendelkezik internet kapcsolattal, de más gépeket nem látják őt beleértve a gazda gépet is. Ha a virtuális gép csak ezzel a hálózattal rendelkezik, akkor egyetlen módja a gazda gépről való elérésének a megfelelő portok átirányítása. A NAT hálózatot választva, a **"Haladó"** beállításokat lenyitva, alul elérhetővé válik a **"Port továbbítása"** gomb. Arra kattintva pedig előugrik az ablak, ahol az új szabályokat felvehetjük. A jobb oldali plusz és kereszt ikonokkal lehet felvenni új szabályt, vagy törölni a kijelöltet. A plusz ikonra kattintás után pedig az egyes mezőkre duplán kattintva lehet azok tartalmát szerkeszteni.

* **Név**: A szabály neve. Például webszerver használatánál a 80-as portot a "WEB" nevű szabályként menthetjük, vagy bármi egyéb beszédes néven.
* **Protokoll**: Ez UDP vagy TCP lehet. Valószínűleg, leggyakrabban a TCP-re van szükség. Webszerver, adatbázis, ftp, ssh stb.. mind TCP protokollt igényelnek.
* **Gazda IP**: Ez a gazda gép IP-je, amin a VirtualBox fut. Ez persze sokféle lehet, hiszen a lokális gépre a 127.0.0.1, 127.0.0.2, 127.1.0.34 stb... címek bármelyikén hivatkozhatunk. Ezen kívül router mögött a belső hálózati ip cím is használható. Csak a beállított gazda IP-ről lehet majd elérni a virtuális gép adott portját, de több, különböző gazda IP is felvehető külön szabályként ugyanahhoz a vendég porthoz és IP-hez.
* **Gazda port**: A gazda gép portja. Azaz a GAZDAIP:GAZDAPORT címen lehet majd hivatkozni a vendég gépen levő adott címre.
* **Vendég IP**: Ez a vendég gép IP-je, azaz a virtuális gépé. Itt azt az IP-t kell megadni, amit a NAT hálózatban kapott a virtuális gép. Linuxban ennek root-ként az ifconfig, Windows-ban az ipconfig paranccsal lehet utánanézni.
* **Vendég port**: Ez a virtuális gépnek azon portja, amit a gazda gépről szeretnénk elérni.

A gazda és vendég gép portja megegyezhet, ha a gazda gépen egyébként nem fut semmi az adott porton. De teljesen különböző is megadható. Végül a GAZDAIP:GAZDAPORT címen lesz elérhető a VENDEGIP:VENDEGPORT cím.

### 3.2. Bridge-elt kártya
Bridge-elt kártyával a virtuális gép rácsatlakoztatható a gazda gép hálózatára. Azaz a virtuális gép és a gazda gép tud kommunikálni egymással és a virtuális gépnek is lesz internetelérése. Router mögötti belső hálózatnál ez annyit jelent, hogy a router úgy fogja látni a virtuális gépet, mintha az is egy fizikai gép lenne.

Ha ezt a megoldást választjuk, a **"Név"** mező is lenyíló listává válik és a gazda gépben levő hálókártyák közül lehet választani egyet.

### 3.3. Belső csatoló
A belső csatoló több virtuális gép között hoz létre hálózati kapcsolatot. Így tehát egy olyan belső hálózat alakítható ki tetszőlegesen választott virtuális gépekből, amiknek tagjai egymással kommunikálhatnak. Internet hozzáférésük viszont nincs, így ehhez szükség van egy NAT hálózatra is. Ilyenkor a 4 **"Kártya"** fül közül lehet választani, ahol az új interfész létrehozható.

A belső csatolóról még annyit kell tudni, hogy ezt választva szintén lenyíló lista lesz a **"Név"** mezőből, de szabadon is beírhatunk egy nevet. Ez a név a belső hálózat neve lesz. Több belső hálózat esetén a már használtak közül lehet választani, hogy az adott gép melyik alhálózat tagja legyen. Az alapértelmezett név az **"intnet"**. Ha nincs több ilyen belső hálózatunk, akkor ez is tökéletesen megteszi.

### 3.4. Host-only kártya
A **"Host-only"** kártya magyarul **"Csak gazdagép"**-nek lenne csúfolható. Azaz ebben az esetben a gazda gép és a virtuális gép tud csak egymással kommunikálni. Illetve az azonos host-only hálózatba tartozó virtuális gépek egymással. Az internetkapcsolathoz itt is szükséges egy NAT kártya. Viszont a virtuális gépet nem lehet elérhetővé tenni a külvilág számára.

Ennél a megoldásnál a **"Név"** mezőben előre meghatározott szoftveres hálózati adapterek közül lehet választani, amik megjelennek a gazda gép hálózatlistájában. Ezeken bonyolódik le a forgalom.

### 3.5. Host-only hálózati adapter
Ezt a hálózati adaptert a VirtualBox **"File"** menü **"Gazda hálózat kezelő"** almenüjében lehet elérni.
Jobb oldalt a már létrehozott adapterek vannak. A plusz ikonra kattintva új adaptert lehet létrehozni, a keresztre kattintva pedig a kiválasztottat törölni. A csillagra kattintva pedig megváltoztatni egyet.

Új adapter létrehozásához kattintsunk a plusz ikonra! Majd a következő ablakban az első fülön a hálózat ip címét kell megadni (ezt a címet látja a vendég gép egy felé küldött kéréskor), a másodikon pedig beállítható egy DHCP szerver az automatikus IP cím kiosztásához. Ez utóbbi nem feltétlenül szükséges. Kézzel is beállíthatók az IP címek.

* IPv4 cím: 192.168.56.1
* IPv4 hálózati maszk: 255.255.255.0

Ezzel a beállítással a hálózat címe 192.168.56.1 lesz. A maszk pedig gondoskodik róla, hogy csak az IP cím utolsó része változhasson a gépek IP címeinél. Az első három fixen 192.168.56 marad.

A DHCP szerver beállításával lesz egy IP tartomány, amiből szabadon kaphatnak egy címet a hálózatra csatlakozáskor. FIX IP híján használható ez. Vagy akár a két megoldás keverve.

* Szerver címe: 192.168.56.100 (A DHCP szerver IP címe)
* Szerver maszkja: 255.255.255.0
* Alsó címhatár: 192.168.56.101 ( Legkisebb kiosztható IP cím )
* Felső címhatár: 192.168.56.254 ( Legnagyobb kiosztható IP cím )

A fix ip címeket pedig 192.168.56.2 és 192.168.56.99 között lehet beállítani.

### 3.6. Általános meghajtó
Az általános driver a **"+1"**. hálózat opció. Ennél ugyanis olyan lehetőségeink vannak, mint az **"UDP Tunnel networking"** és a **"VDE networking"**. Összeköthetünk például két különböző gazdagépen futó virtuális gépet az UDP tunnel segítségével. Ez már egy komolyabb szint és ritkábban van rá szükség.

UDP Tunnel esetén a **"Név"** mezőbe az **"UDPTunnel"**-t kell írni, mint a driver nevét. A **"Haladó"** beállításokat lenyitva pedig az **"Általános tulajdonságok"** mezőben kulcs-érték párként egyenlőség jellel elválasztva kell beírni a tulajdonságot és annak értékét (dest, sport, dport). Egy újabb tulajdonság beviteléhez **SHIFT+ENTER** billentyűkombinációt kell használni.