## 1. Beveztés
A keresőmotorok olyan online eszközök, amelyek segítenek az interneten elérhető információk gyors és hatékony megtalálásában. Ezek az eszközök kulcsfontosságú szerepet játszanak a modern információkeresésben, és nélkülözhetetlenek mindennapi online tevékenységeink során.

## 2. Keresőmotorok működése
### 2.1. **Adatgyűjtés (Crawling)**
A keresőmotorok robotjai, más néven **webrobotok** vagy **botok**, folyamatosan "bejárják" az internetet. Felkeresnek weboldalakat, majd a tartalmukat letöltik és eltárolják. Ez a folyamat:

- **Hiperhivatkozások** alapján navigál az oldalakról más oldalakra.
- Szisztematikusan térképezi fel az internetet.

### 2.2. **Indexelés (Indexing)**
A letöltött adatokat a keresőmotor egy óriási adatbázisban rendezi és tárolja. Ez a folyamat:

- Kategorizálja az oldalak tartalmát (pl. kulcsszavak, metaadatok alapján).
- Értékeli az oldal szerkezetét és relevanciáját.
- Optimalizálja az információt gyors keresésre.

### 2.3. **Keresési algoritmus (Ranking)**
Amikor valaki keresést indít, a keresőmotor:

- Elemzi a keresési kifejezést (query).
- A lekérdezéshez kapcsolódó legjobb találatokat jeleníti meg.
- Az algoritmusok figyelembe veszik több szempontot is:
    - Kulcsszavak relevanciája.
    - Weboldalak népszerűsége és hitelessége (pl. más oldalak mennyire hivatkoznak rájuk).
    - Oldalbetöltési sebesség, mobilbarát kialakítás.

### 2.4. **Találatok megjelenítése (Results Display)**
A keresőmotorok a találati oldalakon (SERP - Search Engine Results Page) jelenítik meg az eredményeket. Ezek lehetnek:

- **Organikus találatok**: Az algoritmus alapján rangsorolt, nem fizetett találatok.
- **Fizetett hirdetések**: Reklámként megjelenített, fizetett helyek (pl. Google Ads).

## 3. Keresőmotor típusok
### 3.1. **Általános keresőmotorok**
Ezek a motorok az internet szinte teljes tartalmát indexelik.

- **Google**: A világ legnépszerűbb keresőmotorja.
- **Bing**: A Microsoft keresőmotorja.
- **Yahoo**: Bár veszített a népszerűségéből, még mindig jelentős szereplő.

### 3.2. **Speciális keresőmotorok**
Meghatározott témákra vagy tartalmakra fókuszálnak.

- **PubMed**: Orvosi és tudományos cikkek keresésére.
- **Wolfram Alpha**: Tudományos és matematikai kérdésekre ad választ.

### 3.3. **Privát keresőmotorok**
Az adatvédelemre helyezik a hangsúlyt, nem követik a felhasználók tevékenységeit.

- **DuckDuckGo**
- **Startpage** - A Google Dork használható itt.

### 3.4. **Metakeresők**
Több keresőmotor eredményeit egyesítik.

- **Dogpile**
- **Metacrawler**

## 4. Népszerű keresőmotorok
### 4.1. **Google**
- A keresőpiac vezetője, 90% körüli piaci részesedéssel.
- Fejlett keresési algoritmusok.
- Szolgáltatások integrációja (pl. Gmail, Google Maps).
- Gépi tanulás használata a relevancia javítására.
- [Google Dork](/ethical-hacking/informacio-gyujtes/dorking/google-dorking)

### 4.2. **Bing**
- A Microsoft fejlesztése.
- Jelentőséggel bír Észak-Amerikában és az üzleti szférában.
- Gyönyörű képek a nyitólapon.
- Speciális multimédiás keresések.

### 4.3. **DuckDuckGo**
- Adatvédelem-orientált.
- Nem követi a felhasználók keresési tevékenységeit.
- Egyszerű, tiszta találati oldalak.
- Nincsenek személyre szabott hirdetések.
- [DuckDuckGo Dork](/ethical-hacking/informacio-gyujtes/dorking/duckduckgo-dorking)

### 4.4. **Yahoo**
- Egykor vezető szerepet töltött be.
- Ma inkább e-mail szolgáltatásai miatt ismert.
