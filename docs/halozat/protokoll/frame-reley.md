## 1. Bevezetés
A frame relay a csomagkapcsolt hálózatok egy kommunikációs szabványa, amelyet az ANSI és a CCITT közösen dolgozott ki. Gyors adatátvitelt biztosít, a változó hosszúságú adatcsomagok úgynevezett "keretekbe" foglalva utaznak a hálózaton; jellemzően helyi hálózatok Internetre való kapcsolásához használják. Létezik állandó, és kapcsolt vonali szabványa.

Eredetileg ISDN vonalon történő adatátvitelre tervezték, de sok egyéb vonal/kábeltípuson is használják. A csomagkapcsolt hálózat különböző keretméretekkel, lehetővé teszi a meglévő fizikai sávszélesség jó kihasználását és felosztását különböző adatfolyamok között.

A 'frame relay protokollt gyakran az X25 utódjaként tartják számon, amely egyszerűbb felépítésével alkalmazkodik a 70-es évek óta megbízhatóvá vált nagy távolságú összeköttetésekhez.

Kifejlesztését az 1990-es években kezdte el a DEC, Cisco Systems, StrataCom, Northern Telecom által létrehozott konzorcium
