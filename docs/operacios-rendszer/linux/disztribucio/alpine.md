## 1. Bevezetés
Az Alpine Linux egy Linux disztribúció, amelyet kicsire, egyszerűre és biztonságosra terveztek. A legtöbb Linux disztribúciótól eltérően az Alpine a musl, BusyBox és OpenRC-t használja a gyakrabban használt glibc, GNU Core Utilities és systemd helyett.

A biztonság érdekében az Alpine az összes felhasználói terület bináris fájlját pozíciófüggetlen végrehajtható fájlként fordítja le, veremtörő védelemmel.

Kis mérete miatt gyakran használják konténerekben, amelyek gyors rendszerindítási időt biztosítanak.

## 2. Történelmi áttekintés
Eredetileg az Alpine Linux a LEAF Project ágaként indult. A LEAF tagjai továbbra is olyan Linux disztribúciót akartak készíteni, amely egyetlen hajlékonylemezre is elfér, míg az Alpine Linux néhány súlyosabb csomagot kívánt beépíteni: Squid és Samba. Biztonsági funkciókat és újabb kernelt is hozzáadtak.

Az Alpine csomagkezelő rendszere, az `apk-tools` eredetileg shell szkriptek gyűjteménye volt, de később átírták C-ben.
2014.június 4-től az Alpine Linux musl uClibc-ről váltott C szabványkönyvtárra.

## 3. Jellemzők
Az Alpine Linux RAM-ból futtatható operációs rendszerként telepíthető. Az LBU (Alpine Local Backup) eszköz opcionálisan lehetővé teszi az összes konfigurációs fájl biztonsági mentését egy APK overlay fájlba, egy tar.gz fájlba, amely alapértelmezés szerint az összes módosított fájl másolatát tárolja a /etc könyvtárban. (további könyvtárak hozzáadásának lehetőségével). Ez lehetővé teszi az Alpine számára, hogy megbízhatóan működjön nagy igénybevételt jelentő beágyazott környezetekben, vagy (átmenetileg) túlélje a részleges lemezhibákat, amint azt nyilvános felhőkörnyezetekben néha tapasztalják.

Alapértelmezés szerint az Alpine olyan javításokat tartalmaz, amelyek lehetővé teszik a hatékony hálós VPN-ek használatát a DMVPN szabvány használatával.

Az Alpine megbízhatóan kiválóan támogatja a Xen hipervizorokat a legfrissebb verziókban, ami elkerüli az Enterprise Distributions esetében tapasztalt problémákat. (A szabványos Linux hypervisor KVM is elérhető.)

Alpine Configuration Framework (ACF): Noha opcionális, az ACF egy Alpine Linux gép beállítására szolgáló alkalmazás, amelynek céljai hasonlóak a Debian debconf-jához. Ez egy szabványos keretrendszer, amely egyszerű Lua szkripteken alapul. 

## 4. Csomagkezelő (apk)
`apk` (Alpine Package Keeper) – az Alpine disztribúció csomagkezelője. A rendszer csomagjainak (szoftver és egyéb) kezelésére szolgál. Ez a további szoftverek telepítésének elsődleges módja, és elérhető a `apk-tools` csomagban.

### 4.1 Normál használat
Az `apk` információkat szerez be az elérhető csomagokról, valamint magukról a csomagokról a különböző tükrökből, amelyek különböző tárolókat tartalmaznak. Néha ezeket a kifejezéseket felcserélhetően használják. Íme a vonatkozó definíciók összefoglalása:

* Mirror: Adattárakat tároló webhely.
* Release: Pillanatképek gyűjteménye különböző adattárakról.
* Repository: Csomagok kategóriája, amelyeket valamilyen attribútum köt össze.

Jelenleg három adattár létezik:

* main: Hivatalosan támogatott csomagok, amelyektől ésszerűen elvárható, hogy egy alaprendszerben legyenek.
* community: A tesztelésből származó, tesztelt csomagok.
* testing: Új, törött vagy elavult csomagok, amelyeket tesztelni kell. Csak a következőn érhető el `edge`.

Kétféle kiadás létezik:

* stable (például 3.16): 6 havonta jelenik meg. A támogatás biztonsági javításokat jelent az adott szolgáltatás verziókhoz. Minden stabil kiadásnak megvan a maga `main` és `community` adattára. A `main` adattár 2 évig támogatott. A `community` tároló csak a megfelelő kiadástól számított 6 hónapig támogatott. Egy kiadás EOL-nak (End Of Life) számít, ha nem támogatja a main repo.
* edge: Gördülő kiadási ág. Ez tartalmazza a legújabb csomagokat, amelyek a `master` branch-en van. Kevésbé stabil, de ahhoz elég stabil, hogy a mindennapi használatban a fejlesztéshez frissített szoftverre van szükséged. Megvan a saját `main` és `community` repói, akárcsak a stabil kiadásai. Benne van a `testing` repository, ami jelentősen megnöveli az elérhető csomagok számát.

Technikailag lehetséges a különböző ágak keverése. Ez a megközelítés azonban nem ajánlott, és töréseket okoz.

A tárolók konfigurálhatók a `/etc/apk/repositories` fájlban. Minden sor egy adattárnak felel meg. A formátum a következő:

```bash
[@tag] [protocol][/path][/release]/repository
http://dl-cdn.alpinelinux.org/alpine/edge/main 
@testing http://dl-cdn.alpinelinux.org/alpine/edge/testing 
/var/apk/my-packages
```

### 4.2. Csomagok keresése
Ahhoz, hogy tudjuk, milyen csomagot kell telepíteni, meg kell találni a csomagokat. Az Alpine speciális webes felülettel rendelkezik , amely a különféle elérhető csomagok áttekintésére szolgál. Azonban az , `apk` beépített kereső mechanizmust is biztosít. Lehetséges, hogy bármit kereshet a csomag indexben, amely többek között tartalmazza a megadott binárisokat és könyvtárakat. Ezért íme néhány példa a keresésre:

```bash
apk search libsqlite3.so            # Kereshet részleges könyvtárnevekre.
apk search consul                   # Kereshet bináris nevekre is.
apk search -e vim                   # A részleges egyezéseket a '-e' kapcsoló használatával zárhatja ki.
apk search -e so:libsqlite3.so.*    # A '-e' kapcsoló segítségével megadhatja, hogy mit keres, ha egy könyvtár so:előtag (vagy a cmd:előtag a parancsokhoz, és pc:pkg-config fájlok előtagja).
```

### 4.3. Csomagok telepítése
Ha már tudod, hogy melyik csomagot szeretnéd telepíteni, akkor tudnod kell, hogyan kell azt megtenni. Az `apk add` parancs szigorúbb, mint a `search` parancs – a helyettesítő karakterek például nem állnak rendelkezésre. Azonban a cmd:, so:és pc:előtagok továbbra is elérhetők.

Íme néhány példa a csomagok hozzáadására:

```bash
apk add busybox-extras      # Meg kell adnia a csomag pontos nevét.
apk add bash zsh            # Egyszerre több csomagot is hozzáadhatsz.
apk add cmd:bash cmd:zsh    # Ennek egyenértékűnek kell lennie az előző példával.
apk add so:libmpack.so.0    # Lehetséges, de nem ajánlott, konkrét kívánt könyvtárakat megadni.
apk add pc:msgpack          # Végül lehetőség van pkg-config függőségek megadására.
```

### 4.4. Csomagok frissítése
A rendszer frissítése `apk` használatával nagyon egyszerű. Csak futattni kell az `apk upgrade` parancsot. Technikailag ez két lépésből áll: `apk update`, utána `apk upgrade`. Az első lépésben egy frissített csomagindexet tölt le a tárolókból, míg a második lépésben a World összes csomagját, valamint azok függőségeit.

Az `apk` elkerüli az esetleg módosított fájlok felülírását. Ezek általában a `/etc` könyvtárban vannak. Ha az `apk` telepíteni akar egy fájlt, de észreveszi, hogy az potenciálisan szerkesztett és már létezik, akkor a fájlt erre a fájlnévre nevezi át `.apk-new`. Ezeket kézzel is kezelheted, de látezik egy `update-conf` parancs is erre. Egyszerűen a normál meghívása megmutatja a két fájl közötti különbséget, és különféle lehetőségeket kínál a konfliktusok kezelésére.

### 4.5. Csomaginformációk lekérdezése
Egyes esetekben hasznos lehet megvizsgálni a csomagokat vagy fájlokat a különféle részletek megtekintéséhez. Ehhez az `info` alparancs létezik. Bármely csomagon használható, függetlenül attól, hogy telepítve van-e vagy sem, bár az utóbbira vonatkozó információk korlátozottabbak lesznek. Használható fájlok speciális jelzőivel is. Alapértelmezés szerint, az `info` listázza a csomag leírását, a weboldalt és a telepített méretet. További részletekért (például az alparancs által támogatott kapcsolók listájáért) használhatja a `apk info -h` parancsot.

### 4.6. Csomagok eltávolítása
Gyakran kívánatos a csomag eltávolítása. Ezt a `del` alparancsal lehet megtenni, amelynek alapszintaxisa megegyezik a `add` alparancséval. A `del` alparancs is támogatja a `-r` kapcsolót, amely eltávolítja az összes olyan csomagot, amely az eltávolított csomagtól is függ, ahelyett, hogy a csomagra szükség lenne a hiba miatt.

### 4.7. Tisztítás
Sok csomagkezelő speciális funkciókkal rendelkezik a "megtisztításhoz". Az `apk` alapértelmezés szerint ezt teszi a csomagok eltávolításakor.

Lehetőség van az `apk` gyorsítótár törlésére is, feltéve, hogy engedélyezve van. Ezt az `apk cache clean` segítségével teheted meg.