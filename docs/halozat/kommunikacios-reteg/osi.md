## 1. Bevezetés
Az OSI-modell (angolul: Open Systems Interconnection Model) egy olyan elvi modell, amely egy távközlési vagy számítástechnikai rendszerek kommunikációs funkcióit jellemzi és szabványosítja, tekintet nélkül a mögöttes belső szerkezetre és technológiára. Célja a különféle kommunikációs rendszerek szabványos kommunikációs protokollokkal való átjárhatósága. A leírást gyakran az OSI hétrétegű modellje vagy OSI hivatkozási modell néven is emlegetik.

Az elnevezésben az Open Systems Interconnection (magyarul: Nyílt rendszerek összekapcsolása) arra utal, hogy nyílt rendszerek összekapcsolásával foglalkozik. A modell a kommunikációs rendszerben lévő adatáramlást hét absztrakciós rétegre osztja fel, a kommunikációs csatornán keresztüli bitek átvitelének fizikai megvalósításától kezdve az elosztott alkalmazások adatainak legmagasabb szintű megjelenítéséig.

## 2. Céljai
Az OSI modellje a különböző protokollok által nyújtott funkciókat egymásra épülő rétegekbe sorolja. Minden réteg csak és kizárólag az alsóbb rétegek által nyújtott funkciókra támaszkodhat, és az általa megvalósított funkciókat pedig csak felette lévő réteg számára nyújthatja. A rendszert, amelyben a protokollok viselkedését az egymásra épülő rétegek valósítják meg, gyakran nevezik 'protokoll veremnek' vagy 'veremnek'. A protokoll verem mind hardver szinten, mind pedig szoftveresen is megvalósítható, vagy a két megoldás keverékeként is. Tipikusan csak az alsóbb rétegek azok, amelyeket hardver szinten (is) megvalósítanak, míg a felsőbb rétegek szoftveresen kerülnek megvalósításra.
Az Osi rétegei, adatáramlás

Ez az OSI modell alapvetően meghatározó volt a számítástechnika és hálózatokkal foglalkozó ipar számára. A legfontosabb eredmény az volt, hogy olyan specifikációkat határoztak meg, amelyek pontosan leírták, hogyan léphet egy réteg kapcsolatba egy másik réteggel. Ez azt jelenti a gyakorlatban, hogy egy gyártó által írt réteg programja együtt tud működni egy másik gyártó által készített programmal (feltéve, hogy az előírásokat mindketten pontosan betartották). Az említett specifikációkat a TCP/IP közösség a Requests for Comments vagy „RFC”-k néven ismeri. Az OSI közösségben használt szabványokat itt lehet megtalálni: ISO szabványok.

Az OSI referencia modellje, a hét réteg hierarchikus rendszere meghatározza a két számítógép közötti kommunikáció feltételeit. A modellt az International Organization for Standardization az ISO 7498-1 számú szabványában írta le. A cél az volt, hogy biztosítsa a hálózati együttműködést különböző gyártók különböző termékei között, különböző platformok alkalmazása esetén, anélkül, hogy lényeges lenne, melyik elemet ki gyártotta, illetve készítette. A modell kidolgozása 1977-ben kezdődött meg, és maga a szabvány 1984-ben jelent meg.

Természetesen, időközben a TCP/IP is terjedni kezdett. A TCP/IP az ARPANET alapjául szolgált, és innen fejlődött ki az internet. (A legfontosabb különbségeket a TCP/IP és az ARPANET között, lásd RFC 871.)

Ma a teljes OSI modell egy részhalmazát használják csak. Széles körben elterjedt nézet, hogy a specifikáció túlzottan bonyolult, és a teljes modell megvalósítása nagyon időigényes lenne, ennek ellenére nagyon sokan támogatják a teljes modell megvalósítását.

Másik oldalról, többen úgy érzik, hogy az ISO alapú hálózati fejlesztéseket mielőbb be kellene fejezni, mert így komoly károk előzhetők meg.

## 3. Az OSI rétegek definiálása
### 3.1. Fizikai réteg – Physical Layer az 1. szint
A fizikai réteg feladata a bitek kommunikációs csatornára való juttatása. Ez a réteg határoz meg minden, az eszközökkel kapcsolatos fizikai és elektromos specifikációt, beleértve az érintkezők kiosztását, a használatos feszültség szinteket és a kábel specifikációkat. A szinten "Hub"-ok, "repeater"-ek és "hálózati adapterek" számítanak a kezelt berendezések közé. A fizikai réteg által megvalósított fő funkciók:

* Felépíteni és lezárni egy csatlakozást egy kommunikációs médiummal.
* Részt venni egy folyamatban, amelyben a kommunikációs erőforrások több felhasználó közötti hatékony megosztása történik. Például, kapcsolat szétosztás és adatáramlás vezérlés.
* Moduláció, vagy a digitális adatok olyan átalakítása, konverziója, jelátalakítása, ami biztosítja, hogy a felhasználó adatait a megfelelő kommunikációs csatorna továbbítani tudja. A jeleket vagy fizikai kábelen – réz vagy optikai szál, például – vagy rádiós kapcsolaton keresztül kell továbbítani.

Paralell SCSI buszok is használhatók ezen a szinten. A számos Ethernet szabvány is ehhez a réteghez tartozik; az Ethernetnek ezzel a réteggel és az adatkapcsolati réteggel is együtt kell működnie. Hasonlóan együtt kell tudni működnie a helyi hálózatokkal is, mint például a Token ring, FDDI, és az IEEE 802.11.

### 3.2. Adatkapcsolati réteg – Data-Link Layer a 2. szint
Az adatkapcsolati réteg biztosítja azokat a funkciókat és eljárásokat, amelyek lehetővé teszik az adatok átvitelét két hálózati elem között. Jelzi, illetve lehetőség szerint korrigálja a fizikai szinten történt hibákat is. A használt egyszerű címzési séma fizikai szintű, azaz a használt címek fizikai címek (MAC címek) amelyeket a gyártó fixen állított be hálózati kártya szinten. Megjegyzés: A legismertebb példa itt is az Ethernet. Egyéb példák: ismert adatkapcsolati protokoll a HDLC és az ADCCP a pont-pont vagy csomag-kapcsolt hálózatoknál és az Aloha a helyi hálózatoknál. Az IEEE 802 szerinti helyi hálózatokon, és néhány nem-IEEE 802 hálózatnál, mint például az FDDI, ez a réteg használja a Media Access Control (MAC) réteget és az IEEE 802.2 Logical Link Control (LLC) réteget is.

Ez az a réteg, ahol a bridge-ek és switch-ek működnek. Ha helyi hálózat felé kell a kapcsolatot kiépíteni, akkor kapcsolódást csak a helyi hálózati csomópontokkal kell létrehozni, a pontos részleteket a „2.5 réteg” írja le.

### 3.3. A 2.5 réteg
Ez a réteg valójában nem része az eredeti OSI modellnek. A „2.5 réteg” kifejezés jelzi, hogy a kategóriába tartozó protokollok a 2-es és 3-as réteghez egyaránt kapcsolhatók. Ilyenek például a Multiprotocol Label Switching (MPLS) műveletek az adatcsomagokkal (2. réteg) illetve az IP protokoll címzése (3. réteg) amely speciális jelzéseket használ az útvonalirányítás során.

### 3.4. Hálózati réteg – Network layer a 3. szint
A hálózati réteg biztosítja a változó hosszúságú adat sorozatoknak a küldőtől a címzetthez való továbbításához szükséges funkciókat és eljárásokat, úgy, hogy az adatok továbbítása a szolgáltatási minőség függvényében akár egy vagy több hálózaton keresztül is történhet. A hálózati réteg biztosítja a hálózati útvonalválasztást, az adatáramlás ellenőrzést, az adatok szegmentálását/deszegmentálását, és főként a hiba ellenőrzési funkciókat. Az útvonalválasztók (router-ek) ezen a szinten működnek a hálózatban – adatküldés a bővített hálózaton keresztül, és az internet lehetőségeinek kihasználása (itt dolgoznak a 3. réteg (vagy IP) switch-ek). Itt már logikai címzési sémát használ a modell – az értékeket a hálózat karbantartója (hálózati mérnök) adja meg egy hierarchikus szervezésű címzési séma használatával. A legismertebb példa a 3. rétegen az Internet Protocol (IP).

### 3.5. Szállítási réteg – Transport layer a 4. szint
A szállítási réteg biztosítja, hogy a felhasználók közötti adatátvitel transzparens legyen. A réteg biztosítja, és ellenőrzi egy adott kapcsolat megbízhatóságát. Néhány protokoll kapcsolat orientált. Ez azt jelenti, hogy a réteg nyomonköveti az adatcsomagokat, és hiba esetén gondoskodik a csomag vagy csomagok újraküldéséről. A legismertebb 4. szintű protokoll a TCP.

### 3.6. Viszony réteg – Session layer az 5. szint
A viszony réteg a végfelhasználói alkalmazások közötti dialógus menedzselésére alkalmas mechanizmust valósít meg. A megvalósított mechanizmus lehet duplex vagy félduplex, és megvalósítható ellenőrzési pontok kijelölési, késleltetések beállítási, befejezési, illetve újraindítási eljárások.
(A mai OSI modellben a Viszonylati réteg a Szállítási rétegbe lett integrálva.)

### 3.7. Megjelenítési réteg – Presentation layer a 6. szint
A megjelenítési réteg biztosítja az alkalmazási réteg számára, hogy az adatok a végfelhasználó rendszerének megfelelő formában álljon rendelkezésre. MIME visszakódolás, adattömörítés, titkosítás, és egyszerűbb adatkezelések történnek ebben a rétegben. Példák: egy EBCDIC-kódolású szöveges fájl ASCII-kódú szövegfájllá konvertálása, vagy objektum és más adatstruktúra sorossá alakítása és XML formába alakítása vagy ebből a formából visszaalakítása valamilyen soros formába.
A mai OSI modellben az Adatmegjelenítési réteg az Alkalmazási rétegbe lett integrálva. (A mai OSI ezért valójában 5 rétegű mivel a régi 7 rétegű modell 5. rétege a 4. illetve a 6. rétege a 7. rétegbe integrálódott. )

Feladata:

* Két számítógép között logikai kapcsolat létesítése
* Párbeszéd szervezése
* Vezérjelkezelés
* Szinkronizálás

### 3.8. Alkalmazási réteg – Application layer a 7. szint
Az alkalmazási réteg szolgáltatásai támogatják a szoftver alkalmazások közötti kommunikációt, és az alsóbb szintű hálózati szolgáltatások képesek értelmezni alkalmazásoktól jövő igényeket, illetve, az alkalmazások képesek a hálózaton küldött adatok igényenkénti értelmezésére. Az alkalmazási réteg protokolljain keresztül az alkalmazások képesek egyeztetni formátumról, további eljárásról, biztonsági, szinkronizálási vagy egyéb hálózati igényekről. A legismertebb alkalmazási réteg szintű protokollok a HTTP, az SMTP, az FTP és a Telnet.
