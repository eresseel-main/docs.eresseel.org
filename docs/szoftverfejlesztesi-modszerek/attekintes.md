## 1. Bevezetés
A projektmenedzsment nem újkeletű dolog. Már az 1960-as években felismerték annak igényét, hogy a nagyobb, jellemzően egyedi termék, vagy szolgáltatás létrehozását, fejlesztését irányított folyamat szerint érdemes kézben tartani.

A nemzetközileg elfogadott definíció szerint a projektmenedzsment a projektkövetelmények teljesítése érdekében végzett tevékenységek során tudás, képességek, eszközök és technikák alkalmazása. Ettől függetlenül mi jobban szeretjük az egyszerűbb, a “dolgok megvalósíttatásának tudománya” megfogalmazást.

A projektmenedzsment is, mint oly sok szakma az évek során hol organikusan, hol tudatosan fejlődött. Kezdetben a projektmenedzsment még nem önálló szakmaként jelent meg, ez sok esetben csupán egy “sapka” volt, amit a projektben (még ha nem is így nevezték) dolgozó egy-egy szereplő kapott meg. Az idő múlásával és a feladatok komplexitásának növekedésével azonban ez is önálló, teljes értékű szereppé alakult.

Bármilyen projektről is beszéljünk, alapjaiban minden ilyen jellegű feladatot három tényező befolyásol leginkább. Ezek a terjedelem (mit csináljunk), idő (mikorra szállítsunk) és a költségek (mennyibe kerül).

A mai napig kis túlzással minden projekt e háromszögben mozog és próbálja a megrendelői igényeket (mindent, gyorsan, olcsón) egyensúlyban tartani és kielégíteni.

Az 1970-es években elterjedt vízesés modell alapú megközelítéssel a projektek sikerességi mutatója komoly mértékben nőtt. Ennek oka, hogy tudatos tervezéssel egy viszonylag jól kiszámítható, akkor még a maihoz képest nem túl gyorsan változó környezetben lehetett sikereket elérni.

Ez a ’90-es években azonban már egyre többször kevésnek bizonyult, a technológia hirtelen fejlődésével és a projektek számának növekedésével egyre több utólag fölöslegesnek bizonyult munka és egyre több csúszás, vagy költségtúllépés jelent meg.

Ez a felismerés adott teret a különböző, vízeséstől eltérő módszertanoknak is.

## 2. Irányzatok
A projektmenedzsment fejlődésével különböző irányzatok alakultak ki, melyek eltérő módszertanokban öltöttek testet. Projektmenedzsment módszertannak nevezzük a projektekben közreműködők által használt gyakorlatok, technikák, folyamatok és szabályok rendszerét.

Mára elképesztően sok módszertan került kialakításra, vannak “dobozosak”, de vannak szervezeti szinten fejlesztett megoldások is.

Amennyiben ezeket csoportosítani szeretnénk, alapvetően mi 4 féle irányzatot szoktunk definiálni. Ez a változások mértéke és a szállítás gyakorisága mentén azonosíthatók be, tehát a megközelítés többek közt attól függ, hogy a szállítást egy-két, vagy több modulon, részterméken, elemen, ún. inkrementumon keresztül szeretnénk-e megvalósítani, valamint, hogy milyen mértékű változásra számítunk az elvárásokkal, vagy akár ezek gyakori kiváltó okával, a környezettel kapcsolatban.