## 1. Bevezetés
A számítógép-hálózat olyan speciális rendszer, amely a számítógépek egymás közötti kommunikációját biztosítja. A számítógépek az egymással való információcseréhez digitális összeköttetéseken keresztül közös kommunikációs protokollokat használnak. Ezek a kapcsolódások különböző távközlési technológiákból épülnek fel, amelyek fizikailag lehetnek vezetékes, azon belül réz vagy optikai kábeles, illetve vezeték nélküli, különféle rádiófrekvenciás megoldások.

A számítógép-hálózat csomópontjai lehetnek személyi számítógépek, szerverek, hálózati hardverek, mint a modemek, a routerek és a switchek, vagy egyéb speciális illetve általános célú gazdagépek.

A számítógépes hálózatok számos kritérium alapján csoportosíthatóak, ideértve a jelek továbbítására használt átviteli közeget, a sávszélességet, a hálózati forgalom szervezésére szolgáló kommunikációs protokollokat, a hálózat kiterjedését és méretét, a topológiát, a forgalomirányítási mechanizmust és a szervezeti célokat.

A számítógép-hálózat lehet fix (kábelalapú, állandó) vagy ideiglenes (mint például a modemen vagy null modemen keresztüli kapcsolat). A vezeték nélküli internet általában vagy a cellás (mobil) szolgáltatásra, vagy a wifis megoldásra épül.

## 2. Története
A számológépek és a korai számítógépek közötti utasítások továbbítását kezdetben maguk az emberek végezték. 1940 szeptemberében George Stibitz telexgépet használt arra, hogy a Bell laboratórium kutatási projektjének keretein belül készült Complex Number Calculator nevű gépnek utasításokat küldjön a New Hampshire-ben lévő Dartmouth College-ban tartott bemutató helyszínéről New Yorkba, ahol a gép üzemelt, illetve az eredményeket hasonló módon kapta vissza. A számítógépek kimeneti perifériáinak (telexgépek) összekapcsolását először 1962-ben, az Advanced Research Projects Agency ARPA keretében végezte el J. C. R. Licklider az általa kidolgozott „Intergalactic Computer Network” nevű hálózati koncepció alapján. A kutatók 1964-ben Dartmouthban kifejlesztették az időosztásos rendszert, amely lehetővé tette egy nagy számítógép szolgáltatásainak nagyszámú felhasználó közötti megosztását. Még ugyanebben az évben az MIT, valamint a General Electric és a Bell Labs fejlesztőiből álló csoport egy DEC PDP–8-as számítógéppel megvalósította egy telefonközpont vezérlését.

Paul Baran 1968-ban tett javaslatot egy olyan hálózati rendszerre, amelyben adatcsomagokat, ún. datagramokat továbbítanak. Ez a rendszer lett az alapja a csomagkapcsolt számítógépes hálózatoknak. 1969-ben a University of California (Los Angeles), az SRI (Stanford), a University of California (Santa Barbara) és a University of Utah kialakították a gépeik összekapcsolásával az ARPANET hálózatot, amely még 50 kbit/s hurok használatával működött.

A hálózatok és a technológiák fejlődése, a különféle összeköttetési lehetőségek bővülése, a számítógépek egymással és egymáson keresztüli kapcsolatai iránti igények növekedése ösztönözte az iparág egyes területeinek fejlesztéseit és fejlődését (hardver, szoftver, perifériák). E fejlődés eredményeként ugrásszerűen megnőtt a hálózatot használók száma, mind az üzleti területeken, mind pedig az otthoni alkalmazásoknál, és napi gyakorlattá válik a hálózati szolgáltatások növekvő méretű használata.

## 3. Területi kiterjedés
A számítógép-hálózatok egyik lehetséges rendszerezési módja a földrajzi értelemben vett kiterjedésük és elhelyezkedésük alapján történő kategorizálás.

* **Személyi hálózat (angolul: Personal Area Network, röviden: PAN)**: Olyan kis kiterjedésű – általában 10 méternél kisebb hatótávolságú – hálózat, amely lehetővé teszi, hogy egy ember környezetében lévő eszközök kommunikáljanak egymással.

* **Helyi hálózat (angolul: Local Area Network, röviden: LAN)**: Olyan kis kiterjedésű – 10 métertől kezdve, akár 1 kilométeres területet lefedő - hálózat, amely számítógépeket és eszközöket köt össze korlátozott földrajzi területen, például otthonokban, iskolákban, irodaházakban vagy egymáshoz közel elhelyezkedő épületcsoportokban.

* **Otthoni hálózat (angolul: Home Area Network, röviden: HAN)**: Otthoni digitális eszközök közötti kommunikációra használják, általában kis számú személyi számítógép és perifériák, például nyomtatók és mobil számítástechnikai eszközök, mint otthon-automatizálási berendezések és szenzorok.

* **Városi hálózat (angolul: Metropolitan Area Network, röviden: MAN)**: Városi méretű hálózat. Egyik példája a sok városban elérhető kábeltévé-hálózat (pl. DOCSIS).

* **Nagy kiterjedésű hálózat (angolul: Wide Area Network, röviden: WAN)**: nagy távolságú, nagyméretű hálózat.

### 3.1. Hálózatok osztályozása kiterjedés szerint
| Processzorok közötti távolság  | Processzorok elhelyezkedése | Példa                           |
|--------------------------------|-----------------------------|---------------------------------|
| 1 m                            | Asztalon                    | Személyi hálózat (PAN)          |
| 10 m                           | Szobában                    | Helyi hálózat (LAN)             |
| 100 m                          | Épületben                   | Helyi hálózat (LAN)             |
| 1 km                           | Egyetemen, üzemben          | Helyi hálózat (LAN)             |
| 10 km                          | Városban                    | Városi hálózat (MAN)            |
| 100 km                         | Országban                   | Nagy kiterjedésű hálózat (WAN)  |
| 1000 km                        | Földrészen                  | Nagy kiterjedésű hálózat (WAN)  |
| 10 000 km                      | Bolygón                     | Internet (GAN)                  |

A hálózatok összekapcsolásával létrejött hálózatot összekapcsolt (angolul: internetwork) hálózatnak hívjuk.

## 4. Hálózati topológiák
A hálózati topológia a számítógép-hálózatok esetén a hálózathoz tartozó csomópontok közötti kapcsolatokat határozza meg. Egy adott csomópont vagy egy másik csomóponthoz, vagy több másik csomóponthoz kapcsolódhat, különböző minták szerint. A legegyszerűbb kapcsolat két csomópont között az egyirányú kapcsolat. Egy újabb kapcsolat hozzáadásával már kétirányú kapcsolat valósítható meg a csomópontok között.

A hálózati topológia esetében a csomópontok közötti összeköttetések ténye érdekes mindössze: sem az összeköttetés módja, átviteli sebessége, a csomópontok távolsága, az összeköttetés fizikai módja, irányultsága(i) stb. nem érdekesek, az összeköttetéseket irányítatlannak tekintjük, így a hálózati topológia a gráfelmélet eszközeivel tárgyalható. Egy adott topológia alapján megvalósított hálózat esetében természetesen már lényegesek a csomópontok közötti távolságok, a fizikai összeköttetési módja, az átvitel sebessége, de a hálózat topológiájára nincsenek hatással, az csak egy adott mintát követhet.

### 4.1. Centralizált
* A [**busztopológia**](/halozat/topologia/centralizalt/busz) tulajdonképpen egy olyan csillagtopológia, amelyben nincsen központi csomópont.

* A **csillag** topológia csökkenti a hálózati meghibásodás esélyét azzal, hogy minden csomópont kapcsolatban áll a központi csomóponttal. Ha alkalmazzuk ezt a megoldást egy busz hálózatra, akkor ez a központi [**hub**](/halozat/eszkoz/hub/hub) minden kapott üzenetet szétküld minden csomópont számára, ami azt jelenti, hogy két "perifériális" csomópont mindig a központi csomóponton keresztül tud kommunikálni. A központi csomópont és egy "perifériális" csomópont közötti átviteli vonal meghibásodása ugyan elszigeteli a hálózattól az adott csomópontot, de a hálózat többi részének a működőképességére ez nincsen hatással.

    A központi csomópont viselkedhet passzívan, ekkor a küldő csomópontnak képesnek kell lennie arra, hogy kezelje azt az esetet, amikor a saját, már egyszer elküldött üzenetét bizonyos késleltetéssel (a központi csomópontig és vissza út ideje, plusz a központi csomópont feldolgozási ideje) ismételten megkapja. Ezt nevezik üzenet visszhangnak. Aktív hálózati csomópont esetén ez előző jelenség nem lép fel.

* A [**fatopológia**](/halozat/topologia/centralizalt/fa/) (hierarchikus topológia) látszólag hierarchikusan rendezett csillag topológiák gyűjteménye. Minden fatopológiában létezhetnek egyedülálló, perifériális csomópontok, ezek az úgynevezett "levelek". A csillag topológiától eltérően, ahol az üzenetek közvetítését a központi csomópont végzi, a fa topológiában ez a funkció nem központosított, az ágaknál lévő csomópontok valósítanak meg hasonló funkciót.

Egy hagyományos csillag elrendezés esetén a meghibásodás egy csomópontot elszigetelhet a hálózat többi részétől, amelyek azonban működőképesek. Egy fa elrendezésnél egy levél szintén izolálódhat, a fennmaradó rész üzemképes marad; egy ág leválásával viszont a hálózat esetleg jelentős része izolálódhat a maradéktól.

Annak érdekében, hogy csökkenthető legyen a hálózaton belüli forgalom mennyisége, különös tekintettel a minden csomópont felől küldött üzenetekre, néhány fejlettebb központi csomópont már alkalmazkodik a hálózat topológiájához. Ezek a hálózati switchek az első szétküldött üzenetekre kapott válaszok után mintegy „megtanulják” az aktuális hálózati topológiát.

### 4.2. Decentralizált
A legegyszerűbb topológia a lánctopológia, ahol a hálózati csomópontok között egy kapcsolat van csak. Ez a topológia egyszerű, viszont a hálózat egy kapcsolat kiesése miatt két önálló "szigetre" esik szét.

Ha egy lánc hálózat első és utolsó csomópontját is összekötjük, akkor a gyűrűtopológiát kapjuk.

Mindkét eddig felsorolt hálózati topológiának van hurkolt változata is, a hurkolt lánc, illetve a hurkolt gyűrű: ekkor a csomópontok között két kapcsolat van, ami nagymértékben megnöveli a hálózatok hibatűrő képességét. A lánc hálózat esetében a lánc nem szakad szét szigetekre, a gyűrű pedig nem alakul át lánc topológiává, ha egy kapcsolat megszakad.

Egy hurkolt topológia esetében van legalább két olyan hálózati csomópont, amelyek két vagy több csomóponton keresztül érhetők el. Egy hurok egy speciális esete, amikor korlátozott a két csomópont közötti "közbülső" csomópontok száma, egy hiperkocka. A villa esetében a "közbülső" csomópontok száma tetszés szerinti, az ilyen hurkolt típusú hálózatok tervezése és megvalósítása igen bonyolult, ugyanakkor a decentralizált természetük miatt nagyon praktikusak. Bizonyos szempontból hasonlóak a rácsos hálózatok, ahol egy gyűrű vagy lánc topológiák kapcsolódnak össze több kapcsolattal. Egy több dimenziós gyűrű topológia például a toroid (tórusz) topológia.

Egy teljesen összekapcsolt, teljes topológiájú vagy teljesen hurkolt topológia olyan hálózati topológia, amelyben minden csomópont pár össze van kapcsolva. Egy n csomópontból álló, teljes hálózat n(n-1)/2 közvetlen csomóponti kapcsolatot tartalmaz. Az ilyen topológia alapján tervezett és megvalósított hálózatok általában nagyon drágák, cserébe viszont kimagaslóan megbízhatók, ami annak köszönhető, hogy több kapcsolaton is eljuthatnak az információk a csomópontok között. Fentiek miatt ezek a hálózati topológiák a katonai célú hálózatokban terjedtek el.

### 4.3. Hibrid
A hibrid hálózatok bármilyen két, vagy több hálózati típus összekapcsolásával kialakíthatók, ha azok egyik sztenderd formára sem hasonlítanak. Például, fa hálózatok összekapcsolása újabb fa hálózathoz vezet, viszont két csillag hálózat összekapcsolása (nevezik kiterjesztett csillag hálózatnak) már hibrid hálózatot eredményez. Egy hibrid hálózat minden esetben előállítható két különböző típusú hálózat összekapcsolásával.

Például vegyük a következő két hibrid hálózatot: a csillag gyűrű hálózatot és a csillag busz hálózatot:

* Egy csillag gyűrű hálózat két vagy több csillag topológiájú hálózatból áll, amelyeket egy multistation access unit (MAU) használatával, mint központi hubbal valósíthatunk meg.
* Egy csillag busz hálózat két vagy több csillag topológiájú hálózatból áll, amelyek központi csomópontjait egy busz hálózat kapcsol össze.

Amíg a rács hálózatok egyre népszerűbbek lettek a nagy teljesítményű (multiprocesszoros)számítógépek illetve alkalmazások területén, néhány rendszert genetikus algoritmusok alapján terveztek meg, hogy minél kevesebb olyan csomópontja legyen a hálózatnak, amelyek csak egymással állnak kapcsolatban. Néhány ilyen hálózat, annak ellenére, hogy nem teljesen átfogó, igen jól működik.

## 5. Hálózati technológiák
A számítógépes hálózatoknál használt technológiáknak két típusa van: az adatszórásos hálózatok és a pont-pont hálózatok.

### 5.1. Adatszóró hálózatok
Az adatszórásos hálózatok (broadcasting) egyetlen kommunikációs csatornával rendelkeznek, amelyet a hálózatra csatlakozó összes gép közösen használ. Ez a gyakorlatban azt jelenti, ha a gazdagép (host) egy rövid üzenetet küld, akkor azt a hálózat összes gépe megkapja. Ezeket a rövid üzeneteket a használt protokolltól függően csomagnak (packet), keretnek (frame) vagy cellának (cell) nevezik. A feladót és a címzettet a rövid üzeneten belüli címmezőben lehet azonosítani. Ha egy gazdagép kap egy ilyen üzenetet, akkor megnézi a címmezőt. Ha az üzenet nem neki szól, akkor nem tesz vele semmit, ellenkező esetben viszont feldolgozza.

Az adatszóró rendszerek általában lehetővé teszik, hogy a címmező speciális beállításával az adott üzenetet minden gép megkapja és feldolgozza, ez az adatszóró (broadcasting) működési mód. Egyes rendszerek megengedik, hogy a hálózati gépek egy bizonyos csoportja kapja csak meg az üzenetet. Ez az üzemmód a többesküldés (multicasting). A gazdagépek „előfizethetnek” bizonyos címcsoportokra, de akár az összes címcsoportra is. Azok, akik nem „fizettek elő” egy címcsoportra, azok hiába kapják meg az üzenetet, az számukra olyan, mintha nem nekik szólna. A multicasting mód használata esetében a címmező n bitjéből 1-et fenntartunk az üzemmód jelzésére, n-1 bit pedig a csoport(ok) címzésére használható.

### 5.2. Pont-pont hálózatok
A pont-pont hálózatok (point-to-point network) sok olyan kapcsolatból állnak, amelyek géppárokat kötnek össze. Ez azt jelenti, hogy egy üzenet továbbítása egy, esetleg több csomóponton keresztül történik, és lehetséges, hogy egynél több lehetséges úton is eljuthat egy üzenet a céljához. Ezekben a hálózatokban az útvonal optimális megválasztása alapvető fontosságú. Ezt a hálózati technológiát nevezik még egyesküldésnek (unicasting) is.

## 6. Kategóriák
### 6.1. Funkcionális kapcsolatok szerint
* [Kliens-szerver](/halozat/funkcionalis-kapcsolat/kliens-szerver/)
* Host-terminal (vendéglátó terminál)
* [Peer-to-peer](/halozat/funkcionalis-kapcsolat/peer-to-peer/) (egyenrangú munkacsoportok)

### 6.2. Speciális funkciók szerint
* Tároló hálózatok
* Szerver farmok
* Folyamat irányító hálózatok
* Értéknövelt hálózat
* SOHO hálózat
* Drót nélküli nyilvános hálózat
* XML appliance

## 7. Adatátviteli és hálózati protokollok
A számítógépes hálózatok számtalan adatátviteli/hálózati protokollt használnak, az átvitel, a hálózat, az átviteli közeg, a feladat, a gép architektúrájának függvényében. A leggyakrabban használt, és legismertebb protokollok (a teljesség igénye nélkül) a következők:

* [ARCNET](/halozat/protokoll/arcnet/)
* AppleTalk
* ATM
* [Bluetooth](/halozat/protokoll/bluetooth/)
* DECnet
* [Ethernet](/halozat/protokoll/ethernet/)
* FDDI
* [Frame Relay](/halozat/protokoll/frame-reley/)
* HIPPI
* [IEEE 1394](/halozat/protokoll/ieee-1394/)
* [IEEE 802.11](/halozat/protokoll/ieee-80211/)
* IEEE-488
* [IP](/halozat/protokoll/ip/)
* IPX
* Myrinet
* QsNet
* RS-232
* SPX
* System Network Architecture
* [Token-Ring](/halozat/protokoll/token-ring/)
* [TCP](/halozat/protokoll/tcp/)
* [USB](/halozat/protokoll/usb/)
* [UDP](/halozat/protokoll/udp/)
* [ARP](/halozat/protokoll/arp/)
* [SNMP](/halozat/protokoll/snmp/)
* X.25

## 8. Kommunikációs rétegek
Az adatátviteli/hálózati protokollokat a megvalósított szolgáltatásaik alapján 'rétegekbe csoportosították. Minden réteg a saját szolgáltatásai megvalósításánál csak az alatta lévő réteg nyújtotta szolgáltatásokra támaszkodhat. A rétegek által nyújtott szolgáltatásokat először az [ISO/OSI](/halozat/kommunikacios-reteg/osi/osi/) (az OSI a Open System Interconnection, a nyílt rendszerek összekapcsolása rövidítése) szabvány modellje határozta meg, és a réteg fogalmát is itt vezették be.

 Az ISO (International Standards Organization) – modell logikailag rétegekre bontja a hálózatot az alábbi elvek szerint:

* Hasonló funkciókat azonos rétegbe kell gyűjteni.
* Lényegesen különböző funkciókat különböző rétegekbe kell csoportosítani.
* A réteghez tartozó funkciók és protokollok cseréjét engedélyezni kell anélkül, hogy más rétegeket befolyásolnának.
* A réteg teljes mértékben újratervezhető és protokolljai cserélhetők legyenek úgy, hogy az architektúra a hardver- vagy szoftver-technológia előnyeit kihasználhassa a felső szomszédos réteg számára szükséges szolgálatok maradéktalan biztosítása mellett.
* Új réteget kell létrehozni ott, ahol az adat kezelése különböző szintű absztrakciókat igényel.
* Minden réteg csak a közvetlen szomszédjával határos.
* Meg kell hagyni azokban a pontokban a határokat, amelyeket a gyakorlati tapasztalat régebbről indokol.
* Határvonalat kell létrehozni abban a pontban, ahol a szolgálatok leírása rövid és a határon keresztüli interakciók száma minimális.
* Határvonalat kell létrehozni ott, ahol a szükséges interfészre szabványosítási igény van kilátásban.
* Ne legyen olyan sok réteg, hogy a rendszerintegrációs leírások és a rétegek integrációja bonyolultabbá váljon, mint amennyire az szükséges.

A számítógépes hálózat, tekinthető úgy, mint egy bizonyos számú réteg. Minden egyes szint, amely egyben hardver, és szoftver segítségével megvalósított funkcionális réteg, egy funkció készletet biztosít.

Az adatok továbbítása a fizikai hálózaton protokollok szerint történik. A kommunikációs rendszer minden egyes szintjén egy jól definiált szabály halmazt kell használni ahhoz, hogy a kommunikáció sikeres legyen. Egy adott rétegen a szabályok összességét protokollnak nevezzük.
