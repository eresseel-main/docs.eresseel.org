## 1. Bevezetés
A Damn Vulnerable Web Application (DVWA) egy PHP/MySQL alapú webalkalmazás, amely szándékosan nagyon sebezhető. Fő célja, hogy segítséget nyújtson biztonsági szakembereknek képességeik és eszközeik teszteléséhez jogszerű környezetben, valamint segítse a webfejlesztőket a webalkalmazások biztonságának megértésében. Ezen kívül hasznos lehet diákok és tanárok számára is, hogy egy ellenőrzött oktatási környezetben tanulhassanak a webalkalmazás-biztonságról.

A DVWA célja, hogy lehetőséget nyújtson a leggyakoribb webes sebezhetőségek gyakorlására, különböző nehézségi szinteken, egyszerű és átlátható felületen. Fontos megjegyezni, hogy az alkalmazásban vannak dokumentált és nem dokumentált sebezhetőségek is, szándékosan. Bátorítanak arra, hogy felfedezz minél több problémát.

## 2. Letöltés
A forráskódját letudod tölteni github-ról:
```bash
git clone https://github.com/digininja/DVWA
```

## 3. Telepítés
A legeegyszerűbb ha docker containereket használsz.
```bash
cd DVWA
docker compose up -d
```

A webes felületet a 127.0.0.1:4280-en éred el. Alapértelmezett felhasználó és jelszó: admin/password. Ha beléptél csináld meg a resetet, `Create/Reset Database` gombra kattintva, ha sikerült újra a bejelentkező képernyőt látod.

# 4.DVWA Beállítások
A **DVWA Security** menüponthoz a bal oldali sávból férhetsz hozzá, közel az aljához.

A DVWA négy lehetőséget kínál a kívánt pentest nehézségi szint beállítására: **Low**, **Medium**, **High**, valamint egy további, **Impossible** nevű opciót. Ezek a szintek határozzák meg, hogy a DVWA mennyire legyen sebezhető a támadásokkal szemben.

### 4.1. **Low** (Alacsony)
- Ez a biztonsági szint teljesen sebezhető, egyáltalán nincsenek biztonsági intézkedések.
- Bemutatja, hogy a rossz kódolási gyakorlatok hogyan vezetnek sérülékenységekhez, és platformként szolgál az alapszintű kihasználási technikák tanítására vagy elsajátítására.

### 4.2. **Medium** (Közepes)
- Ez a beállítás főként rossz biztonsági gyakorlatokat demonstrál, bemutatva, hogy a fejlesztők próbálták, de nem sikerült biztosítaniuk az alkalmazást.
- Kihívást jelent a felhasználók számára, hogy finomítsák kihasználási technikáikat.

### 4.3. **High** (Magas)
- Ez a szint a Medium kiterjesztése, ahol a fejlesztők nehezebb vagy alternatív rossz gyakorlatokat vezettek be a kód védelmére.
- A sebezhetőségek ezen a szinten korlátozzák a kihasználási lehetőségeket, hasonlóan ahhoz, ahogyan a különböző **Capture The Flag (CTF)** versenyek során kezelhetőek lennének.

### 4.4. **Impossible** (Lehetetlen)
- **Alapértelmezett beállítás** a Docker telepítések esetén.
- Az ajánlott gyakorlat az, hogy összehasonlítsd a sebezhető forráskódot a fejlesztők által viszonylag biztonságosnak tartott forráskóddal.

#### 4.1.1. Megjegyzés az Impossible szintről
Bár a fejlesztők azt állítják, hogy az Impossible szint minden sebezhetőség ellen védett, a **cyberbiztonságban** jártas szakemberek tudják, hogy az abszolút biztonság nem létezik. Ez annak köszönhető, hogy létezhetnek nem dokumentált sebezhetőségek és **zero-day** támadások.

A cyberbiztonság **mindig relatív**, és az Impossible szint is ezt tükrözi.

## 5. DVWA Lecke/Kihívások

Miután kiválasztottad a kívánt pentesting nehézségi szintet, felfedezheted a bal oldali sávban található menüpontokat. Ezek a támadások a DVWA ismert sérülékenységeire vonatkoznak.

A pentesterek ezeket **"leckéknek"** vagy **"kihívásoknak"** nevezik, mivel a jobb alsó sarokban két gomb található: **"View Source"** és **"View Help"**.

### 5.1. **View Source** (Forrás megtekintése)
- A **View Source** gombbal elemezheted a PHP forráskódot minden lecke/kihívás oldalán, hogy meghatározd a támadási stratégiádat.

### 5.2. **View Help** (Segítség megtekintése)
- A **View Help** gomb elmagyarázza a támadás célját, hogy miért hackeled az adott oldalt, és mire számíthatsz, amikor megváltoztatod a pentesting nehézségi szintet.
- Ha rákattintasz a spoiler által kitakart szövegre, akkor **tippeket** kapsz a szint megoldásához.

A következő szakasz konkrét példát mutat arra, hogy mit is jelent mindez, és megvizsgálja, hogyan működik a DVWA, amikor megpróbálod kihasználni a sérülékenységeit.
