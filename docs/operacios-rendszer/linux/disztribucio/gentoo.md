## 1. Bevezetés
A Gentoo Linux (kiejtés /ˈdʒɛntuː/ dzsen-tú) egy Linux-disztribúció. A tervezésénél fő szempont volt a modularitás, hordozhatóság és az optimalizálhatóság a felhasználó gépének megfelelően. Ezt úgy éri el, hogy az összes program alapvetően forrásból telepíthető a felhasználó által megadott optimalizálási paraméterekkel. A nagyobb programcsomagok rendelkezésre állnak előre lefordított, bináris formátumban is különböző architektúrákra. A Gentoo mindezt a Portage csomagkezelési rendszer használatával éri el.

A Gentoo Linux a hosszú farkú szamárpingvin (Pygoscelis papua) angol megfelelőjéről kapta a nevét a gyorsaságuk és alkalmazkodóképességük miatt. A szamárpingvin akár 36 km/h-s sebességgel is képes úszni és kiválóan alkalmazkodik a legzordabb időjáráshoz is.

A Gentoo forrásalapúsága és nagyfokú rugalmassága miatt kiválóan alkalmas forkolásra, saját disztribució kiadására, hisz könnyen saját igényre szabható a rendszer. Köztük, olyan nem kis cégek is vannak, mint pl. a Google a Chromium OS disztribúciójával.

## 2. Történelmi áttekintés
Az első Gentoo Linuxot, még Enoch Linux néven Daniel Robbins alkotta meg. A cél egy binárisok nélküli disztribúció készítése volt, mely lehetővé a hardverekre való optimalizálást a legszükségesebb programok használatával. Az Enoch disztribúciót 1999 decemberében adták ki 0.75-ös verziószám alatt.

Daniel Robbins és társai kifejlesztettek egy GCC klónt EGCS néven és ezen a ponton átkeresztelték a disztribúciót Gentoora. A változtatások olyan sikeresek voltak, hogy később hivatalosan is a GCC (v2.95) részévé váltak.

Ahogy az Enoch egyre finomodott, átnevezték Gentoo Linuxnak, noha párhuzamosan kiadtak még néhány Enoch verziót. Az első stabil verzió, a Gentoo Linux 1.0 2002 március 31 jelent meg. Robbins ez idő tájt még egy 450 MHz-es órajelre tuningolt Celeron 300-ason futtatta a Linuxait, de vett egy újabb gépet, amin nem működött megfelelően a rendszer. Később realizálódott benne, hogy másoknak is ugyanilyen gondjai vannak, amit nem tud megoldani és - szégyenszemre, mint Linux fejlesztő - FreeBSD-re váltott, majd néhány hónap múlva a BSD-s tapasztalatokkal felvértezve újult erővel vágott bele a Gentoo fejlesztésébe.

Noha a 2.2-es Linux kernel kiadása csalódás volt számára, a javított 2.4-es szériával úgy érezte új világ nyílt meg előtte, szóval a Gentoo Linux feléledt hamvaiból. E kudarcnak és a BSD-nek hála ekkor vágott bele a Portage koncepcióba, ami a Gentoo disztribúciók nélkülözhetetlen alapkellékévé vált.

Robbins 2000. január 3-án Új Mexikói székhellyel bejegyezte a Gentoo Technologies, Inc. céget, majd miután a Gentoo az egyik legmeghatározóbb Linux disztribúcióvá vált, 2004. 05. 28-ai dátummal bejegyezte a non-profit Gentoo Alapítványt (Gentoo Foundation, Inc. néven) és levédette a jogokat. A Gentoo Alapítványnak ma az elnök mellett öt tagja van.

A nagyobb döntések meghozatalához létrehoztak egy Gentoo Tanácsot, melynek jelenleg hét tagja van, melyeket egy évre választanak és havonta, vagy, ha a tagok szerint szükséges többször üléseznek egy nyilvános meeting keretein belül.

## 3. Jellemzők
A Gentoo főként azon Linux felhasználók tetszését nyerheti el, akik teljes kontroll alatt akarják tartani a gépüket. Ideális azok számára, akiknek van kedvük pepecselni azzal, hogy a lehető legjobban a saját hardverükre legyen optimalizálva a rendszer kezdve az architektúrától, a kernel modulokon át a felhasználó szintű szoftverekig.

Ennek az optimalizálhatóságnak köszönhetően a Gentoo kiváló asztali gépeknek éppúgy, mint szervereknek és régebbi gépeken is meglehetősen jól fut bizonyos keretek közt.

A Gentoo kiváló a tanulni vágyóknak is, programozóknak, adminisztrátoroknak, hisz a legalapvetőbb szinten kell a rendszert telepíteni, konfigurálni. 

### 3.1. Portage
A Portage a Gentoo szoftver disztribúció és csomagkezelő rendszere. Az eredeti koncepció a port rendszer volt, melyet Berkeley Software Distribution (BSD) rendszerekhez fejlesztettek ki és amely nagyfokú rugalmasságot enged meg a telepítés során éppúgy, mint a szoftverek karbantartása területén. Erről a flexibilitásról a fordítási időben használható un. USE flagek gondoskodnak, melyek lehetővé teszik a függőségek kezelését, biztonságos telepítést (sandbox-on keresztül), a rendszer profilok használatát, vagy éppen a telepített fájlok felülírását, vagy megőrzését. A Portage rendszert Python nyelven programozták, ez a disztribúció „lelke”.

Jelen pillanatban közel 20 000 csomag található a hivatalos Portage rendszerben, de léteznek nem hivatalos kiterjesztések is (Gentoo Portage Overlays), sőt a rendszer azt is megengedi, hogy saját magunk is létrehozzunk egyet a Layman repo menedzselő eszköz jóvoltából.

Noha a Gentoot nem bináris disztribúciónak tervezték, a Portage rendszer lehetővé teszi a bináris csomagok telepítését is, abban az esetben, ha azok a túl nagyok, vagy épp nem érhetők el forrásban.

### 3.2. Hordozhatóság
A Gentoo és a Portage rendszer lehetővé teszi, hogy telepítés közben forrásból natív kódot állítsunk elő az adott architektúrára. Így noha a fordítás jelentős időt és erőforrást emészt fel, lehetővé válik a hordozhatóság, virtuális gép futtatása nélkül is.

Eredetileg a Gentoot x86-os architektúrára tervezték, de jelenleg az alpha, amd64, arm, arm64, hppa, ia64, ppc, ppc64, risc, sparc, x86 architektúrákat támogatja.

Az architektúra kiválasztását, valamint a fordító paraméterezéseit a /etc/portage/make.conf fájlban lehet megtenni, pl., ha Sandy Bridge chipünk van, akkor következőképpen: 

```bash
CHOST="x86_64-pc-linux-gnu"
CFLAGS="-march=sandybridge -O2 -pipe"
CXXFLAGS="${CFLAGS}"
```

A CPU család és modell meghatározásában segíthet /proc/cpuinfo tartalma, amit a következő módon nézhetünk meg:

`user# grep -m1 -A3 "vendor_id" /proc/cpuinfo`

A biztonságos Gentoo C flagek használatáról itt olvashatsz, további információk pedig GCC dokumentációjában.

### 3.3. Modularitás, optimalizáció
A Gentoo a forrás alapúságának és a Portage rendszernek köszönhetően nem csak csomagokat képes kezelni, hanem a csomagokon belül is tud optimalizálni.

Íme néhány szempont, ami miatt a Portage modulárisnak tekinthető:

* profilok (BSD, Linux és az architektúrák stb.) választhatósága
* a Portage rendszerben a csomagok kategória, valamint azon belüli csomagnév szerint érhetőek el.
* vannak olyan csomagok, melyek összessége "meta-csomagok" is elérhetőek
* egyes megkülönböztetett, dinamikus csomaghalmazok az emerge parancs előre definiált paramétereként is elérhetőek
* USE flagek (csomagokon belüli modulok pl. USB, nyelvi modulok stb.)

Pár érv, mely bizonyítja, hogy a Gentoo a legmélyebben optimalizálható rendszerek körébe tartozik:

* make paraméterek választhatósága
* több architektúra (x86, amd64) támogatásának konfigurálhatósága (akár csomagonként)
* a már szemléltetett architektúra választhatóság, C flagek használata
* USE flagek (SSE, MMX, több szálas támogatás stb.)
* verziókezelés (rugalmas maszkolhatóság, csomagonként, architektúránként)
* maszkolás licencek alapján
* külső (akár saját) csomagok integrálhatósága

## 4. Telepítés és konfigurálás
A Gentoo-t többféleképpen lehet telepíteni. A leggyakoribb módja a gentoo telepítésének a Minimális Telepítő CD, vagy Hibrid ISO használata, de más LiveCD, LiveUSB, vagy akármilyen másik Linux segítségével, de akár hálózatról is telepíthető.

A koncepció lényege az, hogy egy működő rendszer segítségével elő lehet készíteni a telepítést (particionálás, formázás stb.), majd a felmásolt alap – magát már fejleszteni képes – Gentoo rendszer (un. stage) számára át lehet adni a környezeti paramétereket és a vezérlést, úgynevezett chroot környezet használatával.

További információ a Gentoo kézikönyvben található. 

### 4.1. Stage-ek
A stage-ek (állapotok) tömörített formában érhetőek el. 2005 októbere után az első két stage hivatalosan már nem támogatott, mivel folyamatos körkörös hibákat okozott és a teljes rendszer újrafordításával ugyanúgy elérhető a megfelelően optimalizált állapot a stage 3 esetében is.

* Stage 1: újrafordítás és konfigurálás az alap toolchaintől kezdve.
* Stage 2: a toolchain félkész állapotban van, de még egyes elemek fordítást igényelnek.
* Stage 3: a rendszer már indításra kész, s az alaprendszer is le van fordítva. Csak a kernelt kell fordítani, illetve igény szerint speciális eszközöket (boot loader, cron és syslog démon). Adott esetben konfigurációs fájlok szerkesztése.
* Stage 4: teljes Gentoo környezetet tartalmazó bootolható rendszer. Kiválóan alkalmas backupok számára, vagy olyan helyeken, ahol problémás a chrootolás.

A processzor architektúráknak és a rendszer elsődleges céljának megfelelően több stage 3 és stage 4 tölthető le, ilyenek pl. a minimal, multilib, cloud, hardened verziók. 

### 4.2. Kernelek
A kernel (rendszermag), mint általában minden csomag, szintén forrásban érkezik a Gentooba. Mint a legtöbb Linux disztribúció a Gentoo is kínál egy előre konfigurált kernelt, de szabad kezet ad a konfigurálásban is, ugyanakkor annyit segít, hogy számos patch settel elérhető a hivatalos Linux kernel.

Néhány kernel a teljesség igénye nélkül:

* genkernel: előre konfigurált kernel azok számára, akik nem értenek hozzá, vagy nem akarnak a konfigurálással foglalkozni.
* gentoo-sources: ajánlott rendszermag, ami több a Gentoo által alkalmazott foltot tartalmaz.
* hardened-sources: szerverek számára ajánlott Gentoo kernel.
* vanilla-sources: a hivatalos kernel fa.
* git-sources: a hivatalos kernel fa napi frissítése, ami leginkább fejlesztőknek, tesztelőknek ajánlott.
* ck-sources: Con Kolivas kernel patch setje, a maximális interaktivitás és reagálási képesség elérése céljából.

### 4.3. Rendszerbetöltő
Miután a kernel telepítve lett, egy boot loadert (rendszerbetöltőt) kell még feltennünk, hogy a rendszert külső indítóeszköz nélkül tudjuk elindítani. Ez a rendszerbetöltő telepíthető a MBR-ba, egy adott partícióra, vagy egy másik eszközre és általában alkalmas több rendszer indítására (másik Linux, Windows stb.) egy menü segítségével. Ilyen rendszerbetöltők pl. a Grub, vagy a Lilo.

### 4.4. Szolgáltatások
Ahhoz, hogy egy rendszer a mindennapi használatra alkalmas legyen szükség van szolgáltatások (fájlrendszerek csatolása, naplózás, hálózatkezelés, grafikus környezet stb.) elindítására. Ezeket végzik az init (indító rendszerek).
Szolgáltatások[halott link] és futási szintek listája

A Gentoo több lehetőséget kínál fel, használható az alapértelmezett OpenRC, de választható pl. a legtöbb Linux disztribúció által támogatott systemd is. 

Az alapértelmezett OpenRC nem rugaszkodik el sem az eredeti unixos filozófiától (KISS), sem az init rendszerek eredeti céljától, ugyanakkor a mai igényeknek megfelelően rugalmas is, így egy modern, de Unix közeli operációs rendszer számára ideális. A Gentoo init szkriptjei 100%-osan kompatibilisek az OpenRC-vel, sőt a Gentoo külön OpenRC kompatibilis szolgáltatásokat tart karban pl. eudev (ami a ma már sysemd-be integrált udev alternatívája).

## 5. Csomagkezelő (emerge)
Az `emerge` parancssori felülete a Portage, és a legtöbb felhasználó így kommunikál a Portage-al. Ez a Gentoo egyik legfontosabb parancsa.

Az `emerge` szoftvercsomagok telepítésére, frissítésére és általános karbantartására szolgál Gentoo Linuxon.

### 5.1. Használat
Az `emerge` gazdag kimenetet biztosít a végrehajtandó változtatásokról, valamint információkat és figyelmeztetéseket biztosít az egyes csomagokról vagy a rendszerről. Az `--ask`, `--pretend`, és `--verbose` opciók hasznosak ahhoz, hogy a Portage több információt mutasson – alapértelmezés szerint az `emerge` parancs azonnal végrehajtja a kért műveletet.

A `--help` opció információkat nyújt a parancssori beállításokról:

```bash
emerge --help
```

### 5.2. Csomag telepítése
Telepítsd a `net-proxy/tinyproxy` csomagot az `--ask` és `--verbose` lehetőségekkel:

```bash
emerge --ask --verbose net-proxy/tinyproxy
```

### 5.3. Csomag keresése
Csomagok keresése proxyval a nevükben:

```bash
emerge --search proxy
```

Csomagok keresése proxyval a nevükben vagy leírásukban:

```bash
emerge --searchdesc proxy
```

Csomagok keresése reguláris kifejezéssel:

```bash
emerge -s '%^python$'
```

Sorold fel az összes csomagot egy kategóriában:

```bash
emerge -s '@net-ftp'
```

### 5.4. Távolítsd el a csomagokat
Távolítsd el a `net-proxy/tinyproxy` csomagot a függőségi érzékenységgel `--depclean` (-c) kapcsolóval:

```bash
emerge --ask --verbose --depclean net-proxy/tinyproxy
```

Ez csak azokat a csomagokat távolíthatja el, amelyekre nincs szükség egy jelenleg telepített csomag függőségeként.

A csomagok eltávolításához használja az emerge `--deselect` parancsot a `--depclean` parancs alternatívájaként.

### 5.5. Csomagok frissítése
Futtassd az `emerge`-t a teljes rendszer frissítéséhez a függőségekkel eggyütt:

```bash
emerge --ask --verbose --update --deep --newuse @world
```

Vagy rövid opciókkal:

```bash
emerge -avuDN @world
```

A `--changed-use` kapcsoló helyett használható a `--newuse`, bár ezzel az opcióval egyes USE jelzők már nem tükrözik a Gentoo lerakatában lévők aktuális állapotát. A `--with-bdeps=y` felépítési idő függőségek frissítésére is használható.

Ügyelj a Portage által a frissítés végén közölt információkra. Ezen információk egy része elérhető lehet a Portage naplójában.

Ha a Portage függőségi problémákat jelent, néha használhatod a `--backtrack=30` (vagy még nagyobb szám) segíthet. Alapértelmezés szerint a Portage viszonylag alacsony korláttal rendelkezik arra vonatkozóan, hogy meddig próbálja feloldani a függőségeket (teljesítmény okokból), esetenként ez nem elég.