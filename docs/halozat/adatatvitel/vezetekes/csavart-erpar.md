## 1. Bevezetés
Az Unshielded Twisted Pair (UTP) egy árnyékolatlan, csavart érpáras hálózati kábeltípus a számítástechnikában. A kifejezés magyar jelentése árnyékolatlan csavart érpár. A csavart érpáras vezetékeket Alexander Graham Bell találta fel 1881-ben.

## 2. Felépítése
Az UTP kábel 4 sodort érpárból álló, rézalapú átviteli közeg. Az UTP kábeleknek mind a 8 rézvezetéke szigetelőanyaggal van körbevéve.

Fontos jellemzője, hogy a vezetékek párosával össze vannak sodorva, így csökkentve az elektromágneses és rádiófrekvenciás interferencia jeltorzító hatását és a sugárzás miatti veszteséget. Az árnyékolatlan érpárok közötti áthallást úgy csökkentik, hogy az egyes párokat eltérő mértékben sodorják.

## 3. Előnyei
### 3.1. Interferencia, áthallás
A külső zavaró jelek a két, egymáshoz közeli és azonos tulajdonságú dróton megegyező amplitúdóval és fázissal jelennek meg, így a két drót közötti feszültségben a külső jelek hatása nagymértékben kioltja egymást.

A csavarásnak köszönhetően a két drót hasonlóan erősen csatolódik a külső jelekhez, hiszen néha az egyik, néha a másik drót van közelebb a zavaró jel forrásához.

Emiatt kell a 4 érpárt más menetemelkedéssel csavarni, hogy ne legyenek minden menetben ugyanúgy összecsatolódva a különböző párok tagjai. Az érpárok áthallása így minimalizálható.

### 3.2. Sugárzás
A pár két tagján ellentétes fázisú jelet továbbítva a két drót sugárzása erősen kioltja egymást, így nem fog antennaként viselkedni a kábel.

## 4. Általános ismertetése
A leggyakrabban alkalmazott kábeltípus az Ethernet hálózatokon. Maximális átviteli távolsága 100 m.

Viszonylagos olcsósága, könnyű telepíthetősége tette rendkívül népszerűvé az évek során. Előnyeként szolgál kis átmérője, mely által kevesebb helyet foglal a kábelcsatornákban. Hátrányaként említhető külső interferenciaforrások elleni viszonylagos védtelensége, valamint kis átviteli távolsága.

A kábel végein 8P8C (gyakran hibásan RJ-45-nek nevezett) csatlakozók találhatók, amellyel a hálózati interfészekhez csatlakozik. A kábeleket kategóriákba sorolják és CAT+szám típusú jelzéssel látják el.

A 10Base-T és 100Base-TX kábelek átvitelkor csak az 1, 2 (küldésre) és a 3, 6 (fogadásra) érpárokat alkalmazzák. 1000Base-TX szabványú átvitel esetén mind a 4 érpár részt vesz az adatátvitelben. Egy vezetéken maximum 125 MB/s átviteli sebesség érhető el. A nagy mennyiségű adat átvitelét ráadásul duplex módon valósítják meg.

## 5. Típusai
### 5.1. Adatátviteli sebesség szerint
A csavarástól függően különböző kategóriákba lehet sorolni a kábeleket.

    CAT1 - telefonkábel (hangátvitel, 2 érpár)
    CAT2 - maximum 4 Mbit/s adatátviteli sebesség érhető el vele.
    CAT3 - 10 Mbit/s az adatátviteli sebessége. Csillag topológiánál alkalmazzák, ethernet hálózatokban (Legacy Ethernet [10 Mbit/s-os] közege).
    CAT4 - max. 20 Mbit/s adatátviteli sebességű.
    CAT5 - 100 Mbit/s adatátviteli sebességű, csillag topológiánál alkalmazzák, ethernet hálózatokban.
    CAT5e, CAT6 - 1000 Mbit/s átviteli sebesség.

A felsőbb kategóriás kábelek visszafelé kompatibilisek.

    Cat. 1. 2 Mbit/s (telefonvonal)
    Cat. 2. 84-113 ohm 4 Mbit/s (Local Talk)
    Cat. 3. 100 ohm 10 Mbit/s 100 m (Ethernet)
    Cat. 4. 100 ohm 20 Mbit/s 100 m (16 Mbit/s Token Ring)
    Cat. 5. 100 ohm 100 Mbit/s 100 m (Fast Ethernet)
    Cat. 6. 100 ohm 1000 Mbit/s 100 m
    Cat. 7. 100 ohm 10000 Mbit/s 100 m

### 5.2. Bekötési sorrend szerint
#### 5.2.1. Egyeneskötésű (link)
* Személyi számítógép - Switch
* Switch - Router
* Hub - Személyi számítógép

#### 5.2.2. Keresztkötésű (cross-link)
* Switch port - Switch port
* Switch port - hub port
* Hub port - hub port
* Router port - Router port
* PC - Router port
* PC - PC

Manapság az új hálózati eszközök már automatikusan megállapítják, hogy milyen kábelt csatlakoztattak hozzá, és úgy működnek.

Konzol (cross-over): Számítógép soros portja és router/switch konzol portja (DB-9 - RJ-45 átalakító) közötti átvitelhez.

## 6. Beszerelés szerint
Beszerelés szerint kétfajta kábeltípust különböztetünk meg, a lengőkábelt (patch-kábel) és a fali kábelt. A megkülönböztetés alapját a kábelezés telepítésekor a különböző elhelyezkedésből adódó igények hozták létre. A lengőkábel külső védőburkolata kevésbé merev, mint a fali kábeleké. A lengőkábel rézsodrony, míg a fali kábel általában tömör rézereket tartalmaz, emiatt a csatlakozó is eltérő.

A fali kábelt a TIA/EIA-568-A, míg a lengő kábelt a TIA/EIA-568-B szabvány szerint kell mindkét végén bekötni. Az említett szabvány azt is előírja, hogy a fali csatlakozótól a lengő kábel nem haladhatja meg az 5 m-t. Vannak ennél hosszabb gyári kábelek is, de ezek nem szabványosak, ill. ott alkalmazhatók, ahol nem készítettek fali kábelezést.

Érdemes utánanézni az ISO 11801, az EN 50 173 Nemzetközi installációs előkábelezési szabványoknak.

## 7. Bekötési színsorrend
Az 1-es lábat úgy kell nézni, hogy ha lefelé álló pöcökkel bedugjuk (felénk áll ki a dugóból a kábel), akkor balról van az 1-es láb. A színek bekötési sorrendje a lengő kábelekre vonatkozó TIA/EIA-568B szabvány szerint értendő.

1. Narancs-fehér | 2. Narancs | 3. Zöld-fehér | 4. Kék | 5. Kék-fehér | 6. Zöld | 7. Barna-fehér | 8. Barna

* Egyeneskötésű kábel szerelése:
```
1. - - - - - 1.
2. - - - - - 2.
3. - - - - - 3.
4. - - - - - 4.
5. - - - - - 5.
6. - - - - - 6.
7. - - - - - 7.
8. - - - - - 8.
```

1. Zöld-fehér | 2. Zöld | 3. Sárga - fehér | 4. Kék | 5. Kék-fehér | 6. Sárga | 7. Barna-fehér | 8. Barna

* Keresztkötésű kábel szerelése:
```
1. - - - - - 3.
2. - - - - - 6.
3. - - - - - 1.
4. - - - - - 4.
5. - - - - - 5.
6. - - - - - 2.
7. - - - - - 7.
8. - - - - - 8.
```

* Roll-over kábel szerelése:
```
1. - - - - - 8.
2. - - - - - 7.
3. - - - - - 6.
4. - - - - - 5.
5. - - - - - 4.
6. - - - - - 3.
7. - - - - - 2.
8. - - - - - 1.
```
