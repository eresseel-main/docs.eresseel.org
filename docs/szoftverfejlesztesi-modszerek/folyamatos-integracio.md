## 1. Bevezetés
A szoftverfejlesztés területén a folyamatos integráció (CI – continuous integration) az a fejlesztési folyamat, amikor a fejlesztők a munkájuk másolatát naponta akár többször is megosztják a verziókezelő rendszerben. Grady Booch 1991-es módszerében javasolta először a CI kifejezést, bár nem igazán támogatta a napi többszöri integrációt. Később az extrém programozás (XP) átvette a CI koncepcióját, és előnyben részesítette a napi egynél többszöri integráció folyamatát – még akár a napi tíz integrációt is.

## 2. Indoklás
Amikor a fejlesztő változtatni kezd a kódon, akkor másolja az aktuális forráskódot, amelyen dolgozik. Mivel a többi fejlesztő is megváltoztatott kódot küld a verziókezelő rendszerbe, ezért ez a példány fokozatosan eltűnik a verziókezelő rendszerből. Természetesen nem csak a meglévő forráskód változtatható meg, hanem új kód is hozzáadható, valamint új könyvtárak és egyéb erőforrások hozzáadása is lehetséges, amelyek viszont függőségeket és potenciális konfliktusokat hozhatnak létre.

Minél hosszabb ideig folytatjuk a fejlesztést egy ágazaton (Branch) belül anélkül, hogy egyesítenénk a fővonallal (master-branch), annál nagyobb a többszörös integrációs konfliktusok és kudarcok kockázatának lehetősége, amikor a fejlesztett ágazatot végül egyesítjük a fővonalon. Amikor a fejlesztők kódot tesznek be a verziókezelőbe, először frissíteniük kell a kódot, hogy szemléltessék a másolat készítése óta bekövetkezett változásokat. Minél több változást tartalmaz a lerakat (repository), annál több munkát kell elvégezniük a fejlesztőknek, mielőtt benyújtanák a saját változásaikat.

Végül a lerakat annyira különbözhet a fejlesztők verzióitól, hogy beléphetnek az úgynevezett „integrációs pokolba” ahol az integrációhoz szükséges idő meghaladhatja az eredeti módosítások elvégzéséhez szükséges időt.

## 3. Munkafolyamatok
### 3.1. Helyi tesztek futtatása
A CI-t az automatizált egységteszttel kombinálva, a tesztvezérelt fejlesztés során kell felhasználni. Ezt úgy végezzük, hogy az összes egységtesztet futtatjuk és átadjuk a fejlesztőkörnyezetnek, mielőtt hozzáadnánk a fővonalhoz. Ez segít elkerülni, hogy az egyik fejlesztő befejezetlen munkáját megsemmisítse egy másik fejlesztő munkája. Szükség esetén a részben teljes funkciókat le lehet tiltani a végrehajtás előtt, például a funkcióváltások használatával.

### 3.2. Kód fordítása CI-ben
A beépített fordítók rendszeresen vagy akár minden módosítás után összeállítják a kódot, és az eredményeket reprezentálják a fejlesztőknek. Az összeállító szerverek használatát már az XP-n (Extrém programozás) kívül vezették be, viszont a közösség és sok szervezet is elfogadta a CI-t anélkül, hogy az XP összes elvét elfogadták volna.

### 3.3. Tesztek futtatása CI-ben
Az automatikus egységtesztek mellett a CI-t használó szervezetek általában építőkiszolgálót is használnak a folyamatos minőség-ellenőrzési folyamatok végrehajtása során – ez általában kis erőfeszítésekkel jár, viszont gyakran alkalmazva. Az egység és az integrációs tesztek futtatása mellett az ilyen folyamatok további statikus elemzéseket is futtatnak,és ezen kívül mérik és profilozzák a teljesítményt, kivonják és formázzák a forráskódot, valamint megkönnyítik a minőségbiztosítási folyamatokat. Például a nyílt forráskódú Travis CI szolgáltatáson a CI munkák csak 58,64% végez teszteket a folyamat során.

A minőség-ellenőrzés folyamatos alkalmazásának a célja a szoftver minőségének javítása és a továbbításhoz szükséges idő lecsökkentése úgy, hogy a fejlesztés befejezése után a hagyományos ellenőrzési gyakorlatokat kiváltja. Ez nagyon hasonlít az eredeti elképzeléshez, amely szerint az integráció megkönnyítése érdekében a gyakoribb integrációt csak a minőségbiztosítási folyamatok során alkalmazzák.

### 3.4. Tárgy telepítése a CI-ből
A CI gyakran összefonódik a folyamatos szállítással vagy a folyamatos telepítéssel az úgynevezett CI / CD csővezetékben. A CI gondoskodik arról, hogy a fővonalon bejelentkezett szoftver mindig olyan állapotban legyen, amely a felhasználók számára telepíthető, amit viszont a CD telepítési folyamat teljesen automatizál.

## 4. Történet
A folyamatos integrációval kapcsolatos legkorábbi ismert munka az Infuse környezet volt, amelyet Kaiser G. E., Perry D. és Schell W. M. fejlesztett ki.

Grady Booch 1994-ben a folyamatos integráció kifejezést használta az Objektum-orientált elemzés és alkalmazás tervezésében (2. kiadás) amelyben elmagyarázza, hogy hogyan fejlesztenek amikor mikrofolyamatokat végeznek „a belső kibocsátások a rendszer egyfajta folyamatos integrációját jelentik".

1997-ben Kent Beck és Ron Jeffries feltalálta az Extrém Programozási (XP) módszert, miközben a Chrysler átfogó kompenzációs rendszer projektjén alkalmazta a folyamatos integrációt. Beck 1998-ban publikálta a folyamatos integrációt, amelyben hangsúlyozza a személyes kommunikáció fontosságát a technológiai támogatás mellett. 1999-ben Beck bővebben foglalkozott az Extrém programozásról szóló könyvével. A CruiseControl, volt az első nyílt forráskódú CI-eszközök egyike, ami 2001-ben jelent meg.

## 5. Általános gyakorlatok
Ez a szakasz felsorolja a különböző szerzők által javasolt bevált gyakorlatokat a folyamatos integráció eléréséhez és ezen gyakorlatok automatizálására. A legjobb gyakorlat az Automatikus összeépítés (Build automation).

A folyamatos integrációnak – az új vagy megváltoztatott kódoknak a kódtárba történő integrálásának elég gyakorinak kell lennie, hogy ne fordulhasson elő hiba a beiktatás és az összeállítás között, valamint nem szabad hibáknak sem felmerülniük oly módon ,hogy a fejlesztők ne vegyék észre ezen hibákat, ugyanis ekkor nem tudnák azonnal kijavítani őket. Normál gyakorlat az, hogy ezeket az összeépítéseket minden változtatás után végrehajtja, nem pedig egy időszakosan ütemezett összeállítást hajt végre. A gyors elkötelezettségek több fejlesztői környezetében történő elvégzésének gyakorlati lehetősége az ha minden egyes elkötelezettség után rövid időre elindul, és elkezdi összeépítést, amikor az időzítő lejár, vagy amikor az utolsó összeállítás óta hosszabb idő telik el. Érdemes figyelembe venni, hogy mivel minden új változtatás visszaállítja a használt időzítőt, ezért ez ugyanaz a technika, mintha egy gombokat lenyomó algoritmust használnánk. Ilyen módon az elkövetési eseményeket „lebontják”, hogy elkerüljék a szükségtelen összegyűjtéseket a gyors változtatások sorozatai között. Számos automata eszköz automatikusan kínálja ezt az ütemezési lehetőséget.

Egy másik fontos tényező az atomi feladatokat támogató verziószabályozó rendszer megléte, ami a fejlesztő összes változását egyetlen átadási műveletnek tekinti. Ezért nincs értelme a megváltoztatott fájloknak csak a felét felépíteni.

Ezen célok elérésének érdekében a folyamatos integráció a következő elveken alapszik.

### 5.1. Kódtároló fenntartása
Ez a gyakorlat a projekt forráskódjának felülvizsgálatát végző rendszer használatát támogatja. A projekt felépítéséhez szükséges összes tárgyat a lerakatba(repository) kell helyezni.Ebben a gyakorlatban és a revízió-ellenőrző közösségekben az a konvenció, hogy a rendszernek egy úgynevezett pénztárból kell felépülnie, ezáltal nem igényel további függőségeket.Az Extrém Programozás támogatója, Martin Fowler azt is megemlíti, hogy ha az elágazásokat eszközök támogatják, akkor annak a minimalizálását kell megkönnyíteni. Ehelyett inkább érdemes a változtatások integrálását előtérbe helyezni, mint a szoftver többi verziójának egyszerre történő karbantartását.A fővonalnak viszont tartalmaznia kell a szoftver működő verzióját.

### 5.2. Automatikus összeállítás
Egyetlen parancsnak képesnek kell lennie a rendszer felépítésére. Számos összeállító eszköz (Build tools), mint például a gyártó, évek óta létezik. Újabb eszközöket gyakran használnak a folyamatos integrációs környezetekben. Az automatikus összeállításnak magában kell foglalnia az integráció automatizálását, amely gyakran magában foglalja a telepítést egy termelés jellegű környezetbe. Sok esetben az összeállító szkript nem csak bináris fájlokat állít össze, hanem dokumentációkat, weboldalakat, statisztikákat és terjesztési eszközöket is (például Debian DEB, Red Hat RPM vagy Windows MSI fájlokat) generál.
Összeállító önteszt végrehajtása

A kód elkészítése után az összes tesztet futtatni kell annak érdekében, hogy úgy viselkedjenek, ahogy a fejlesztők azt elvárják.

### 5.3. Mindennap, mindenki hozzáadja a munkáját a fővonalhoz
Rendszeres hozzáadással (comit) minden elkövető csökkentheti az ütköző változások számát. Egy hetes munka ellenőrzése azzal a kockázattal járhat, hogy az elkészült kód ellentétes lehet más funkciókkal szemben, és ezt a hibát nagyon nehéz lehet megoldani. Viszont a korai, kis konfliktusok a rendszerben arra késztetik a csapattagokat, hogy kommunikáljanak az általuk végrehajtott változásokról. A folyamatos integráció definíciójának részét képezi az összes változás napi legalább egyszeri hozzáadása a verziókezelőhöz (egyszeri beépített szolgáltatásonként). Ezen felül általában ajánlott egy éjszakai összeállítás végrehajtása is. Viszont ezek alsó határok; a tipikus gyakoriság várhatóan sokkal több.

### 5.4. Minden hozzáadást a fővonalhoz kell igazítani
Először is a rendszernek el kell készítenie a jelenlegi működő verziót, hogy ellenőrizze, hogy megfelelően integrálódnak-e. Általános gyakorlat az automatizált folyamatos integráció használata is, bár ezt manuálisan is meg lehet oldani. Az automatizált folyamatos integráció egy folyamatos integrációs szervert vagy démont(Számítógépes szoftver) alkalmaz, hogy figyelemmel tudja kísérni a revízió-vezérlő rendszert a változások szempontjából, majd automatikusan futtatja az összeállítási folyamatokat.

### 5.5. Minden hibajavításnak kísérleti esetet kell használnia
A hiba kijavításához jó gyakorlat lehet egy olyan próbapéldány elküldése, amely a hibát reprodukálja. Ez elkerüli a javítás visszaállítását és a hiba megjelenését, amelyet regressziónak hívnak. A kutatók javaslatot tettek ennek a feladatnak az automatizálására: ha egy hibajavító feladat nem tartalmaz teszt esetet, akkor a már létező tesztekből is tudunk generálni.

### 5.6. Gyors összeállítás megtartása
Az összeállításnak gyorsan be kell fejeződnie, ezért ha probléma merül fel az integrációval, akkor az gyorsan azonosítható lesz.

### 5.7. Klón tesztelése a fejlesztő környezetben
A tesztkörnyezet könnyen meghibásodáshoz vezethet a tesztelt rendszerekben, amikor telepítik őket a fejlesztői környezetbe, hiszen a fejlesztői környezet jelentősen eltérhet a tesztkörnyezettől.A fejlesztői környezet másolatának elkészítése azonban költséges. Ezért a tesztkörnyezetet vagy egy külön gyártás előtti környezetet ("átmeneti") kell összeállítani úgy, hogy a fejlesztői környezet verziója méretezhető legyen a költségek csökkentése érdekében, miközben megőrzi a technológiai verem (Stack) összetételeit és árnyalatait. Ezekben a tesztkörnyezetekben a szolgáltatás-virtualizációt általában arra használják, hogy igény szerint hozzáférést kapjanak azokhoz a függőségekhez (pl. API-k, harmadik féltől származó alkalmazások, szolgáltatások, mainframek stb.), amelyek még a csapat ellenőrzése alatt állnak, vagy még mindig fejlődnek vagy túl összetettek a konfiguráláshoz egy virtuális tesztlaborban.

### 5.8. Legfrissebb eredmények beszerzésének megkönnyítése
A forráskód elérhetővé tétele az érdekelt felek és a tesztelők számára csökkentheti a szükséges újrafeldolgozások mennyiségét egy olyan szolgáltatás újjáépítésekor, amely nem felel meg a követelményeknek. Ezenkívül a korai tesztelés csökkenti a hibák fennmaradásának esélyét a telepítésig bezárólag. Ha a hibákat korábban megtaláljuk, csökkenthetjük a hibák elhárításához szükséges munkát.

Minden programozónak a projekt frissítésével kell kezdenie a napot. Ezáltal mind naprakészek lesznek.

### 5.9. Mindenki láthatja a fejlesztés legújabb eredményeit
Ez szükséges annak a kiderítésében, hogy az összeépítés megszakadt-e, és ha igen, ki hajtotta végre a vonatkozó változtatást, és mi volt ez a változás.

### 5.10. Telepítés automatizálása
A legtöbb CI-rendszer lehetővé teszi szkriptek futtatását a fejlesztés befejezése után. A legtöbb esetben viszont lehetséges egy olyan szkript írása is, amely az alkalmazást egy élő tesztkiszolgálóra telepíti, amelyet mindenki megnézhet. Ezen gondolkodásmód további előrelépése a folyamatos üzembe helyezés, amely a szoftvernak közvetlenül a gyártásba történő bevezetését követeli meg, gyakran egy kiegészítő automatizálással, a hibák vagy visszamenőleges értékek megelőzése végett.

## 6. Költségek és előnyök
A folyamatos integráció célja, hogy olyan előnyöket biztosítson, mint például:

* Az integrációs hibák a korai észlelések miatt, és a kis változtatások miatt könnyen megtalálhatók. Ez időt és pénzt takarít meg egy projekt élettartama alatt.
* Elkerülhető az utolsó pillanatban kialakuló káosz a kiadás időpontjában,ha mindenki megpróbálja ellenőrizni kissé összeegyeztethetetlen verzióit.
* Ha az egységteszt sikertelen vagy netán hiba jelentkezik, vagy ha a fejlesztőknek hibajavítás nélkül vissza kell állítaniuk a kódbázist hibamentes állapotba, akkor csak kevés módosítás veszik el (mivel az integráció gyakran fordul elő).
* A „jelenlegi” verzió állandó rendelkezésre állása tesztelési, bemutató vagy kiadási célokra.
* A gyakori kódbevitel arra készteti a fejlesztőket, hogy moduláris, kevésbé összetett kódot hozzanak létre.

A folyamatos automatizált tesztelés előnyei a következők:

* Befolyásolja a gyakori automatizált tesztelést.
* Azonnali visszajelzés a helyi változások rendszerszintű hatásairól.
* Az automatikus tesztelésből és a CI-ből generált szoftvermérők (mint például a kód lefedettségének, a kód komplexitásának és a szolgáltatás teljességének) automatikus ellenőrzése miatt a fejlesztők inkább a funkcionális és minőségi kód fejlesztésére összpontosítanak, és elősegítik a csapat lendületének fejlesztését is.

A folyamatos integráció néhány hátránya:

* Az automatizált tesztkészlet felépítése jelentős munkát igényel, ideértve az új funkciók lefedésére és a szándékos kódmódosításokra irányuló folyamatos erőfeszítéseket is.
    * A tesztelést a szoftverfejlesztés legjobb gyakorlatának is tekintik, függetlenül attól, hogy folyamatos integrációt alkalmaz-e, és az automatizálás a projekt módszertanának szerves része, mint például a tesztvezérelt fejlesztés.
    * A folyamatos integráció bármilyen tesztkészlet nélkül elvégezhető, de a kiadható termék előállításához a minőségbiztosítás költségei elég magasak lehetnek, ha manuálisan és gyakran kell elvégezni a teszteket.
* Van némi munka az összeállító rendszer felállításával kapcsolatban, amely összetetté válhat, ezzel megnehezítve a rugalmas módosításokat.
    * Számos folyamatos integrációs szoftverprojekt létezik, mind saját, mind nyílt forráskódú szoftverekkel, amelyek szabadon felhasználhatók.
* A folyamatos integráció nem feltétlenül értékes, ha a projekt hatóköre kicsi vagy nem tesztelhető örökölt kódot is tartalmaz.
* A hozzáadott értékek függnek a tesztek minőségétől és attól, hogy a kód valóban tesztelhető-e.[23]
* A nagyobb csoportok azt jelentik, hogy az új kódot folyamatosan bővítik az integrációs sorba, így a módosítások követése (a minőség megőrzése mellett) nehéz lesz és a sorok felépítése mindenkit lelassíthat.
* Napi többszöri módosítás és integráció esetén a szolgáltatás részleges kódját könnyen el lehet nyomni, és ezért az integrációs tesztek a szolgáltatás befejezéséig sikertelenek lesznek.
* A biztonság és a küldetés szempontjából kritikus fejlesztésbiztosítás (például DO-178C, ISO 26262) szigorú dokumentációt és folyamatbeli felülvizsgálatot igényel, amelyet a folyamatos integrációval nehéz elérni.Az ilyen típusú életciklus gyakran további lépéseket tesz szükségessé a termék kiadása előtt, amikor már a termék hatósági jóváhagyása is szükséges.