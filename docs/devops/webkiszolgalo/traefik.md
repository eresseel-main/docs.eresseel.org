## 1. Bevezetés
A Traefik egy nyílt forráskódú Edge Router, amely szórakoztató és egyszerű élménnyé teszi a szolgáltatások közzétételét. Megkapod a kéréseket a rendszer nevében, és megtudod, mely összetevők felelősek azok kezeléséért.

A Traefik számos funkciója mellett az különbözteti meg egymástól, hogy automatikusan megtalálja a megfelelő konfigurációt a szolgáltatásaidhoz. A varázslat akkor következik be, amikor a Traefik megvizsgálja az infrastruktúrát, ahol releváns információkat talál, és felfedezi, hogy melyik szolgáltatás melyik kérést szolgálja ki.

A Traefik natívan kompatibilis minden nagyobb fürttechnológiával, mint például a Kubernetes, a Docker, a Docker Swarm, az AWS, a Mesos, a Marathon, és a lista folytatható; és sok mindent elbír egyszerre. (Még a vason futó régi szoftvereknél is működik.)

A Traefiknél nincs szükség külön konfigurációs fájl karbantartására és szinkronizálására: minden automatikusan, valós időben történik (nincs újraindítás, nincs kapcsolat megszakítás). A Traefik segítségével az új funkciók fejlesztésével és telepítésével töltheted az időt, nem pedig a működési állapot konfigurálásával és karbantartásával.

A Traefik fejlesztésének a fő célja, hogy egyszerű legyen a használata.

## 2. Előzmények
2019 márciusában a Rancher Labs bejelentette nyílt forráskódú k3s projektjének kiadását, amelyben a Traefik volt az alapértelmezett belépésvezérlő.

2019 szeptemberében a Containous kiadta a Maesh-t a Traefik egyedi implementációjával, hogy szolgáltatásháló-képességeket biztosítson a Kubernetesben, amely támogatja a Service Mesh Interface specifikációt (SMI).

A Traefik 2.0 2019 novemberében jelent meg számos alapvető változtatással és új funkcióval. A kiadás tartalmazza az SNI és a Layer 4 (TCP) támogatását. Ezenkívül bevezették a köztes szoftvereket, amelyek lehetővé teszik a láncszabály-alapú viselkedést, valamint a fejlett telepítési minták támogatását, például a Canary kiadásokat, az A/B tesztelést és a forgalom tükrözését.

2020 májusában a Scaleway elindította a Kubernetes Kapsulét, egy CNCF-tanúsítvánnyal rendelkező platformot, amely a Traefiket tartalmazza az egyik elérhető Ingress vezérlőként.

## 3. Jellemzők
A Traefik két hagyományosan különálló komponenst, a terheléselosztót és a fordított proxyt egyesíti egy alkalmazásban, és úgy tervezték, hogy olyan eszközöket használjon, mint például a Docker, az összetevők konfigurációjának automatizálására. A Docker mellett a Traefik támogatja az automatikus szolgáltatáskeresést és -konfigurálást Kubernetesen, Docker Swarmon, ECS-en, Marathonon. Az alkalmazás támogatja az SSL/TLS megszüntetését, az automatikus biztonsági tanúsítványok kiadását és a Let's Encrypt segítségével történő megújítását, valamint további terheléselosztási lehetőségeket, például megszakítókat és sebességkorlátozást. 

### 3.1. Edge Router
A Traefik egy Edge Router, ami azt jelenti, hogy ez a platform ajtaja, és minden bejövő kérést elfog és irányít: ismeri az összes logikát és minden szabályt, amely meghatározza, hogy mely szolgáltatások milyen kéréseket kezelnek (az elérési út, a gazdagép, a fejlécek stb. alapján ...). 

### 3.2. Auto Service Discovery
Ahol hagyományosan szélső forgalomirányítóknak (vagy fordított proxyknak) olyan konfigurációs fájlra van szükségük, amely minden lehetséges útvonalat tartalmaz a szolgáltatásokhoz, a Traefik ezeket maguktól a szolgáltatásoktól kapja meg.

A szolgáltatások üzembe helyezésekor olyan információkat csatol, amelyek megmondják a Traefik számára, hogy a szolgáltatások milyen kéréseket tudnak kezelni. 

Ez azt jelenti, hogy egy szolgáltatás üzembe helyezésekor a Traefik azonnal észleli, és valós időben frissíti az útválasztási szabályokat. Hasonlóképpen, ha egy szolgáltatást eltávolítanak az infrastruktúrából, a megfelelő útvonal ennek megfelelően törlődik.

Többé nem kell IP-címekkel vagy más szabályokkal telezsúfolt konfigurációs fájlokat létrehoznia és szinkronizálnia. 