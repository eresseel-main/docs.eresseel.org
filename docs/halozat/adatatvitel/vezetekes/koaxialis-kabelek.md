## 1. Bevezetés
A koaxiális kábel a híradástechnikában használt olyan vezetéktípus, ami egy belső vezető érből, dielektrikumból, fémhálóból és külső szigetelésből áll. A fémháló szerepe az elektromos árnyékolás, azaz a belső éren továbbított jel megóvása a külső zavaroktól. Elsősorban rádiófrekvenciás jelek továbbítására használják.

A ko-axiális azt jelenti, hogy "közös tengelyű", ez a név a csőszerű összetételre utal: a belső ér és a külső árnyékolás hosszanti tengelye megegyezik.

Az ideális koaxiális kábelnél az elektromágneses mező csak a belső vezető és az árnyékolás között létezik, így a kábel közelében található fémtárgyak nem okoznak teljesítményveszteséget. Az árnyékolásnak köszönhetően a külső elektromágneses zajok sem zavarják a jelet.

## 2. Története
1880-ban szabadalmaztatta Oliver Heaviside angol villamosmérnök.

## 3. Felépítése
### 3.1. Belső ér
A belső vezető lehet sodrott vagy tömör vezeték. Általában rézből készítik a jó vezetőképessége miatt. Ha nagy mechanikai igénybevételnek van kitéve a vezeték, akkor réz bevonatú acél belső vezetőt érdemes használni, az acél ugyanis jobb mechanikai tulajdonságokkal rendelkezik.

### 3.2. Szigetelés
A leggyakrabban használt anyagok a polietilének, a polipropilének, és a politetrafluoretilén. Létezik félig hézagos szigetelés is, ahol a lyukak helyén a levegő a szigetelő anyag.

### 3.3. Árnyékoló réteg
Általában fonott réz árnyékolást alkalmaznak. Előnye, hogy hajlítható, és az árnyékolás nem gyengül, ha a kábelt meggörbítjük.

### 3.4. Burkolat
Legelterjedtebb a PVC burkolat.

## 4. Jellemzői
A koaxiális kábel egy távvezeték amelynek fizikai leírására használhatók a távíró egyenletek.

### 4.1. Impedancia illesztés
A koaxiális kábel megfelelő használatához a kábel mindkét végén megfelelő illesztés szükséges, ami azt jelenti, hogy a jel forrásánál, a lezárásnál és a jelúton sem változhat az impedancia. Ennek hiányában a jel egy része visszaverődik a belső érre, ez például analóg televíziós adásnál szellemképet okoz. A fáziskésleltetés miatt az is előfordulhat, hogy a visszaverődő jel kioltja a továbbított jelet, ami a jelszint csökkenéséhez vezet.

A kábel nagymértékű meghajlítása, megtörése is megváltoztatja az impedanciát, ezért ez is visszaverődésekhez vezethet.

## 5. Típusai
Két jelentős csoportra oszthatók a koaxiális kábelek: az egyik az alapsávú koaxiális kábel, ezt digitális jelátvitelre használják, a másik pedig a széles sávú koaxiális kábel, amelyet analóg jelátvitelre alkalmaznak.


### 5.1. Alapsávú koaxiális kábel
Ezt a koaxiális kábelt régebben elterjedten használták számítógépes lokális hálózatban, valamint távbeszélőrendszerekben is nagytávolságú átvitelre. A mindenkori sávszélesség a kábel hosszától függ. 1 km-nél kisebb távolságon 10 Mbit/s-os átviteli sebesség valósítható meg.

Ezt az átviteli közeget régebben elterjedten alkalmazták az Ethernet hálózatokban, ahol megkülönböztetünk: vékony koaxiális (10Base2) és vastag koaxiális (10Base5) kábeleket. A típusjelzésben szereplő 2-es és 5-ös szám az Ethernet hálózatban kialakítható maximális szegmenshosszra utal: vékony kábelnél ez 200 méter, vastagnál 500 méter lehet. A digitális átviteltechnikában vékony koaxiális kábelek használatakor csatlakozásra BNC (Bayonet Neill-Concelman) dugókat és aljzatokat használnak.

### 5.2. Széles sávú koaxiális kábel
Ez a fajta kábelrendszer a kábeltelevíziózás szabványos kábelein keresztül analóg jelátvitelt tesz lehetővé. A szabványos kábeltelevíziós technikából adódóan az ilyen széles sávú hálózatok esetén az analóg jelátvitelnek megfelelően (ami kevésbé kritikus, mint a digitális) a kábel akár 100 km-es távolságra, 300 MHz-es, de néha 450 MHz-es jelek átvitelére is alkalmas.

## 6. Általánosan elterjedt kábelértékek
A földfelszíni rádió- és televízióantennákhoz, műholdvevő antennákhoz, kábeltelevízió-hálózatokhoz elsősorban 75 ohm impedanciájú kábelt használnak.

Vezeték nélküli adatátvitelhez, adóantennákhoz, rádióamatőr célokra általában 50 ohmos kábelt használnak.
