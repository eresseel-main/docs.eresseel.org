## 1. Bevezetés
A vízesés modell megközelítést a 60-as években dolgozták ki az USA haditengerészeténél. Alapvető célja az volt, hogy lehetővé tegye a komplex, katonai szoftverek biztonságos, ellenőrzött kifejlesztését. A vízesés modellben minden fázis végén a projekt csapat felülvizsgálja az adott fázist, majd lezárja azt. A fejlesztés nem folyik tovább, amíg a megrendelő nem elégedett az eredménnyel.

Ha változtatás volt szükséges, amely során az előző állapotra kellett visszalépni, akkor ez költség és időigényes lépés volt. A vízesés metódus formális és dokumentumvezérelt. A projekt során nagyszámú dokumentum előállítása biztosítja a tervezett szoftver működőképességét, robosztusságát, megbízhatóságát. Egy másik sajátossága modellnek a tervezés hangsúlyossága, gondos kivitelezése. Az előre megtervezett rendszer minimalizálja a tervezési időigényt a későbbi fázisokban.

Sok olyan le nem szállított vagy leszállított, de sohasem használt szoftverről tudunk, amelyek esetén a fő gond a specifikáció, követelmények menet közbeni változása volt. Ez az eltérés lehet egészen kismértékű pl. 1%, de ha olyan részét változtatja a rendszernek, amely 1-1 fázis teljes újrakezdését vonja maga után, mert logikailag kell áttervezni, akkor a költségek és a határidők jelentősen megnőhetnek.

## 2. Modell
### 2.1. A vízesés modell fázisai
* Követelmények elemzése és meghatározása (specifikáció)
* Rendszer- és szoftvertervezés
* Megvalósítás és egységteszt
* Teljes rendszertesztelés
* Üzemeltetés, működtetés és karbantartás

### 2.2. A vízesés modell problémái
* Lineáris, így nehéz a visszalépés a felmerülő problémák megoldására, és ez jelentősen megnöveli a javítás költség- és időigényét.
* Nem kezeli a szoftverfejlesztésben elterjedt iterációkat.
* Nincs összhangban a szoftverfejlesztés problémamegoldó természetével.
* Az integráció a teljes folyamat végén, egyben, robbanásszeren történik. A korábban fel nem fedezett hibák ilyenkor hirtelen, együttesen jelennek meg, így felderítésük és javításuk egyaránt nehezebb feladat.
* A megrendelő csak a folyamat végén láthatja a rendszert, menet közben nincs lehetősége véleményezni azt.
* A minőség szintén csak a folyamat utolsó fázisában mérhető.
* Minden egyes fázis az előző fázis teljes befejezésére épít, ezzel jelentősen megnő a kockázat.
* A fejlesztés során a követelmények nem módosíthatók, hiszen már az életciklus elején befagyasztjuk őket.
* Már a fejlesztés kezdetén ismernünk kell valamennyi követelményt, azok későbbi módosítására vagy bővítésére nincs lehetőség.
* Elképzelhető, hogy bár a végtermék megfelel valamennyi specifikációnak, mégsem működik (pl. mert az elvárásokban vannak ellentmondások).
* Dokumentumvezérelt, és túlzott dokumentálási követelményeket állít fel.
* Az egész szoftvertermék egy időben készül, nincs lehetőség kisebb részekre osztására.

### 2.3. A vízesés modell előnyei
* Nemcsak a szoftverfejlesztésnél használható, hanem egyéb végfelhasználói projekteknél is.
* A jól érthető és kevésbé komplex projektek esetén szabályozottan rögzíti a komplexitást, és jól megbirkózik azzal.
* Könnyű megérteni.
* Egyfázisú fejlesztéseknél egyszerű a használata.
* Strukturáltságot biztosít még a kevésbé képzett fejlesztők számára is.
* Biztosítja a követelmények rögzítését, és azok nem is változnak a fejlesztés során.
* Meghatározott sablont biztosít arra vonatkozóan, hogy mely módszereket használjuk az analízis, tervezés, kódolás, tesztelés és üzemeltetés során.
* Feszes ellenőrzést biztosít.
* Ha jól alkalmazzuk, a hibák már valamely korai fázisban kiderülnek, amikor javításuk még lehetséges, és kevésbé költségigényes.
* A projekt menedzser számára könnyű a tervezés és a szereplők kiválasztása.
* Ha valaki készen van az adott fázisban rá kiosztott munkával, másik projekten dolgozhat, míg a többiek is elérik a fázis lezárásához szükséges állapotot.
* A mérföldkövei könnyen érthetőek.
* Könnyű ellenőrizni a projekt aktuális állapotát.