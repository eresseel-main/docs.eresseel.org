## 1. Bevezetés
Az Ansible olyan szoftvereszközök készlete, amely kódként teszi lehetővé az infrastruktúrát. Nyílt forráskódú, csomag szoftver-kiépítést, konfigurációkezelést és alkalmazástelepítési funkciókat tartalmaz.

Eredetileg Michael DeHaan készítette. 2015-ben vásárolta meg a Red Hat. Az Ansible-t mind a Unix-szerű rendszerek, mind a Microsoft Windows konfigurálására tervezték. Az Ansible ügynök nélküli, ideiglenes távoli kapcsolatokra támaszkodik SSH-n vagy Windows távfelügyeleten keresztül, amely lehetővé teszi a PowerShell végrehajtását. Az Ansible vezérlőcsomópont a legtöbb Unix-szerű rendszeren fut, amely képes a Python futtatására, beleértve a telepített WSL -t tartalmazó Windows rendszereket is. A rendszer konfigurációját részben a saját deklaratív nyelve határozza meg.

## 2. Története
Az „ansible” kifejezést Ursula K. Le Guin 1966-os Rocannon világa című regényében használta, amivel a kitalált azonnali kommunikációs rendszerekre utal.

Az Ansible eszközt Michael DeHaan, a Cobbler kiépítési szerveralkalmazás szerzője és a Fedora Unified Network Controller (Func) távfelügyeleti keretrendszer társszerzője fejlesztette ki.

Az Ansible, Inc. (eredeti nevén AnsibleWorks, Inc.) a Michael DeHaan, Timothy Gerla és Saïd Ziouani által 2013-ban alapított társaság az Ansible kereskedelmi támogatására és szponzorálására. A Red Hat 2015 októberében megvásárolta az Ansible-t.

Az Ansible a Red Hat tulajdonában lévő Linux Fedora disztribúció részeként található. Elérhető a Red Hat Enterprise Linux, CentOS, openSUSE, SUSE Linux Enterprise, Debian, Ubuntu, Scientific Linux és Oracle Linux rendszereken isc az Extra Packages for Enterprise segítségével. Linux (EPEL), valamint más operációs rendszerekhez.

## 3. Felépítése
Az Ansible segít több gép kezelésében azáltal, hogy kiválasztja az Ansible egyszerű ASCII szövegfájlokban tárolt jegyzék (inventory) részeit. A jegyzék konfigurálható. A célgép jegyzékei dinamikusan vagy felhőalapú forrásokból származhat különböző formátumokban ( YAML, INI ).

Az érzékeny adatok titkosított fájlokban tárolhatók az Ansible Vault segítségével 2014 óta. Más népszerű konfigurációkezelő szoftverekkel ellentétben – mint például a Chef, a Puppet, a Salt és a CFEngine – az Ansible ügynök nélküli architektúrát használ. Az Ansible szoftverrel, amely általában nem fut, vagy nincs is telepítve van a vezérelt csomóponton. Ehelyett az Ansible úgy vezérli a csomópontot, hogy SSH-n keresztül ideiglenesen modulokat telepít és futtat a csomóponton. A vezérlési feladat időtartama alatt a modult futtató folyamat egy JSON -alapú protokollal kommunikál a vezérlő géppel annak szabványos bemenetén és kimenetén keresztül. Ha az Ansible nem kezel egy csomópontot, akkor nem fogyaszt erőforrásokat a csomóponton, mivel nem futnak démonok vagy nincs telepítve rajta szoftver.

### 3.1. Függőségek
Az Ansible megköveteli, hogy a Python telepítve legyen az összes kezelőgépen, beleértve a pip csomagkezelőt, valamint a konfigurációkezelő szoftvert és a függő csomagokat. A felügyelt hálózati eszközök nem igényelnek több függőséget, és ügynök nélküliek.

### 3.2. Vezérlő csomópont
A vezérlőcsomópont (főgazda) a célgépek ("jegyzéknek" nevezett csomópontok) kezelésére (vezérlésére) szolgál, lásd alább. A vezérlőcsomópontok csak Linuxhoz és Unix szerű rendszerekhez érhetők el. A Windows operációs rendszerek nem támogatottak. Több vezérlőcsomópont megengedett. Az Ansible nem igényel egyetlen vezérlőgépet a vezérléshez. Biztosítja, hogy egy összeomlás utáni helyreállítás egyszerű legyen. A csomópontokat a vezérlő csomópont kezeli SSH -n keresztül.

### 3.3. Tervezési célok
Az Ansible tervezési céljai a következők:

* A természetesen minimalista. Az irányítási rendszerek nem várhatnak el további függőséget a környezettől.
* Szilárd. Az Ansible segítségével állandó környezetet kell létrehozni.
* Biztonságos. Az Ansible nem telepít ügynököket a csomópontokra. Csak OpenSSH és Python szükséges a felügyelt csomópontokon.
* Megbízható. Gondosan megírva az Ansible playbook idempotens lehet, hogy megelőzze a felügyelt rendszerek váratlan mellékhatásait. Lehet olyan playbook-ot írni, amelyek nem idempotensek.
* Kevés tanulás szükséges hozzá. A Playbookok egy egyszerű és leíró nyelvet használnak, amely YAML és Jinja sablonokon alapul.

### 3.4. Modulok
Az modulok többnyire önállóak, és egységesített szkriptnyelven (például Python, Perl, Ruby, Bash stb.) írhatók. A modulok egyik irányadó célja az idempotencia, ami azt jelenti, hogy egy művelet többszöri megismétlése esetén is (pl. kimaradás utáni helyreállításkor) mindig ugyanabba az állapotba hozza a rendszert.

### 3.5. Jegyzék (inventory) konfiguráció
A célcsomópontok helyét a `/etc/ansible/hosts` (Linux rendszeren) címen található (INI vagy YAML formátumú) jegyzék konfigurációs listák határozzák meg. A konfigurációs fájl felsorolja az Ansible által elérhető minden csomópont IP-címét vagy gazdagépnevét. Ezenkívül a csomópontok csoportokhoz rendelhetők.

Példa a jegyzékre (INI formátum):
Ez a konfigurációs fájl három csomópontot határoz meg: Az első csomópontot egy IP-cím. Az utóbbi két csomópontot pedig gazdagépnevek alkotják. Ezenkívül az utóbbi két csomópont a webservers csoportjába van csoportosítva.

Az Ansible egyéni Dynamic Inventory szkriptet is használhat, amely képes dinamikusan lekérni az adatokat egy másik rendszerről, és támogatja a csoportok csoportjait.

### 3.6. Előadás jegyzékek (Playbooks)
A Playbookok YAML -fájlok, amelyek a felügyelt csomópontokon végzett ismételt végrehajtások feladatlistáit tárolják. Minden Playbook leképezi (társítja) a gazdagépek egy csoportját egy szerepkörhöz. Minden szerepkört az Ansible feladatok hívásai képviselnek.

### 3.7. Ansible Automation Platform
Az Ansible Automation Platform egy REST API, webszolgáltatás és webalapú felület (alkalmazás), amelynek célja, hogy az Ansible-t elérhetőbbé tegye az informatikai ismeretek széles skálájával rendelkező emberek számára. Ez egy több összetevőből álló platform, beleértve a fejlesztői eszközöket, a műveleti felületet, valamint az Automation Mesh-et, amely lehetővé teszi az adatközpontok közötti méretű automatizálási feladatokat. Az AAP egy kereskedelmi termék, amelyet a Red Hat, Inc. támogat, de több mint 17 upstream nyílt forráskódú projektből származik, beleértve az AWX upstream projektet (korábban az Ansible Towerből származik), amely 2017 szeptembere óta nyílt forráskódú.

A Towernek volt egy másik nyílt forráskódú alternatívája is, a Go -ban írt Semaphore.

## 4. Platform támogatás
A vezérlőgépeknek Linux/Unix gazdagépnek kell lenniük (például BSD, CentOS, Debian, macOS, Red Hat Enterprise Linux, SUSE Linux Enterprise, Ubuntu), és Python 2.7 vagy 3.5 szükséges.

A felügyelt csomópontoknak, ha Unix-szerűek, Python 2.4-es vagy újabb verzióval kell rendelkezniük. A Python 2.5-ös vagy korábbi verziójú felügyelt csomópontokhoz a python-simplejson csomag is szükséges. Az 1.7-es verzió óta az Ansible képes kezelni a Windows csomópontjait is. Ebben az esetben az SSH helyett a WS-Management protokoll által támogatott natív PowerShell távvezérlés kerül felhasználásra.

Az Ansible telepíthető fizikai gazdagépekre, virtuális gépekre és felhőkörnyezetekre.

## 5. Telepítés
```bash
python3 -m pip install --user ansible
ansible --version
```

## 6. Ansible kódkiegészítés hozzáadása bash-hez
```bash
python3 -m pip install --user argcomplete
```

Ha nem rendelkezel bash 4.2-vel, minden szkriptet külön kell regisztrálnod.

```bash
eval $(register-python-argcomplete ansible)
eval $(register-python-argcomplete ansible-config)
eval $(register-python-argcomplete ansible-console)
eval $(register-python-argcomplete ansible-doc)
eval $(register-python-argcomplete ansible-galaxy)
eval $(register-python-argcomplete ansible-inventory)
eval $(register-python-argcomplete ansible-playbook)
eval $(register-python-argcomplete ansible-pull)
eval $(register-python-argcomplete ansible-vault)
```

A fenti parancsokat el kell helyezned a shell profil fájljában, például `~/.profile` vagy `~/.bash_profile`.

## 7. Ansible parancsok
Collection-ök letöltése:
```bash
ansible-galaxy collection install -r ./collections/requirements.yml -p ./collections
```

Role-ok letöltése:
```bash
ansible-galaxy role install -r ./roles/requirements.yml -p ./roles
```

Ansible vault file enkriptálása és dekriptálása:
```bash
ansible-vault encrypt ./files/vagrant/id_rsa
ansible-vault decrypt ./files/vagrant/id_rsa
```

Ansible playbook futtatása:
```bash
ansible-playbook -i ./inventories/development/hosts jenkins-slave.yml
ansible-playbook -i ./inventories/development/hosts jenkins-master.yml --vault-password-file ~/.vagrant.d/vault_password_files.d/vault.txt
```

## 8. Példa kódok GitHub-on
[:octicons-file-code-24: Példakód][3]
[3]: https://github.com/eresseel/ansible_role_apt/