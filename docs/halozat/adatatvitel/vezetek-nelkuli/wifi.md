## 1. Bevezetés
A Wi-Fi (WiFi, Wifi vagy wifi) az IEEE által kifejlesztett, vezeték nélküli mikrohullámú kommunikációt (WLAN) megvalósító, széleskörűen elterjedt szabvány (IEEE 802.11) népszerű neve. A Wi-Fi az elterjedt nézetekkel szemben nem az angol Wireless Fidelity kifejezés rövidítése. Az elnevezést egy marketingcég találta ki játékosan utalva a Hi-Fi/hifi szóra, csak később igyekeztek rövidítésként aposztrofálni és úgy reklámozni.

Hedy Lamarr színésznő és George Antheil zeneszerző 1942-ben szabadalmaztatta azt a szórt spektrumú technikát, ami később a wifi technológiai alapját adta. Így a wifit sokáig csak az amerikai haditengerészet használta, amíg 1985-ben el nem készültek a civilek által is használható vezeték nélküli hálózatok.

|  IEEE szabvány  | Generációs jelölés  | Megjelenés ideje  | Működési frekvencia (GHz)  | Sebesség (maximális) (Mbit/s)  | Hatótávolság beltéren (méter)  | Hatótávolság kültéren (méter)  | Sebesség (jellemző) (Mbit/s)  |
|:---------------:|:-------------------:|:-----------------:|:--------------------------:|:------------------------------:|:------------------------------:|:------------------------------:|:-----------------------------:|
| Eredeti 802.11  | (nincs)             | 1997              | 2,4                        | 2                              | ~20                            | ~100                           | 0,9                           |
| 802.11a         | (Wi-Fi 2)           | 1999              | 5                          | 54                             | ~35                            | ~120                           | 23                            |
| 802.11b         | (Wi-Fi 1)           | 1999              | 2,4                        | 11                             | ~38                            | ~140                           | 4,3                           |
| 802.11g         | (Wi-Fi 3)           | 2003              | 2,4                        | 54                             | ~38                            | ~140                           | 19                            |
| 802.11y         | (nincs)             | 2008              | 3,7                        | 54                             | ~50                            | ~5000                          | 23                            |
| 802.11n         | Wi-Fi 4             | 2009              | 2,4 / 5                    | 600                            | ~70                            | ~250                           | 74                            |
| 802.11ac        | Wi-Fi 5             | 2013              | 5                          | 1300                           | ~140                           | ~350                           | 500                           |
| 802.11ax        | Wi-Fi 6             | 2018              | 2,4 / 5                    | 9600                           |                                |                                |                               |
|                 | Wi-Fi 6E            | 2020              | 2,4 / 5 / 6                |                                |                                |                                |                               |

A táblázatban jelölt hatótávolságok, csupán elméleti értékek! Valós tesztek alapján, beltéren, egy 2G és egy 5G jellel lefedett területen a 2G(b/g/n) hatótávolsága többszöröse az 5G (ac) hatótávolságának. Ugyanakkor az 5 Ghz sebességét tekintve többszöröse a 2,4 Ghz-es wifinek, így belső hálózati kommunikációra (wireless kapcsolat telefon és gép között) az 5 Ghz-es sávszélességet érdemes használni.

* **802.11a**: 5 GHz-es frekvenciasávban működő eszközök; előnye a nagy távolság és sávszélesség, viszont jellemzően csak pont-pont kapcsolatra használják és az ehhez használható eszközök általában drágábbak. Különösen fontos az optikai rálátás a két pont között.
* **802.11b**: 2,4 GHz-es tartományban működő eszközök; hatótávolsága a terepviszonyoktól függően széles skálán mozoghat, lényegesen kisebb, mint a 802.11a, pont-multipont kapcsolatoknál 1 km-es sugarú körön belülre szokták tervezni. Átviteli sebessége max. 11 Mbit/s
* **802.11g**: 2,4 GHz-en működő eszközök, a 802.11b-vel sok tekintetben megegyezik, a routerek nagy része mindkettőt támogatja. Előnye, hogy nagyobb sávszélességet képes átvinni, hátránya pedig, hogy a távolság növekedésével lényegesen romlik a hatásfoka és érzékenyebb az interferenciára. Átviteli sebessége max. 54 Mbit/s.

A 802.11b, 802.11a és 802.11g utólag, nem hivatalosan kapta meg a Wi-Fi 1, 2 és 3 megjelölést; hivatalosan megjelenést a 4, 5 és 6 kapott az új elnevezési szabvány bevezetésekor, 2019-ben.

## 2. Felhasználási területek
Irodákban, nyilvános helyeken (repülőtér, étterem, hotel, magánházak stb.) megvalósított vezeték nélküli helyi hálózat, aminek segítségével a látogatók saját számítógépükkel kapcsolódhatnak a világhálóra.

Kialakítása a következő módokon történhet:

* **Publikus, nyílt hálózat**: bármely wi-fi routerrel kialakítható, az így létrehozott hálózathoz bárki csatlakozhat mindenféle korlátozás nélkül
* **Privát hálózat**: a hálózat saját felhasználásra lett kialakítva, melyet egy titkos jelszó véd, így ahhoz csak a jelszó ismeretében lehet csatlakozni
* **Publikus, zárt hálózat**: egy speciális szoftver gondoskodik arról, hogy a hálózatot csak egy kód ismeretében, korlátozott ideig lehessen használni. Ezt a formát rendszerint éttermek, kávézók használják, ahol az internetelérés fogyasztáshoz van kötve
* **Publikus, részlegesen zárt hálózat**: átmeneti típus a nyílt és egyben publikus hálózatok, illetve a privát hálózatok közt. Két főbb típusa különböztethető meg, így a hozzáférési pont számára elérhető sávszélesség bizonyos, akár igen elenyésző hányadának nyílt, és publikussá tett formája, illetve egy szélesebb kör számára elérhető, publikus, azonban zárt hálózat ismeretes. Céljuk, hogy az internetkapcsolatot ingyenesen használók ne élhessenek vissza, és ne terhelhessék le aránytalanul az adott wi-fi-pontot üzemeltető hálózatát annak terhére. Jelenleg az első, a privát hálózatok és a nyílt hozzáférésű, publikus hálózatok kivitelezése igen körülményes egyszerű felhasználók számára, míg utóbbi hálózatok nem hozzáférhetők mindenki számára, minthogy azokat csak a jelszót ismerő személy, vagy személyek csoportja képes elérni. Ilyen megoldást nyújtanak a Skype, illetve a Google érdekeltségi körébe tartozó FON által kínált olyan wi-fi routerek, melyek az ilyen termékkel rendelkezők számára egymás között elérhetővé teszik az ilyen routereken keresztül megosztott wi-fi hálózatok bizonyos részét, amit egy felhasználói névvel, illetve jelszóval rendelkező - szintén e közösség tagságával bíró személyek - csatlakozhatnak a világ számos különböző pontján elérhető ilyen típusú hálózatok összeséhez. Ezek sávszélességét a tulajdonos határozza meg, egy a közösség tagjai számára részlegesen megosztott 1,5 Mb/s sávszélességű internet kapcsolat 10%-a elegendő, hogy valós idejű, kétoldalú hanghívást bonyolítsunk különböző VoIP-klienseken (Voice Over Internet Protocol) keresztül, mint amilyen a Skype.
* **Kereskedelmi HotSpot szolgáltatás**: a vezeték nélküli hálózat csak díjfizetés ellenében, korlátozott ideig használható

## 3. Vezetéknélküli hálózatok titkosítási szabványai
### 3.1. Wired Equivalent Privacy
A WEP (magyarul kb. a vezetékessel egyenértékű titkosság) volt az első ilyen jellegű szabvány. Létezik 64, 128, 256 és 512 bites változata is. Legelterjedtebb a 64 és a 128 bites WEP.

Nagyon sok oldal tanúskodik arról, hogy még jól beállított eszközök használata mellett is a titkosításhoz használt kulcs hamar (4-5 perc alatt) megfejthető. A WEP titkosítás ugyan védelmet nyújthat az alkalmi próbálkozók ellen, de hamis biztonságérzetet ad, hiszen ingyenes, bárki számára hozzáférhető eszközökkel – mint például az aircrack-ng programcsomag – megfelelő jelerősség esetén nagyon egyszerűen visszafejthető a WEP kulcs. 64bites kulcsot 25 000, 128bites kulcsot 100 000 csomaggal már nagy valószínűséggel lehet törni (A PTW eljárás segítségével, ami az aircrack része). A titkosított csomagok lehallgatása után az aircrack-ng másodpercek alatt megtalálja a használt kulcsot. A szükséges csomagok akkor is kikényszeríthetőek, ha senki se kapcsolódik a hálózatra vezeték nélkül!

Ha eszközünk támogatja a WPA-t, akkor inkább használjuk azt, mert a WEP nyilvánvalóan gyengébb biztonságot nyújt a WPA-hoz képest. Ha a WPA-t nem támogatja eszközünk, akkor lehetőleg minden nap cseréljünk WEP kulcsot, de legalábbis olyan gyakran, ahogy csak tehetjük.

Ezek mellett gyakori a hálózati kártyák fizikai címének (MAC) szűrése, bár ez a szűréstípus egyszerűen kijátszható.

### 3.2. Wi-Fi Protected Access
A WPA (magyarul kb. Wi-Fi védett hozzáférés) egy 2003 óta élő titkosítási szabvány, ma már szinte minden eszköz támogatja – erősen ajánlott használni a WEP helyett. A WPA a TKIP nevű RC4 alapú titkosító algoritmust használja az adatok titkosítására. A TKIP fő előnye, hogy a beállított idő vagy forgalmazott adatmennyiség után új kulcsot generál.

Igazi biztonságot a WPA is csak akkor nyújt, ha kellően hosszú és összetett jelszót használunk, amivel elkerülhetjük a brute force, illetve a szótár alapú támadásokat.

### 3.3. IEEE 802.11i-2004

Az IEEE 802.11i-2004 vagy Wi-Fi Protected Access 2, WPA2 (magyarul kb. Wi-Fi védett hozzáférés 2. generációja) ma már a legtöbb eszköz támogatja, és mivel ez a legbiztonságosabb, ha rendelkezésre áll érdemes ezt használni.

### 3.4. WPA 3
A WPA 3, egy nagyon friss és biztonságos Wi-Fi védelem, amit kevés eszköz támogat még. Ebben a frissítésben a legfontosabb újítás a jelszó találgatásának felismerése.

## 4. Érdekesség
A WiFi-vel eddig elért legnagyobb hatótávolság 382 km, amit Venezuelában egy 2007 júniusában történt kísérletben értek el. A kifejezetten a kísérlet számára kiválasztott helyszínen az Andok Egyetem és egy telekommunikációs technológiákat támogató alapítvány kutatói az El Águila és Platillón hegyek között az Intel által elmaradott térségek számára kifejlesztett irányított antennák és szoftver felhasználásával, de alapvetően kereskedelmi forgalomban is kapható, olcsó (~60 USD) nagy hatótávolságú eszközökkel valósították meg a számítógépek közötti kapcsolatot. Mindkét irányban 3 MB/s sebességet tudtak elérni, hang és videokapcsolatot is sikerült létrehozniuk.

Manapság már lassan nem is létezik olyan eszköz, aminek ne lenne valamilyen, vezeték nélküli internetet használó változata. Ezek elterjedése miatt egyre nagyobb felelősség hárul a bennünket kiszolgáló wifi hálózatokra, amiknek nem csak az eszközök működését, hanem a rácsatlakozott felhasználók igényeinek kielégítését is biztosítaniuk kell.

Bár többféle szemlélet létezik az IoT (dolgok internetje) kifejezéssel kapcsolatban, abban a legtöbb szakértő egyetért, hogy egy olyan fajta digitális világot értünk alatta, ami olyan "intelligens" eszközöket foglal magába, amik egy-egy internet alapú hálózatra csatlakozva képesek a többi, ugyancsak felkapcsolódott eszközzel/eszközökkel kommunikálni és egyúttal felhasználni az általuk megosztott adatokat. Ilyen eszközök például az okosklíma (internettel bárhonnan be tudjuk állítani, hogy mire hazaérünk, kellemesen hűvös lakás várjon), az okosredőny (beépített érzékelőivel manuálisan vagy automatikusan annyira sötétít be, amennyire az időjárás megkívánja), okoshűtő (beépített kamerájával bármikor belenézhetünk, és a fejlettebb típusai még a lejárati időkre is emlékeztetnek) vagy a robotporszívó (gyakorlatilag bármikor elindíthatjuk, hogy megadott időpontra egy sokkal tisztább otthon fogadhasson bennünket).

Mivel egyre több ilyen eszközzel rukkolnak elő a gyártók és ennek hála egyre nagyobb népszerűségnek is örvendenek, így a wifihálózatok jelentősen nagyobb terhelésnek vannak kitéve, miközben számtalan eszközt és egyszerre akár több felhasználót is ki kell szolgálniuk. Éppen ezért a wifihálózatok átalakítására van szükség, hogy a megnövekedett erőforrásigényt megfelelő kapacitással tudják kiszolgálni.

A legnagyobb technológiai szolgáltatók jelenleg úgy látják, hogy az elkövetkező években is a wifi fogja kiszolgálni az egyre szaporodó okoseszközöket, de könnyen lehet, hogy még egyéb, alternatív vezeték nélküli technológiák is segíteni fognak az így teremtett ökoszisztémák működtetésében.
