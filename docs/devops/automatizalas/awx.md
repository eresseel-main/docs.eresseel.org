## 1. Bevezetés
Ansible-t használja az automatizált rendszermenedzsment és konfigurációkezelés végrehajtására. Az AWX egy webes felhasználói felületet és REST API-t biztosít, amelyek segítségével könnyen kezelheted és irányíthatod az Ansible-alapú automatizációs folyamatokat.

Az AWX lehetővé teszi a felhasználók számára, hogy konfigurációs feladatokat hozzanak létre, ütemezzenek, végrehajtsanak és figyeljenek meg a szerverinfrastruktúrában. Ezáltal az AWX egyszerűbbé teszi a konfigurációkezelést és az automatizálást a nagyobb méretű környezetekben, és könnyen skálázhatóvá teszi az Ansible alapú munkafolyamatokat.

Az AWX lehetővé teszi a felhasználók számára a szerepek, jogosultságok és feladatok kezelését, valamint a szerverek és azok állapotának felügyeletét is. Az AWX központi irányítópultot biztosít az Ansible műveletek és tevékenységek nyomon követéséhez, valamint jelentéseket és naplókat is készít a végrehajtott feladatokról.

## 2. Ansible AWX főbb jellemzői
* **Központosított kezelés:** Az Ansible játékkönyvek és leltárak központosított kezelését kínálja az egyszerű adminisztráció érdekében,
* **Szerep alapú hozzáférés-vezérlés:** A beépített RBAC segítségével biztosíthatja az automatizálási feladatok biztonságos és hatékony kezelését,
* **Web-alapú felhasználói felület:** Az AWX intuitív, felhasználóbarát webes felülettel érkezik az Ansible játékkönyvek, leltár és automatizálási feladatok kezelésére,
* **Harmadik féltől származó integrációk:** Az olyan egyéb eszközökkel, mint például a Git a verziókezeléshez való integráció elvégezhető, így az Ansible játékkönyvek idővel történő változásai könnyen kezelhetők és nyomon követhetők,
* **Jelentéskészítés és elemzés:** Az AWX jelentéskészítési és elemzési képességekkel rendelkezik a teljesítmény és a használatból származó előnyök nyomon követésére.

## 2. Telepítés
Telepítés Kubernetes/OpenShift Clusterre vagy Minikube-ba.

### 2.1. Cluster info
Miután a fürt üzembe került és fut, erősítsd meg a használatát `kubectl`parancsal.

```bash
kubectl cluster-info
```

### 2.2. AWX Operator telepítése Kubernetes-be
```bash
sudo apt update
sudo apt install git build-essential curl jq  -y
```

#### 2.2.1. Operator projekt forrásának klónozása
```bash
git clone https://github.com/ansible/awx-operator.git
```

#### 2.2.2. Névtér létrehozása, ahol az operátor kerül telepítésre
```bash
kubectl create ns awx
```

#### 2.2.3. NAMESPACE beállítása:
```bash
kubectl config set-context --current --namespace=awx
```

#### 2.2.4. AWX Operator letöltése és legfrissebb kiadás beállítása.
```bash
cd awx-operator/
RELEASE_TAG=`curl -s https://api.github.com/repos/ansible/awx-operator/releases/latest | grep tag_name | cut -d '"' -f 4`
echo $RELEASE_TAG
git checkout $RELEASE_TAG
```

#### 2.2.5. Operátor telepítése
```bash
export NAMESPACE=awx
make deploy
kubectl get pods -w
```

## 3. Ansible AWX telepítése Kubernetes-be
### 3.1. PVC létrehozása
```bash
cat <<EOF | kubectl create -f -
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: awx-static-data-pvc
  namespace: awx
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 10Gi
EOF
kubectl get pvc
```

### 3.2. AWX telepítése
Itt több perc mire az alkalmazás elindul.

```bash
cat <<EOF | kubectl create -f -
apiVersion: awx.ansible.com/v1beta1
kind: AWX
metadata:
  name: awx
spec:
  service_type: nodeport
  projects_persistence: true
  projects_storage_access_mode: ReadWriteOnce
  web_extra_volume_mounts: |
    - name: static-data
      mountPath: /var/lib/projects
  extra_volumes: |
    - name: static-data
      persistentVolumeClaim:
        claimName:  awx-static-data-pvc
EOF
watch kubectl get all
```

### 3.3. AWX Url lekérése
```bash
minikube service -n awx awx-service --url
```

### 3.4. AWX jelszó lekérése
```bash
kubectl get secret awx-admin-password -o jsonpath="{.data.password}" | base64 --decode ; echo
```