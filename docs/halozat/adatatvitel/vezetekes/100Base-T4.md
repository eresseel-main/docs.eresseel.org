## 1. Bevezetés
A 100Base-T4 egy speciális Ethernet kábelezési rendszer.

A gyors Ethernet (fast Ethernet) szabványt, a 802.3u-t 1995 júniusában fogadta el a IEEE.

A régi keretformátum megtartása, az interfészek változatlansága, de a bitidő 100 ns-ról 10 ns-ra való csökkentése miatt néhány döntést meg kellett hozni. Műszaki szempontból lehetséges lett volna a 10Base-5 vagy a 10Base-2 megőrzése, és annak ellenére, hogy a kábelhosszakat lecsökkennek, az ütközéseket lehetett volna észlelni. A 10Base-T kábelezés előnyei azonban annyira elsöprőek voltak, hogy a gyors Ethernet kábelezését erre a megoldásra alapozták. A gyors Ethernet elosztókat és kapcsolókat használ, a sok bosszúságot okozó vámpír csatlakozók és a BNC csatlakozók nem megengedettek.

A kábelezés tervezésekor három lehetséges kábelt vett a bizottság figyelembe:

* 3-as kategóriájú sodrott érpár
* 5-ös kategóriájú sodrott érpár (lásd: 100Base-TX)
* Fényvezető szál vagy kábel (lásd: 100Base-FX).

A 3-as kategóriájú érpár okozta a legnagyobb gondot. Azt adottságnak tekintették, hogy minden épületben 3-as kategóriájú érpárok (legalább 4 érpár) vagy jobbak rendelkezésre álltak. Azt az előnyt, hogy ezeket a meglévő kábelezéseket használják fel a gyors Ethernethez, senki sem volt hajlandó feladni.

A 3-as kategóriájú érpár hátránya volt az a tény, hogy a 200 megabaudos jeleket (Manchester-kódolással) nem tudta 100 méterre továbbítani, márpedig a 10Base-T szabvány szerint ez követelmény. Ezzel szemben az 5-ös kategóriájú sodrott érpárnak ez nem jelentett gondot, a fényvezető kábelnek meg különösen nem.

A 3-as kategóriájú sodrott érpárt fel kellett javítani, hogy elérje a szükséges szállítási kapacitást.

A 3-as kategóriájú sodrott érpár, amelyet 100Base-T-nek is neveznek 25 megabaudos jelzési sebességet használ (ez csak 25%-kal több az Ethernet szabvány szerinti 20 megabaudnál). Az Ethernet szabvány szerinti Manchesteri kódolás két órajel-periódust igényel a másodpercenkénti 10 millió bithez. A szükséges sávszélesség eléréséhez a 100Base-T négy sodrott érpárt igényel. Szerencsére a telefonvezetékekben már évtizedek óta 4 érpár fut, a legtöbb irodának ez nem jelentett problémát.

A négy érpár közül egy mindig az elosztó felé, egy az elosztó felől, a maradék kettő pedig átkapcsolhatóan az aktuális átvitel irányába szállítja az adatokat. A szükséges sávszélesség elérése érdekében nem használnak manchester kódolást, de a modern órák és a kis távolságok miatt nem is szükséges. Ráadásul, három jelszintű (ternális) jeleket küldenek, így egyetlen órajel alatt a vezetéken 0, 1 vagy 2 is megjelenhet. Ha három sodrott érpár megy a forgalom irányába, és mindegyiken háromszintű jelzést használnak, akkor 27-féle jelet lehet átvinni, tehát némi redundanciával akár 4 bitet is továbbíthatunk. Ha pedig a másodpercenkénti 25 millió órajel-ciklusban mindig 4 bitet továbbítunk, akkor előállt a kívánt 100 Mb/s-os csatorna. Rendelkezésre áll még egy 33,3 Mb/s-os ellenirányú csatorna, amely a megmaradt érpárokat használja. Ezt a sémát 8B/6T (8 bit map to 6 trits – 8 bit leképezése 6 tercre) néven ismerik.
