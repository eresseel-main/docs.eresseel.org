## 1. Bevezetés
Az ARP-t lehet támadni és hamisítani, és ezt a hálózati protokoll gyengeségei teszik lehetővé. Az ARP nem rendelkezik beépített biztonsági mechanizmusokkal, így érzékeny különböző típusú támadásokra, amelyek közül a legismertebb az ARP spoofing (hamisítás) és az ARP cache poisoning (gyorsítótár-mérgezés). Ezeket a támadásokat gyakran használják hálózati támadók a helyi hálózatokon belüli forgalom manipulálására vagy lehallgatására.

## 2. ARP Spoofing (ARP hamisítás)
Az ARP spoofing lényege, hogy a támadó hamis ARP üzeneteket küld a hálózatra, amelyek azt állítják, hogy a támadó eszköz rendelkezik egy adott IP-címhez tartozó MAC-címmel. Ez lehetővé teszi a támadónak, hogy elfogja, módosítsa, vagy átirányítsa a hálózati forgalmat.

Példa:
* Egy támadó elhiteti egy áldozat eszközével, hogy ő az átjáró (router), miközben a routerrel is elhiteti, hogy ő az áldozat.
* Ezzel a támadó a kettő közé helyezkedik el, és képes lesz lehallgatni, átirányítani vagy módosítani a köztük folyó forgalmat. Ezt nevezik man-in-the-middle (MitM) támadásnak.

### 2.1. Hogyan működik az ARP Spoofing
#### 2.1.1. Hamis ARP üzenetek küldése
* A támadó eszköz ARP üzeneteket küld a hálózatra, amelyekben hamis IP–MAC párokat hirdet.
* Például, ha a támadó azt állítja, hogy az ő MAC-címe az a MAC-cím, amihez egy fontos IP-cím tartozik (pl. az alapértelmezett átjáróé), akkor a támadó helyett a támadó MAC-címe lesz hozzárendelve az IP-címhez.

#### 2.1.2. Gyorsítótár mérgezése
* Az ARP üzeneteket fogadó eszközök frissítik a saját ARP gyorsítótárukat a kapott hamis információkkal.
* Ennek eredményeként az eszközök helytelenül küldik a forgalmat a támadó eszközhöz, nem pedig a megfelelő célhoz.

#### 2.1.3. Man-in-the-Middle (MitM) támadás
* A támadó eszköz most már képes lesz lehallgatni, módosítani, vagy akár megakadályozni a hálózati forgalmat az érintett IP-címek között.
* A támadó képes lehet a forgalom dekódolására, adatokat ellopni, vagy módosítani a kommunikációt.

### 2.2. ARP Spoofing Támadás Lépcsői
#### 2.2.1. Célpontok Azonosítása
* A támadó először azonosítja a célzott eszközöket a hálózaton, például az alapértelmezett átjárót és a célzott gépeket.

#### 2.2.2. ARP Kérések Küldése
* A támadó hamis ARP kéréseket küld a hálózatra, amelyekben a támadó MAC-címét állítja be az áldozatok IP-címéhez.

#### 2.2.3. ARP Válaszok Küldése
* A támadó ezután ARP válaszokat küld, amelyek frissítik az áldozatok ARP gyorsítótárait, hogy a támadó MAC-címét tartalmazzák az áldozatok IP-címéhez.

#### 2.2.4. Forgalom Lehallgatása vagy Manipulálása
* A támadó eszköz most már képes lehallgatni vagy manipulálni az IP-címek között folyó forgalmat, például adatokat gyűjthet a felhasználóktól vagy módosíthatja az adatokat.

### 2.3. Példák ARP Spoofing Támadásokra
#### 2.3.1. Személyes Adatok Ellopása
* A támadó lehallgathatja az áldozat kommunikációját, például bejelentkezési adatokat, jelszavakat, vagy más érzékeny információkat.

#### 2.3.2. Adatmanipuláció
* A támadó módosíthatja a forgalmat, például hamisítva a weboldalakat vagy manipulálva az adatokat, amelyeket az áldozatok küldenek.

#### 2.3.3. Denial of Service (DoS)
* A támadó megszakíthatja a hálózati kapcsolatokat azáltal, hogy az adatokat nem továbbítja, így a célállomások nem tudnak kommunikálni egymással.

### 2.4. Védekezési Módszerek
#### 2.4.1. Statikus ARP Bejegyzések
* Ha az eszközök ARP tábláit manuálisan statikus bejegyzésekkel frissítjük, akkor elkerülhetjük a dinamikus ARP frissítések általi manipulációt. Ez azonban nem mindig praktikus nagy hálózatokban.

#### 2.4.2. Dinamikus ARP Ellenőrzés (Dynamic ARP Inspection - DAI)
* Modern hálózati eszközök támogatják a dinamikus ARP ellenőrzést, amely figyeli és blokkolja a hamis ARP üzeneteket.
* Ez az eszközök közötti ARP üzenetek érvényességét ellenőrzi és csak a hitelesített ARP bejegyzéseket fogadja el.

#### 2.4.3. VPN Használata
* Titkosítja a hálózati forgalmat, így a támadók nem tudják olvasni vagy manipulálni a forgalmat.

#### 2.4.4. Hálózati Szegmentálás
* VLAN-ok és egyéb hálózati szegmentációs technikák alkalmazásával csökkenthető az ARP spoofing hatása, mivel a támadó eszközök nem tudnak hozzáférni minden szegmenshez.

#### 2.4.5. Hálózati Monitoring és IDS/IPS Rendszerek
* Behatolás-észlelő és behatolás-megelőző rendszerek képesek észlelni az ARP spoofing támadásokat és figyelmeztethetnek a hálózati adminisztrátorokat.

#### 2.4.6. Hálózati Eszközök Biztonsági Beállításai
* Sok modern switch és router tartalmaz beépített biztonsági funkciókat, például ARP védelemmel rendelkező portok és egyéb védelmi mechanizmusok.

### 2.5. Támadás programokkal Linuxon
#### 2.5.1. ettercap
Ettercap egy népszerű hálózati analizáló és támadási eszköz, amely támogatja az ARP Spoofinget, valamint számos egyéb hálózati támadást és man-in-the-middle (MitM) technikát.

```bash
ettercap -T -M arp:remote /target-ip/ /gateway-ip/
```
* `-T:` Szöveges felület használata.
* `-M arp:remote:` ARP spoofing mód.
* `/target-ip/:` A cél IP-cím.
* `/gateway-ip/:` Az átjáró IP-cím.

#### 2.5.2. arpspoof
Az arpspoof az ARP spoofing támadások egyszerűsített eszköze, amely a dsniff csomag része.
```bash
arpspoof -i <interface> -t <target-ip> <gateway-ip>
```

* `-i <interface>:` A hálózati interfész (pl. eth0, wlan0).
* `-t <target-ip>:` A cél IP-cím.
* `<gateway-ip>:` Az átjáró IP-cím.

#### 2.5.2. bettercap
Bettercap egy fejlett hálózati támadási és monitoring eszköz, amely szintén támogatja az ARP Spoofinget, és számos egyéb funkcióval rendelkezik.

```bash
bettercap -I <interface> -T <target-ip> -G <gateway-ip> --arp
```

* `-I <interface>:` A hálózati interfész.
* `-T <target-ip>:` A cél IP-cím.
* `-G <gateway-ip>:` Az átjáró IP-cím.

## 3. ARP Cache Poisoning (ARP gyorsítótár-mérgezés)
Az ARP Cache Poisoning (más néven ARP Cache Poisoning) egy olyan támadási technika, amely a helyi hálózatokban lévő ARP (Address Resolution Protocol) gyorsítótárának manipulálására irányul. A támadás célja, hogy helytelen IP–MAC párokat írjon a hálózati eszközök ARP gyorsítótárába, ezzel torzítva a forgalom útját, és lehetővé téve a forgalom lehallgatását vagy manipulálását.

Hatások:
* A támadó átirányíthatja a forgalmat egy rosszindulatú eszközre, amely monitorozza vagy módosítja az adatokat.
* A támadó helyettesítheti a hálózati eszközök között lévő kommunikációt, és manipulálhatja azokat.
* Lehetséges denial of service (DoS) támadást indítani úgy, hogy az adatokat nem továbbítja a támadó, így a célállomások közötti kommunikáció megszakad.

### 3.1. Hogyan Működik az ARP Cache Poisoning
#### 3.1.1. ARP Protokoll Alapok
* Az ARP protokoll az IP-címek és MAC-címek közötti leképezést biztosít a helyi hálózaton.
* Amikor egy eszköz kommunikálni szeretne egy másik eszközzel, az ARP kérést küld az adott IP-címhez tartozó MAC-cím lekérdezésére.
* Az ARP válasz tartalmazza a kért MAC-címet, amelyet a lekérdező eszköz elment a gyorsítótárába.

#### 3.1.2. ARP Cache Poisoning Lépései
##### 3.1.2.1. Célpontok Azonosítása
A támadó először azonosítja a célzott eszközöket és az alapértelmezett átjárót (router) a hálózaton. Ezt általában hálózati szkenneléssel vagy ARP kérések küldésével végzik.

##### 3.1.2.2. Hamis ARP Üzenetek Küldése
A támadó hamis ARP üzeneteket küld a hálózatra. Ezek az üzenetek olyan ARP válaszokat tartalmaznak, amelyekben a támadó eszköz MAC-címét rendelik egy adott IP-címhez.

* Például, ha a támadó azt állítja, hogy a saját MAC-címe az a MAC-cím, amihez az alapértelmezett átjáró IP-címe tartozik, akkor a célzott eszközök az új, hamis MAC-címet fognak használni.

##### 3.1.2.3. Gyorsítótár Frissítése
Az ARP válaszokat fogadó eszközök frissítik a saját ARP gyorsítótárukat az új információkkal. A támadó eszköz MAC-címe most már helyettesíti a valódi átjáró MAC-címét.

* Ez azt jelenti, hogy a hálózati forgalom, amelyet az eszközök az átjáró IP-címére küldenek, a támadó eszközhöz fog érkezni.

##### 3.1.2.4. Forgalom Lehallgatása vagy Manipulálása
A támadó eszköz most már képes lehallgatni, módosítani, vagy akár megakadályozni a forgalmat.

* **Lehallgatás:** A támadó rögzítheti a forgalmat, és érzékeny adatokat, például jelszavakat, banki adatokat stb. gyűjthet.
* **Manipuláció:** A támadó módosíthatja az adatokat, például hamis weboldalakat jeleníthet meg, vagy átirányíthatja a felhasználókat hamis szerverekre.
* **Denial of Service (DoS):** A támadó megszakíthatja a kapcsolatokat azáltal, hogy nem továbbítja az adatokat, így a célállomások nem tudnak kommunikálni egymással.

### 3.2. Példák ARP Cache Poisoning Támadásokra
### 3.2.1. Man-in-the-Middle (MitM)
A támadó elfoghatja a hálózati forgalmat a két eszköz között. Például, ha az áldozat egy banki tranzakciót indít, a támadó láthatja a banki adatokat és tranzakciókat.

### 3.2.2. Weboldal Hamisítás
A támadó módosíthatja a weboldalakat, amelyeket az áldozat meglátogat. Például a támadó hamis bejelentkezési oldalakat jeleníthet meg, amelyek adatokat gyűjtenek.

### 3.2.3. DNS Spoofing
A támadó módosíthatja a DNS-kérések válaszait, így az áldozat hamis DNS információkat kap, amelyek egy támadó által irányított weboldalra vezethetnek.

## 4. Védekezési módszerek
Bár az ARP protokoll gyengeségei miatt ezek a támadások egyszerűen kivitelezhetők, számos védekezési technika és eszköz létezik, amelyekkel csökkenthető az ARP-alapú támadások kockázata.

### 4.1. Statikus ARP bejegyzések
* Az egyik megoldás az, hogy manuálisan statikus ARP bejegyzéseket hozunk létre, így az eszközök fix MAC-IP párokat használnak, és nem fogadják el a dinamikus ARP válaszokat.
* Ez azonban nem skálázható jól nagy hálózatokban, mivel minden eszközön manuálisan kell frissíteni az ARP táblát.

### 4.2. Dinamikus ARP védelmi mechanizmusok (Dynamic ARP Inspection - DAI)
* Modern hálózati eszközök (pl. menedzselt switchek) támogatják a dinamikus ARP ellenőrzést, amely figyeli és blokkolja a hamis ARP üzeneteket.
* A DAI lehetővé teszi az ARP csomagok szűrését és validálását, így megelőzi a hamis ARP válaszok küldését a hálózaton.

### 4.3. Hálózati szegmentálás és VLAN-ok
* A hálózatok szegmentálása VLAN-ok segítségével korlátozhatja az ARP támadások hatását egy kisebb hálózati szegmensre, és megnehezítheti a támadóknak, hogy hozzáférjenek más szegmensekhez.

### 4.4. VPN használata
* A VPN (Virtual Private Network) használatával titkosíthatod a hálózati forgalmat, így ha egy támadó sikeresen beékelődik a forgalomba, akkor sem tudja elolvasni vagy módosítani a titkosított adatokat.

### 4.5. Antivirus és IDS/IPS rendszerek
* Egyes behatolásérzékelő (IDS) vagy behatolásmegelőző (IPS) rendszerek képesek észlelni és megakadályozni az ARP spoofingot és más hálózati támadásokat.

## 5. Támadás programokkal Linuxon
### 5.1. ettercap
Ettercap egy népszerű hálózati analizáló és támadási eszköz, amely támogatja az ARP Spoofinget, valamint számos egyéb hálózati támadást és man-in-the-middle (MitM) technikát.

```bash
ettercap -T -M arp:remote /target-ip/ /gateway-ip/
```
* `-T:` Szöveges felület használata.
* `-M arp:remote:` ARP spoofing mód.
* `/target-ip/:` A cél IP-cím.
* `/gateway-ip/:` Az átjáró IP-cím.

#### 5.2. arpspoof
Az arpspoof az ARP spoofing támadások egyszerűsített eszköze, amely a dsniff csomag része.
```bash
arpspoof -i <interface> -t <target-ip> <gateway-ip>
```

* `-i <interface>:` A hálózati interfész (pl. eth0, wlan0).
* `-t <target-ip>:` A cél IP-cím.
* `<gateway-ip>:` Az átjáró IP-cím.

#### 5.3. bettercap
Bettercap egy fejlett hálózati támadási és monitoring eszköz, amely szintén támogatja az ARP Spoofinget, és számos egyéb funkcióval rendelkezik.

```bash
bettercap -I <interface> -T <target-ip> -G <gateway-ip> --arp
```

* `-I <interface>:` A hálózati interfész.
* `-T <target-ip>:` A cél IP-cím.
* `-G <gateway-ip>:` Az átjáró IP-cím.

#### 5.4. MITMf (Man-in-the-Middle Framework)
MITMf egy man-in-the-middle keretrendszer, amely számos támadási lehetőséget kínál, beleértve az ARP Spoofingot is. Az MITMf számos beépített modullal rendelkezik, beleértve a DNS hamisítást és SSL lehallgatást is.

* **Telepítés:** Az MITMf régebben elérhető volt, de jelenlegi aktív fejlesztés hiányában a telepítése bonyolultabb lehet. Használhatod a forráskódból való telepítést GitHubon keresztül.
* **Használat ARP Spoofinghoz:** Az MITMf konfigurálásával különböző ARP spoofing alapú támadásokat indíthatsz, például forgalom lehallgatást és manipulálást.

#### 5.5. Scapy
Egy Python könyvtár, amely képes hálózati csomagok létrehozására és manipulálására. Nagy szabadságot ad egyedi ARP támadások létrehozásához.

Példa egy egyszerű Scapy ARP Spoofing scriptre
```python
from scapy.all import *

target_ip = "target-ip"
gateway_ip = "gateway-ip"
target_mac = getmacbyip(target_ip)
gateway_mac = getmacbyip(gateway_ip)

# Hamis ARP válasz küldése
send(ARP(op=2, pdst=target_ip, psrc=gateway_ip, hwdst=target_mac))
```
