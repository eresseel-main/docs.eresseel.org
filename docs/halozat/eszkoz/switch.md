## 1. Bevezetés
Az adatátviteli kapcsoló vagy switch egy aktív számítógépes hálózati eszköz, amely a rácsatlakoztatott eszközök között adatáramlást valósít meg. Többnyire az OSI-modell adatkapcsolati rétegében (2. réteg, esetleg magasabb rétegekben) dolgozik. Magyar jelentése: vált, kapcsol.

A fizikai rétegbeli feladatokat ellátó hubokkal szemben az Ethernet switchek adatkapcsolati rétegben megvalósított funkciókra is támaszkodnak. A MAC címek vizsgálatával képesek közvetlenül a célnak megfelelő portra továbbítani az adott keretet; tekinthetők gyors működésű, többportos hálózati hídnak is. Portok között tehát nem fordul elő ütközés (mindegyikük külön ütközési tartományt alkot), ebből adódóan azok saját sávszélességgel gazdálkodhatnak, nem kell megosztaniuk azt a többiekkel. A broadcast és multicast kereteket természetesen a switchek is floodolják az összes többi portjukra.

Egy switch képes full-duplex működésre is, míg egy hub csak half-duplex kapcsolatokat tud kezelni. Különbség még, hogy a switchek egy ASIC (Application-Specific Integrated Circuit) nevű hardverelem segítségével jelentős sebességeket érhetnek el, míg a HUB nem más, mint jelmásoló, ismétlő. A fontos funkciók közé tartozik még a hálózati hurkok elkerülésének megoldása (lásd STP), illetve a VLAN-ok kezelése.

Ethernet switcheken kívül léteznek még például ATM, Frame Relay és Fibre Channel kapcsolók is. Fibre Channel kapcsolók SAN hálózatokban használatosak, általában optikai kábelezéssel. segedlet.webnode.hu/segedlet.webnode.hu/

## 2. Feladata
Alapvető feladata:

* Csomagokban található MAC címek megállapítása.
* MAC címek és portok összerendelése (kapcsolótábla felépítése).
* A kapcsolótábla alapján a címzésnek megfelelő port-port összekapcsolása.
* Adatok ütközésének elkerülése, adatok ideiglenes tárolása.

A programozható switchek további feladatokat is elláthatnak:

* Shortest Path Bridging (IEEE 802.1aq)
* Virtuális magánhálózat kezelése
* A végpontokra kötött eszközök MAC cím szerinti azonosítása
* A végpontok prioritásának meghatározása
* A végpontokhoz tartozó sávszélesség korlátozása
* A végpontok használatának időbeli korlátozása

Ezen felül – típustól függően – szinte bármilyen adatáramlással kapcsolatos szabály beállítható lehet.

## 3. Részei
* **Portok**: itt lehet rácsatlakoztatni a hálózat további eszközeit
* **Dedikált port(ok)**: kiemelt interface, amelyen további switchek összekapcsolására van lehetőség, többnyire nagyobb sávszélességű, mint az „általános” portok.
* Állapotjelző LED-ek

## 4. Switch által nyújtott kiegészítő szolgáltatások
Tipikusan az alábbi szolgáltatások fordulhatnak elő:

* Adott portok ki- és bekapcsolása
* Port sebességének korlátozása
* Port prioritásának beállítása
* MAC címek szűrése - biztonsági okokból adott MAC címmel rendelkező eszközök kizárása bizonyos port(ok)ról
* SNMP rendszeren keresztüli eszköz- és portfigyelés
* Portok tükrözése (adott port másolása további port(ok)ra)
* Virtuális hálózatok kezelése
