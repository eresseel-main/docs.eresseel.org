## 1. Bevezetés
A peer-to-peer vagy P2P paradigma lényege, hogy az informatikai hálózat végpontjai közvetlenül egymással kommunikálnak, központi kitüntetett csomópont nélkül. A peer-to-peer fogalom két hasonló, de célját tekintve mégis eltérő fogalomkört is takar: a számítógépek egyenrangú technológiai szintű kapcsolódási módját egy helyi hálózaton, valamint valamilyen célból közvetlenül kapcsolódó szoftver megoldások működési elvét. A közvetlen kapcsolat hibatűrőbb felépítést, skálázhatóságot jelent. Hátrányai: a nehezebb adminisztráció, az erőforrások pazarló használata, a nehezebb megvalósíthatóság.

## 2. Kezdetei
A P2P már az Internet születésénél is jelen volt. Gazdaságossági és technikai okokból a lapos hierarchia folyamatosan kezdett központosodni, míg az internet széles körű elterjedésének idejére már túlnyomórészt a szerver-kliens architektúra volt jellemző. A peer-to-peer rendszerek reneszánszát a sávszélesség növekedésével és a tömörítési algoritmusok (és az ehhez szükséges processzorok) javulásával berobbanó fájlcserélő alkalmazások hozták el.

## 3. A Napster és a szerzői jogi problémák
Első fájlcserélő klasszikusként a Napster jelent meg, és hihetetlen sebességgel tett szert több millió felhasználóra. Architektúráját tekintve még hibrid, központi szerverfarm kapcsolja össze a felhasználóktól jött file-kéréseket a file felajánlásokkal, de maga az adatcsere a felhasználók között már a szervertől függetlenül zajlik. A nagy népszerűség mellett sok pert indítottak a rendszer ellen a szerzői jogok tulajdonosai, ez végül a Napster bezárását eredményezte. A fájlcserélők és ezzel a peer-to-peer szoftverek terjedésének ezzel azonban már nem lehetett gátat vetni. Manapság a Napster fizetős zeneboltként él tovább.

## 4. Alkalmazási területei
Sorra alakultak a további fájlcserélő rendszerek. Ilyen volt a teljesen elosztott Gnutella, az IRC+FTP+fájlkereső összeállítást modellező DC++, vagy a nagy népszerűségre szert tevő Kazaa. A Bittorrent elterjedésének idejére a mérések szerint az internet forgalmának legalább 60%-át már a fájlcserélő (főképp a Bittorrent) hálózatok adják.

A peer-to-peer hálózatok másik jelentős alkalmazási területe a telefonprogramok, például a Skype. Ezek a peer-to-peer hálózatok által olcsón biztosított infrastruktúrát használják ki, hogy olcsón szolgáltassanak telefonos kapcsolatot. A peer-to-peer hálózat miatt jelentősebb befektetett tőke nélkül tudnak olyan szolgáltatást nyújtani, amelyek a korábban óriási pénzekből létrehozott infrastruktúra megtérülése miatt maradtak drágák.

A Skype készítői a telefonprogram fejlesztése során felismerték a peer-to-peer hálózatokban rejlő lehetőségeket, és 2006 decemberében kísérleti jelleggel beindították Joost néven az egyik internetes televíziós szolgáltatást.

A decentralizált, nyílt forráskódú, bárki által használható digitális fizetőeszközök, mint a bitcoin, ether és a többi alternatív kriptovaluták szintén P2P paradigmára épülnek. Úttörője Szatosi Nakamoto ismeretlen programozó, aki 2009-ben indította útjára az első globálisan sikeres peer-to-peer digitális fizetőeszközt, a bitcoint. A Bitcoin protokoll alapjait a fehér könyvében fektette le.

Jelenleg kutatások és próbálkozások folynak a P2P megközelítés szélesebb körű alkalmazására. Például, a masszív többszereplős online játékok következő generációi valószínűleg erre a technológiára is alapoznak majd. Az egy realm-ban (azaz egy szerveren) együtt levő játékosok számát technikai korlátok szűkítik, például a szerverek sávszélessége, a szerverek száma, a hardver teljesítménye. A hagyományos egy szerver - sok kliens megközelítésben rendkívüli költségek lépnének fel a további játékosszám-bővítésre. A P2P megközelítéssel tervezett MMO játékok szinte korlátlan méretű realmok felépítésével kecsegtetnek.
