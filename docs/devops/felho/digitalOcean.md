## 1. Bevezetés
A DigitalOcean, Inc. egy amerikai felhő székhelyű New York-i infrastruktúra-szolgáltató adatközpontokkal világszerte. A DigitalOcean fejlesztők, induló vállalkozások kis és középvállalkozások számára felhőalapú infrastruktúra-szolgáltatás platformokat biztosít.

Minden évben különböző szoftvercégekkel áll partnerségben, köztük a GitHub-bal, a Twilio-val, a Dev.to-val, az Intel -lel, az AppWrite-tel és a DeepSource-val.

## 2. Előzmények
2003-ban Ben Uretsky és Moisey Uretsky, akik megalapították a ServerStack menedzselt hosting üzletágat, olyan új terméket akartak létrehozni, amely egyesíti a webtárhelyet és a virtuális szervereket, és a vállalkozókat célozza meg a szoftverfejlesztőkkel. 2011-ben Uretskyék megalapították a DigitalOcean céget, amely szerverkiépítést és felhőtárhelyet biztosít szoftverfejlesztők számára.

2012-ben Uretskyék találkoztak Mitch Wainer társalapítóval, miután Wainer válaszolt egy Craigslist álláshirdetésre. 
A Mitch Wainer cég 2012 januárjában dobta piacra béta termékét. 2012 közepén az alapítócsapat Ben Uretsky, Moisey Uretsky, Mitch Wainer, Jeff Carr és Alec Hartman alkotta. A DigitalOcean elfogadta a TechStars 2012 startup-gyorsítójának ajánlatát a Colorado állambeli Boulderben, és az alapítók a Boulderhez költöztek, hogy a terméken dolgozzanak. A gyorsítóprogram végén, 2012 augusztusában a vállalat 400 ügyfelet regisztrált, és körülbelül 10 000 felhőszerver példányt indított el. 2018. január 16-án új csepp (virtuális gépek) terveket vezettek be. 2018 májusában a vállalat bejelentette Kubernetes- alapú konténerszolgáltatásának elindítását.

2018 júniusában Mark Templeton korábbi vezérigazgatója, a Citrix váltotta a társalapítót, Ben Uretskyt a cég vezérigazgatói posztján. 2019 júliusában Yancey Spruill, a SendGrid (egy másik Techstars cég) korábbi pénzügyi igazgatója és vezérigazgatója váltotta Templetont a vezérigazgatói poszton. Bill Sorensont, az EnerNOC korábbi pénzügyi igazgatóját nevezték ki a vállalat új pénzügyi igazgatójának.

2021 szeptemberében a DigitalOcean bejelentette a Nimbella, a szerver nélküli startup felvásárlásának tervét. 2022 márciusában a vállalat felvásárolta a CSS-Tricks-t, egy oktatási webhelyet a front-end fejlesztők számára.

2022 májusában a vállalat kiadta a DigitalOcean Functions-t. A Nimbellától és a nyílt forráskódú Apache OpenWhisk projekten alapuló technológián alapuló DigitalOcean Functions egy szerver nélküli platform, amely lehetővé teszi a fejlesztők számára, hogy kiszolgálók kezelése nélkül építsenek és futtassanak alkalmazásokat.

2022 augusztusában a DigitalOcean 350 millió dollárért megvásárolta a Cloudways pakisztáni felhőtárhely-szolgáltatót.

## 3. Növekedés
2013.január 15-én a DigitalOcean az egyik első felhőszolgáltató vállalat lett, amely SSD-alapú virtuális gépeket kínált. A TechCrunch áttekintését követően, amelyet a Hacker News közölt, a DigitalOcean-nél gyorsan növekedett az ügyfelek száma. 2013 decemberében a DigitalOcean megnyitotta első európai adatközpontját Amszterdamban. 2014 folyamán a vállalat folytatta terjeszkedését, új adatközpontokat nyitott Szingapúrban és Londonban. 2015 folyamán a DigitalOcean tovább bővült egy adatközponttal Torontóban, Frankfurtban és Németországban. Később, 2016-ban folytatták a terjeszkedést az indiai Bangalore-ban.

## 4. Finanszírozás
A vállalat 2013 júliusában 3,2 millió USD-t gyűjtött be. Az Andreessen Horowitz kockázatitőke-társaság által vezetett finanszírozási sorozat 2014 márciusában 37,2 millió USD-t gyűjtött össze. 2014 decemberében a DigitalOcean 50 millió USD adósságfinanszírozást vett fel a Fortress Investment Group-tól, ötéves futamidejű kölcsön formájában. 2015 júliusában a vállalat 83 millió USD-t gyűjtött össze. 2016 áprilisában a vállalat 130 millió USD hitelfinanszírozást biztosított új felhőszolgáltatások kiépítéséhez. 2020 májusában a Digital Ocean további 50 millió dollárt gyűjtött az Access Industriestől és Andreessen Horowitztól. 2021. március 24-én a DigitalOcean tőzsdén jegyzett társasággá vált,amelynek kezdeti nyilvános jegyzési ára részvényenként 47 dollár volt.

## 5. Termékek és üzleti modell
A DigitalOcean virtuális magánszervereket (VPS) vagy "cseppeket" kínál DigitalOcean terminológiával, KVM -et használva hypervisorként, és különféle méretekben (2 osztályba osztva: szabványos és optimalizált), 13 különböző adatközponti régióban hozható létre. (2020. decemberi állapot szerint), és különféle opciókkal, köztük hat Linux disztribúcióval és több tucat egykattintásos alkalmazással.

2017 elején a DigitalOcean terheléselosztókkal bővítette szolgáltatásait. ​​Platformjuk egy alternatív felhőalapú kínálat, és a cég a kisebb fejlesztőket célozza meg, lehetővé téve számukra, hogy mindössze öt dollárt költsenek a platformjukra.

A DigitalOcean webes felületen vagy parancssorból lehet kezelni a `doctl` parancs használatával.

A DigitalOcean blokk- és objektumalapú tárolást, valamint 2018 májusa óta Kubernetes - alapú konténerszolgáltatást is kínál.

A véleményezők megjegyezték, hogy a DigitalOcean megköveteli a felhasználóktól, hogy rendelkezzenek némi tapasztalattal a sysadmin és DevOps terén. Greg Laden író a ScienceBlogs számára írt kritikájában figyelmeztetett: "A DigitalOcean nem mindenkinek való. Legalább egy kicsit jártasnak kell lennie a Linuxban..."

2021-ben a DigitalOcean elindított egy felügyelt MongoDB adatbázis-szolgáltatást.