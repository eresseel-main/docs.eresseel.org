## 1. Bevezetés
Portainer egy konténer menedzselő alkalmazás, mely az általunk használt konténerek adatait, folyamatait, logjait mutatja és monitorozza azokat.

A Portainer felgyorsítja a konténerek használatát. Csökkenti a működési bonyolultságot, és megbirkózik a Docker, Swarm, Nomad és Kubernetes konténerek futtatásával járó biztonsági kihívásokkal.

**Portainer Community Edition (CE)** az alapítványunk több mint félmillió rendszeres felhasználóval. A CE egy hatékony, nyílt forráskódú eszközkészlet, amely lehetővé teszi a Docker, Docker Swarm, Kubernetes és Azure ACI konténerek egyszerű felépítését és kezelését. 

**Portainer Business Edition (BE)** a mi kereskedelmi ajánlatunk. Vállalkozásoknak és nagyobb szervezeteknek szánt funkciókkal, mint pl Role-Based Access Control, registry management és dedikált támogatás. A Portainer BE egy hatékony eszközkészlet, amely lehetővé teszi a Docker, Docker Swarm, Kubernetes és Azure ACI konténerek egyszerű felépítését és kezelését.

A Portaier a konténerek kezelésének bonyolultságát egy könnyen használható felhasználói felület mögé rejti. A CLI használatának, a YAML írásának vagy a manifesztek megértésének megszüntetésével a Portainer olyan egyszerűvé teszi az alkalmazások telepítését és a problémák hibaelhárítását, hogy bárki megteheti.

## 2. Portainer CE server telepítése Dockerrel Linuxra
### 2.1. Bevezetés
A Portainer két elemből áll, a Portainer szerverből és a Portainer ügynökből. Mindkét elem Docker konténerként fut. Ez a dokumentum segít a Portainer Server tároló telepítésében, Linux környezetben.

A kezdéshez a következőkre lesz szükséged:

* A Docker legújabb verziója telepítve van és működik,
* `sudo` hozzáférés azon a gépen, amelyen a Portainer Server példányát fogod tárolni,
* Alapértelmezés szerint a Portainer Server a felhasználói felületet a 9443-as porton, a TCP alagútkiszolgálót pedig a 8000-es porton keresztül teszi elérhetővé. Ez utóbbi opcionális, és csak akkor szükséges, ha az Edge számítási szolgáltatásait Edge ügynökökkel kívánod használni.

A telepítési utasítások a következőket feltételezi a környezeteddel kapcsolatban:

* Míg a Portainer más konfigurációkkal is működhet, előfordulhat, hogy konfigurációs változtatásokat igényel, vagy korlátozott a funkcionalitása,
* A Dockert Unix socket-en keresztül éri el. Alternatív megoldásként TCP-n keresztül is csatlakozhatsz,
* A SELinux le van tiltva a Dockert futtató gépen. Ha SELinuxra van szükséged, a `--privileged` jelzőt át kell adnia a Dockernek a Portainer telepítésekor,
* A Docker rootként fut. A root nélküli Dockerrel rendelkező Portaiernek vannak bizonyos korlátai, és további konfigurációt igényel.

### 2.2. Telepítés
Először hozd létre azt a kötetet, amelyet a Portainer Server az adatbázisának tárolására fog használni:
```bash
docker volume create portainer_data
```

Ezután töltsd le és telepítsd a Portainer Server tárolót:
```bash
docker run -d -p 8000:8000 -p 9443:9443 --name portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce:latest
```

### 2.3. Bejelentkezés
Most, hogy a telepítés befejeződött, bejelentkezhetsz a Portainer Server-példányba. Ehhez nyisd meg a webböngészőt, és lépj a következő helyre:
```bash
https://localhost:9443
```

Cseréld ki a localhost-ot a megfelelő IP-címre vagy FQDN-re, ha szükséges módosítsd a portot.

## 3. Portainer ügynök telepítése Dockerrel Linuxra
### 3.1. Bevezetés
A Portainer a Portainer Agent konténer segítségével kommunikál a Portainer Server példányával, és hozzáférést biztosít a csomópont erőforrásaihoz. Ez a dokumentum bemutatja, hogyan telepítheti a Portainer Agentet a csomópontra, és hogyan csatlakozhat hozzá a Portainer Server példányból.

A kezdéshez a következőkre lesz szükséged:

* A Docker legújabb verziója telepítve van és működik,
* `sudo` hozzáférés azon a gépen, amelyre telepíteni szeretné a Portainer Agentet,
* A 9001-es port ezen a gépen elérhető a Portainer Server példányból. Ha ez nem elérhető, javasoljuk az Edge Agent használatát.

A telepítési utasítások a következőket feltételezi a környezeteddel kapcsolatban:

* Míg a Portainer más konfigurációkkal is működhet, előfordulhat, hogy konfigurációs változtatásokat igényel, vagy korlátozott a funkcionalitása,
* A Dockert Unix socket-en keresztül éri el. A Portainer Agent nem támogatja a Docker engine-hez való csatlakozást TCP-n keresztül,
* A SELinux le van tiltva a Dockert futtató gépen. Ha SELinuxra van szüksége, a `--privileged` jelzőt át kell adnod a Dockernek a Portainer telepítésekor,
* A Docker rootként fut. A root nélküli Dockerrel rendelkező Portaiernek vannak bizonyos korlátai, és további konfigurációt igényel,
* Nem állítottál be egyéni `AGENT_SECRET` értéket a Portainer Server példányán. Ha van, meg kell adnod ezt a titkot az ügynöknek, amikor telepíted: `-e AGENT_SECRET=yoursecret`

### 3.2. Telepítés
Futtassd a következő parancsot a Portainer Agent telepítéséhez:
```bash
docker run -d -p 9001:9001 --name portainer_agent --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/docker/volumes:/var/lib/docker/volumes portainer/agent:latest
```

### 3.3. Új környezet hozzáadása
Az agent telepítése után készen állsz a környezet hozzáadására a Portainer Server telepítéséhez.
